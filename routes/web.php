<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/config/cache', function () {
    $clearcache = Artisan::call('cache:clear');
    echo "Cache cleared<br>";

    $clearview = Artisan::call('view:clear');
    echo "View cleared<br>";

    $clearconfig = Artisan::call('config:cache');
    echo "Config cleared<br>";


});
Route::get('privacy-policy', 'PageController@privacyPolicy');
Route::get('terms-of-use', 'PageController@termsOfUse');

Route::get('/', 'DashboardController@index')
    ->middleware('auth');


/*
 * Route đăng nhập
 */
Route::get('/login', 'CustomAuth\LoginController@index')->name('login');
Route::post('/login', 'CustomAuth\LoginController@index')->name('login');

/*
 * Route khởi động lại phiên làm viêc
 */
Route::get('/restart', 'CustomAuth\AccountController@restart');

/*
 * Route đăng xuất
 */
Route::get('/logout', 'CustomAuth\AccountController@logout');
//
Route::group(['prefix'=>'account', 'middleware' => ['auth']], function (){
    Route::get('', 'CustomAuth\AccountController@profile');
     Route::get('profile', 'CustomAuth\AccountController@profile');

    Route::get('change-password', 'CustomAuth\AccountController@changePassword');
    Route::post('change-password', 'CustomAuth\AccountController@changePassword');

    Route::get('setting', 'CustomAuth\AccountController@setting');
    Route::post('setting', 'CustomAuth\AccountController@setting');
});
/*
|--------------------------------------------------------------------------
| developer: lê kim hiển
| content: tổng quát hệ thống
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix'=>'api', 'middleware' => ['auth']], function (){
    Route::get('', 'TemplateController@index');
    Route::get('notification', 'TemplateController@indexNotification');
    /** bank */
    Route::get('bank', 'BankController@index');
    Route::get('bank/store', 'BankController@store');
    Route::post('bank/create', 'BankController@create');
    Route::get('bank/edit/{id}', 'BankController@edit');
    Route::put('bank/update/{id}', 'BankController@update');
    /** currency */
    Route::get('currency', 'CurrencyController@index');
    Route::get('currency/store', 'CurrencyController@store');
    Route::post('currency/create', 'CurrencyController@create');
    Route::get('currency/edit/{id}', 'CurrencyController@edit');
    Route::put('currency/update/{id}', 'CurrencyController@update');
    /** country */
    Route::get('country', 'CountryController@index');
    Route::get('country/store', 'CountryController@store');
    Route::post('country/create', 'CountryController@create');
    Route::get('country/edit/{id}', 'CountryController@edit');
    Route::put('country/update/{id}', 'CountryController@update');
    /** Tax */
    Route::get('tax', 'TaxController@index');
    Route::get('tax/store', 'TaxController@store');
    Route::post('tax/create', 'TaxController@create');
    Route::get('tax/edit/{id}', 'TaxController@edit');
    Route::put('tax/update/{id}', 'TaxController@update');
    /** Warehouses */
    Route::get('warehouses', 'WareHouseController@index');
    Route::get('warehouses/store', 'WareHouseController@store');
    Route::post('warehouses/create', 'WareHouseController@create');
    Route::get('warehouses/edit/{id}', 'WareHouseController@edit');
    Route::put('warehouses/update/{id}', 'WareHouseController@update');
    /** Brand */
    Route::get('brand', 'BrandController@index');
    Route::get('brand/store', 'BrandController@store');
    Route::post('brand/create', 'BrandController@create');
    Route::get('brand/edit/{id}', 'BrandController@edit');
    Route::put('brand/update/{id}', 'BrandController@update');
    /** Position */
    Route::get('position', 'PositionController@index');
    Route::get('position/store', 'PositionController@store');
    Route::post('position/create', 'PositionController@create');
    Route::get('position/edit/{id}', 'PositionController@edit');
    Route::put('position/update/{id}', 'PositionController@update');
    /** Decentralization */
    Route::get('decentralization', 'DecentralizationController@index');
    Route::get('decentralization/store', 'DecentralizationController@store');
    Route::post('decentralization/create', 'DecentralizationController@create');
    Route::get('decentralization/edit/{id}', 'DecentralizationController@edit');
    Route::put('decentralization/update/{id}', 'DecentralizationController@update');
    /** Staff */
    Route::get('staffs', 'StaffController@index');
    Route::get('staffs/store', 'StaffController@store');
    Route::post('staffs/create', 'StaffController@create');
    Route::get('staffs/edit/{id}', 'StaffController@edit');
    Route::post('staffs/update/{id}', 'StaffController@update');
    Route::get('staffs/delete/{id}', 'StaffController@delete');
    /** Supplier */
    Route::get('suppliers', 'SupplierController@index');
    Route::get('suppliers/store', 'SupplierController@store');
    Route::post('suppliers/create', 'SupplierController@create');
    Route::get('suppliers/edit/{id}', 'SupplierController@edit');
    Route::put('suppliers/update/{id}', 'SupplierController@update');
    /** units */
    Route::get('units', 'UnitController@index');
    Route::get('units/store', 'UnitController@store');
    Route::post('units/create', 'UnitController@create');
    Route::get('units/edit/{id}', 'UnitController@edit');
    Route::put('units/update/{id}', 'UnitController@update');
    /** productType */
    Route::get('productType', 'ProductTypeController@index');
    Route::get('productType/store', 'ProductTypeController@store');
    Route::post('productType/create', 'ProductTypeController@create');
    Route::get('productType/edit/{id}', 'ProductTypeController@edit');
    Route::put('productType/update/{id}', 'ProductTypeController@update');
    /** origin */
    Route::get('origin', 'OriginController@index');
    Route::get('origin/store', 'OriginController@store');
    Route::post('origin/create', 'OriginController@create');
    Route::get('origin/edit/{id}', 'OriginController@edit');
    Route::put('origin/update/{id}', 'OriginController@update');
    /** product */
    Route::get('products', 'ProductController@index');
    Route::get('products/store', 'ProductController@store');
    Route::post('products/create', 'ProductController@create');
    Route::get('products/edit/{id}', 'ProductController@edit');
    Route::post('products/update/{id}', 'ProductController@update');
    /** Purchases */
    Route::get('purchases', 'purchasesController@index');
    Route::get('purchases/store', 'purchasesController@store');
    Route::post('purchases/create', 'purchasesController@create');
    Route::get('purchases/edit/{id}', 'purchasesController@edit');
    Route::post('purchases/update/{id}', 'purchasesController@update');
     /** inventories */
    Route::get('inventories', 'purchasesController@listInventories');
    /** Customers */
    Route::get('customers', 'CustomerController@index');
    Route::get('customers/store', 'CustomerController@store');
    Route::post('customers/create', 'CustomerController@create');
    Route::get('customers/edit/{id}', 'CustomerController@edit');
    Route::get('customers/customer-type', 'CustomerController@customerType');
    Route::put('customers/update/{id}', 'CustomerController@update');
    /** Staff Meet Customers gặp gỡ khách hàng */
    Route::get('staff-meet-customers/list', 'CustomerController@staffMeetCustomersList');
    Route::get('staff-meet-customers/edit/{id}', 'CustomerController@staffMeetCustomersEdit');
    /** payment */
    Route::get('payment', 'PaymentsController@index');
    Route::get('payment/store', 'PaymentsController@store');
    Route::post('payment/create', 'PaymentsController@create');
    Route::get('payment/edit/{id}', 'PaymentsController@edit');
    Route::put('payment/update/{id}', 'PaymentsController@update');
    /** orders */
    Route::get('orders', 'StockController@index');
    Route::get('orders/store', 'StockController@store');
    Route::post('orders/create', 'StockController@create');
    Route::get('orders/edit/{id}', 'StockController@edit');
    Route::put('orders/cancel/{id}', 'StockController@update');
    Route::put('orders/send-approve/{id}', 'StockController@sendApprove');
    Route::put('orders/approve/{id}', 'StockController@Approve');
    Route::put('orders/cancel-approve/{id}', 'StockController@cancelApprove');
    Route::get('orders/product/{id}', 'StockController@stockListPurchases');
    /** companyAssets TSAN CTY*/
    Route::get('company-assets', 'companyAssetsController@index');
    Route::get('company-assets/store', 'companyAssetsController@store');
    Route::post('company-assets/create', 'companyAssetsController@create');
    Route::get('company-assets/edit/{id}', 'companyAssetsController@edit');
    Route::post('company-assets/update/{id}', 'companyAssetsController@update');
    /** customerHandoverStaff bàn giao công việc*/
    Route::get('customer-handover-staff', 'customerHandoverStaffController@index');
    Route::get('customer-handover-staff/store', 'customerHandoverStaffController@store');
    Route::post('customer-handover-staff/create', 'customerHandoverStaffController@create');
    Route::get('customer-handover-staff/edit/{name}/{id}', 'customerHandoverStaffController@edit');
    Route::put('customer-handover-staff/approve/{id}', 'customerHandoverStaffController@approve');
    /** customerCare */
    Route::get('customer-care', 'CustomerCareController@index');
    Route::put('customer-care/care-cskh/{id}', 'CustomerCareController@customersCare');
    /** customerGIFT khách nhận quà tặng */
    Route::get('customer-gift', 'CustomerCareController@CustomerGifts');
    /** orders-gift đơn hàng quà tặng */
    Route::get('orders-gift', 'ordersGiftController@index');
    Route::get('orders-gift/store', 'ordersGiftController@store');
    Route::post('orders-gift/create', 'ordersGiftController@create');
    Route::get('orders-gift/edit/{id}', 'ordersGiftController@edit');
    Route::put('orders-gift/cancel/{id}', 'ordersGiftController@update');
    Route::put('orders-gift/send-approve/{id}', 'ordersGiftController@sendApprove');
    Route::put('orders-gift/approve/{id}', 'ordersGiftController@Approve');
    Route::put('orders-gift/cancel-approve/{id}', 'ordersGiftController@cancelApprove');
    Route::get('orders-gift/product/{id}', 'ordersGiftController@stockListPurchases');
    /** finance phiếu thu tiền */
    /** monthlycashflow Dòng tiền hàng tháng */
    Route::get('monthlycashflow', 'monthlycashflowController@index');
    Route::post('monthlycashflow/export', 'monthlycashflowController@monthlyCashFlowExport');
    /** Receipts phiếu thu */
    Route::get('receipts', 'ReceiptsController@index');
    Route::get('receipts/load-customer/{id}', 'ReceiptsController@receiptsLoadCustomer');
    Route::get('receipts/load-amount-batch/{id}/{stock_id}', 'ReceiptsController@loadAmountBatch');
    Route::get('receipts/store', 'ReceiptsController@store');
    Route::post('receipts/create', 'ReceiptsController@create');
    Route::get('receipts/edit/{id}', 'ReceiptsController@edit');
    Route::put('receipts/cancel/{id}', 'ReceiptsController@update');
    Route::post('receipts/send-approve/{id}', 'ReceiptsController@sendApprove');
    Route::post('receipts/export-receipts', 'ReceiptsController@receiptExport');
    Route::get('receipts/list-export', 'ReceiptsController@listReceipt');
    /** admin tiến hành approve */
    Route::get('receipts/edit-approve/{id}', 'ReceiptsController@editApprove');
    Route::get('receipts/approve-index', 'ReceiptsController@approvesIndex');
    Route::put('receipts/approve/{id}', 'ReceiptsController@Approve');
    Route::put('receipts/cancel-approve/{id}', 'ReceiptsController@cancelApprove');

    /** danh sách cấp bậc nhân viên */
    Route::get('rank', 'RankController@index');
    Route::get('rank/assign-manage', 'RankController@assignManage');
    Route::get('rank/assign-sub-manage', 'RankController@assignSubManage');
    Route::post('rank/create', 'RankController@create');
    Route::post('rank/create-manage-sales', 'RankController@createManageSales');
    Route::get('rank/edit/{id}', 'RankController@edit');
    Route::get('rank/sales-edit-sub-manage/{id}', 'RankController@SalesEditSubManageType');
    Route::put('rank/update-sub-manage/{id}', 'RankController@updateSubManage');

    Route::get('rank/edit-manage-sales/{id}', 'RankController@editManageSales');
    Route::put('rank/update-manage-sales/{id}', 'RankController@updateManageSales');

    /** kpi cho nv cấp sales do chi nhánh trưởng thực hiện */
    Route::get('sales-kpi-payment', 'SalesKpiPaymentController@index');
    Route::get('sales-kpi-payment/customers-period-kpi', 'SalesKpiPaymentController@customersPeriodKpi');
    Route::get('sales-kpi-payment/store', 'SalesKpiPaymentController@store');
    Route::post('sales-kpi-payment/create', 'SalesKpiPaymentController@create');
    Route::get('sales-kpi-payment/edit/{id}', 'SalesKpiPaymentController@edit');
    Route::put('sales-kpi-payment/update/{id}', 'SalesKpiPaymentController@update');
    //ds công nợ khách hàng
    Route::get('sales-kpi-payment/debt-customer', 'SalesKpiPaymentController@debtCustomer');

    /** kpi cho nv cấp sub sales do chi nhánh trưởng thực hiện */
    Route::get('sub-sales-kpi', 'SubSalesKPIController@index');
    Route::get('sub-sales-kpi/customers-period-kpi-sub', 'SubSalesKPIController@customersPeriodKpi');
    Route::get('sub-sales-kpi/store', 'SubSalesKPIController@store');
    Route::post('sub-sales-kpi/create', 'SubSalesKPIController@create');
    Route::get('sub-sales-kpi/edit/{id}', 'SubSalesKPIController@edit');
    Route::put('sub-sales-kpi/update/{id}', 'SubSalesKPIController@update');

    /** kpi cho nv cấp manage sales do chi nhánh trưởng thực hiện */
    Route::get('manage-sales-kpi', 'ManageSalesKPIController@index');
    Route::get('manage-sales-kpi/customers-period-kpi-sub', 'ManageSalesKPIController@customersPeriodKpi');
    Route::get('manage-sales-kpi/store', 'ManageSalesKPIController@store');
    Route::post('manage-sales-kpi/create', 'ManageSalesKPIController@create');
    Route::get('manage-sales-kpi/edit/{id}', 'ManageSalesKPIController@edit');
    Route::put('manage-sales-kpi/update/{id}', 'ManageSalesKPIController@update');

    /** kpi cho nv sales show để nắm ttin */
    Route::get('staffs-sales-kpi', 'staffSalesKPIController@index');
    Route::get('staffs-sales-kpi/kpi-ranks-sub-sales', 'staffSalesKPIController@searchkpiRanksSubSales');
    Route::get('staffs-sales-kpi/kpi-ranks-manage-sales', 'staffSalesKPIController@searchkpiRanksManageSales');

});
