<?php

namespace App\Services\PM;

use App\Models\PM\PMEndtClassSetup;

class PMEndtClassSetupService
{
	public static function getModelByCodeAndClassCode($code,$classCode)
	{
		return PMEndtClassSetup::where("ecs_es_code", $code)->where("ecs_class_code", $classCode)->first();
	}

	public static function getModelByCode($code)
	{
		return PMEndtClassSetup::where("ecs_es_code", $code)->first();
	}
}
