<?php

namespace App\Services\PM;

use App\Models\PMPartner;
use App\Services\Employee\EmployeeService;


class PMPartnerService
{
  public static function getModelByEmployeeId($id)
  {
    return PMPartner::where("pn_employee_id",$id)->first();

  }
  public static function getModelByParnerCode($code)
  {
    return PMPartner::where("pn_partner_code",$code)->first();

  }
  public static function getModelByUserCode($code)
  {
    return PMPartner::where("pn_user_code",$code)->first();

  }
}
