<?php

namespace App\Services\PM;

use App\Models\PM\PMServicesCenterLoc;
use App\Services\Employee\EmployeeService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PMServicesCentersLoc
{
	public static function getModelById($id)
	{
		return PMServicesCenterLoc::find($id);
	}
    public static function getByBrand($sclBranchCode,$sclProvinceCode,$sclDistrictCode)
    {
        return PMServicesCenterLoc::where('scl_branch_code', "LIKE", "%". $sclBranchCode. "%")
            ->where('scl_province_code', "LIKE", "%". $sclProvinceCode. "%")
            ->where('scl_district_code', "LIKE", "%". $sclDistrictCode. "%")
            ->get();
    }
    public static function getByBrandEdit($sclBranchCode,$sclProvinceCode,$sclDistrictCode,$id)
    {
        return PMServicesCenterLoc::where('scl_sys_id','!=',$id)
            ->where('scl_branch_code', "LIKE", "%". $sclBranchCode. "%")
            ->where('scl_province_code', "LIKE", "%". $sclProvinceCode. "%")
            ->where('scl_district_code', "LIKE", "%". $sclDistrictCode. "%")->get();
    }
	public static function createModels($data)
	{
        $model = new PMServicesCenterLoc();
        $data["created_by"] = EmployeeService::getUserCurrent()->name;
        return $model->create($data);
	}

	public static function destroyModel($id)
	{
        $model = self::getModelById($id);
        $model->scl_status = 'inactive';
        $model->save();
        return $model;
	}

	public static function updateModel($data, $id)
	{
        $model = self::getModelById($id);
        $data["updated_by"] = EmployeeService::getUserCurrent()->name;
        $model->update($data);
        return $model;
	}
    public static function getAllServiceLoc($params,$limit)
    {
        $models = PMServicesCenterLoc::select('*');
        if($params['sclBranchCode'] && $params['sclBranchCode'] !== '-1')
        {
            $models->whereIn("scl_branch_code", json_decode($params['sclBranchCode']));
        }
        if($params['sclProvinceCode'] && $params['sclProvinceCode'] !== '-1')
        {
            $models->where("scl_province_code", "LIKE", "%". $params['sclProvinceCode']. "%");
        }
        if($params['sclDistrictCode'] && $params['sclDistrictCode'] !== '-1')
        {
            $models->where("scl_district_code", "LIKE", "%". $params['sclDistrictCode']. "%");
        }
        if($params['fromDate'])
            $models->where('created_at', '>=', $params['fromDate'].' 00:00:00');
        if($params['toDate'])
            $models->where('created_at', '<=', $params['toDate'].' 00:00:00');

        if($params['searchInput']){
            $models->where(function ($models) use ($params){
                $models->where("scl_scr_comp_code", "LIKE", "%". $params['searchInput']. "%");
            });
        }
        $models->with('lossTypeProvide')
            ->with('lossTypeDistrict')
            ->orderBy('scl_branch_code', $params["orderBy"]);
        return $models->paginate($limit);
    }
}
