<?php

namespace App\Services\PM;

use App\Models\PM\PMServicesCenter;
use App\Models\PM\PMServiceComp;
use App\Services\Employee\EmployeeService;
use App\Services\General\GeneralService;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PMServicesCenterService
{
	public static function getModelByParamater($param)
	{
		$models = PMServicesCenter::select('*')->whereNull("scr_parent_id")->with('childrenRecursive')->with('compService')->get();

		return $models;
	}
	public static function createPMServiceComp($brand_id, $asc_id)
	{
		$data = [
			"pm_scr_sys_id" => $asc_id,
			"pm_pc_code" => $brand_id
		];

		$model = new PMServiceComp($data);
		return $model->save();
	}

	public static function getModelById($id)
	{
		return PMServicesCenter::select("*")
												->where('scr_sys_id', $id)
												->with('compService')->get();
	}

	public static function createModels($data)
	{
		$model = new PMServicesCenter();
		return $model->create($data);
	}

	public static function destroyModel($id)
	{
		$model = self::getModelById($id);
		return $model->delete();
	}

	public static function updateModel($data, $id)
	{
		$model = PMServicesCenter::where("scr_sys_id", $id)->first();
		$data["scr_upd_user"] = EmployeeService::getUserCurrent()->name;

		return $model->update($data);
	}

	public static function formatData($data)
	{
		$name = EmployeeService::getUserCurrent()->name;
		$scr_region_name = GeneralService::getInfoByCodeAndType($data->scr_region_id, 'REGION')->pc_name ?? null;
		$scr_province_name = GeneralService::getInfoByCodeAndType($data->scr_province_id, 'PROVINCE')->pc_name ?? null;
		$scr_district_name = GeneralService::getInfoByCodeAndType($data->scr_district_id, 'DISTRICT')->pc_name ?? null;
		$scr_sign_dt=null;
        if(Carbon::hasFormat($data->scr_sign_dt,'m-d-Y')){
           $scr_sign_dt = Carbon::createFromFormat('m-d-Y', $data->scr_sign_dt)->format('Y-m-d');
        }

		return [
			"scr_parent_id" => $data->scr_parent_id ?? null,
			"scr_vn_name" => $data->scr_vn_name,
			"scr_en_name" => $data->scr_en_name,
			"scr_comp_code" => $data->scr_comp_code,
			"scr_vn_address" => $data->scr_vn_address,
			"scr_en_address" => $data->scr_vn_address,
			"scr_mobile" => $data->scr_mobile,
			"scr_mobile2" => $data->scr_mobile2 ?? null,
			"scr_frz_flag" => $data->scr_frz_flag,
			"scr_cr_user" => $name,
			"scr_upd_user" => $name,
			"scr_region_id" => $data->scr_region_id,
			"scr_region_name" => $scr_region_name,
			"scr_province_id" => $data->scr_province_id,
			"scr_province_name" => $scr_province_name,
			"scr_district_id" => $data->scr_district_id,
			"scr_district_name" => $scr_district_name,
			"scr_ward_id" => $data->scr_ward_id ?? null,
			"scr_ward_name" => $data->scr_ward_name ?? null,
			"scr_type"=> $data->scr_type ?? null,
			"scr_sign_dt" =>$scr_sign_dt
		];
	}

	public static function validateCreate($request)
	{
		if(isset($request->scr_vn_name) && !empty($request->scr_vn_name)){
			return 'Tên đơn vị xử lý không được để trống.';
		}

		if(isset($request->scr_comp_code) && !empty($request->scr_comp_code)){
			return 'Mã vị trí không được để trống.';
		}
		if(isset($request->scr_vn_address) && !empty($request->scr_vn_address)){
			return 'Địa chỉ đơn vị xử lý không được để trống.';
		}
		if(isset($request->scr_mobile) && !empty($request->scr_mobile)){
			return 'TEL đơn vị xử lý không được để trống.';
		}

		if(isset($request->scr_frz_flag) && !empty($request->scr_frz_flag)){
			return 'Trạng thái không để trống.';
		}

		return 0;
	}

}
