<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\WarrantyForm;
use Illuminate\Database\Eloquent\Model;

class WarrantyFormService
{
    function getModelById($id){
        return WarrantyForm::find($id);
    }
    function getQuery(){
        return WarrantyForm::all();
    }
    public static function getByCode($code)
    {
        return WarrantyForm::where('warranty_form_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return WarrantyForm::where('id','!=',$id)->where('warranty_form_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('warranty_form_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("warranty_form_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('warranty_form_name', '=', $param['searchInput']);
            });
        }
        $model->paginate($limit);
        return $model;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function creeModel($data)
    {
        $model=new WarrantyForm();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new WarrantyForm();
        return $model->create($data);
    }
}
