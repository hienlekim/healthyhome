<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Brand;
use App\Services\DM\CurrencyService;
use App\Services\DM\CountryService;
use Illuminate\Database\Eloquent\Model;

class BrandService
{

    public static function getQuery(){
        return Brand::select('brand.*')
            ->with('countries')
            ->with('currencies')
            ->with('bankconnect01')
            ->with('bankconnect02')
            ->with('bankconnect03')
            ->with('bankconnect04')
            ->with('bankconnect05');
    }
    public static function getModelById($id){
        return self::getQuery()->find($id);
    }
    public static function getByCode($code)
    {
        return Brand::where('brand_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Brand::where('id','!=',$id)->where('brand_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('brand_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("brand_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('brand_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);;
        return $models;
    }

    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Brand();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Brand();
        return $model->create($data);
    }

}
