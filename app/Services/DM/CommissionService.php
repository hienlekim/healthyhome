<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Commission;
class CommissionService
{
    function getModelById($id){
        return Commission::find($id);
    }
    function getQuery(){
        return Commission::all();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('origin_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("origin_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('origin_name', 'LIKE', "". $param['searchInput']. "%");
            });
        }
        $model->paginate($limit);
        return $model;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Commission();
        return $model->create($data);
    }
    public static function RemoveModel($data,$id)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
}
