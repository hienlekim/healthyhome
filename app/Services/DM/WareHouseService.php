<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\WareHouse;
class WareHouseService
{
    public static function getModelById($id){
        return WareHouse::find($id);
    }
    public static function getQuery(){
        return WareHouse::select("ware_house.*");;
    }
    public static function getByCode($code)
    {
        return WareHouse::where('ware_house_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return WareHouse::where('id','!=',$id)->where('ware_house_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('ware_house_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("ware_house_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('ware_house_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new WareHouse();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new WareHouse();
        return $model->create($data);
    }
}
