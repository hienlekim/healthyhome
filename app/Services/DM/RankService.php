<?php

namespace App\Services\DM;
use App\Models\Staff;
use App\Services\DM\GeneralService;
use Carbon\Carbon;
use App\Models\Rank;
class RankService
{
    public static function getModelById($id){
        return Staff::find($id);
    }
    public static function getQuery(){
        $modal = GeneralService::getStaffRank();
        return $modal;
    }
    public static function getStaffChildrent($id)
    {
        $modal = staff::where('staff_parent_id', $id)->where('staff_rank_id',3)
            ->get();
        return $modal;
    }
    public static function getByCodeEdit($code,$id)
    {
        return Rank::where('id','!=',$id)->where('rank_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getStaffChildrentSub($id)
    {
        $modal = staff::where('staff_parent_id', $id)
            ->get();
        return $modal;
    }
    public static function getModelByManageSales()
    {
        $model=self::getQuery();
        $model->where('staff.staff_rank_id',2);
        $models = $model->get();
        return $models;
    }
    public static function getModelBySubManageSales()
    {
        $model=self::getQuery();
        $model->where('staff_rank_id',3);
        $models = $model->get();
        return $models;
    }
    public static function getModelSubManageSalesNoAssign()
    {
        $model=self::getQuery();
        $model->where('staff_rank_id',3)->whereNull('staff_parent_id');
        $models = $model->get();
        return $models;
    }
    //ds nv sales
    public static function getModelBySales()
    {
        $model=self::getQuery();
        $model->whereNull('staff_rank_id');
        $models = $model->get();
        return $models;
    }
    //update nv
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Rank();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Rank();
        return $model->create($data);
    }
    public static function getlistSubManage()
    {
        $model=self::getQuery();
        $model->whereNull('staff_rank_id')->whereNull('staff_parent_id');
        $models = $model->get();
        return $models;
    }
    //ds nv sales cấp 2 chưa được assign cho manage
    public static function getNoSalesManageBy()
    {
        $model=self::getQuery();
        $model->where('staff_rank_id',3)->whereNull('staff_parent_id');
        $models = $model->get();
        return $models;
    }

    public static function getNoEditSalesManabBy()
    {
        $model=self::getQuery();
        $models = $model->get();
        return $models;
    }
    //hiển thị những nv sales còn lại đã được assign
    public static function getNoEditSalesSubBy()
    {
        $model=self::getQuery();
        $models = $model->get();
        return $models;
    }
    //hiển thị những nv sales còn lại chưa được assign
    public static function getNoSalesSubBy()
    {
        $model=self::getQuery();
        $models = $model->whereNull('staff_rank_id')->whereNull('staff_parent_id')->get();
        return $models;
    }
    public static function updateRankSalesModel($array_sales,$data)
    {
        $model=Staff::whereIn('id',$array_sales)->update($data);
        return $model;
    }
    //hiển thị những nv sub sales còn lại đã được assign
    public static function getNoEditSalesManagebBy()
    {
        $model=self::getQuery();
        $models = $model->where('staff_rank_id',3)->whereNull('staff_parent_id')->get();
        return $models;
    }
}
