<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Tax;
class TaxService
{
    public static function getModelById($id){
        return Tax::find($id);
    }
    public static function getQuery(){
        return Tax::select("tax.*");
    }
    public static function getByCode($code)
    {
        return Tax::where('tax_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Tax::where('id','!=',$id)->where('tax_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('tax_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("tax_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('tax_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Tax();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Tax();
        return $model->create($data);
    }
}
