<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Decentralization;
class DecentralizationService
{
    public static function getModelById($id){
        return Decentralization::find($id);
    }
    public static function getQuery(){
        return Decentralization::select('decentralization.*')->with('positions');
    }
    public static function getByCode($decen_position_id)
    {
        return Decentralization::where('decen_position_id',$decen_position_id)->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Decentralization::where('id','!=',$id)->where('position_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Decentralization();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Decentralization();
        return $model->create($data);
    }
}
