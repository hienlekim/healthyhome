<?php
/**
 * date:10/08/2021
 * content:phiếu thu từ đơn hàng, khi tới tháng phải trả góp nv có
 * thẩm quyền sẽ tạo phiếu thu dựa trên mã đơn hàng, sau đó chờ xếp duyệt nếu khách hàng đã ck
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Services\Stock\CustomerService;
use App\Services\Stock\CustomerCareService;
use App\Services\Stock\ImagesService;
use App\Services\Stock\ReceiptsService;
use App\Services\Stock\PeriodService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\GeneralService;
use App\Services\DM\HistoryService;
use App\Services\AbstractService;
use App\Exports\receiptsExport;
use Maatwebsite\Excel\Facades\Excel;

class ReceiptsController extends Controller
{
    public function index(Request $request)
    {
        $date_start = date('Y-m-01');
        $date_end = date('Y-m-t');
        $rowsPerPage = $request->limit;
        $param=[
            'fromDate'=>$request->input('fromDate')?? $date_start,
            'toDate'=>$request->input('toDate')?? $date_end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = ReceiptsService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("receipts.index",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    public function receiptsType(Request $request)
    {
        $StksTypes = GeneralService::getReceiptsTypeQuery();
        return AbstractService::DataSuccess($StksTypes);
    }
    /** khi nhân viên sale chọn sp thì được load lên */
    public function receiptsLoadCustomer(Request $request,$id)
    {
        $product = ReceiptsService::getCustomersReceipt($id);
        if($product){
            $date = date('d-m-Y', strtotime($product->period_date));
        }else{
            $date=0;
        }
        $data = [
            'date'=>$date,
            'product'=>$product
        ];
        return AbstractService::DataSuccess($data);
    }
//    /** khi nhân viên sale chọn sp thì được load lên */
    public function loadAmountBatch(Request $request,$id,$stock_id)
    {
        $modal = ReceiptsService::getAmountBatchReceipt($id,$stock_id);
        return AbstractService::DataSuccess($modal);
    }

    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $name_user = \Illuminate\Support\Facades\Auth::user()->name;
            $brandUser = GeneralService::getBrandUser();
            $Custom = GeneralService::getOdersPeroid();
            $Customer = GeneralService::getOdersCustomBrand($Custom);
            $date = date('d-m-Y');
            return view('receipts.create',compact('date','Customer','brandUser','name_user'))->render();
        }
    }
    public function create(Request $request){
        $query = ReceiptsService::getByCode($request->receipts_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $receipts_amount_date = date('Y-m-d');
            $data =[
                'receipts_code'=>$request->receipts_code,
                'receipts_stock_id'=>$request->receipts_stock_id,
                'receipts_customer_id'=>$request->receipts_customer_id,
                'receipts_period_id'=>$request->receipts_period_id,
                'receipts_staff_id'=>$request->receipts_staff_id,
                'receipts_currency_id'=>$request->receipts_currency_id,
                'receipts_brand_id'=>$request->receipts_brand_id,
                'receipts_period_date'=>$request->period_date,
                'receipts_period_amount'=>str_replace(".","",$request->period_amount),
                'receipts_amount'=>str_replace(".","",$request->period_amount_batch),
                'receipts_period_payments_id'=>$request->period_payments_id,
                'interest_rate'=>$request->interest_rate,
                'receipts_amount_date'=>$receipts_amount_date,
                'number_days_overdue'=>$request->number_days_overdue,
                'fines'=>$request->fines,
                'overdue_fines'=>str_replace(".","",$request->overdue_fines),
                'actual_amount'=>str_replace(".","",$request->actual_amount),
                'receipts_description'=>$request->receipts_description,
                'receipts_type'=>'PHIEUTHUKHACHHANG',
                'receipts_status'=>1,
            ];
            $asc = ReceiptsService::createModel($data);
            if($asc) {
              /**lich su*/
                $data_his = [
                    'history_code'=>$asc->receipts_code,
                    'history_name'=>'Phiếu thu Tiền -'.$asc->receipts_code,
                    'history_staff_id'=>$asc->receipts_staff_id,
                    'history_action'=>'tạo mới',
                    'history_price'=>$asc->receipts_amount
                ];
                HistoryService::createModel($data_his);
//                /**kết thúc lịch sử*/
                /**cập nhật lại trạng thái bên ds thanh toán (period) */
                $dataperoid = [
                    'period_receipts_actual_amount'=>$asc->actual_amount,
                    'period_status'=>0,
                    'period_date_reality'=>$asc->receipts_amount_date
                ];
                PeriodService::updateModel($asc->receipts_period_id,$dataperoid);
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = ReceiptsService::getModelById($id);
        $images_approve = $data['receipts_images'];
        $brandUser = GeneralService::getBrandUser();
        $Custom = GeneralService::getOdersPeroid();
        $Customer = GeneralService::getOdersCustomBrand($Custom);
        $Customers = GeneralService::getCustomerQuery();
        $date = date('d-m-Y');
        $Images="default-product-img.jpg";
        return view('receipts.approve', compact('Customers','Images','images_approve','brandUser','Custom','data','Customer'))->render();
    }
    public function update(Request $request, $id)
    {
        $brandUser = GeneralService::getBrandUser();
        $data =[
            'receipts_status'=>0
        ];
        $asc = ReceiptsService::updateModel($id,$data);
        if($asc) {
            $data = ReceiptsService::getModelById($id);
            /**lich su*/
            $data_his = [
                'history_code'=>$asc['receipts_code'],
                'history_name'=>'Hủy phiếu thu',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'CancelOderGift',
            ];
            HistoryService::createModel($data_his);
            /**cập nhật lại trạng thái bên ds thanh toán (period) */
            $dataperoid = [
                'period_receipts_actual_amount'=>0,
                'period_status'=>1,
                'period_date_reality'=>''
            ];

            PeriodService::updateModel($data['receipts_period_id'],$dataperoid);
            return AbstractService::ResultSuccess($asc);

        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function sendApprove(Request $request, $id)
    {
        $brandUser = GeneralService::getBrandUser();
        //trường hợp đã thêm mới nhưng ko có images
        if($request->has('images_avata_1') ) {
            $file_local =  $request->file('images_avata_1');
            $extension = $request->file('images_avata_1')->getClientOriginalExtension();
            if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                $new_images_product = rand() . '.' . $extension;
                $file_local->move(public_path('images/receipts'), $new_images_product);
                $dataImages=[
                    "receipts_images"=>$new_images_product,
                    'receipts_status'=>2,
                ];
                $asc = ReceiptsService::updateModel($id,$dataImages);
                if ($asc) {
                    $data = ReceiptsService::getModelById($id);
                    /**lich su*/
                    $data_his = [
                        'history_code'=>$data['receipts_code'],
                        'history_name' => 'Gửi duyệt phiếu thu',
                        'history_staff_id' => $brandUser['id'],
                        'history_action' => 'Send Approve',
                    ];
                    HistoryService::createModel($data_his);
                    return AbstractService::ResultSuccess($asc);
                }
            }
        }else{
            $dataImages=[
                'receipts_status'=>2,
            ];
            $asc = ReceiptsService::updateModel($id,$dataImages);
            if ($asc) {
                $data = ReceiptsService::getModelById($id);
                /**lich su*/
                $data_his = [
                    'history_code'=>$data['receipts_code'],
                    'history_name' => 'Gửi duyệt phiếu thu',
                    'history_staff_id' => $brandUser['id'],
                    'history_action' => 'Send Approve',
                ];
                HistoryService::createModel($data_his);
                return AbstractService::ResultSuccess($asc);
            }
        }

    }
    public function editApprove($id)
    {
        $data = ReceiptsService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $Custom = GeneralService::getOdersPeroid();
        $Customer = GeneralService::getOdersCustomBrand($Custom);
        $Images="default-product-img.jpg";
        $Customers = GeneralService::getCustomerQuery();
        $images_approve = $data['receipts_images'];
        return view('receipts.approveAdmin', compact('images_approve','Customers','Images','brandUser','Custom','data','Customer'))->render();
    }
    public function approvesIndex(Request $request)
    {
        $date_start = date('Y-m-01');
        $date_end = date('Y-m-t');
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'searchBranch'=>$request->input('searchBranch'),
            'searchInputCustomer'=>$request->input('searchInputCustomer'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = ReceiptsService::getModelByApprovesIndex($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("receipts.approveAdminIndex",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    public function Approve(Request $request, $id)
    {
        $brandUser = GeneralService::getBrandUser();
        $date = date('d-m-Y');
        $data =[
            'receipts_status'=>3,
            'receipts_staff_approve_id'=>$brandUser['id'],
            'receipts_staff_approve_date'=>$date
        ];
        $asc = ReceiptsService::updateModel($id,$data);
        if($asc) {
            /**lich su*/
            $data = ReceiptsService::getModelById($id);
            /**lich su*/
            $data_his = [
                'history_code'=>$data['receipts_code'],
                'history_name'=>'Duyệt phiếu thu',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'Send Approve',
            ];
            HistoryService::createModel($data_his);
            return AbstractService::ResultSuccess($asc);

        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function cancelApprove(Request $request, $id)
    {
        $brandUser = GeneralService::getBrandUser();
        $data =[
            'receipts_status'=>1
        ];
        $asc = ReceiptsService::updateModel($id,$data);
        if($asc) {
            $data = ReceiptsService::getModelById($id);
            /**lich su*/
            $data_his = [
                'history_code'=>$data['receipts_code'],
                'history_name'=>'Admin hủy duyệt phiếu thu',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'CancelOderGift',
            ];
            HistoryService::createModel($data_his);
            return AbstractService::ResultSuccess($asc);

        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function receiptExport(Request $request){
        if ($request->session()->token() != $request->_token) {
            response()->json(["status" => false, "message" => "Dữ liệu không hợp lệ"]);
        }
        $date = date('d-m-Y');
        $id = $request->idReceipt;
        $data = ReceiptsService::getModelById($id);
        $price_last = ReceiptsService::getAmountBatchReceipt($data->receipts_period_id,$data->receipts_stock_id);
        $customer =$data['receiptcustomer']['customers_name'];
        $receipts_period_amount =number_format($data['receipts_period_amount'], 2, ',', '.');
        $phone =$data['receiptcustomer']['customers_phone'];
//        $receipts_amount =number_format($data['receipts_amount'], 2, ',', '.');
        $receipts_currency =$data['receiptsCurrency']['currency_code'];
        $customers_address =$data['receiptcustomer']['customers_address'];
        $period_payments =$data['receiptspayment']['payments_code'];
        $receipts_period_date =date('d-m-Y', strtotime($data['receipts_period_date']));
        $interest_rate =$data['interest_rate'];
        $period_name = $data['receiptsperiod']['period_name'];
        $period_date =date('d-m-Y', strtotime($data['receipts_period_date']));
        $receipts_amount_date =date('d-m-Y', strtotime($data['receipts_amount_date']));
        $receipts_amount = number_format($data['receipts_amount'], 2, ',', '.');
        $price_lasts = number_format($price_last, 2, ',', '.');
        $number_days_overdue =$data['number_days_overdue'];
        $overdue_fines = number_format($data['overdue_fines'], 2, ',', '.');
        $actual_amount =number_format($data['actual_amount'], 2, ',', '.');
        $staff =$data['receiptstaff']['staff_name'];
        return Excel::download(new receiptsExport([
            'customer'=>$customer,
            'receipts_period_amount'=>$receipts_period_amount,
            'phone'=>$phone,
            'receipts_amount'=>$receipts_amount,
            'receipts_currency'=>$receipts_currency,
            'customers_address'=>$customers_address,
            'period_payments'=>$period_payments,
            'receipts_period_date'=>$receipts_period_date,
            'interest_rate'=>$interest_rate,
            'period_name'=>$period_name,
            'period_date'=>$period_date,
            'receipts_amount_date'=>$receipts_amount_date,
            'price_lasts'=>$price_lasts,
            'number_days_overdue'=>$number_days_overdue,
            'overdue_fines'=>$overdue_fines,
            'actual_amount'=>$actual_amount,
            'staff'=>$staff,
            'date'=>$date,
        ]), 'phieu_thu.xlsx');

    }
}

