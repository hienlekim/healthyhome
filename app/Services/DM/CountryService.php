<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Country;
class CountryService
{
    public static function CountryCode($brandCurrencyId)
    {
        $model=Country::where("country_code", "LIKE", "%". $brandCurrencyId. "%")->first();
        return $model;
    }
    public static function getByCode($code)
    {
        return Country::where('country_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Country::where('id','!=',$id)->where('country_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelById($id){
        return Country::find($id);
    }
    public static function getQuery(){
        return Country::select("country.*");
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('country_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("country_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('country_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Country();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Country();
        return $model->create($data);
    }
}
