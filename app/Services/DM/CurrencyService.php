<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Currency;
use Illuminate\Database\Eloquent\Model;

class CurrencyService
{
    public static function BrandCurrencyCode($brandCurrencyId)
    {
        $model=Currency::where("currency_code", "LIKE", "%". $brandCurrencyId. "%")->first();
        return $model;
    }
    public static function getByCode($code)
    {
        return Currency::where('currency_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Currency::where('id','!=',$id)->where('currency_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelById($id){
        return Currency::find($id);
    }
    public static function getQuery(){
        return Currency::select("currency.*");
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('currency_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("currency_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('currency_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Currency();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Currency();
        return $model->create($data);
    }
}
