<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Unit;
class UnitService
{
    public static function getModelById($id){
        return Unit::find($id);
    }
    public static function getQuery(){
        return Unit::select('unit.*');
    }
    public static function getByCode($code)
    {
        return Unit::where('unit_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Unit::where('id','!=',$id)->where('unit_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('unit_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("unit_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('unit_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Unit();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Unit();
        return $model->create($data);
    }
}
