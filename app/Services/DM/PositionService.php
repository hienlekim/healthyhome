<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Position;
class PositionService
{
    public static function getModelById($id){
        return Position::find($id);
    }
    public static function getQuery(){
        return Position::select('position.*');
    }
    public static function getByCode($code)
    {
        return Position::where('position_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Position::where('id','!=',$id)->where('position_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('position_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("position_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('position_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Position();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Position();
        return $model->create($data);
    }
}
