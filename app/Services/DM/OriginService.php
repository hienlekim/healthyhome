<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Origin;
class OriginService
{
    public static function getModelById($id){
        return Origin::find($id);
    }
    public static function getQuery(){
        return Origin::select('origin.*');
    }
    public static function originCode($OriginCode)
    {
        $model=Origin::where("origin_code", "LIKE", "%". $OriginCode. "%")->first();
        return $model;
    }
    public static function getByCode($code)
    {
        return Origin::where('origin_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Origin::where('id','!=',$id)->where('origin_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('origin_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("origin_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('origin_name', 'LIKE', "". $param['searchInput']. "%");
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Origin();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Origin();
        return $model->create($data);
    }
}
