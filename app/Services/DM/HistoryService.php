<?php

namespace App\Services\DM;
use App\Models\History;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class HistoryService
{
    public static function createModel($data)
    {
        $model=new History();
        return $model->create($data);
    }
}
