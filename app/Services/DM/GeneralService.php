<?php

namespace App\Services\DM;
use App\Models\Brand;
use App\Models\ProductType;
use App\Models\Receipts;
use App\Models\Stock;
use Carbon\Carbon;
use App\Models\Bank;
use App\Models\Currency;
use App\Models\Customer;
use App\Models\CustomerCare;
use App\Models\History;
use App\Models\Images;
use App\Models\Origin;
use App\Models\Payments;
use App\Models\Period;
use App\Models\Position;
use App\Models\Product;
use App\Models\Rank;
use App\Models\Staff;
use App\Models\StaffKpiOrder;
use App\Models\Supplier;
use App\Models\Tax;
use App\Models\Unit;
use App\Models\WareHouse;
use App\Models\WarrantyForm;
use App\Models\Country;
use App\Models\Decentralization;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/** hiển thị danh sách của danh mục tự điển */
class GeneralService
{
    public static function getWareHouseQuery(){
        return WareHouse::select("ware_house.*")->get();
    }
    public static function getBankQuery(){
        return Bank::select("bank.*")->get();
    }
    public static function getCountryQuery(){
        return Country::select("country.*")->get();
    }
    public static function getCurrencyQuery(){
        return Currency::select("currency.*")->get();
    }
    public static function getCustomerQuery(){
        return Customer::select("customers.*")->get();
    }
    public static function getPositionQuery(){
        return Position::select("position.*")->get();
    }
    public static function getBankQueryBrand($bankId){
        $brand = Brand::select('brand_bank_id','brand_bank_id_1','brand_bank_id_2','brand_bank_id_3','brand_bank_id_4')->where('id',$bankId)->get()->toArray();
        if($brand){
           $data =[];
           array_push($data,$brand[0]['brand_bank_id'],$brand[0]['brand_bank_id_1'],$brand[0]['brand_bank_id_2'],$brand[0]['brand_bank_id_3'],$brand[0]['brand_bank_id_4']);
        }else{
            $data =[];
        }
        $model = Bank::select("bank.*")->whereIn('id',$data)->get();
        return $model;
    }
    public static function getPositionDecentral(){
        return Decentralization::select("decentralization.*")->with('positions')->get();
    }
    public static function getBrand(){
        return Brand::select("brand.*")->get();
    }
    public static function getCountry(){
        return Country::select("country.*")->get();
    }
    public static function getOriginQuery(){
        return Origin::select("origin.*")->get();
    }
    public static function getSuperliserQuery(){
        return Supplier::select("supplier.*")->get();
    }
    public static function getProductQuery(){
        return Product::select("product.*")->get();
    }
    public static function getLocationQuery($id){
        return WareHouse::select("ware_house.*")->where('id',$id)->get();
    }
    public static function getLocationAll(){
        return WareHouse::select("ware_house.*")->get();
    }
    /** hiển thị kho hàng theo chi nhánh trong kho **/
    public static function getBrandUser(){
        $id_user =\Illuminate\Support\Facades\Auth::user()->id;
        return Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions')
            ->where('staff.staff_user_id',$id_user)->first();
    }
    /** hiển thị thông tin chỉ nhân viên đang đăng nhập **/
    public static function getBrandUserStaff(){
        $staffs = self::getBrandUser();
        $model = Staff::select("staff.*")
            ->with('brands')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho quản trị cao nhất **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->whereIn('staff.staff_brand_id',[3,4]);
            /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $model->where('staff.staff_user_id',$staffs['staff_user_id']);
        }
        $staff = $model;
        return $staff;
    }
    /** hiển thị danh sách khách hàng theo chi nhánh trưởng và nhân viên sale và quan trị  **/
    public static function getCustomersBrand(){
        $staffs = self::getBrandUser();
        $model =  Customer::select('customers.*')
            ->with('brandCustomer')
            ->with('customerStaffs')
            ->with('customersCountry');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->whereIn('customers.customers_brand_id',[3,4]);
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
           $model->where('customers.customers_brand_id',$staffs['brands']['id']);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('customers.customers_staffs_id',$staffs['id']);
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $model->whereNull('customers.customers_staffs_id');
        }
        $customers = $model;
        return $customers;
    }
    public static function getCustomersBrandEdit($id){
        $customers = Customer::select('customers.*')
            ->with('brandCustomer')
            ->with('customerStaffs')
            ->with('customersCountry')->where('id',$id)->first();
        return $customers;
    }
    public static function getUnitQuery(){
        return Unit::select("unit.*")->get();
    }
    public static function getPaymentQuery(){
        return Payments::select("payments.*")->get();
    }
    public static function getproductTypeQuery(){
        return ProductType::select("product_type.*")->get();
    }
    // mã nhập kho
    public static function getpurchasesTypeQuery(){
        $sql = Stock::select("stock_code")->where('Type','NHAPKHO')->where('stock_status','>','0')->orderBy('stock_code', 'desc')->max('stock_code');
        if($sql){
            $str = explode("PN",$sql);
            $sum = $str[1]+1;
            if($sum <10){
                $subString = 'PN0000'.$sum;
            }elseif ($sum >9 && $sum < 100){
                $subString = 'PN000'.$sum;
            }elseif ($sum >99 && $sum < 1000){
                $subString = 'PN00'.$sum;
            }elseif ($sum >999 && $sum < 10000){
                $subString = 'PN0'.$sum;
            }
        }else{
            $subString = 'PN00001';
        }
        return $subString;
    }
    public static function getStocksTypeQuery(){
        $sql = Stock::select("stock_code")->where('Type','XUATKHO')->where('stock_status','>','0')->orderBy('stock_code', 'desc')->max('stock_code');
        if($sql){
            $str = explode("PX",$sql);
            $sum = $str[1]+1;
            if($sum <10){
                $subString = 'PX0000'.$sum;
            }elseif ($sum >9 && $sum < 100){
                $subString = 'PX000'.$sum;
            }elseif ($sum >99 && $sum < 1000){
                $subString = 'PX00'.$sum;
            }elseif ($sum >999 && $sum < 10000){
                $subString = 'PX0'.$sum;
            }
        }else{
            $subString = 'PX00001';
        }
        return $subString;
    }
    public static function getStafsTypeQuery(){
        $sql = Staff::select("staff_code")->whereNull('staff_status')->orderBy('staff_code', 'desc')->max('staff_code');
        if($sql){
            $str = explode("NV",$sql);
            $sum = $str[1]+1;
            if($sum <10){
                $subString = 'NV0000'.$sum;
            }elseif ($sum >9 && $sum < 100){
                $subString = 'NV000'.$sum;
            }elseif ($sum >99 && $sum < 1000){
                $subString = 'NV00'.$sum;
            }elseif ($sum >999 && $sum < 10000){
                $subString = 'NV0'.$sum;
            }
        }else{
            $subString = 'NV00001';
        }
        return $subString;
    }
    //khach hàng
    public static function getCustomersTypeQuery(){
        $sql = Customer::select("customers_code")->orderBy('customers_code', 'desc')->max('customers_code');
        if($sql){
            $str = explode("KH",$sql);
            $sum = $str[1]+1;
            if($sum <10){
                $subString = 'KH0000'.$sum;
            }elseif ($sum >9 && $sum < 100){
                $subString = 'KH000'.$sum;
            }elseif ($sum >99 && $sum < 1000){
                $subString = 'KH00'.$sum;
            }elseif ($sum >999 && $sum < 10000){
                $subString = 'KH0'.$sum;
            }
        }else{
            $subString = 'KH00001';
        }
        return $subString;
    }
    //xuat kho qua tang
    public static function getStocksGiftTypeQuery(){
        $sql = Stock::select("stock_code")->where('Type','XUATKHOQUATANG')->where('stock_status','>','0')->orderBy('stock_code', 'desc')->max('stock_code');
        if($sql){
            $str = explode("PXQT",$sql);
            $sum = $str[1]+1;
            if($sum <10){
                $subString = 'PXQT0000'.$sum;
            }elseif ($sum >9 && $sum < 100){
                $subString = 'PXQT000'.$sum;
            }elseif ($sum >99 && $sum < 1000){
                $subString = 'PXQT00'.$sum;
            }elseif ($sum >999 && $sum < 10000){
                $subString = 'PXQT0'.$sum;
            }
        }else{
            $subString = 'PXQT00001';
        }
        return $subString;
    }
    //xuat kho qua tang
    public static function getCompanyAssetsTypeQuery(){
        $sql = Stock::select("stock_code")->where('Type','XUATTAISANCONGTY')->where('stock_status','>','0')->orderBy('stock_code', 'desc')->max('stock_code');
        if($sql){
            $str = explode("TSC",$sql);
            $sum = $str[1]+1;
            if($sum <10){
                $subString = 'TSC0000'.$sum;
            }elseif ($sum >9 && $sum < 100){
                $subString = 'TSC000'.$sum;
            }elseif ($sum >99 && $sum < 1000){
                $subString = 'TSC00'.$sum;
            }elseif ($sum >999 && $sum < 10000){
                $subString = 'TSC0'.$sum;
            }
        }else{
            $subString = 'TSC00001';
        }
        return $subString;
    }
    /** danh sách nhập hàng*/
    public static function getpurchasesBrand(){
        $staffs = self::getBrandUser();
        $model = Stock::select('stock.*')
            ->with('units')
            ->with('suppliers')
            ->with('brands')
            ->with('staffStock')
            ->with('productStocks')
            ->with('stockers')
            ->with('stockLocation')
            ->with('currencies');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $stocks = $model->where('Type','like','%NHAPKHO%')->groupBy('stock_code');
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $stocks = $model
                ->where('stock_brand_id',$staffs['brands']['id'])
                ->where('Type','like','%NHAPKHO%')->groupBy('stock_code');
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model
                ->whereNull('stock_brand_id')
               ->groupBy('stock_code');
        }
        return $stocks;
    }
    /** danh sách đơn hàng sẽ theo*/
    public static function getOdersBrand(){
        $staffs = self::getBrandUser();
        $model = Stock::select('stock.*')
            ->with('units')
            ->with('customers')
            ->with('brands')
            ->with('staffStock')
            ->with('stockers')
            ->with('stockLocation')
            ->with('currencies')->where('Type','like','%XUATKHO%');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->groupBy('stock_code');
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $model->where('stock.stock_brand_id',$staffs['brands']['id'])->groupBy('stock_code');
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('stock.stock_staff_id',$staffs['id'])->groupBy('stock_code');
        }
        $stocks =  $model->where('stock_status','>',0);
        return $stocks;
    }
    /** danh sách đơn hàng sẽ theo*/
    public static function getOdersCustomerBrand(){
        $staffs = self::getBrandUser();
        $model =  Customer::select('customers.*')
            ->with('brandCustomer')
            ->with('customerStaffs')
            ->with('customersCountry');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
          $model->where('customers.customers_staffs_id',$staffs['id']);
        }
        $customers = $model->get();
        return $customers;
    }
    /** danh sách đơn hàng sẽ theo*/
    public static function getOdersCustomerBrandAll(){
        $staffs = self::getBrandUser();
        $model =  Customer::select('customers.*')
            ->with('brandCustomer')
            ->with('customerStaffs')
            ->with('customersCountry');
        $customers = $model->get();
        return $customers;
    }
    public static function getpurchaseshange(){
        $staffs = self::getBrandUser();
        $model = Stock::select('stock.*')
            ->with('units')
            ->with('suppliers')
            ->with('brands')
            ->with('staffStock')
            ->with('productStocks')
            ->with('stockers')
            ->with('stockLocation')
            ->with('currencies')->where('Type','like','%NHAPKHO%');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->groupBy('stock_product_id');
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker' && $staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
           $model->where('stock_brand_id',$staffs['brands']['id'])
                ->groupBy('stock_product_id');
        }
        $stocks = $model;
        return $stocks;
    }
    public static function getStockshange(){
        $staffs = self::getBrandUser();
        $model = Stock::select('stock.*')
            ->with('units')
            ->with('suppliers')
            ->with('brands')
            ->with('staffStock')
            ->with('productStocks')
            ->with('stockers')
            ->with('stockLocation')
            ->with('currencies');
            $model->where('stock_brand_id',$staffs['brands']['id'])
                ->whereIn('Type',['NHAPKHO','XUATKHO']);
        $stocks = $model;
        return $stocks;
    }
    /** hiển thị thông tin chỉ nhân viên đang đăng nhập **/
    public static function getCustomerCare(){
        $staffs = self::getBrandUser();
        $model = CustomerCare::select("customer_care.*")
            ->with('custom')
            ->with('brandCustom')
            ->with('staffCustom');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->whereIn('customer_brand_id',[3,4]);;
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('customer_brand_id',$staffs['brands']['id']);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
           $model->where('customer_staff_id',$staffs['id']);
        }
        $models =$model;
        return $models;
    }
    /** nhân viên thuộc c nhánh **/
    public static function getBrandStaff(){
        $staffs = self::getBrandUser();
        if($staffs['staff_brand_id']>0){
            $brandId=$staffs['brands']['id'];
        }else{
            $brandId="";
        }
        return Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions')
            ->where('staff.staff_brand_id',$brandId)->get();
    }
    /** nhân viên thuộc c nhánh **/
    public static function getBrandCustomers(){
        $staffs = self::getBrandUser();
        if($staffs['staff_brand_id']>0){
            $brandId=$staffs['brands']['id'];
        }else{
            $brandId="";
        }
        return Customer::select("customers.*")
            ->where('customers.customers_brand_id',$brandId)->get();
    }
    /** danh sách đơn hàng sẽ theo*/
    public static function getOdersGiftBrand(){
        $staffs = self::getBrandUser();
        $model = Stock::select('stock.*')
            ->with('units')
            ->with('customers')
            ->with('brands')
            ->with('staffStock')
            ->with('stockers')
            ->with('stockLocation')
            ->with('currencies')->where('Type','like','%XUATKHOQUATANG%');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->groupBy('stock_code');
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $model->where('stock.stock_brand_id',$staffs['brands']['id'])->groupBy('stock_code');
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('stock.stock_staff_id',$staffs['id'])->groupBy('stock_code');
        }
        $stocks =  $model->where('stock_status','>',0);
        return $stocks;
    }
    /** danh sách đơn hàng sẽ theo*/
    public static function getcompanyAssetsBrand(){
        $staffs = self::getBrandUser();
        $model = Stock::select('stock.*')
            ->with('units')
            ->with('customers')
            ->with('brands')
            ->with('staffStock')
            ->with('stockers')
            ->with('stockLocation')
            ->with('currencies')->where('Type','like','%XUATTAISANCONGTY%');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->groupBy('stock_code');
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $model->where('stock.stock_brand_id',$staffs['brands']['id'])->groupBy('stock_code');
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('stock.stock_staff_id',$staffs['id'])->groupBy('stock_code');
        }
        $stocks =  $model->where('stock_status','>',0);
        return $stocks;
    }
    public static function getCustomerChildren(){
        $staffs = self::getBrandUser();
        $model =  Customer::select('id');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('customers_staffs_id',$staffs['id']);
        }else{
            $model;
        }
        $model->whereNotNull('customers_parent_id');
        $customers = $model->get()->toArray();
        return $customers;
    }
    public static function getStockCustomerChildren()
    {
        $customerChildren = self::getCustomerChildren();
        $model = Stock::select('stock_customer_id')->where('stock_status', 3)->whereIn('stock_customer_id', $customerChildren)->get()->toArray();
        return $model;
    }
    public static function customerChildren(){
        $CustomerChildren = self::getStockCustomerChildren();
        $model =  Customer::select('customers_parent_id');
        $model->whereIN('id',$CustomerChildren);
        $customers = $model->get()->toArray();
        return $customers;
    }
    /** danh sách khách hàng nhận quà */
    public static function getOdersGiftCustomer(){
        $staffs = self::getBrandUser();
        $model =  Customer::select('customers.*')
            ->with('brandCustomer')
            ->with('customerStaffs')
            ->with('customersCountry');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('customers.customers_staffs_id',$staffs['id']);
        }else{
            $model;
        }
        //khách giới thiệu thanh công
        $customerChildren =self::customerChildren();
        $customers = $model->whereIn('customers.id',$customerChildren)->orderBy('customers.customers_code', 'desc');
        return $customers;
    }
    //phiếu thu
    public static function getReceiptsTypeQuery(){
        $sql = Receipts::select("receipts_code")->where('receipts_status','>','0')->where('receipts_type','PHIEUTHUKHACHHANG')->orderBy('receipts_code', 'desc')->max('receipts_code');
        if($sql){
            $str = explode("PT",$sql);
            $sum = $str[1]+1;
            if($sum <10){
                $subString = 'PT0000'.$sum;
            }elseif ($sum >9 && $sum < 100){
                $subString = 'PT000'.$sum;
            }elseif ($sum >99 && $sum < 1000){
                $subString = 'PT00'.$sum;
            }elseif ($sum >999 && $sum < 10000){
                $subString = 'PT0'.$sum;
            }
        }else{
            $subString = 'PT00001';
        }
        return $subString;
    }
    /** danh sách phiếu thu sẽ theo*/
    public static function getReceiptsBrand(){
        $staffs = self::getBrandUser();
        $model = Receipts::select('receipts.*')
            ->with('receiptstock')
            ->with('receiptcustomer')
            ->with('receiptsbranch')
            ->with('receiptsbank')
            ->with('receiptstaff')
            ->with('receiptstaffaprove')
            ->with('receiptsCurrency');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->groupBy('receipts_code');
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $model->where('receipts.receipts_brand_id',$staffs['brands']['id'])->groupBy('receipts_code');
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('receipts.receipts_staff_id',$staffs['id'])->groupBy('receipts_code');
        }
        $stocks =  $model->where('receipts_status','>',0);
        return $stocks;
    }
    /** danh sách phiếu thu sẽ theo*/
    public static function getReceiptsCustomer($customerId){
        $staffs = self::getBrandUser();
        $model = Period::select('period.*')
            ->with('periodstock')
            ->with('periodcustomer')
            ->with('periodbranch')
            ->with('periodpayments')
            ->with('periodstaff')
            ->where('period_status',1)
            ->where('period_customers_id',$customerId)->get();
        return $model;
    }
    /** danh sách phiếu thu sẽ theo lãi xuất*/
    public static function getReceiptsAmountBatch($id,$period_id){
        $model = Period::select('period.*')
            ->with('periodstock')
            ->with('periodcustomer')
            ->with('periodbranch')
            ->with('periodpayments')
            ->with('periodstaff')
            ->where('period_status','=','1')
            ->where('period_stock_id',$period_id)
            ->where('id','!=',$id)->sum('period_amount_batch');
        return $model;
    }
    /** danh sách phiếu thu sẽ theo*/
    public static function getReceiptsPeriods($Id){
        $model = Period::select('period.*')
            ->with('periodstock')
            ->with('periodcustomer')
            ->with('periodbranch')
            ->with('periodpayments')
            ->with('periodstaff')
            ->where('period_status','>','0')
            ->where('id',$Id)->first();
        return $model;
    }
    /** danh sách khách hàng đang nợ thuộc nhân viên quản lý*/
    public static function getOdersPeroid(){
        $staffs = self::getBrandUser();
        $model =  Period::select('period.period_customers_id')->groupBy('period_customers_id')
            ->where('period.period_status','=','1');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('period.period_staff_id',$staffs['id']);
        }
        $customers = $model->get()->toArray();
        return $customers;

    }
    public static function getOdersCustomBrand($customers){
        $staffs = self::getBrandUser();
        $model =  Customer::select('customers.*')
            ->with('brandCustomer')
            ->with('customerStaffs')
            ->with('customersCountry')
            ->whereIn('id',$customers);
        $customers = $model->get();
        return $customers;

    }
    /** danh sách phiếu thu chờ duyệt */
    public static function getReceiptsApprovesIndex(){
        $staffs = self::getBrandUser();
        $model = Receipts::select('receipts.*')
            ->with('receiptstock')
            ->with('receiptcustomer')
            ->with('receiptsbranch')
            ->with('receiptsbank')
            ->with('receiptstaff')
            ->with('receiptstaffaprove')
            ->with('receiptsCurrency');
        /**  -----hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
            $model->groupBy('receipts_code');
       /** ---- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }
        $stocks =  $model->where('receipts_status','=',2);
        return $stocks;
    }
    /** admin sẽ tìm nhân viên */
    public static function getReceiptStatusStaff(){
       $query = Receipts::select('receipts_staff_id')->where('receipts_status','=','2')->get()->groupBy('receipts_staff_id');
        return $query;
    }
    /** admin sẽ tìm nhân viên */
    public static function getReceiptStatusCustomer(){
        $query = Receipts::select('receipts_customer_id')->where('receipts_status','=','2')->get()->groupBy('receipts_customer_id');
        return $query;
    }
    /** admin sẽ tìm nhân viên */
    public static function getReceiptsStaff(){
        $stafss = self::getReceiptStatusStaff();
        $query = Staff::select('*')->whereIn('id',$stafss)->get();
        return $query;
    }
    /** admin sẽ tìm khách hàng */
    public static function getReceiptCustomer(){
        $customer = self::getReceiptStatusCustomer();
        $query = Customer::select('*')->whereIn('id',$customer)->get();;
        return $query;
    }
    /** admin sẽ tìm khách hàng */
    public static function getReceiptBank(){
        $query = Bank::select('*')->get();
        return $query;
    }
    /** hiển thị danh sách khách hàng để nhân viên chuyển giao công việc */
    public static function getCustomersHandover(){
        $staffs = self::getBrandUser();
        $model = Customer::select('customers.*')->where('customers.customers_staffs_id',$staffs['id'])->get();
        return $model;
    }
    /** lấy ds nhân viên nhận bàn giao **/
    public static function getUser(){
        $id_user =\Illuminate\Support\Facades\Auth::user()->id;
        $staffs = self::getBrandUser();
        $model = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions');
        if ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('staff.staff_user_id','!=',$id_user)
                ->whereNotIn('staff.staff_decentralization_id',[2,4,5]);
        }else{
            $model->whereNull('staff.staff_user_id');
        }
        $models = $model->get();
        return $models;
    }
    public static function getCustomersHandoverBrand(){
        $staffs = self::getBrandUser();
        $model =  Customer::select('customers.*')
            ->with('brandCustomer')
            ->with('customerStaffs')
            ->with('customersCountry')->where('customers_handover_status','>=',1);
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['Users']['name'] == 'administrators' || $staffs['Users']['name'] == 'admin'){
           $model->whereIn('customers.customers_brand_id',[3,4]);
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('customers.customers_brand_id',$staffs['brands']['id']);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('customers.customers_staffs_id',$staffs['id'])->orwhere('customers_user_created','LIKE','%'.$staffs['staff_name'].'%');
        }
        $customers = $model;
        return $customers;
    }
    /** hiển thị danh sách khách hàng để nhân viên chuyển giao công việc */
    public static function getCustomersHandoverApprove($id){
        $model = Customer::select('customers.*')->where('customers.customers_staffs_id',$id)->get();
        return $model;
    }
    public static function getCustomersHandoverEdit($id){
        $model = Customer::select('customers.*')->where('customers.customers_staffs_id',$id)->first();
        return $model;
    }
    /** lấy ds nhân viên nhận bàn giao **/
    public static function getUserEdit(){
        $staffs = self::getBrandUser();
        $model = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions');
        if ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->whereNotIn('staff.staff_decentralization_id',[2,4,5]);
        }else{
            $model->whereNull('staff.staff_user_id');
        }
        $models = $model->get();
        return $models;
    }
    /** danh sách phiếu thu sẽ theo*/
    public static function getMonthlycashflowBrand($year,$month){
        $staffs = self::getBrandUser();
        $model = Period::select('period.*',DB::RAW("CONCAT(YEAR(created_at),'-',MONTH(created_at)) as month"),DB::RAW("MONTH(created_at) as month"),DB::RAW("YEAR(created_at) as year"))
            ->with('periodcustomer')
            ->with('periodpayments')
            ->with('periodbranch')
            ->with('periodReceipt')
            ->with('periodstaff');
        $model->where('period.period_brand_id',$staffs['brands']['id']);
        $models =  $model->where(DB::RAW("YEAR(period.created_at)"),$year)
            ->where(DB::RAW("MONTH(period.created_at)"),$month)->groupBy('period.period_customers_id')->get();
        return $models;
    }
    /** danh sách dòng tiền theo tháng */
    public static function getMonthlycash(){
        $staffs = self::getBrandUser();
        $model = Period::select(DB::RAW("MONTH(created_at) as month"),DB::RAW("YEAR(created_at) as year"),DB::RAW("CONCAT(YEAR(created_at),'-',MONTH(created_at)) as month_1"),DB::RAW("CONCAT(MONTH(created_at),'-',YEAR(created_at)) as month_2"));
        if($staffs['staff_brand_id'] > 0){
            $model->where('period.period_brand_id',$staffs['brands']['id']);
        }else{
            $model->whereNull('period.period_brand_id');
        }
        $models = $model;
        return $models;
    }
    /** liệt kê số tiền khách đang nợ hay đã trả */
    public static function getPeriodDate($period_customers_id,$year,$month){
        $model = Period::select('period.*',DB::RAW("MONTH(period_date) as month"))
            ->with('periodcustomer')
            ->with('periodstock')
            ->with('periodpayments')
            ->with('periodbranch')
            ->with('periodReceipt')
            ->with('periodstaff');
        $model->where(DB::RAW("YEAR(created_at)"),$year)
            ->where(DB::RAW("MONTH(created_at)"),$month)
            ->where('period.period_customers_id',$period_customers_id)->GroupBy('period.period_stock_id');
        $models = $model->get();
        return $models;
    }
    /** danh sách dòng tiền theo tháng (liệt kê số tiền khách đang nợ hay đã trả )*/
    public static function getPeriodGroupDate(){
        $staffs = self::getBrandUser();
        $model = Period::select('period.*',DB::RAW("MONTH(period_date) as month"),DB::RAW("YEAR(period_date) as year"))
            ->with('periodstock')
            ->with('periodcustomer')
            ->with('periodpayments')
            ->with('periodbranch')
            ->with('periodReceipt','periodReceipt.receiptsbank')
            ->with('periodstaff');
        if($staffs['staff_brand_id'] > 0) {
            $model->where('period.period_brand_id', $staffs['brands']['id']);
        }else{
            $model->whereNull('period.period_brand_id');
        }
        $models =  $model;
        return $models;
    }
    /** danh sách khách hàng nhận quà */
    public static function getOdersGiftAll(){
        $staffs = self::getBrandUser();
        $model =  Stock::select('stock.*');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'sales' || $staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch' || $staffs['decenTLizaTion']['positions']['position_code'] == 'stocker'){
            $model->where('stock.stock_brand_id',$staffs['brands']['id']);
        }else{
            $model->whereNull('stock.stock_brand_id');
        }
        $OdersGift = $model->where('stock.Type','like','%XUATKHOQUATANG%');
        return $OdersGift;
    }

    /** liệt kê số tiền khách đang nợ hay đã trả */
    public static function getPeriodOrdersDate($period_stock_id){
        $model = Period::select('period.*',DB::RAW("MONTH(period_date) as month"),DB::RAW("YEAR(period_date) as year"))
            ->with('periodcustomer')
            ->with('periodstock')
            ->with('periodpayments')
            ->with('periodbranch')
            ->with('periodReceipt')
            ->with('periodstaff');
        $model->where('period.period_stock_id',$period_stock_id);
        $models = $model->get();
        return $models;
    }
    /** danh sách nv sales theo từng cấp bậc=> chi nhánh trưởng */
    public static function getStaffRank(){
        $staffs = self::getBrandUser();
        $model = Staff::select('staff.*')
            ->with('ranks')
            ->with('brands');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        }else{
            $model->whereNull('staff.staff_brand_id');
        }
        $model->where('staff.staff_decentralization_id',3);
        $models = $model;
        return $models;
    }

    /** hiển thị danh sách nv sales được thanh toán kpi **/
    public static function getKPIPaymentStaffSales(){
        $staffs = self::getBrandUser();
        $model =  StaffKpiOrder::select('staff_kpi_order.*')
            ->with('kpiBrand')
            ->with('kpiStaffSales','kpiStaffSales.ranks');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('staff_kpi_order.staff_kpi_order_brand_id',[3,4])->where('staff_kpi_sales_status',1);
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff_kpi_order.staff_kpi_order_brand_id',$staffs['brands']['id'])->where('staff_kpi_sales_status',1);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }else{
            $model->whereNull('staff_kpi_order.staff_kpi_order_brand_id');
        }
        $data = $model;
        return $data;
    }
    // hiển thị ds nv sales tính kpi theo branch
    public static function getStaffRankKpi(){
        $staffs = self::getBrandUser();
        $model = Staff::select('staff.*')
            ->with('ranks')
            ->with('brands');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        }elseif($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('staff.staff_brand_id',[3,4]);
        }else{
            $model->whereNull('staff.staff_brand_id');
        }
        $model->where('staff.staff_decentralization_id',3);
        $models = $model->get();
        return $models;
    }
    // hiển thị ds nv sales tính kpi theo branch
    public static function getSalesKpiPayment(){
        $staffs = self::getBrandUser();
        //lọc lấy ds phiếu đã thanh toán với khách nhưng chưa được tính kpi với nv sales
        $model = Period::select('period_staff_id');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('period_brand_id',$staffs['brands']['id']);
        }elseif($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('period_staff_id',$staffs['id']);
        }else{
            $model->whereNull('period_brand_id');
        }
        //period_kpi_status=1 chưa thanh toán kpi
        $model->where('period.period_status',0)->whereNull('period.period_kpi_status');
        $models = $model->groupBy('period_staff_id')->get()->toArray();
        //chỉ lấy ds nv có kpi cần tính thôi
        $data = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions');
        if($model->get()->count() >0){
            $dataModels = $data->whereIn('staff.id',$models)->get();
        }else{
            $dataModels = $data->whereNull('staff.id')->get();
        }
        return $dataModels;
    }
    //lấy ds customer thuộc nv đang cvaanf thanh toán kpi
    public static function getcustomersPeriodKpi($staff_id){
        //lọc lấy ds khách nhưng chưa được tính kpi với nv sales
        $model = Period::select('period_customers_id');
        $model->where('period_staff_id',$staff_id)->where('period.period_status',0)->whereNull('period.period_kpi_status');
        $models = $model->groupBy('period_customers_id')->get()->toArray();
        $dataCustomer = Customer::select('customers.*','customers.id')->whereIn('customers.id',$models)->get();
        return $dataCustomer;
    }
    //gom ds đơn hàng thuộc customers
    public static function getCustomerPaymentKpi($id){
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->where('period_customers_id',$id)->where('period.period_status',0)->whereNull('period.period_kpi_status')->groupBy('period_stock_id')->get();
        return $modal;
    }
    //lấy ds customer thuộc nv đang cvaanf thanh toán kpi
    public static function getPeriodPaymentCustomersAll($period_customers_id,$period_stock_id){
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->where('period_customers_id',$period_customers_id)->where('period_stock_id',$period_stock_id)->where('period.period_status',0)->whereNull('period.period_kpi_status')->get();
        return $modal;
    }
    //lấy ds đươn hàng thuộc customer kpi sales
    public static function getListPeriodStockIdSales($val,$periodId){
        $modal =  Period::select('period_stock_id')
            ->where('period_customers_id',$val)->whereIn('id',$periodId)->groupBy('period_stock_id')->get();
        return $modal;
    }
    //lấy id ds phiếu thu thuộc customer kpi sales
    public static function getListPeriodIdSales($val,$periodId,$period_stock_id){
        $modal =  Period::select('id')
            ->where('period_customers_id',$val)->whereIn('id',$periodId)->where('period_stock_id',$period_stock_id)->get();
        return $modal;
    }
    public static function getStaffKpiOrderEdit($id){
        $customers = StaffKpiOrder::select('*')->where('id',$id)->first();
        return $customers;
    }
    public static function getStaffKpiCodeQuery(){
        $sql = StaffKpiOrder::select("staff_kpi_code")->where('staff_kpi_sales_status',1)->orderBy('staff_kpi_code', 'desc')->max('staff_kpi_code');
        return $sql;
    }
    public static function getAllCustomersOffSales($staff_kpi_sales_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_id',$staff_kpi_sales_id)->groupBy('staff_kpi_customers_id')->get();
        return $sql;
    }
    public static function getAllOrdersOffSales($staff_kpi_customers_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_customers_id',$staff_kpi_customers_id)->groupBy('staff_kpi_period_stock_id')->get();
        return $sql;
    }
    public static function getAllKPICodeOffSales($staff_kpi_period_stock_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_period_stock_id',$staff_kpi_period_stock_id)->get();
        return $sql;
    }
    //lấy ds customer thuộc nv đang nợ
    public static function getcustomersPeriod(){
        $staffs = self::getBrandUser();
        //lọc lấy ds khách nhưng chưa được tính kpi với nv sales
        $model = Period::select('period_customers_id','period_brand_id','period_staff_id')
            ->with('periodcustomer')
            ->with('periodbranch')
            ->with('periodstaff');
        if($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('period_brand_id',[3,4]);
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('period_brand_id',$staffs['brands']['id']);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('period_staff_id',$staffs['id']);
        }
        $models = $model;
        return $models;
    }
    //lấy ds customer thuộc nv đang nợ
    public static function getcustomersGroupPeriod(){
        $staffs = self::getBrandUser();
        //lọc lấy ds khách nhưng chưa được tính kpi với nv sales
        $model = Period::select('period_customers_id')
            ->with('periodcustomer')
            ->with('periodbranch')
            ->with('periodstaff');
        if($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('period_brand_id',[3,4]);
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('period_brand_id',$staffs['brands']['id']);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('period_staff_id',$staffs['id']);
        }
        $models = $model->groupBy('period_customers_id')->get();
        return $models;
    }
    //gom ds đơn hàng thuộc customers công nợ
    public static function getCustomerPayment($id){
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->where('period_customers_id',$id)->groupBy('period_stock_id')->get();
        return $modal;
    }
    //lấy ds customer thuộc nv đang cvaanf thanh toán kpi
    public static function getPeriodPayments($period_customers_id,$period_stock_id){
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->where('period_customers_id',$period_customers_id)->where('period_stock_id',$period_stock_id)->get();
        return $modal;
    }
    //lấy ds customer thuộc nv đang cvaanf thanh toán kpi
    public static function getcustomersPeriodKpiEdit($staff_id){
        //lọc lấy ds khách nhưng chưa được tính kpi với nv sales
        $model = Period::select('period_customers_id');
        $model->whereIn('id',$staff_id);
        $models = $model->groupBy('period_customers_id')->get()->toArray();
        $dataCustomer = Customer::select('customers.*','customers.id')->whereIn('customers.id',$models)->get();
        return $dataCustomer;
    }
    //gom ds đơn hàng thuộc customers
    public static function getCustomerPaymentKpiEdit($id){
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->whereIn('id',$id)->groupBy('period_stock_id')->get();
        return $modal;
    }
    //lấy ds customer thuộc nv đang cvaanf thanh toán kpi
    public static function getPeriodPaymentCustomersAllEdit($staff_kpi_order_period_id){
        $kpi_order_period_id =explode(',', $staff_kpi_order_period_id);
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->whereIn('id',$kpi_order_period_id)->get();
        return $modal;
    }
    // hiển thị ds nv sub sales tính kpi theo branch
    public static function getStaffSubSalesKpi(){
        $staffs = self::getBrandUser();
        $model = Staff::select('staff.*')
            ->with('ranks')
            ->with('brands');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        }elseif($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('staff.staff_brand_id',[3,4]);
        }else{
            $model->whereNull('staff.staff_brand_id');
        }
        $model->where('staff.staff_decentralization_id',3)->where('staff.staff_rank_id',3);
        $models = $model->get();
        return $models;
    }
    /** hiển thị danh sách nv sales được thanh toán kpi **/
    public static function getKPIPaymentStaffSubSales(){
        $staffs = self::getBrandUser();
        $model =  StaffKpiOrder::select('staff_kpi_order.*')
            ->with('kpiBrand')
            ->with('kpiStaffSales','kpiStaffSubSales','kpiStaffManageSales','kpiStaffSales.ranks');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('staff_kpi_order.staff_kpi_order_brand_id',[3,4]);
            /** ------------- hiển thị thông tin nhân viên danh cho chi nhánh trưởng **/
        }elseif ($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff_kpi_order.staff_kpi_order_brand_id',$staffs['brands']['id']);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }else{
            $model->whereNull('staff_kpi_order.staff_kpi_order_brand_id');
        }
        $data = $model;
        return $data;
    }
    public static function getsalesOffSubSales($staff_kpi_sales_sub_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSubSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_sub_id',$staff_kpi_sales_sub_id)->groupBy('staff_kpi_sales_id')->get();
        return $sql;
    }
    // hiển thị ds nv sales tính kpi theo branch
    public static function getSalesSubManagePayment(){
        $staffs = self::getBrandUser();
        //chỉ lấy ds nv có kpi cần tính thôi
        $data = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $data->where('staff_brand_id',$staffs['brands']['id']);
        }else{
            $data->whereNull('staff_brand_id');
        }
        $dataModels = $data->whereIn('staff_rank_id',[2,3])->get();
        return $dataModels;
    }
    // hiển thị ds nv sub sales tính kpi theo branch
    public static function getStaffSubSalesPayment($sales_kpi){
        $staffs = self::getBrandUser();
        $model = Staff::select('staff.*')
            ->with('ranks')
            ->with('brands');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        }elseif($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('staff.staff_brand_id',[3,4]);
        }elseif($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        }else{
            $model->whereNull('staff.staff_brand_id');
        }
        $model->where('staff.staff_decentralization_id',3)->where('staff_parent_id',$sales_kpi)->whereNull('staff_rank_id');
        $models = $model->get();
        return $models;
    }
    public static function getAllCustomersOffSalesEdit($staff_kpi_sales_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_id',$staff_kpi_sales_id)->groupBy('staff_kpi_customers_id')->get();
        return $sql;
    }
    public static function getAllOrdersOffSalesEdit($staff_kpi_customers_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_customers_id',$staff_kpi_customers_id)->groupBy('staff_kpi_period_stock_id')->get();
        return $sql;
    }
    public static function getAllKPICodeOffSalesEdit($staff_kpi_period_stock_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_period_stock_id',$staff_kpi_period_stock_id)->where('staff_kpi_sales_sub_status',1)->get();
        return $sql;
    }
    public static function getAllCustomersOffSubSalesEdit($staff_kpi_sales_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_id',$staff_kpi_sales_id)->whereNull('staff_kpi_sales_sub_status')->groupBy('staff_kpi_customers_id')->get();
        return $sql;
    }
    public static function getAllOrdersOffSubSales($staff_kpi_customers_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_customers_id',$staff_kpi_customers_id)->whereNull('staff_kpi_sales_sub_status')->groupBy('staff_kpi_period_stock_id')->get();
        return $sql;
    }
    public static function getAllKPICodeOffSubSales($staff_kpi_period_stock_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_period_stock_id',$staff_kpi_period_stock_id)->whereNull('staff_kpi_sales_sub_status')->get();
        return $sql;
    }
    public static function getsalesOffManageSales($staff_kpi_sales_sub_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSubSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_sub_id',$staff_kpi_sales_sub_id)->whereNull('staff_kpi_sales_manage_status')->groupBy('staff_kpi_sales_id')->get();
        return $sql;
    }
    public static function getAllCustomersOffSalesManage($staff_kpi_sales_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_id',$staff_kpi_sales_id)->whereNull('staff_kpi_sales_manage_status')->groupBy('staff_kpi_customers_id')->get();
        return $sql;
    }
    public static function getAllOrdersOffManageSales($staff_kpi_customers_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_customers_id',$staff_kpi_customers_id)->whereNull('staff_kpi_sales_manage_status')->groupBy('staff_kpi_period_stock_id')->get();
        return $sql;
    }
    public static function getAllKPICodeOffSalesManage($staff_kpi_period_stock_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_period_stock_id',$staff_kpi_period_stock_id)->whereNull('staff_kpi_sales_manage_status')->get();
        return $sql;
    }
    // hiển thị ds nv sales tính kpi theo branch
    public static function getSalesManageManagePayment(){
        $staffs = self::getBrandUser();
        //chỉ lấy ds nv có kpi cần tính thôi
        $data = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $data->where('staff_brand_id',$staffs['brands']['id']);
        }else{
            $data->whereNull('staff_brand_id');
        }
        $dataModels = $data->where('staff_rank_id',2)->get();
        return $dataModels;
    }
    public static function getsalesOffSubManageSales($staff_kpi_sales_manage_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSubSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_manage_id',$staff_kpi_sales_manage_id)->groupBy('staff_kpi_sales_sub_id')->get();
        return $sql;
    }
    // ds nv có chức vụ là kpi sales all
    // hiển thị ds nv sales tính kpi theo branch
    public static function getStaffsKPISalesAll($staff_user_id){
        $staffs = self::getBrandUser();
        //chỉ lấy ds nv có kpi cần tính thôi
        $data = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions')
            ->where('staff_decentralization_id',3);
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $data->where('id',$staff_user_id);
        }else{
            $data->whereNull('staff_brand_id');
        }
        $dataModels = $data->first();
        return $dataModels;
    }
    // hiển thị ds nv sales tính kpi theo branch
    public static function getAllCustomerStaffKPISales($staff_kpi_sales_id){
        $sql = StaffKpiOrder::select("*")
            ->with('kpiBrand')
            ->with('kpiCustomers')
            ->with('kpiStocks')
            ->with('kpiStaffSales','kpiStaffSales.ranks')
            ->where('staff_kpi_sales_id',$staff_kpi_sales_id)->groupBy('staff_kpi_customers_id')->get();
        return $sql;
    }
    public static function getAllCustomerStaffAll($array_custom){
        $sql = Customer::select("*")
            ->whereIN('id',$array_custom)->get();
        return $sql;
    }
    /** hiển thị danh sách nv sales được thanh toán kpi **/
    public static function getKPIStaffSales(){
        $staffs = self::getBrandUser();
        $model =  StaffKpiOrder::select('staff_kpi_order.*')
            ->with('kpiBrand')
            ->with('kpiStaffSales','kpiStaffSales.ranks');
        /**  ------------- hiển thị thông tin chỉ nhân viên all danh cho admin **/
        if ($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('staff_kpi_order.staff_kpi_sales_id',$staffs['staff_user_id']);
            /**  ------------- hiển thị thông tin chỉ nhân viên **/
        }else{
            $model->whereNull('staff_kpi_order.staff_kpi_order_brand_id');
        }
        $data = $model;
        return $data;
    }
    public static function getKPIStaffSubSales(){
        $staffs = self::getBrandUser();
        $model =  StaffKpiOrder::select('staff_kpi_order.*')
            ->with('kpiBrand')
            ->with('kpiStaffSales','kpiStaffSubSales','kpiStaffManageSales','kpiStaffSales.ranks');
        $data = $model;
        return $data;
    }
    public static function getSalesKpiPaymentid($id){
        //chỉ lấy ds nv có kpi cần tính thôi
        $data = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions');
        $dataModels = $data->where('staff.id',$id)->first();
        return $dataModels;
    }
    // hiển thị ds nv sub sales tính kpi theo branch
    public static function getStaffManaSalesPayment($sales_kpi){
        $staffs = self::getBrandUser();
        $model = Staff::select('staff.*')
            ->with('ranks')
            ->with('brands');
        if($staffs['decenTLizaTion']['positions']['position_code'] == 'headbranch'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        }elseif($staffs['users']['name'] == 'administrators' || $staffs['users']['name'] == 'admin'){
            $model->whereIn('staff.staff_brand_id',[3,4]);
        }elseif($staffs['decenTLizaTion']['positions']['position_code'] == 'sales'){
            $model->where('staff.staff_brand_id',$staffs['brands']['id']);
        }else{
            $model->whereNull('staff.staff_brand_id');
        }
        $model->where('staff.staff_decentralization_id',3)->where('staff_parent_id',$sales_kpi)->where('staff_rank_id',3);
        $models = $model->get();
        return $models;
    }

}
