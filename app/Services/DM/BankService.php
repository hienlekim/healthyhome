<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Bank;
class BankService
{
    public static function getModelById($id){
        return Bank::find($id);
    }
    static protected function getQuery()
    {
        return Bank::select("bank.*");
    }
    public static function getByCode($code)
    {
        return Bank::where('bank_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Bank::where('id','!=',$id)->where('bank_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('bank_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("bank_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('bank_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Bank();
        $model->create($data);
        return $model;
    }
    public static function RemoveModel($data)
    {
        $model=new Bank();
        return $model->create($data);
    }
}
