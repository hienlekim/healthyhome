<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\ProductType;
class ProductTypeService
{
    public static function getModelById($id){
        return ProductType::find($id);
    }
    public static function getQuery(){
        return ProductType::select('product_type.*');
    }
    public static function getByCode($code)
    {
        return ProductType::where('product_type_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $models=self::getQuery();
        if($param['searchInput']){
            $models->where(function ($query) use ($param){
                $query->where("product_type_code", "LIKE", "%". $param['searchInput']. "%")
                    ->orWhere('product_type_name', "%". $param['searchInput']. "%");
            });
        }
        $models=$models->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new ProductType();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new ProductType();
        return $model->create($data);
    }
}
