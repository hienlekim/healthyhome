<?php

namespace App\Services\DM;
use Carbon\Carbon;
use App\Models\Payments;
class PaymentsService
{
    public static function getModelById($id){
        return Payments::find($id);
    }
    public static function getQuery(){
        return Payments::select('payments.*');
    }
    public static function getByCode($code)
    {
        return Payments::where('payments_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Payments::where('id','!=',$id)->where('payments_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('payments_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("payments_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('payments_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Payments();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Payments();
        return $model->create($data);
    }
}
