<?php


namespace App\Services\Permission;
use App\Models\Permission;
use \DB;

class PermissionService
{
	public static function getModelByParamater($param, $limit=100) {
		$models = Permission::select('*')->where('guard_name', "eclaim");;

		if($param['searchInput']){
			$models->where(function($query) use ($param){
				$query->where('id', '=', $param['searchInput'])
							->orWhere('name', 'LIKE', '%' . $param['searchInput'] . '%');
			});
		}

		$models->orderBy($param["sortBy"], $param['orderBy']);
		$models = $models->paginate($limit);

		return $models;
	}

	public static function updateModel($id, $data)
	{
		$model = self::getModelById($id);

		return $model->update($data);
	}

	public static function getModelById($id)
	{
		return Permission::find($id);
	}
}
