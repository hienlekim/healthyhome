<?php

namespace App\Services;

use Carbon\Carbon;

class AbstractService
{
	public static function ResultSuccess($data)
	{
		return response()->json(
            ['message' => 'successfull',
			'errors'=>false,
			'result'=>"Success",
                'data'=>$data,
            ], 200);
	}
    public static function DataSuccess($data)
    {
        return response()->json(
            [
                'errors'=>false,
                'result'=>"Success",
                'data'=>$data,
            ], 200);
    }
	public static function ResultError($message)
	{
		return response()->json(
            ['message' => $message,
			'errors'=>true,
			'result'=>'ERROR',
//            'data'=>$data,
            ], 500);
	}
    public static function ResultErrors($message)
    {
        return response()->json(
            ['message' => $message,
                'errors'=>true,
                'result'=>'ERROR',
//            'data'=>$data,
            ]);
    }
	public static function Result($status,$message)
	{

		return response()->json([
            "Status" => $status,
            "Message" => [
                "PartnerCode" => $message[0],
                "RequestID" => $message[1],
                "Description" => $message[2],
                "Amount" => $message[3]
            ]
        ], $status ==0 ? 200 : 500);
	}

	public static function ResultMobile($status, $message)
	{
		return response()->json([
            "Status" => $status,
            "Message" => [
                "PartnerCode" => $message[0],
                "RequestID" => $message[1],
                "Description" => $message[2],
								"IMEI" => $message[3],
								"OrderId" => $message[4]
            ]
        ], $status ==0 ? 200 : 500);
	}
	public static function ResultData($models)
	{
		return $models->withPath('');
	}
	public static function getEmail()
	{
		return 'no-reply@protecttrip.vn';
	}
	public static function getFrontPage()
	{
		return 'http://dev.quanly.protecttrip.vn';
	}
	public static function getTokenKey()
	{
		return md5("protecttrip247");
	}

}
