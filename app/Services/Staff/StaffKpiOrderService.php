<?php

namespace App\Services\Staff;
use Carbon\Carbon;
use App\Models\StaffKpiOrder;
class StaffKpiOrderService
{
    function getModelById($id){
        return StaffKpiOrder::find($id);
    }
    function getQuery(){
        return StaffKpiOrder::all();
    }
    public static function getModelByParamater($param,$limit)
    {
        $models=self::getQuery();
        $user = EmployeeService::getUserCurrent();
        if($param['searchInput']){
            $models->where(function ($query) use ($param){
                $query->where("clm_claimant", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('clm_fnol', '=', $param['searchInput']);
            });
        }
        $models=$models->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new StaffKpiOrder();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new StaffKpiOrder();
        return $model->create($data);
    }
}
