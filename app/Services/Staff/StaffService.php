<?php

namespace App\Services\Staff;
use Carbon\Carbon;
use App\Models\Staff;
use App\Models\User;
use App\Services\DM\GeneralService;
use Illuminate\Database\Eloquent\Model;

class StaffService
{
    public static function getModelById($id){
        return Staff::find($id);
    }
    public static function getModelUserId($id){
        return User::find($id);
    }
    public static function getQuery(){
        $staffs = GeneralService::getBrandUserStaff();
        return $staffs;
    }
    public static function getByCode($code)
    {
        return Staff::where('staff_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getById($code)
    {
        return Staff::where('staff_name', "LIKE", "%". $code. "%")->first();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('staff_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("staff_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('staff_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Staff();
        return $model->create($data);
    }
    public static function createImagesModel($data)
    {
        $model=new Staff();
        $models = $model->create($data);
        return $models;
    }
    public static function updateImagesModel($id,$data)
    {
        $model=self::getModelById($id);
        $models =$model->update($data);
        return $models;
    }
    public static function RemoveUserModel($id)
    {
        $modelUser=self::getModelUserId($id);
        $models = $modelUser->delete();
        return $models;
    }
    public static function RemoveStaffModel($id)
    {
        $modelStaff=self::getModelById($id);
        $models = $modelStaff->delete();
        return $models;
    }
    public static function createUserModel($data)
    {
        $model=new User();
        $models = $model->create($data);
        return $models;
    }
    public static function updateUserModel($id,$data)
    {
        $model=self::getModelUserId($id);
        return $model->update($data);
    }
    public static function getByCodeUser($email,$name)
    {
        return User::where('name', "LIKE", "%". $name. "%")->where('email', "LIKE", "%". $email. "%")->get();
    }
    public static function getByCodeUserUpdate($id,$email,$name)
    {
        return User::where('id','!=',$id)->where('name', "LIKE", "%". $name. "%")->where('email', "LIKE", "%". $email. "%")->get();
    }
}
