<?php

namespace App\Services\Stock;
use App\Models\Period;
use App\Models\Stock;
use Carbon\Carbon;
use App\Models\Customer;
use App\Services\DM\GeneralService;
use App\Models\CustomerCare;
use App\Models\Receipts;
use Illuminate\Support\Facades\DB;
class CustomerService
{
    public static function getModelById($id){
        return GeneralService::getCustomersBrandEdit($id);
    }
    public static function getByCode($code){
        return Customer::where('customers_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getCustomerParent(){
        $staff = GeneralService::getBrandUser();
//        whereNull('customers_parent_id')
        return Customer::where('customers_staffs_id',$staff->id)->get();
    }
    public static function getCustomerParentEdit($id){
        return Customer::where('id',$id)->first();
    }
    public static function getStaffCustomerBrand(){
        return GeneralService::getBrandUser();
    }
    public static function getQuery(){
        return GeneralService::getCustomersBrand();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('customers_code', $param["orderBy"]);
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['SreachName'] || $param['SreachName'] !=""){
            $model->where('customers_name', 'LIKE','%'.$param['SreachName'].'%');
        }
        if ($param['searchBranch']){
            $model->where('customers_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }else{
            $models = $model->paginate($limit);
        }
        return $models;
    }
    public static function getCustomerChildrent($id)
    {
        $model=self::getQuery();
        $models = $model->where('customers_parent_id',$id)->orderBy('customers_code', 'asc')->get();
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Customer();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Customer();
        return $model->create($data);
    }
    /**---------customerHandoverStaff------------*/
    public static function getCustomerHandoverQuery()
    {
        return GeneralService::getCustomersHandoverBrand();
    }
    public static function getModelCustomerHandoverByParamater($param,$limit)
    {
        $model=self::getCustomerHandoverQuery();
        $model->groupBy('customers_staffs_id');
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(customers_handover_date_start)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(customers_handover_date_start)"), '<=', $param['toDate']);

        if ($param['searchBranch']){
            $model->where('customers_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }else{
            $models = $model->paginate($limit);
        }
        return $models;
    }
    /** hiển thị danh sách khách hàng để nhân viên chuyển giao công việc */
    public static function getCustomersHandoverList(){
        return GeneralService::getCustomersHandover();
    }
    /** hiển thị danh sách nhân viên tham gia nhận cv bàn giao */
    public static function getStaff(){
        return GeneralService::getUser();
    }
    public static function getCustomerHandoverEdit($id){
        return Customer::where('customers_staffs_id',$id)->get();
    }
    public static function getCustomerHandEdit($id){
        return Customer::where('customers_staffs_id',$id)->first();
    }
    public static function getPeriodHandoverEdit($id){
        return Period::where('period_staff_id',$id)->get();
    }
    public static function getModelHandoverById($id){
        return Customer::getCustomersBrandEdit($id);
    }
    public static function getStockHandoverEdit($id){
        return Stock::where('stock_staff_id',$id)->get();
    }
    public static function getReciptskHandoverEdit($id){
        return Receipts::where('receipts_staff_id',$id)->get();
    }
    public static function getCustomersCareHandoverEdit($id){
        return CustomerCare::where('customer_staff_id',$id)->where('customer_care_status','!=',2)->get();
    }
    public static function getByParamaterMeet($param)
    {
        $model=self::getQuery()->orderBy('created_at', 'DESC');;
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);
        $models = $model->groupBy('customers_staffs_id')->get();
        return $models;
    }
    public static function getByCountCustomers($param,$id)
    {
        $model=self::getQuery();
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);
        $models = $model->where('customers_staffs_id',$id)->count('id');
        return $models;
    }
    public static function getByCustomersofStaff($param,$id)
    {
        $model=self::getQuery()->orderBy('created_at', 'DESC');;
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);
        $models = $model->where('customers_staffs_id',$id)->get();
        return $models;
    }
}
