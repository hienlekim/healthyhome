<?php

namespace App\Services\Stock;
use Carbon\Carbon;
use App\Models\Supplier;
class SupplierService
{
    public static function getModelById($id){
        return Supplier::find($id);
    }
    public static function getQuery(){
        return Supplier::select("supplier.*")->with('counupplier');
    }
    public static function getByCode($code)
    {
        return Supplier::where('supplier_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getByCodeEdit($code,$id)
    {
        return Supplier::where('supplier_id','!=',$id)->where('supplier_code', "LIKE", "%". $code. "%")
            ->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery();
        $model->orderBy('supplier_code', $param["orderBy"]);
        if($param['searchInput']){
            $model->where(function ($model) use ($param){
                $model->where("supplier_code", "LIKE", "". $param['searchInput']. "%")
                    ->orWhere('supplier_name', '=', $param['searchInput']);
            });
        }
        $models = $model->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Supplier();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Supplier();
        return $model->create($data);
    }
}
