<?php

namespace App\Services\Stock;
use Carbon\Carbon;
use App\Models\CustomerCare;
use App\Services\DM\GeneralService;
use Illuminate\Support\Facades\DB;
class CustomerCareService
{
    public static function getModelById($id){
        return CustomerCare::find($id);
    }
    /** code đếm 3 tháng chắm sóc khách hàng khi khách hàng mới  */
    public static function countTimeCustomer($date){
//        $date = date('Y-m-d');
        $newDate = strtotime('+3 month', strtotime(($date)));
        $newdate = date('Y-m-j',$newDate);
        return $newdate;
    }
    public static function getbranchStaff(){
        $modal = GeneralService::getBrandStaff();
        return $modal;
    }
    public static function getbranchCustomers(){
        $modal = GeneralService::getBrandCustomers();
        return $modal;
    }
    public static function getQuery(){
        $modal = GeneralService::getCustomerCare();
        return $modal;
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery()->orderBy('created_at', 'DESC');;
//        if($param['fromDate'])
//            $models->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);
//
        if($param['toDate'])
            $model->where(DB::RAW("DATE(customer_care_date_end)"), '<=', $param['toDate']);
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(customer_care_date_end)"), '>=', $param['fromDate']);

        if($param['fromDate'] || $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }else{
            $models = $model->paginate($limit);
        }
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new CustomerCare();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new CustomerCare();
        return $model->create($data);
    }
    //danh sách khách hàng nhận quà
    public static function getQueryCustomerGift(){
        $modal = GeneralService::getOdersGiftCustomer();
        return $modal;
    }
    public static function getModelCustomerGiftByParamater($param,$limit)
    {
        $model=self::getQueryCustomerGift();
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchBranch']){
            $model->where('customers_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }else{
            $models = $model->paginate($limit);
        }
        return $models;
    }
    //chi tiết đơn hàng mà khách nhận quà
    public static function getQueryOdersGift(){
        $modal = GeneralService::getOdersGiftAll()->get();
        return $modal;
    }

}
