<?php

namespace App\Services\Stock;
use App\Models\Bank;
use Carbon\Carbon;
use App\Models\Receipts;
use App\Services\DM\GeneralService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class ReceiptsService
{
    public static function  getModelById($id){
        return Receipts::with('receiptstock')
            ->with('receiptsbranch')
            ->with('receiptsbank')
            ->with('receiptstaff')
            ->with('receiptstaffaprove')
            ->with('receiptsperiod')
            ->with('receiptspayment')
            ->with('receiptsCurrency')
            ->find($id);
    }
    public static function  getCustomersReceipt($customerId){
        $modal = GeneralService::getReceiptsCustomer($customerId)->Min('id');
        $Periods = GeneralService::getReceiptsPeriods($modal);
        return $Periods;
    }
    //lấy giá trị còn lại để tính lãi xuất
    public static function  getAmountBatchReceipt($id,$period_id){
        $modal = GeneralService::getReceiptsAmountBatch($id,$period_id);
        return $modal;
    }

    public static function getByCode($code){
        return Receipts::where('receipts_code', "LIKE", "%". $code. "%")->where('receipts_status','>','0')->get();
    }
    public static function getQuery(){
        $modal = GeneralService::getReceiptsBrand();
        return $modal;
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery()->orderBy('created_at', 'DESC');;
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchBranch']){
            $model->where('receipts_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }else{
            $models=$model->paginate($limit);
        }
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        $models = $model->update($data);
        return $models;
    }
    public static function createModel($data)
    {
        $model=new Receipts();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Receipts();
        return $model->create($data);
    }
    public static function getQuerypprovesIndex(){
        $modal = GeneralService::getReceiptsApprovesIndex();
        return $modal;
    }
    public static function getQueryStaff(){
        $modal = GeneralService::getReceiptsStaff();
        return $modal;
    }
    public static function getQueryBank(){
        $modal = GeneralService::getReceiptBank();
        return $modal;
    }
    public static function getQueryCustomer(){
        $modal = GeneralService::getReceiptCustomer();
        return $modal;
    }
    public static function getModelByApprovesIndex($param,$limit)
    {
        $models=self::getQuerypprovesIndex();
        if($param['searchInput'])
            $models->where('actual_amount', str_replace(".","",$param['searchInput']));

        if($param['searchReceiptCode'])
            $models->where('receipts_code','LIKE', '%'.$param['searchReceiptCode'].'%');

        if($param['searchBank'])
            $models->where('receipts_bank_id',$param['searchBank']);

        if ($param['searchBranch']){
            $models->where('receipts_brand_id', $param['searchBranch']);
        }
        if ($param['searchCustomer']){
            $models->where('receipts_customer_id', $param['searchCustomer']);
        }
        if ($param['searchStaff']){
            $models->where('receipts_staff_id', $param['searchStaff']);
        }
        $models=$models->paginate($limit);
        $modal = $models;
        return $modal;
    }
    public static function getModelListParamater($param)
    {
        $models=self::getQuery();
        if($param['fromDate'])
            $models->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $models->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        $models=$models->where('receipts_status','>=',1)->get();
        return $models;
    }
}
