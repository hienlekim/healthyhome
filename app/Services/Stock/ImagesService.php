<?php

namespace App\Services\Stock;
use Carbon\Carbon;
use App\Models\Images;
use function Complex\sech;

class ImagesService
{
    public static function getQuery(){
        return Images::select('images.*');
    }
    public static function getModelById($id){
        $images = Images::where('product_images_id',$id)->get();
        return $images;
    }
    public static function getModelByImages($imges){
        $imgs = self::getQuery()->where('images', "LIKE", "%". $imges. "%")->first();
        return $imgs;
    }

    public static function getModelBy($id){
        return Images::find($id);
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelBy($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=self::getQuery();
        return $model->create($data);
    }
    public static function RemoveModel($id)
    {
        return Images::where('id',$id)->delete();
    }
}
