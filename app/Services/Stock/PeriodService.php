<?php

namespace App\Services\Stock;
use Carbon\Carbon;
use App\Models\Period;
class PeriodService
{
    public static function  getModelBycancel($id){
        return Period::find($id);
    }
    public static function getModelById($id){
        return Period::where('period_stock_id',$id)->get();
    }
    //ds phiếu thanh toán gom theo đơn hàng, 1 khách hàng có thể có 1 đơn or nhiều đơn..............
    public static function getModelByCustomer($id){
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->where('period_customers_id',$id)->groupBy('period_stock_id')->get();
        return $modal;
    }
    //lấy ds phiếu thanh toán theo đơn hàng
    public static function getByCustomerPeriodPayment($id){
        $modal =  Period::select('period.*')
            ->with('periodstock')
            ->with('periodstaff')
            ->with('periodpayments')
            ->where('period_customers_id',$id)
            ->get();
        return $modal;
    }
    public static function getModePaymentId($id){
        return Period::where('period_stock_id',$id)->first();
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelBycancel($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Period();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Period();
        return $model->create($data);
    }
}
