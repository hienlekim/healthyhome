<?php

namespace App\Services\Stock;
use App\Services\DM\GeneralService;
use Carbon\Carbon;
use App\Models\Stock;
use Illuminate\Support\Facades\DB;
class purchasesService
{
    public static function  getModelById($id){
        return Stock::with('brands')->with('staffStock')->find($id);
    }
    public static function  getModelByProduct($code){
        $model = Stock::where('stock_code', $code)->get();
        return $model;
    }
    public static function getQuery(){
        $model = GeneralService::getpurchasesBrand();
        return $model;
    }
    public static function getQueryProduct(){
        $stock_product = GeneralService::getpurchaseshange();
        return $stock_product;
    }
    public static function getlistInventoriesQuery(){
        $product = self::getQueryProduct();
        return $product;
    }
    public static function getByCode($code){
        return Stock::where('stock_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getModelByParamater($param,$limit)
    {
//
        $model=self::getQuery()->orderBy('created_at', 'DESC');;
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchBranch']){
            $model->where('stock_brand_id', $param['searchBranch']);
        }

        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }else{
            $models=$model->paginate($limit);
        }
        return $models;
    }
    public static function getlistInventoriesParamater($param,$limit)
    {
//
        $models=self::getlistInventoriesQuery();
//        if($param['fromDate'])
//            $models->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);
//
//        if($param['toDate'])
//            $models->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchBranch']){
            $models->where('stock_brand_id', $param['searchBranch']);
        }
        $models=$models->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        $mdal = $model->update($data);
        return $mdal;
    }
    public static function createModel($data)
    {
        $model=new Stock();
        $mdal = $model->create($data);
        return $mdal;
    }
    public static function RemoveModel($data)
    {
        $model=new Stock();
        return $model->create($data);
    }
}
