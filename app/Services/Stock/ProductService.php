<?php

namespace App\Services\Stock;
use Carbon\Carbon;
use App\Models\Product;
class ProductService
{
    public static function getModelById($id){
        return Product::find($id);
    }
    public static function getQuery(){
        return Product::select('product.*');
    }
    public static function getByCode($code)
    {
        return Product::where('product_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getModelByParamater($param,$limit)
    {
        $models=self::getQuery();
        if($param['searchInput']){
            $models->where(function ($query) use ($param){
                $query->where("product_code", "LIKE", "%". $param['searchInput']. "%")
                    ->orWhere('product_name', "LIKE", "%". $param['searchInput']. "%");
            });
        }
        $models=$models->paginate($limit);
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Product();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Product();
        return $model->create($data);
    }
}
