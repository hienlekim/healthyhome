<?php

namespace App\Services\Stock;
use App\Models\Period;
use Carbon\Carbon;
use App\Models\Customer;
use App\Services\DM\GeneralService;
use App\Models\Receipts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class monthlycashflowService
{
    public static function getModelByParamater($year,$month)
    {
        $modal = GeneralService::getMonthlycashflowBrand($year,$month);
        return $modal;
    }
    public static function getMonthQuery(){
        $modal = GeneralService::getMonthlycash();
        return $modal;
    }
    public static function getMonthParams($param){
        $models=self::getMonthQuery()->orderBy('created_at', 'DESC');;
        if($param['fromDate'])
            $models->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $models->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        $models=$models->groupby(DB::RAW("YEAR(created_at), MONTH(created_at)"))->get();
        return $models;
    }
    public static function getMonthPeriodQuery(){
        $modal = GeneralService::getPeriodGroupDate();
        return $modal;
    }
    public static function getMonthPeriodDateParams($param){
        $models=self::getMonthPeriodQuery();
        if($param['fromDate'])
            $models->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $models->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        $models=$models->groupby(DB::RAW("YEAR(period_date), MONTH(period_date)"))->get();
        return $models;
    }
    public static function getPeriodDateQuery($period_customers_id,$year,$month){
        $modal = GeneralService::getPeriodDate($period_customers_id,$year,$month);
        return $modal;
    }
    public static function getCustomersOrdersQuery($period_stock_id){
        $modal = GeneralService::getPeriodOrdersDate($period_stock_id);
        return $modal;
    }
}
