<?php

namespace App\Services\Stock;
use App\Models\Staff;
use App\Models\Period;
use App\Models\Customer;
use App\Services\DM\GeneralService;
use App\Models\StaffKpiOrder;
use Illuminate\Support\Facades\DB;
class manageSalesKPIService
{
    public static function getModelById($id){
        return GeneralService::getStaffKpiOrderEdit($id);
    }
    public static function getQuery(){
        return GeneralService::getKPIPaymentStaffSubSales();
    }
    public static function getModelByParamater($param)
    {
        $model=self::getQuery();
        $model->where('staff_kpi_sales_manage_status',1)->orderBy('created_at', 'DESC');
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchInput']){
            $model->where('staff_kpi_sales_manage_id',$param['searchInput']);
        }
        if ($param['searchBranch']){
            $model->where('staff_kpi_order_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->groupBy('staff_kpi_sales_manage_id')->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->groupBy('staff_kpi_sales_manage_id')->paginate($model->get()->count());
        }else{
            $models = $model->groupBy('staff_kpi_sales_manage_id')->paginate(10);
        }
        return $models;
    }
    public static function getListPeriodGrouByStockId($val,$periodId){
        $model =  GeneralService::getListPeriodStockIdSales($val,$periodId);
        return $model;
    }
    public static function getListPeriodStockId($val,$periodId,$period_stock_id){
        $model =  GeneralService::getListPeriodIdSales($val,$periodId,$period_stock_id);
        return $model;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function removeModel($id)
    {
        return self::getModelById($id)->delete();
    }
    public static function createModel($data)
    {
        $model=new StaffKpiOrder();
        return $model->create($data);
    }
    public static function updatePeriodStockKPIModel($id)
    {
        return Period::whereIn("id", $id)->update(["period_kpi_status" => 1]);
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerOffStaffSales($staff_kpi_sales_id){
        $model =  GeneralService::getAllCustomersOffSales($staff_kpi_sales_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListOrdersOffStaffSales($staff_kpi_customers_id){
        $model =  GeneralService::getAllOrdersOffSales($staff_kpi_customers_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListKPICodeOffStaffSales($staff_kpi_period_stock_id){
        $model =  GeneralService::getAllKPICodeOffSales($staff_kpi_period_stock_id);
        return $model;
    }
    /** ds custonmer đang nợ */
    public static function getPeriodQuery(){
        return GeneralService::getcustomersPeriod();
    }
    public static function getModelPeriodByParamater($param)
    {
        $model=self::getPeriodQuery();
        $model->orderBy('period_code', $param["orderBy"]);
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchInput']){
            $model->where('period_customers_id',$param['searchInput']);
        }
        if ($param['searchBranch']){
            $model->where('period_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->groupBy('period_customers_id')->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->groupBy('period_customers_id')->paginate($model->get()->count());
        }else{
            $models = $model->groupBy('period_customers_id')->paginate(10);
        }
        return $models;
    }
    /** lấy ds customer group */
    public static function getListGroupCustomers(){
        $model =  GeneralService::getcustomersGroupPeriod();
        return $model;
    }
    public static function updatePeriodStockKPIModelEdit($id)
    {
        return Period::whereIn("id", $id)->update(["period_kpi_status" =>null]);
    }
    /** lấy ds sales thuộc nv cấp sub sales  */
    public static function getListsalesOffSubSales($staff_kpi_sales_sub_id){
        $model =  GeneralService::getsalesOffSubSales($staff_kpi_sales_sub_id);
        return $model;
    }
    public static function getQuerySub(){
        return GeneralService::getKPIPaymentStaffSubSales();
    }
    public static function getModelBySub($customersPeriodKpi)
    {
        $model=self::getQuerySub();
        $models = $model->whereIn('staff_kpi_sales_id',$customersPeriodKpi)->whereNull('staff_kpi_sales_sub_status')->groupBy('staff_kpi_sales_id')->get();
        return $models;
    }
    public static function getModelByEdit($customersPeriodKpi)
    {
        $model=self::getQuerySub();
        $models = $model->whereIn('staff_kpi_sales_id',$customersPeriodKpi)->where('staff_kpi_sales_sub_status',1)->groupBy('staff_kpi_sales_id')->get();
        return $models;
    }
    public static function getModelByManages($customersPeriodKpi)
    {
        $model=self::getQuerySub();
        $models = $model->whereIn('staff_kpi_sales_sub_id',$customersPeriodKpi)->whereNull('staff_kpi_sales_manage_status')->groupBy('staff_kpi_sales_sub_id')->get();
        return $models;
    }
    /** lấy ds sales thuộc nv cấp sub sales  */
    public static function getListsalesOffManageSales($staff_kpi_sales_sub_id){
        $model =  GeneralService::getsalesOffSubSales($staff_kpi_sales_sub_id);
        return $model;
    }
    public static function getModelByManagesEdit($customersPeriodKpi)
    {
        $model=self::getQuerySub();
        $models = $model->whereIn('staff_kpi_sales_sub_id',$customersPeriodKpi)->where('staff_kpi_sales_manage_status',1)->groupBy('staff_kpi_sales_sub_id')->get();
        return $models;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerOffManageSales($staff_kpi_sales_id){
        $model =  GeneralService::getsalesOffManageSales($staff_kpi_sales_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerManageSales($staff_kpi_sales_id){
        $model =  GeneralService::getAllCustomersOffSalesManage($staff_kpi_sales_id);
        return $model;
    }
    public static function getListOrdersOffManageSales($staff_kpi_customers_id){
        $model =  GeneralService::getAllOrdersOffManageSales($staff_kpi_customers_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListKPICodeOffManageSales($staff_kpi_period_stock_id){
        $model =  GeneralService::getAllKPICodeOffSalesManage($staff_kpi_period_stock_id);
        return $model;
    }
    /** lấy ds sales thuộc nv cấp sub sales  */
    public static function getListsalesOffsubManagesSales($staff_kpi_sales_manage_id){
        $model =  GeneralService::getsalesOffSubManageSales($staff_kpi_sales_manage_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListKPICodeOffStaffSalesMange($staff_kpi_period_stock_id){
        $model =  GeneralService::getAllKPICodeOffSales($staff_kpi_period_stock_id);
        return $model;
    }

    /** ds nv sub đã được manage nhận kpi */
    public static function getModelByManagesAssign($customersPeriodKpi)
    {
        $model=self::getQuerySub();
        $models = $model->whereIn('staff_kpi_sales_sub_id',$customersPeriodKpi)->groupBy('staff_kpi_sales_sub_id')->get();
        return $models;
    }
    public static function getModelByEditManages($id)
    {
        $model=self::getQuerySub();
        $models = $model->where('staff_kpi_sales_manage_id',$id)->where('staff_kpi_sales_manage_status',1)->get();
        return $models;
    }

}
