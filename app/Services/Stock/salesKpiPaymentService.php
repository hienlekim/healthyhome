<?php

namespace App\Services\Stock;
use App\Models\Staff;
use App\Models\Period;
use App\Models\Customer;
use App\Services\DM\GeneralService;
use App\Models\StaffKpiOrder;
use Illuminate\Support\Facades\DB;
class salesKpiPaymentService
{
    public static function getModelById($id){
        return GeneralService::getStaffKpiOrderEdit($id);
    }
    public static function getQuery(){
        return GeneralService::getKPIPaymentStaffSales();
    }
    public static function getModelByParamater($param)
    {
        $model=self::getQuery();
        $model->orderBy('staff_kpi_code', $param["orderBy"]);
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchInput']){
            $model->where('staff_kpi_sales_id',$param['searchInput']);
        }
        if ($param['searchBranch']){
            $model->where('staff_kpi_order_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->groupBy('staff_kpi_sales_id')->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->groupBy('staff_kpi_sales_id')->paginate($model->get()->count());
        }else{
            $models = $model->groupBy('staff_kpi_sales_id')->paginate(10);
        }
        return $models;
    }
    public static function getListPeriodGrouByStockId($val,$periodId){
        $model =  GeneralService::getListPeriodStockIdSales($val,$periodId);
        return $model;
    }
    public static function getListPeriodStockId($val,$periodId,$period_stock_id){
        $model =  GeneralService::getListPeriodIdSales($val,$periodId,$period_stock_id);
        return $model;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function removeModel($id)
    {
        return self::getModelById($id)->delete();
    }
    public static function createModel($data)
    {
        $model=new StaffKpiOrder();
        return $model->create($data);
    }
    public static function updatePeriodStockKPIModel($id)
    {
        return Period::whereIn("id", $id)->update(["period_kpi_status" => 1]);
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerOffStaffSales($staff_kpi_sales_id){
        $model =  GeneralService::getAllCustomersOffSales($staff_kpi_sales_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListOrdersOffStaffSales($staff_kpi_customers_id){
        $model =  GeneralService::getAllOrdersOffSales($staff_kpi_customers_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListKPICodeOffStaffSales($staff_kpi_period_stock_id){
        $model =  GeneralService::getAllKPICodeOffSales($staff_kpi_period_stock_id);
        return $model;
    }
    /** ds custonmer đang nợ */
    public static function getPeriodQuery(){
        return GeneralService::getcustomersPeriod();
    }
    public static function getModelPeriodByParamater($param)
    {
        $model=self::getPeriodQuery()->orderBy('created_at', 'DESC');;
        $model->orderBy('period_code', $param["orderBy"]);
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchInput']){
            $model->where('period_customers_id',$param['searchInput']);
        }
        if ($param['searchBranch']){
            $model->where('period_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->groupBy('period_customers_id')->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->groupBy('period_customers_id')->paginate($model->get()->count());
        }else{
            $models = $model->groupBy('period_customers_id')->paginate(10);
        }
        return $models;
    }
    /** lấy ds customer group */
    public static function getListGroupCustomers(){
        $model =  GeneralService::getcustomersGroupPeriod();
        return $model;
    }
    public static function updatePeriodStockKPIModelEdit($id)
    {
        return Period::whereIn("id", $id)->update(["period_kpi_status" =>null]);
    }
    public static function updatePeriodStockKPIModelSubSales($id,$data)
    {
        return StaffKpiOrder::whereIn("id", $id)->update($data);
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerOffStaffSalesEdit($staff_kpi_sales_id){
        $model =  GeneralService::getAllCustomersOffSalesEdit($staff_kpi_sales_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListOrdersOffStaffSalesEdit($staff_kpi_customers_id){
        $model =  GeneralService::getAllOrdersOffSalesEdit($staff_kpi_customers_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListKPICodeOffStaffSalesEdit($staff_kpi_period_stock_id){
        $model =  GeneralService::getAllKPICodeOffSalesEdit($staff_kpi_period_stock_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerOffStaffSubSalesEdit($staff_kpi_sales_id){
        $model =  GeneralService::getAllCustomersOffSubSalesEdit($staff_kpi_sales_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListOrdersOffStaffSubSales($staff_kpi_customers_id){
        $model =  GeneralService::getAllOrdersOffSubSales($staff_kpi_customers_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListKPICodeOffStaffSubSales($staff_kpi_period_stock_id){
        $model =  GeneralService::getAllKPICodeOffSubSales($staff_kpi_period_stock_id);
        return $model;
    }
  /** khu vực hiển thị ds theo show cho nv cấp sales biết doanh thu */
    public static function getQuerySalesKPI(){
        return GeneralService::getKPIStaffSales();
    }
    public static function getModelByParamaterSalesKPI($param)
    {
        $model=self::getQuerySalesKPI();
        $model->orderBy('created_at', 'DESC');
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchInput']){
            $model->where('staff_kpi_customers_id',$param['searchInput']);
        }
        if($param['fromDate'] && $param['toDate']){
            $models=$model->groupBy('staff_kpi_customers_id')->paginate($model->get()->count());
        }else{
            $models = $model->groupBy('staff_kpi_customers_id')->paginate(10);
        }
        return $models;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerKPISalesAll($staff_kpi_sales_id){
        $model =  GeneralService::getAllCustomerStaffKPISales($staff_kpi_sales_id);
        return $model;
    }
    /** lấy ds customer thuộc nv cấp sales  */
    public static function getListCustomerSalesAll($array_custom){
        $model =  GeneralService::getAllCustomerStaffAll($array_custom);
        return $model;
    }
}
