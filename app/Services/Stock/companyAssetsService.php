<?php

namespace App\Services\Stock;
use App\Services\DM\GeneralService;
use Carbon\Carbon;
use App\Models\Stock;
use Illuminate\Support\Facades\DB;
class companyAssetsService
{
    public static function  getModelById($id){
        return Stock::with('brands')->with('currencies')->with('staffStock')->find($id);
    }
    public static function  getModelByProduct($code){
        $model = Stock::with('units')->where('stock_code', $code)->get();
        return $model;
    }
    public static function getQueryProduct($productId){
        $stock_product = GeneralService::getpurchaseshange()->where('stock_type','like','%XUATTAISANCONGTY%')->where('stock_product_id',$productId)->first();
        return $stock_product;
    }
    public static function getQuerySumSlProduct($productId){
        $stock_product = GeneralService::getpurchaseshange()->where('stock_type','like','%XUATTAISANCONGTY%')->where('stock_product_id',$productId)->sum('stock_quality_reality');
        return $stock_product;
    }
    public static function getQuery(){
        $stock_order = GeneralService::getcompanyAssetsBrand();
        return $stock_order;
    }
    public static function getByCode($code){
        return Stock::where('stock_code', "LIKE", "%". $code. "%")->get();
    }
    public static function getCodeSumAmount($stockCode){
        return Stock::where('stock_code', "LIKE", "%". $stockCode. "%")->sum('stock_amount');
    }
    public static function getModelByParamater($param,$limit)
    {
        $model=self::getQuery()->orderBy('created_at', 'DESC');
        $date = date('Y-m-d');
        if($param['fromDate'])
            $model->where(DB::RAW("DATE(created_at)"), '>=', $param['fromDate']);

        if($param['toDate'])
            $model->where(DB::RAW("DATE(created_at)"), '<=', $param['toDate']);

        if ($param['searchBranch']){
            $model->where('stock_brand_id', $param['searchBranch']);
        }
        if($param['fromDate'] && $param['toDate'] || $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }elseif($param['fromDate'] && $param['toDate'] && $param['searchBranch']){
            $models=$model->paginate($model->get()->count());
        }else{
            $models = $model->paginate($limit);
        }
        return $models;
    }
    public static function updateModel($id,$data)
    {
        $model=self::getModelById($id);
        return $model->update($data);
    }
    public static function createModel($data)
    {
        $model=new Stock();
        return $model->create($data);
    }
    public static function RemoveModel($data)
    {
        $model=new Stock();
        return $model->create($data);
    }
    public static function  getModelProduct($code){
        $model = Stock::where('stock_code', $code)->get();
        return $model;
    }
}
