<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WarrantyForm extends Model
{
    protected $table = 'warranty_form';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
