<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $table = 'stock';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function productStocks()
    {
        return $this->hasOne('App\Models\Product', 'id','stock_product_id')->select('id','product_price_ex','product_name');
    }
    public function units()
    {
        return $this->hasOne('App\Models\Unit', 'id','stock_product_unit_id')->select('id','unit_name');
    }
    public function unitConvert()
    {
        return $this->hasOne('App\Models\Unit', 'id','stock_product_unit_covert_id')->select('id','unit_name');
    }
    public function suppliers()
    {
        return $this->hasOne('App\Models\Supplier', 'id','stock_supplier_id')->select('id','supplier_name');
    }
    public function customers()
    {
        return $this->hasOne('App\Models\Customer', 'id','stock_customer_id')->select('id','customers_name');
    }
    public function brands()
    {
        return $this->hasOne('App\Models\Brand', 'id','stock_brand_id')->select('id','brand_name');
    }
    public function staffStock()
    {
        return $this->hasOne('App\Models\Staff', 'id','stock_staff_id')->select('id','staff_name');
    }
    public function stockers()
    {
        return $this->hasOne('App\Models\Staff', 'id','stock_staff_stocker_id')->select('id','staff_name');
    }
    public function warrantyForm()
    {
        return $this->hasOne('App\Models\warrantWarrantyFormyform', 'id','stock_warranty_form_id')->select('id','warranty_form_name');
    }
    public function currencies()
    {
        return $this->hasOne('App\Models\Currency', 'id','stock_currency_id')->select('id','currency_name','currency_symbol');
    }
    public function amountTax()
    {
        return $this->hasOne('App\Models\Tax', 'id','stock_amount_tax_id')->select('id','tax_name');
    }
    public function locationOut()
    {
        return $this->hasOne('App\Models\WareHouse', 'id','stock_location_Out')->select('id','ware_house_name');
    }
    public function locationIut()
    {
        return $this->hasOne('App\Models\WareHouse', 'id','stock_location_Int')->select('id','ware_house_name');
    }
    public function stockLocation()
    {
        return $this->hasOne('App\Models\WareHouse', 'id','stock_location')->select('id','ware_house_name');
    }
    public function staffStockerId()
    {
        return $this->hasOne('App\Models\Staff', 'id','stock_staff_stocker_id')->select('id','staff_name');
    }
}
