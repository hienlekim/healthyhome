<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WareHouse extends Model
{
    protected $table = 'ware_house';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
