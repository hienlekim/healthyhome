<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brand';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function countries()
    {
        return $this->hasOne('App\Models\Country', 'id','brand_country_id')->select('id','country_name');
    }
    public function currencies()
    {
        return $this->hasOne('App\Models\Currency',  'id','brand_currency_id')->select('id','currency_name','currency_code');
    }
    public function bankconnect01()
    {
        return $this->hasOne('App\Models\Bank',  'id','brand_bank_id')->select('id','bank_number');
    }
    public function bankconnect02()
    {
        return $this->hasOne('App\Models\Bank',  'id','brand_bank_id_1')->select('id','bank_number');
    }
    public function bankconnect03()
    {
        return $this->hasOne('App\Models\Bank',  'id','brand_bank_id_2')->select('id','bank_number');
    }
    public function bankconnect04()
    {
        return $this->hasOne('App\Models\Bank',  'id','brand_bank_id_3')->select('id','bank_number');
    }
    public function bankconnect05()
    {
        return $this->hasOne('App\Models\Bank',  'id','brand_bank_id_4')->select('id','bank_number');
    }

}
