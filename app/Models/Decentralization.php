<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Decentralization extends Model
{
    protected $table = 'decentralization';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function positions()
    {
        return $this->hasOne('App\Models\Position', 'id','decen_position_id')->select('id','position_code','position_name');
    }
}
