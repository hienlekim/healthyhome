<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receipts extends Model
{
    protected $table = 'receipts';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function receiptstock()
    {
        return $this->hasOne('App\Models\Stock', 'id','receipts_stock_id')->select('id','stock_code');
    }
    public function receiptcustomer()
    {
        return $this->hasOne('App\Models\Customer', 'id','receipts_customer_id')->select('id','customers_name','customers_address','customers_phone');
    }
    public function receiptsbranch()
    {
        return $this->hasOne('App\Models\Brand', 'id','receipts_brand_id')->select('id','brand_name');
    }
    public function receiptsbank()
    {
        return $this->hasOne('App\Models\Bank', 'id','receipts_bank_id')->select('id','bank_code','bank_name');
    }
    public function receiptstaff()
    {
        return $this->hasOne('App\Models\Staff', 'id','receipts_staff_id')->select('id','staff_name');
    }
    public function receiptstaffaprove()
    {
        return $this->hasOne('App\Models\Staff', 'id','receipts_staff_approve_id')->select('id','staff_name');
    }
    public function receiptsCurrency()
    {
        return $this->hasOne('App\Models\Currency', 'id','receipts_currency_id')->select('id','currency_name','currency_code');
    }
    public function receiptsperiod()
    {
        return $this->hasOne('App\Models\Period', 'id','receipts_period_id')->select('id','period_name','period_code','period_amount','period_amount_batch','period_amount_batch');
    }
    public function receiptspayment()
    {
        return $this->hasOne('App\Models\Payments', 'id','receipts_period_payments_id')->select('id','payments_code','payments_name');
    }
}
