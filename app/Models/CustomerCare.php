<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerCare extends Model
{
    protected $table = 'customer_care';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function custom()
    {
        return $this->hasOne('App\Models\Customer', 'id','customer_id')->select('id','customers_name');
    }
    public function brandCustom()
    {
        return $this->hasOne('App\Models\Brand', 'id','customer_brand_id')->select('id','brand_name');
    }
    public function staffCustom()
    {
        return $this->hasOne('App\Models\Staff', 'id','customer_staff_id')->select('id','staff_name');
    }
}
