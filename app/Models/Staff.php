<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function Users()
    {
        return $this->hasOne('App\Models\User', 'id','staff_user_id')->select('id','email','name');
    }
    public function brands()
    {
        return $this->hasOne('App\Models\Brand', 'id','staff_brand_id')->select('id','brand_name','brand_ware_house_id','brand_currency_id');
    }
    public function ranks()
    {
        return $this->hasOne('App\Models\Rank', 'id','staff_rank_id')->select('id','rank_code','rank_name');
    }
    public function decenTLizaTion()
    {
        return $this->hasOne('App\Models\Decentralization', 'id','staff_decentralization_id')
            ->select('id','decen_position_id','decen_general_directory','decen_history_system','decen_formality_payment'
            ,'decen_receipts','decen_payment','decen_approve_receipts','decen_approve_payment','decen_Staff',
                'decen_kpi_sales','decen_position','decen_rank','decen_commission_rank',
                'decen_commission','decen_supplier','decen_supplier_order_list','decen_supplier_debt_list',
                'decen_customers','decen_customer_debt_list','decen_customers_care','decen_customers_receive_gifts',
                'decen_product','decen_product_cate','decen_purchases','decen_successful_order',
                'decen_inventory','decen_synthesis_report','decen_Order','decen_installment_orders',
                'decen_full_payment_order','decen_gift_order','decen_payment_methods');
    }

}
