<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaffKpiOrder extends Model
{
    protected $table = 'staff_kpi_order';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function kpiStaffSales()
    {
        return $this->hasOne('App\Models\Staff', 'id','staff_kpi_sales_id')->select('id','staff_code','staff_name','staff_rank_id');
    }
    public function kpiStaffSubSales()
    {
        return $this->hasOne('App\Models\Staff', 'id','staff_kpi_sales_sub_id')->select('id','staff_code','staff_name');
    }
    public function kpiStaffManageSales()
    {
        return $this->hasOne('App\Models\Staff', 'id','staff_kpi_sales_manage_id')->select('id','staff_code','staff_name');
    }
    public function kpiBrand()
    {
        return $this->hasOne('App\Models\Brand', 'id','staff_kpi_order_brand_id')->select('id','brand_code','brand_name');
    }
    public function kpiCustomers()
    {
        return $this->hasOne('App\Models\Customer', 'id','staff_kpi_customers_id')->select('id','customers_name','customers_code');
    }
    public function kpiStocks()
    {
        return $this->hasOne('App\Models\Stock', 'id','staff_kpi_period_stock_id')->select('id','stock_code');
    }
}
