<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommissionRank extends Model
{
    protected $table = 'commission_rank';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
