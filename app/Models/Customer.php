<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function brandCustomer()
    {
        return $this->hasOne('App\Models\Brand', 'id','customers_brand_id')->select('id','brand_name');
    }
    public function customerStaffs()
    {
        return $this->hasOne('App\Models\Staff', 'id','customers_staffs_id')->select('id','staff_name');
    }
    public function customersCountry()
    {
        return $this->hasOne('App\Models\Country', 'id','customers_country_id')->select('id','country_name');
    }
}
