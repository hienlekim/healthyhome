<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function counupplier()
    {
        return $this->hasOne('App\Models\Country', 'id','supplier_country_id')->select('id','country_name');
    }
}
