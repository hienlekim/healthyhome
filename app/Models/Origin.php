<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
    protected $table = 'origin';
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
