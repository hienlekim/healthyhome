<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $table = 'period';
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function periodstock()
    {
        return $this->hasOne('App\Models\Stock', 'id','period_stock_id')->select('id','stock_code');
    }
    public function periodcustomer()
    {
        return $this->hasOne('App\Models\Customer', 'id','period_customers_id')->select('id','customers_name');
    }
    public function periodbranch()
    {
        return $this->hasOne('App\Models\Brand', 'id','period_brand_id')->select('id','brand_name');
    }
    public function periodpayments()
    {
        return $this->hasOne('App\Models\Payments', 'id','period_payments_id')->select('id','payments_code','payments_name','payments_number');
    }
    public function periodstaff()
    {
        return $this->hasOne('App\Models\Staff', 'id','period_staff_id')->select('id','staff_name');
    }
    public function periodReceipt()
    {
        return $this->hasOne('App\Models\Receipts', 'id','period_receipts_id')->select('id','receipts_bank_id');
    }
}
