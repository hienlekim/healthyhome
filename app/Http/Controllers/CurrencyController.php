<?php
/**
 * date:10/08/2021
 * content: tiền tệ có 2 loại tiền chính $,VNĐ, sẽ phụ thuộc vào chi nhánh
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\CurrencyService;
use App\Services\AbstractService;

class CurrencyController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results =  CurrencyService::getModelByParamater($param, $rowsPerPage);
        $count = $results->count();
        return view("Currency.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('Currency.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "currency_code"=>$request->currency_code,
            "currency_symbol"=>$request->currency_symbol,
            "currency_name"=>$request->currency_name
        ];
        $query = CurrencyService::getByCode($request->currency_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = CurrencyService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = CurrencyService::getModelById($id);
        return view("Currency.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "currency_name"=>$request->currency_name
        ];
        $asc = CurrencyService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
