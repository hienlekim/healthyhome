<?php
/**
 * date:10/08/2021
 * content: danh sách báo cáo dòng tiền hàng tháng
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Services\Stock\CustomerService;
use App\Services\Stock\CustomerCareService;
use App\Services\Stock\ImagesService;
use App\Services\Stock\monthlycashflowService;
use App\Services\Stock\PeriodService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DM\GeneralService;
use App\Services\AbstractService;
use App\Exports\monthlyCashFlowExport;
use Maatwebsite\Excel\Facades\Excel;

class monthlycashflowController extends Controller
{
    public function index(Request $request)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        //gom nhóm lấy group theo tháng tạo phiếu ttoan
        $month=monthlycashflowService::getMonthParams($param);
        //gom theo nhóm theo tháng thanh toán Period
        $monthPeriodDate=monthlycashflowService::getMonthPeriodDateParams($param);
        $listAll = [];
        $year_now = date('Y');
        $listMonth = [
            'January'=>1,
            'February'=>2,
            'March'=>3,
            'April'=>4,
            'May'=>5,
            'June'=>6,
            'July'=>7,
            'August'=>8,
            'September'=>9,
            'October'=>10,
            'November'=>11,
            'December'=>12
        ];
        $listmonthlYearycash=[];
        $listmonthlycash=[];
        $listYearscash=[];
//        lấy được month-year
        foreach ($monthPeriodDate as $key){
            foreach ($listMonth as $ks=>$vs){
                if($key['month'] == $vs){
                    array_push($listmonthlycash,$vs);
                    array_push($listYearscash,$key['year']);
                    array_push($listmonthlYearycash,[
                        'monthyear'=>$ks.'-'.$key['year'],
                    ]);
                }
            }
        }
//        print_r($listmonthlYearycash);
        //bắt đầu lấy ds khách hàng mua theo tháng
        foreach ($month as $key) {
            $results = monthlycashflowService::getModelByParamater($key->year,$key->month);
            $listChildrent = [];
            $tRows = 0;
            //gom nhóm được những phiếu có ngày tạo từ ngày -> đến ngày
            $countDate=[];
            foreach ($results as $items) {
                $tRows += 1;
                $customersPeriodDate = [];
                if ($items['periodbranch']['brand_name'] == "CN Việt Nam") {
                    $currency = "VNĐ";
                } else {
                    $currency = "$";
                }
//                $customersPeriod = monthlycashflowService::getPeriodDateQuery($items->period_customers_id,$param);
                $customersPeriod = monthlycashflowService::getPeriodDateQuery($items->period_customers_id,$items->year,$items->month);
                array_push($countDate,$customersPeriod->count('created_at'));
                // lấy ds khách hàng giao dịch trong tháng
                $listChildrent_2 = [];
                $tRowsChild = 0;
                foreach ($customersPeriod as $ks){
                    $tRowsChild += 1;
                    // lấy theo mỗi khách hàng có 1 or n đơn hàng
                    $customersPeriodGroupBystock = monthlycashflowService::getCustomersOrdersQuery($ks->period_stock_id);
                    $montheriod =[];
                    $yearseriod =[];
                    $periodAmountBatch = [];
                    foreach ($customersPeriodGroupBystock as $kmonth){
                        array_push($montheriod,$kmonth['month']);
                        array_push($yearseriod,$kmonth['year']);
                        array_push($periodAmountBatch,[
                            'period_amount_batch'=>$kmonth['period_amount_batch'],
                            'period_status'=>$kmonth['period_status'],
                            'currency'=>$currency,
                        ]);
                    }
                    $minListmonthlycash = reset($listmonthlycash);
                    $minMontheriod = reset($montheriod);
                    $sumFist = abs($minListmonthlycash-$minMontheriod);
                    //tiền
                    $arrFist =[];
                    for ($i=0;$i<$sumFist;$i++){
                        array_push($arrFist,0);
                    }
//                    tính kc cuối cùng của sum last
                    // lấy tháng cuối cùng và năm cuối cùng của mảng tháng thanh toán
                    $maxMontheriod= end($montheriod);
                    $maxYearseriod = end($yearseriod);
                    // lấy tháng cuối cùng và năm cuối cùng của mảng tháng trên title table
                    $maxsmonthlycash=end($listmonthlycash);
                    $maxsYearscash=end($listYearscash);
//                   công thức tính = abs((year-year)x12) - abs(month-month)
                    if($maxsmonthlycash < $maxMontheriod){
                        $sumLast = abs(($maxsYearscash - $maxYearseriod) *12) - abs($maxsmonthlycash - $maxMontheriod);
                    }elseif($maxsmonthlycash >= $maxMontheriod){
                        $sumLast = abs(($maxsYearscash - $maxYearseriod) *12) + abs($maxsmonthlycash - $maxMontheriod);
                    }
                    $arrLast =[];
                    for ($i=0;$i<abs($sumLast);$i++){
                        array_push($arrLast,0);
                    }
                    $customersPeriodDate = array_merge($arrFist,$periodAmountBatch,$arrLast);
                    // liệt kê từng đơn hàng theo khách hàng
                    array_push($listChildrent_2,[
                        'date'=>$ks->created_at,
                        'ordersCode'=>$ks['periodstock']['stock_code'],
                        'payment'=>$items['periodpayments']['payments_code'],
                        'customersPeriodDate'=>$customersPeriodDate
                    ]);
                }
                //array lấy ds nv trong tháng 6
                array_push($listChildrent,[
                    'staff'=>$items['periodstaff']['staff_name'],
                    'customerId'=>$items['period_customers_id'],
                    'listChildrent_2'=>$listChildrent_2,
                    'tRowsChild'=>$tRowsChild,
                    'customer'=>$items['periodcustomer']['customers_name'],
                ]);
            }
            foreach ($listMonth as $k=>$v){
                if($v==$key->month){
//                    print_r($tRowsChild);
                    array_push($listAll,[
                        'months'=>$k.'-'.$key->year,
                        'tRows'=>array_sum($countDate),
                        'data'=>$listChildrent,
                    ]);
                }
            }
        }
        return view("monthlycashflow.index",compact('year_now','listAll','date_end','date_start','monthPeriodDate','listmonthlYearycash'));
    }
    public function monthlyCashFlowExport(Request $request){
        if ($request->session()->token() != $request->_token) {
            response()->json(["status" => false, "message" => "Dữ liệu không hợp lệ"]);
        }
        $date_start = $request->fromDate;
        $date_end = $request->toDate;
        $brandUser = GeneralService::getBrandUser();
        $branch = $brandUser['brands']['brand_name'];
        $staff_name= $brandUser['staff_name'];
        $currency =$brandUser['brands']['currencies']['currency_code'];
        $position =$brandUser['decenTLizaTion']['positions']['position_name'];;
        return Excel::download(new monthlyCashFlowExport([
            'date_start'=>$date_start,
            'date_end'=>$date_end,
            'branch'=>$branch,
            'staff_name'=>$staff_name,
            'currency'=>$currency,
            'position'=>$position,
        ]), 'Monthly_Cash_Flow.xlsx');
    }
}
