<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\Stock\salesKpiPaymentService;
use App\Services\Stock\subSalesKPIService;
use App\Services\AbstractService;
use App\Services\Stock\CustomerService;
use App\Services\DM\GeneralService;
use App\Services\Stock\PeriodService;
use App\Services\Stock\manageSalesKPIService;

class staffSalesKPIController extends Controller
{
    public function index(Request $request)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $User = CustomerService::getStaffCustomerBrand();
        $chooseCustomers = salesKpiPaymentService::getListCustomerOffStaffSales($User['id']);
        /** lấy ds customers thuộc nv cấp sales */
        $dbCustomers = subSalesKPIService::getModelByParamaterRanksSales($param,$User['id']);
        $listCustomer =[];
        $trowParent=0;
        foreach ($dbCustomers as $key){
            /** lấy ds order thuộc customers nv cấp sales */
            $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSales($key['staff_kpi_customers_id']);
            $listOrders =[];
            $trowOrders =0;
            foreach ($dbOrders as $ks){
                /** lấy ds kpoi_code thuộc stock_code nv cấp sales */
                $listKPICode =[];
                $trowKPICode =0;
                $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSales($ks['staff_kpi_period_stock_id']);
                foreach ($dbKPICode as $ki){
                    $trowKPICode +=1;
                    $trowOrders +=1;
                    $trowParent +=1;
                    array_push($listKPICode,[
                        'id'=>$ki['id'],
                        'staff_kpi_code'=>$ki['staff_kpi_code'],
                        'brand_name'=>$ki['kpiBrand']['brand_name'],
                        'staff_kpi_sales_status'=>$ki['staff_kpi_sales_status']
                    ]);
                }
                /** end */
                array_push($listOrders,[
                    'stock_code'=>$ks['kpiStocks']['stock_code'],
                    'trowKPICode'=>$trowKPICode,
                    'listKPICode'=>$listKPICode,
                ]);
            }
            /** end */
            /** lấy ds customers thuộc nv cấp sales */
            array_push($listCustomer,[
                'customers_name'=>$key['kpiCustomers']['customers_name'],
                'trowOrders'=>$trowOrders,
                'listOrders'=>$listOrders,
            ]);
        }
        return view("staffsKPISales.index",compact('date_end','date_start','dbCustomers',
            'listCustomer','chooseCustomers'));
    }
    public function searchkpiRanksSubSales(Request $request){
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $paramSubsales=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchInputSubSales'=>$request->input('searchInputSubSales'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $paramSubsales['fromDate'];
        $date_end = $paramSubsales['toDate'];
        //rank nv sales
        $User = CustomerService::getStaffCustomerBrand();
        $salesoffSubKpi = GeneralService::getStaffSubSalesPayment($User['id']);
        $array_id_sub_sales = [];
        foreach ($salesoffSubKpi as $vs)
        {
            if($vs)
            {
                array_push($array_id_sub_sales,$vs['id']);
            }
        }
        //ds mảng show phiếu thanh toán
        $results_sub_sales = subSalesKPIService::getModelByParamaterSubSales($paramSubsales,$array_id_sub_sales);
        $listAllSubSales =[];
        $stt=0;
        foreach ($results_sub_sales as $val){
            /** lấy ds customers thuộc nv cấp sales */
            $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSalesEdit($val['staff_kpi_sales_id']);
            $listCustomer =[];
            $trowParent=0;
            $countRowsCustomers=0;
            foreach ($dbCustomers as $key){
                $countRowsCustomers +=1;
                /** lấy ds order thuộc customers nv cấp sales */
                $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSalesEdit($key['staff_kpi_customers_id']);
                $listOrders =[];
                $trowOrders =0;
                foreach ($dbOrders as $ks){
                    /** lấy ds kpoi_code thuộc stock_code nv cấp sales */
                    $listKPICode =[];
                    $trowKPICode =0;
                    $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSalesEdit($ks['staff_kpi_period_stock_id']);
                    foreach ($dbKPICode as $ki){
                        $trowKPICode +=1;
                        $trowOrders +=1;
                        $trowParent +=1;
                        $stt +=1;
                        array_push($listKPICode,[
                            'id'=>$ki['id'],
                            'stt'=>$stt,
                            'staff_kpi_code'=>$ki['staff_kpi_code'],
                            'brand_name'=>$ki['kpiBrand']['brand_name'],
                            'staff_kpi_sales_status'=>$ki['staff_kpi_sales_status'],
                            'staff_kpi_sales_sub_status'=>$ki['staff_kpi_sales_sub_status']
                        ]);
                    }
                    /** end */
                    array_push($listOrders,[
                        'stock_code'=>$ks['kpiStocks']['stock_code'],
                        'trowKPICode'=>$trowKPICode,
                        'listKPICode'=>$listKPICode,
                    ]);
                }
                /** end */
                /** lấy ds customers thuộc nv cấp sales */
                array_push($listCustomer,[
                    'customers_name'=>$key['kpiCustomers']['customers_name'],
                    'trowOrders'=>$trowOrders,
                    'listOrders'=>$listOrders,
                ]);
            }
            array_push($listAllSubSales,[
                'staffs_name'=>$val['kpiStaffSales']['staff_name'],
                'trowParent'=>$trowParent,
                'listCustomer'=>$listCustomer,
            ]);
        }
        return view("staffsKPISales.index_sub_sales",compact('date_end','date_start','salesoffSubKpi','listAllSubSales','results_sub_sales'));
    }
    public function searchkpiRanksManageSales(Request $request){
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $paramManagesales=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchInputManageSalesName'=>$request->input('searchInputManageSalesName'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $paramManagesales['fromDate'];
        $date_end = $paramManagesales['toDate'];
        //rank nv sales
        $User = CustomerService::getStaffCustomerBrand();

        //lấy ds khách hàng
        $ranksManageKpi = GeneralService::getStaffManaSalesPayment($User['id']);
        $array_id_manage_sales = [];
        foreach ($ranksManageKpi as $vs)
        {
            if($vs)
            {
                array_push($array_id_manage_sales,$vs['id']);
            }
        }
        //ds mảng show phiếu thanh toán
        $results_manage = subSalesKPIService::getModelByParamaterManagesSales($paramManagesales,$array_id_manage_sales);
        $listAll_Manage_sales =[];
        $stt=0;
        foreach ($results_manage as $val){
            /** lấy ds nv sales thuộc nv cấp sub sales */
            $countSales=0;
            $dbSalesOfSubsales = manageSalesKPIService::getListsalesOffManageSales($val['staff_kpi_sales_sub_id']);
            $trowSales=0;
            $listSales =[];
            foreach ($dbSalesOfSubsales as $values){
                $countSales +=1;
                $trowCustomers = 0;
                $listCustomers =[];
                /** lấy ds customers thuộc nv cấp sales */
                $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSales($values['staff_kpi_sales_id']);
                foreach ($dbCustomers as $value){
                    $trowOrders = 0;
                    $listOrders =[];
                    /** lấy ds orders thuộc nv cấp sales */
                    $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSales($value['staff_kpi_customers_id']);
                    foreach ($dbOrders as $v){
                        $trowKPIOders = 0;
                        $listKPIOders =[];
                        /** lấy ds kpi orders thuộc nv cấp sales */
                        $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSales($v['staff_kpi_period_stock_id']);
                        foreach ($dbKPICode as $vs){
                            $trowKPIOders +=1;
                            $trowOrders +=1;
                            $trowCustomers +=1;
                            $trowSales +=1;
                            $stt +=1;
                            array_push($listKPIOders,[
                                'id'=>$vs['id'],
                                'stt'=>$stt,
                                'staff_kpi_code'=>$vs['staff_kpi_code'],
                                'brand_name'=>$vs['kpiBrand']['brand_name'],
                                'staff_kpi_sales_sub_status'=>$vs['staff_kpi_sales_sub_status'],
                                'staff_kpi_sales_manage_status'=>$vs['staff_kpi_sales_manage_status']
                            ]);
                        }
                        array_push($listOrders,[
                            'stock_code'=>$v['kpiStocks']['stock_code'],
                            'trowKPIOders'=>$trowKPIOders,
                            'listKPIOders'=>$listKPIOders
                        ]);
                    }
                    array_push($listCustomers,[
                        'customer_name'=>$value['kpiCustomers']['customers_name'],
                        'trowOrders'=>$trowOrders,
                        'listOrders'=>$listOrders
                    ]);
                }
                array_push($listSales,[
                    'sales_name'=>$values['kpiStaffSales']['staff_name'],
                    'listCustomers'=>$listCustomers,
                    'trowCustomers'=>$trowCustomers
                ]);
            }
            array_push($listAll_Manage_sales,[
                'sales_sub_name'=>$val['kpiStaffSubSales']['staff_name'],
                'sales_sub_id'=>$val['staff_kpi_sales_sub_id'],
                'trowSales'=>$trowSales,
                'listSales'=>$listSales,
            ]);
        }
        return view("staffsKPISales.index_manage_sales",compact('date_end','date_start','ranksManageKpi','listAll_Manage_sales','results_manage'));
    }
}
