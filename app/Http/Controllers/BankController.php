<?php
/**
 * date:10/08/2021
 * content: ngân hàng, mỗi chi nhánh sẽ thực hiện giao dịch 1 ngân hàng nhất định
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\BankService;
use App\Services\AbstractService;
use Illuminate\Support\Facades\Redis;

class BankController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = BankService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("Bank.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('Bank.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "bank_code"=>$request->bank_code,
            "bank_name"=>$request->bank_name,
            "bank_number"=>$request->bank_number
        ];
        $query = BankService::getByCode($request->bank_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = BankService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = BankService::getModelById($id);
        return view("Bank.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "bank_code"=>$request->bank_code,
            "bank_name"=>$request->bank_name,
            "bank_number"=>$request->bank_number
        ];
        $asc = BankService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
