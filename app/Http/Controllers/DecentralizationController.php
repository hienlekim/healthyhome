<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\DM\DecentralizationService;
use App\Services\DM\GeneralService;
use App\Services\AbstractService;

class DecentralizationController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = DecentralizationService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("decentralization.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $dataPosition = GeneralService::getPositionQuery();
            return view('decentralization.create',compact('dataPosition'))->render();
        }
    }
    public function create(Request $request){
        $data=[
            "decen_position_id"=>$request->decen_position_id,
            "decen_general_directory"=>$request->decen_general_directory,
            "decen_history_system"=>$request->decen_history_system,
            "decen_formality_payment"=>$request->decen_formality_payment,
            "decen_receipts"=>$request->decen_receipts,
            "decen_payment"=>$request->decen_payment,
            "decen_approve_receipts"=>$request->decen_approve_receipts,
            "decen_approve_payment"=>$request->decen_approve_payment,
            "decen_Staff"=>$request->decen_Staff,
            "decen_kpi_sales"=>$request->decen_kpi_sales,
            "decen_position"=>$request->decen_position,
            "decen_rank"=>$request->decen_rank,
            "decen_commission_rank"=>$request->decen_commission_rank,
            "decen_commission"=>$request->decen_commission,
            "decen_supplier"=>$request->decen_supplier,
            "decen_supplier_order_list"=>$request->decen_supplier_order_list,
            "decen_supplier_debt_list"=>$request->decen_supplier_debt_list,
            "decen_customers"=>$request->decen_customers,
            "decen_customer_debt_list"=>$request->decen_customer_debt_list,
            "decen_customers_care"=>$request->decen_customers_care,
            "decen_customers_receive_gifts"=>$request->decen_customers_receive_gifts,
            "decen_product"=>$request->decen_product,
            "decen_product_cate"=>$request->decen_product_cate,
            "decen_purchases"=>$request->decen_purchases,
            "decen_successful_order"=>$request->decen_successful_order,
            "decen_inventory"=>$request->decen_inventory,
            "decen_synthesis_report"=>$request->decen_synthesis_report,
            "decen_Order"=>$request->decen_Order,
            "decen_installment_orders"=>$request->decen_installment_orders,
            "decen_full_payment_order"=>$request->decen_full_payment_order,
            "decen_gift_order"=>$request->decen_gift_order,
            "decen_payment_methods"=>$request->decen_payment_methods,
        ];
        $query = DecentralizationService::getByCode($request->decen_position_id);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = DecentralizationService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = DecentralizationService::getModelById($id);
        $dataPosition = GeneralService::getPositionQuery();
        return view("decentralization.update",compact('data','dataPosition'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "decen_general_directory"=>$request->decen_general_directory,
            "decen_history_system"=>$request->decen_history_system,
            "decen_formality_payment"=>$request->decen_formality_payment,
            "decen_receipts"=>$request->decen_receipts,
            "decen_payment"=>$request->decen_payment,
            "decen_approve_receipts"=>$request->decen_approve_receipts,
            "decen_approve_payment"=>$request->decen_approve_payment,
            "decen_Staff"=>$request->decen_Staff,
            "decen_kpi_sales"=>$request->decen_kpi_sales,
            "decen_position"=>$request->decen_position,
            "decen_rank"=>$request->decen_rank,
            "decen_commission_rank"=>$request->decen_commission_rank,
            "decen_commission"=>$request->decen_commission,
            "decen_supplier"=>$request->decen_supplier,
            "decen_supplier_order_list"=>$request->decen_supplier_order_list,
            "decen_supplier_debt_list"=>$request->decen_supplier_debt_list,
            "decen_customers"=>$request->decen_customers,
            "decen_customer_debt_list"=>$request->decen_customer_debt_list,
            "decen_customers_care"=>$request->decen_customers_care,
            "decen_customers_receive_gifts"=>$request->decen_customers_receive_gifts,
            "decen_product"=>$request->decen_product,
            "decen_product_cate"=>$request->decen_product_cate,
            "decen_purchases"=>$request->decen_purchases,
            "decen_successful_order"=>$request->decen_successful_order,
            "decen_inventory"=>$request->decen_inventory,
            "decen_synthesis_report"=>$request->decen_synthesis_report,
            "decen_Order"=>$request->decen_Order,
            "decen_installment_orders"=>$request->decen_installment_orders,
            "decen_full_payment_order"=>$request->decen_full_payment_order,
            "decen_gift_order"=>$request->decen_gift_order,
            "decen_payment_methods"=>$request->decen_payment_methods,
        ];
        $asc = DecentralizationService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
