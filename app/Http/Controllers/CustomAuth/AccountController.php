<?php

namespace App\Http\Controllers\CustomAuth;

use App\Helpers\Auth\Authen;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AccountController extends Controller
{
    public function profile(Request $request){
        if($request->isMethod("get")){
            return view("auth/profile", [

            ]);
        }
    }

    public function changePassword(Request $request){
        if($request->isMethod("get")) {
            return view("auth/change-password", []);
        }else{
            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'new_password' => 'required'
            ]);

            $validator->validate();

            $old_password =  $request->old_password;
            $new_password =  $request->new_password;

            $User = User::where(["email"=>Auth::user()->email])->first();
            if(!$User){
                return redirect()->back()->withErrors("Dữ liệu không hợp lệ");
            }

            if(!Hash::check($old_password, $User->password)){
                return redirect()->back()->withErrors("Mật khẩu hiện tại không đúng");
            }

            $User->password = Hash::make($new_password);
            $User->save();

            return view('auth/change-password-success', []);
        }
    }

    public function setting(Request $request){
        if($request->isMethod("get")) {
            if($request->isMethod("get")){
                $ee_code = session()->get("LawERPAuth")->ee_code;
                $objEmployee = Employee::leftJoin('hrm_employee_group', 'ee_group', 'heg_code')
                    ->leftJoin('hrm_department', 'ee_department', 'dept_id')
                    ->leftJoin('hrm_position', 'ee_position', 'posit_id')
                    ->where(["ee_code"=>$ee_code])->firstOrFail();
                return view("auth/setting", [
                    'objEmployee'=>$objEmployee
                ]);
            }else{

            }
        }else{

        }
    }

    public function restart(){
        $route_allow = Decentralization::getRouteDecentralizationByEmployee(Authen::email());
        $objEmployee = Employee::select('ee_id', 'email', 'name', 'ee_avatar')->where("email", Authen::email())->first();
        $Navigation = NavigationDecentralization::getNavigationByEmployee(Authen::email());

        session()->put('navigation', $Navigation);
        session()->put('HGBAAuth', $objEmployee);
        session()->put('route', $route_allow);

        return redirect()->back();
    }

    public function logout(){
        Auth::guard()->logout();
        return redirect("/");
    }
}
