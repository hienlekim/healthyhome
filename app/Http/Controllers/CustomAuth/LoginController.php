<?php

namespace App\Http\Controllers\CustomAuth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function __construct()
    {
        if(Auth::guard()->check()){
            return redirect('/');
        }
    }

    public function index(Request $request){
        if($request->isMethod("get")){
            if(Auth::guard()->check()){
                return redirect('/');
            }
            return view("auth/login");
        }else{
            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];
//            print_r(Hash::make($request->password));exit();
            $customMessages = [
                'email.required' => 'Vui lòng nhập email hợp lệ',
                'password.required' => 'Vui lòng nhập mật khẩu hợp lệ'
            ];

            $validator = Validator::make( $request->all(), $rules, $customMessages);
            if(!$validator->validate()){
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $email = $request->input("email");
            $password = $request->input("password");
//            $remember = $request->input("remember");
//            $hashed = Hash::make($password);
//            echo $hashed;
            if(Auth::guard()->attempt(['email' => $email, 'password' => $password])){
                return redirect()->intended();
            }else{
                $validator->errors()->add('email', 'Tài khoản hoặc mật khẩu không đúng');
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
    }
}
