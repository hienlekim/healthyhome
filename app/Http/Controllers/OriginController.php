<?php
/**
 * date:10/08/2021
 * content:nhà sản xuất
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\OriginService;
use App\Services\AbstractService;
class OriginController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = OriginService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("Origin.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('Origin.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "origin_code"=>$request->origin_code,
            "origin_name"=>$request->origin_name,
        ];
        $query = OriginService::getByCode($request->origin_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = OriginService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = OriginService::getModelById($id);
        return view("Origin.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "origin_name"=>$request->origin_name
        ];
        $asc = OriginService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
