<?php
/**
 * date:10/08/2021
 * content:cấp bậc nhân viên sale để hưởng hoa hồng
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\DM\GeneralService;
use App\Services\Stock\manageSalesKPIService;
use App\Services\Stock\subSalesKPIService;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\DM\RankService;
use App\Services\AbstractService;
class RankController extends Controller
{
    public function index(Request $request)
    {
        //sales Manages
        $resultSalesManages = RankService::getModelByManageSales();
        //sales sub Manages
        $resultSalesSubManages = RankService::getModelBySubManageSales();
        //sales sub Manages chuaw assign
        $salesSubManagesNoAssign = RankService::getModelSubManageSalesNoAssign();
        //sales
        $resultSales= RankService::getModelBySales();
        $count = $resultSalesManages->count();
        return view("Rank.index",compact('resultSalesManages','count','resultSalesSubManages','resultSales','salesSubManagesNoAssign'));
    }
    public function assignSubManage(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $salesSub =RankService::getlistSubManage();
            return view('Rank.assign_sub_manage',compact('salesSub'));
        }
    }
    public function create(Request $request){
        //duyệt qua data sales ko dc trung sub sales
        $array_sales = [];
        foreach($request->sales as $key){
            if($key != $request->sub_manage_sales){
                array_push($array_sales,$key);
            }
        }
        //update cấp bậc sub manage sales
        $dataSubManageSales = ['staff_rank_id'=>3];
        $subManageSales = RankService::updateModel($request->sub_manage_sales, $dataSubManageSales);
        if($subManageSales==true){
            $dataAssignSales = [
                'staff_parent_id'=>$request->sub_manage_sales,
            ];
            $asc = RankService::updateRankSalesModel($array_sales, $dataAssignSales);
        }
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function assignManage(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $salesManage =RankService::getNoSalesManageBy();
            return view('Rank.assign_manage',compact('salesManage'));
        }
    }
    public function createManageSales(Request $request){
        //duyệt qua data sales ko dc trung sub sales
        $array_sales = [];
        foreach($request->sub_sales as $key){
            if($key != $request->manage_sales){
                array_push($array_sales,$key);
            }
        }
        //update cấp bậc sub manage sales
        $dataSubManageSales = ['staff_rank_id'=>2];
        $subManageSales = RankService::updateModel($request->manage_sales, $dataSubManageSales);
        if($subManageSales==true){
            $dataAssignSales = [
                'staff_parent_id'=>$request->manage_sales,
            ];
            $asc = RankService::updateRankSalesModel($array_sales, $dataAssignSales);
        }
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function edit($id)
    {
        $data = RankService::getModelById($id);
        $dataChildrent = RankService::getStaffChildrentSub($id);
        $array_id = [];
        foreach ($dataChildrent as $k){
            array_push($array_id,['id'=>$k->id]);
        }
        $salesSub =RankService::getNoEditSalesSubBy();
        $array_select =[];
        foreach ($salesSub as $k){
            foreach($array_id as $val){
                if($k->id == $val['id']){
                    array_push($array_select,['id'=>$k->id,'staff_name'=>$k->staff_name]);
                }
            }
        }
        $salesSubChildrent =RankService::getNoSalesSubBy();
        return view("Rank.edit_assign_sub_manage",compact('data','salesSub','array_id','salesSubChildrent','array_select'));
    }
    public function updateSubManage(Request $request, $id)
    {
        //ds nv ddax assign cho rank sub sales trong ds thanh toans KPI sales
        //lấy ds khách hàng
        $customersPeriodKpi = GeneralService::getStaffSubSalesPayment($id);
        $array_id_sales = [];
        foreach ($customersPeriodKpi as $vs)
        {
            if($vs)
            {
                array_push($array_id_sales,$vs['id']);
            }
        }
        $db_staffs_sales = subSalesKPIService::getModelBySubAssig($array_id_sales);
        $array_saleskpi=[];
        foreach ($db_staffs_sales as $items){
            array_push($array_saleskpi,$items['staff_kpi_sales_id']);
        }
        if($request->sales=="" && count($array_saleskpi)>0)
        {
            $array_diff_sales= $array_saleskpi;
        }elseif($request->sales !="" && count($array_saleskpi)>0){
            $array_diff_sales=array_diff($request->sales,$array_saleskpi);
        }elseif($request->sales !="" && count($array_saleskpi)==0){
            $array_diff_sales= $request->sales;
        }elseif($request->sales =="" && count($array_saleskpi)==0){
            $array_diff_sales= array();
        }
        //kiểm tra ds sales asign =="" thì gán cấp bậc sub sales xuống
        if(count($array_diff_sales) == 0){
            $dataSubManageSales = ['staff_rank_id'=>null,'staff_status_ranks'=>null];
            $results = RankService::updateModel($id, $dataSubManageSales);
            //delete all staff childrent đã được assign trước đó
            if($results==true){
                $dataChildrent = RankService::getStaffChildrentSub($id);
                $array_id = [];
                foreach ($dataChildrent as $k){
                    array_push($array_id,['id'=>$k->id]);
                }
                $dataAssignSales = [
                    'staff_parent_id'=>null,
                    'staff_status_ranks'=>null
                ];
                $asc = RankService::updateRankSalesModel($array_id, $dataAssignSales);
            }
            if ($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }else{
            //delete all staff childrent đã được assign trước đó
            $dataChildrent = RankService::getStaffChildrentSub($id);
            $array_id = [];
            foreach ($dataChildrent as $k){
                array_push($array_id,['id'=>$k->id]);
            }
            $dataAssignSales = [
                'staff_parent_id'=>null,
                'staff_status_ranks'=>null
            ];
            $results = RankService::updateRankSalesModel($array_id, $dataAssignSales);
            //sau khi xóa xong tiến hành assign cái mới nhất
            if ($results == true) {
                $array_merge = array_merge($array_saleskpi,$array_diff_sales);
                $array_sales = [];
                foreach($array_merge as $key){
                    if($key != $request->sub_manage_sales){
                        array_push($array_sales,$key);
                    }
                }
                $dataAssignSales = [
                    'staff_parent_id'=>$request->sub_manage_sales,
                    'staff_status_ranks'=>1
                ];
                $asc = RankService::updateRankSalesModel($array_sales, $dataAssignSales);
                if ($asc) {
                    return AbstractService::ResultSuccess($asc);
                } else {
                    return AbstractService::ResultError("An error occurred, please try again");
                }
            }
        }
    }
    //edit staff manage
    public function editManageSales($id)
    {
        $data = RankService::getModelById($id);
        $dataChildrent = RankService::getStaffChildrent($id);
        $array_id = [];
        foreach ($dataChildrent as $k){
            array_push($array_id,['id'=>$k->id]);
        }
        $salesSub =RankService::getNoEditSalesManabBy();
        $array_select =[];
        foreach ($salesSub as $k){
            foreach($array_id as $val){
                if($k->id == $val['id']){
                    array_push($array_select,['id'=>$k->id,'staff_name'=>$k->staff_name]);
                }
            }
        }
        $salesSubChildrent =RankService::getNoEditSalesManagebBy();
        return view("Rank.edit_assign_manage",compact('data','salesSub','array_id','salesSubChildrent','array_select'));
    }
    public function updateManageSales(Request $request, $id)
    {
        //ds nv ddax assign cho rank sub sales trong ds thanh toans KPI sales
        //lấy ds khách hàng
        $customersPeriodKpi = GeneralService::getStaffManaSalesPayment($id);
        $array_id_sales = [];
        foreach ($customersPeriodKpi as $vs)
        {
            if($vs)
            {
                array_push($array_id_sales,$vs['id']);
            }
        }
        $db_staffs_sales = manageSalesKPIService::getModelByManagesAssign($array_id_sales);
        $array_saleskpi=[];
        foreach ($db_staffs_sales as $items){
            array_push($array_saleskpi,$items['staff_kpi_sales_sub_id']);
        }
        if($request->sales=="" && count($array_saleskpi)>0)
        {
            $array_diff_sales= $array_saleskpi;
        }elseif($request->sales !="" && count($array_saleskpi)>0){
            $array_diff_sales=array_diff($request->sales,$array_saleskpi);
        }elseif($request->sales !="" && count($array_saleskpi)==0){
            $array_diff_sales= $request->sales;
        }elseif($request->sales =="" && count($array_saleskpi)==0){
            $array_diff_sales= array();
        }
        //kiểm tra ds sales asign =="" thì gán cấp bậc sub sales xuống
        if(count($array_diff_sales) == 0){
            $dataSubManageSales = ['staff_rank_id'=>3];
            $results = RankService::updateModel($id, $dataSubManageSales);
            //delete all staff childrent đã được assign trước đó
            if($results==true){
                $dataChildrent = RankService::getStaffChildrent($id);
                $array_id = [];
                foreach ($dataChildrent as $k){
                    array_push($array_id,['id'=>$k->id]);
                }
                $dataAssignSales = [
                    'staff_parent_id'=>null,
                ];
                $asc = RankService::updateRankSalesModel($array_id, $dataAssignSales);
            }
            if ($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }else{
            //delete all staff childrent đã được assign trước đó
            $dataChildrent = RankService::getStaffChildrent($id);
            $array_id = [];
            foreach ($dataChildrent as $k){
                array_push($array_id,['id'=>$k->id]);
            }
            $dataAssignSales = [
                'staff_parent_id'=>null,
            ];
            $results = RankService::updateRankSalesModel($array_id, $dataAssignSales);
            //sau khi xóa xong tiến hành assign cái mới nhất
            if ($results == true) {
                $array_merge = array_merge($array_saleskpi,$array_diff_sales);
                $array_sales = [];
                foreach($array_merge as $key){
                    if($key != $request->sub_manage_sales){
                        array_push($array_sales,$key);
                    }
                }
                $dataAssignSales = [
                    'staff_parent_id'=>$request->sub_manage_sales,
                ];
                $asc = RankService::updateRankSalesModel($array_sales, $dataAssignSales);
                if ($asc) {
                    return AbstractService::ResultSuccess($asc);
                } else {
                    return AbstractService::ResultError("An error occurred, please try again");
                }
            }
        }
    }
}
