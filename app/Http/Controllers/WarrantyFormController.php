<?php
/**
 * date:10/08/2021
 * content: hình thức bảo hành
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\WarrantyFormService;
use App\Services\AbstractService;
class WarrantyFormController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = WarrantyFormService::getModelByParamater($param,$rowsPerPage);
        return AbstractService::ResultData($results);
    }
    public function store(Request $request)
    {
        $data =$request->all();
        $query = WarrantyFormService::getByCode($request->warranty_form_code);
        if($query->count() > 0){
            return AbstractService::ResultError("Data registered before, please try again !");
        }else{
            $asc = WarrantyFormService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        try {
            $data = WarrantyFormService::getModelById($id);
            if($data)
            {
                return AbstractService::ResultSuccess($data);
            }
        } catch (Exception $e)
        {
            return AbstractService::ResultError("Data does not exist");
        }
    }
    public function update(Request $request, $id)
    {
        $data =$request->all();
        $query = WarrantyFormService::getByCodeEdit($request->warranty_form_code,$id);
        if($query->count() > 0){
            return AbstractService::ResultError("Data registered before, please try again !");
        }else {
            $asc = WarrantyFormService::updateModel($data, $id);
            if ($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
}
