<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\Stock\salesKpiPaymentService;
use App\Services\Stock\manageSalesKPIService;
use App\Services\AbstractService;
use App\Services\Stock\CustomerService;
use App\Services\DM\GeneralService;
use App\Services\Stock\PeriodService;
class ManageSalesKPIController extends Controller
{
    public function index(Request $request)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = manageSalesKPIService::getModelByParamater($param);
        $listAll =[];
        foreach ($results as $vale) {
            $trowSubSales = 0;
            $listSubSales = [];
            $dbSalesOfmanagesales = manageSalesKPIService::getListsalesOffsubManagesSales($vale['staff_kpi_sales_manage_id']);
            foreach ($dbSalesOfmanagesales as $val) {
                /** lấy ds nv sales thuộc nv cấp sub sales */
                $countSales = 0;
                $dbSalesOfSubsales = manageSalesKPIService::getListsalesOffSubSales($val['staff_kpi_sales_sub_id']);
                $trowSales = 0;
                $listSales = [];
                foreach ($dbSalesOfSubsales as $values) {
                    $countSales += 1;
                    $trowCustomers = 0;
                    $listCustomers = [];
                    /** lấy ds customers thuộc nv cấp sales */
                    $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSales($values['staff_kpi_sales_id']);
                    foreach ($dbCustomers as $value) {
                        $trowOrders = 0;
                        $listOrders = [];
                        /** lấy ds orders thuộc nv cấp sales */
                        $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSales($value['staff_kpi_customers_id']);
                        foreach ($dbOrders as $v) {
                            $trowKPIOders = 0;
                            $listKPIOders = [];
                            /** lấy ds kpi orders thuộc nv cấp sales */
                            $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSales($v['staff_kpi_period_stock_id']);
                            foreach ($dbKPICode as $vs) {
                                $trowKPIOders += 1;
                                $trowOrders += 1;
                                $trowCustomers += 1;
                                $trowSales += 1;
                                $trowSubSales += 1;
                                array_push($listKPIOders, [
                                    'id' => $vs['id'],
                                    'staff_kpi_code' => $vs['staff_kpi_code'],
                                    'brand_name' => $vs['kpiBrand']['brand_name'],
                                    'staff_kpi_sales_sub_status' => $vs['staff_kpi_sales_sub_status']
                                ]);
                            }
                            array_push($listOrders, [
                                'stock_code' => $v['kpiStocks']['stock_code'],
                                'trowKPIOders' => $trowKPIOders,
                                'listKPIOders' => $listKPIOders
                            ]);
                        }
                        array_push($listCustomers, [
                            'customer_name' => $value['kpiCustomers']['customers_name'],
                            'trowOrders' => $trowOrders,
                            'listOrders' => $listOrders
                        ]);
                    }
                    array_push($listSales, [
                        'sales_name' => $values['kpiStaffSales']['staff_name'],
                        'listCustomers' => $listCustomers,
                        'trowCustomers' => $trowCustomers
                    ]);
                }
                array_push($listSubSales, [
                    'sales_sub_name' => $val['kpiStaffSubSales']['staff_name'],
                    'trowSales' => $trowSales,
                    'listSales' => $listSales,
                ]);
            }
            array_push($listAll, [
                'sales_manage_name' => $vale['kpiStaffManageSales']['staff_name'],
                'staff_manage_id' => $vale['staff_kpi_sales_manage_id'],
                'trowSubSales' => $trowSubSales,
                'listSubSales' => $listSubSales,
            ]);
        }
        $count = $results->count();
        $branch = GeneralService::getBrand();
        $Userbranch = GeneralService::getBrandUser();
        $Staffs = GeneralService::getSalesManageManagePayment();
//        return AbstractService::ResultSuccess($listAll);
        return view("manageSalesKPI.index",compact('date_end','date_start','results','count','branch','Userbranch','Staffs','listAll'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $salesSub = GeneralService::getSalesManageManagePayment();
            $listAllPaymentPeriod=[];
            $Brand = CustomerService::getStaffCustomerBrand();
            $sales ='';
            $count = 0;
            return view('manageSalesKPI.create',compact('salesSub','listAllPaymentPeriod','Brand','sales','count'))->render();
        }
    }
    // lấy ds customer của nv sales cần thanh toán kpi
    public function customersPeriodKpi(Request $request){
        //lấy ds khách hàng
        $customersPeriodKpi = GeneralService::getStaffManaSalesPayment($request->sales_kpi);
        $array_id_sales = [];
        foreach ($customersPeriodKpi as $vs)
        {
            if($vs)
            {
                array_push($array_id_sales,$vs['id']);
            }
        }
        //ds mảng show phiếu thanh toán
        $results = manageSalesKPIService::getModelByManages($array_id_sales);
        $listAll =[];
        $stt=0;
        foreach ($results as $val){
            /** lấy ds nv sales thuộc nv cấp sub sales */
            $countSales=0;
            $dbSalesOfSubsales = manageSalesKPIService::getListCustomerOffManageSales($val['staff_kpi_sales_sub_id']);
            $trowSales=0;
            $listSales =[];
            foreach ($dbSalesOfSubsales as $values){
                $countSales +=1;
                $trowCustomers = 0;
                $listCustomers =[];
                /** lấy ds customers thuộc nv cấp sales */
                $dbCustomers = manageSalesKPIService::getListCustomerManageSales($values['staff_kpi_sales_id']);
                foreach ($dbCustomers as $value){
                    $trowOrders = 0;
                    $listOrders =[];
                    /** lấy ds orders thuộc nv cấp sales */
                    $dbOrders = manageSalesKPIService::getListOrdersOffManageSales($value['staff_kpi_customers_id']);
                    foreach ($dbOrders as $v){
                        $trowKPIOders = 0;
                        $listKPIOders =[];
                        /** lấy ds kpi orders thuộc nv cấp sales */
                        $dbKPICode = manageSalesKPIService::getListKPICodeOffManageSales($v['staff_kpi_period_stock_id']);
                        foreach ($dbKPICode as $vs){
                            $trowKPIOders +=1;
                            $trowOrders +=1;
                            $trowCustomers +=1;
                            $trowSales +=1;
                            $stt +=1;
                            array_push($listKPIOders,[
                                'id'=>$vs['id'],
                                'stt'=>$stt,
                                'staff_kpi_code'=>$vs['staff_kpi_code'],
                                'brand_name'=>$vs['kpiBrand']['brand_name'],
                                'staff_kpi_sales_sub_status'=>$vs['staff_kpi_sales_sub_status'],
                                'staff_kpi_sales_manage_status'=>$vs['staff_kpi_sales_manage_status']
                            ]);
                        }
                        array_push($listOrders,[
                            'stock_code'=>$v['kpiStocks']['stock_code'],
                            'trowKPIOders'=>$trowKPIOders,
                            'listKPIOders'=>$listKPIOders
                        ]);
                    }
                    array_push($listCustomers,[
                        'customer_name'=>$value['kpiCustomers']['customers_name'],
                        'trowOrders'=>$trowOrders,
                        'listOrders'=>$listOrders
                    ]);
                }
                array_push($listSales,[
                    'sales_name'=>$values['kpiStaffSales']['staff_name'],
                    'listCustomers'=>$listCustomers,
                    'trowCustomers'=>$trowCustomers
                ]);
            }
            array_push($listAll,[
                'sales_sub_name'=>$val['kpiStaffSubSales']['staff_name'],
                'sales_sub_id'=>$val['staff_kpi_sales_sub_id'],
                'trowSales'=>$trowSales,
                'listSales'=>$listSales,
            ]);
        }
        $salesSub = GeneralService::getSalesManageManagePayment();
        $Brand = CustomerService::getStaffCustomerBrand();
        $sales = $request->sales_kpi;
        $count = $results->count();
        return view('manageSalesKPI.create',compact('listAll','salesSub','stt','Brand','sales','count'))->render();
    }
    public function create(Request $request){
        $periodId = explode(",",$request->periodId);
        $arrayperiodId= [];
        foreach ($periodId as $p){
            if($p){
                array_push($arrayperiodId,$p);
            }
        }
        $data = [
            'staff_kpi_sales_manage_id'=>$request->staff_kpi_sales_id,
            'staff_kpi_sales_manage_status'=>1
        ];
        $asc = salesKpiPaymentService::updatePeriodStockKPIModelSubSales($arrayperiodId,$data);
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function edit($id)
    {

        $salesSub = GeneralService::getSalesManageManagePayment();
        //lấy ds khách hàng
        $customersPeriodKpi = GeneralService::getStaffManaSalesPayment($id);
        $array_id_sales = [];
        foreach ($customersPeriodKpi as $vs)
        {
            if($vs)
            {
                array_push($array_id_sales,$vs['id']);
            }
        }
        //ds mảng show phiếu thanh toán
        $results = manageSalesKPIService::getModelByManagesEdit($array_id_sales);
        $listAll =[];
        $stt=0;
        $count = $results->count();
        foreach ($results as $val){
            /** lấy ds nv sales thuộc nv cấp sub sales */
            $countSales=0;
            $dbSalesOfSubsales = manageSalesKPIService::getListsalesOffManageSales($val['staff_kpi_sales_sub_id']);
            $trowSales=0;
            $listSales =[];
            foreach ($dbSalesOfSubsales as $values){
                $countSales +=1;
                $trowCustomers = 0;
                $listCustomers =[];
                /** lấy ds customers thuộc nv cấp sales */
                $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSales($values['staff_kpi_sales_id']);
                foreach ($dbCustomers as $value){
                    $trowOrders = 0;
                    $listOrders =[];
                    /** lấy ds orders thuộc nv cấp sales */
                    $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSales($value['staff_kpi_customers_id']);
                    foreach ($dbOrders as $v){
                        $trowKPIOders = 0;
                        $listKPIOders =[];
                        /** lấy ds kpi orders thuộc nv cấp sales */
                        $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSales($v['staff_kpi_period_stock_id']);
                        foreach ($dbKPICode as $vs){
                            $trowKPIOders +=1;
                            $trowOrders +=1;
                            $trowCustomers +=1;
                            $trowSales +=1;
                            $stt +=1;
                            array_push($listKPIOders,[
                                'id'=>$vs['id'],
                                'stt'=>$stt,
                                'staff_kpi_code'=>$vs['staff_kpi_code'],
                                'brand_name'=>$vs['kpiBrand']['brand_name'],
                                'staff_kpi_sales_sub_status'=>$vs['staff_kpi_sales_sub_status'],
                                'staff_kpi_sales_manage_status'=>$vs['staff_kpi_sales_manage_status']
                            ]);
                        }
                        array_push($listOrders,[
                            'stock_code'=>$v['kpiStocks']['stock_code'],
                            'trowKPIOders'=>$trowKPIOders,
                            'listKPIOders'=>$listKPIOders
                        ]);
                    }
                    array_push($listCustomers,[
                        'customer_name'=>$value['kpiCustomers']['customers_name'],
                        'trowOrders'=>$trowOrders,
                        'listOrders'=>$listOrders
                    ]);
                }
                array_push($listSales,[
                    'sales_name'=>$values['kpiStaffSales']['staff_name'],
                    'listCustomers'=>$listCustomers,
                    'trowCustomers'=>$trowCustomers
                ]);
            }
            array_push($listAll,[
                'sales_sub_name'=>$val['kpiStaffSubSales']['staff_name'],
                'sales_sub_id'=>$val['staff_kpi_sales_sub_id'],
                'trowSales'=>$trowSales,
                'listSales'=>$listSales,
            ]);
        }
        $Brand = CustomerService::getStaffCustomerBrand();
        return view("manageSalesKPI.update",compact('listAll','salesSub','id','count','Brand'));
    }
    public function update(Request $request, $id)
    {
//        $customersPeriodKpi = GeneralService::getStaffManaSalesPayment($id);
//        $array_id_sales = [];
//        foreach ($customersPeriodKpi as $vs)
//        {
//            if($vs)
//            {
//                array_push($array_id_sales,$vs['id']);
//            }
//        }
        //ds mảng show phiếu thanh toán
        $results = manageSalesKPIService::getModelByEditManages($id);
        $periodId=[];
        foreach ($results as $val){
            array_push($periodId,$val->id);
        }
        $data = [
            'staff_kpi_sales_manage_id'=>null,
            'staff_kpi_sales_manage_status'=>null
        ];
        $asc = salesKpiPaymentService::updatePeriodStockKPIModelSubSales($periodId,$data);
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
