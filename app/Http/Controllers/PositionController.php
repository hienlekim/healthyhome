<?php
/**
 * date:10/08/2021
 * content:chức vụ nhân viên thủ kho, cửa hàng trưởng....
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\PositionService;
use App\Services\AbstractService;
class PositionController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = PositionService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("position.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('position.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "position_code"=>$request->position_code,
            "position_name"=>$request->position_name,
        ];
        $query = PositionService::getByCode($request->position_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = PositionService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = PositionService::getModelById($id);
        return view("position.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "position_name"=>$request->position_name
        ];
        $asc = PositionService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
