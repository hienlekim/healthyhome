<?php
/**
 * date:10/08/2021
 * content: quốc gia có 2 chính đó là việt nam, campuchia
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\CountryService;
use App\Services\AbstractService;

class CountryController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results =  CountryService::getModelByParamater($param, $rowsPerPage);
        $count = $results->count();
        return view("country.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('country.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "country_code"=>$request->country_code,
            "country_name"=>$request->country_name,
        ];
        $query = CountryService::getByCode($request->country_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = CountryService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = CountryService::getModelById($id);
        return view("country.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "country_name"=>$request->country_name
        ];
        $asc = CountryService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
