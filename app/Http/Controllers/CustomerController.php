<?php
/**
 * date:10/08/2021
 * content: khách hàng, nếu khách giới thiệu được khách hàng thì sẽ được tặng quà,
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Services\DM\GeneralService;
use App\Services\Stock\PeriodService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\Stock\CustomerService;
use App\Services\AbstractService;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'SreachName'=>$request->input('SreachName'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = CustomerService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("customers.index",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $customers =CustomerService::getCustomerParent();
            $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
            $code = GeneralService::getCustomersTypeQuery();
            return view('customers.create',compact('StaffCustomerBrand','customers','code'))->render();
        }
    }
    public function customerType(Request $request)
    {
        $data = GeneralService::getCustomersTypeQuery();
        return AbstractService::DataSuccess($data);
    }
    public function create(Request $request){
        $query = CustomerService::getByCode($request->customers_code);
        if($query->count() > 0){
            $code = GeneralService::getCustomersTypeQuery();
            $customers_code = $code;
        }else{
            $customers_code = $request->customers_code;
        }
        $data=[
            "customers_code"=>$customers_code,
            "customers_name"=>$request->customers_name,
            "customers_staffs_id"=>$request->customers_staffs_id,
            "customers_brand_id"=>$request->customers_brand_id,
            "customers_parent_id"=>$request->customers_parent_id,
            'customers_status'=>0,
            "customers_email"=>$request->customers_email,
            "car"=>$request->car,
            "house_type"=>$request->house_type,
            "level"=>$request->level,
            "customers_phone"=>$request->customers_phone,
        ];
        $asc = CustomerService::createModel($data);
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function edit($id)
    {
        $data = CustomerService::getModelById($id);
        if($data['customers_parent_id']==''){
            $customer_name='';
        }else{
            $customer =CustomerService::getCustomerParentEdit($data['customers_parent_id']);
            $customer_name=$customer->customers_name;
        }
        $customer_id=$data->id;
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $customerChildrent = CustomerService::getCustomerChildrent($id);
        $count = $customerChildrent->count();
        //hiển thị ds phiếu thanh toán theo đơn hàng
        $Period =  PeriodService::getModelByCustomer($data['id']);
        //lấy ds phiếu thanh toán theo đơn hàng
        $periodPayment =  PeriodService::getByCustomerPeriodPayment($data['id']);
        //ds mảng show phiếu thanh toán
        $listPaymentPeriod =[];
        foreach ($Period as $key){
            $listpayment =[];
            $trow=0;
            foreach ($periodPayment as $val){
                if($key->period_stock_id == $val->period_stock_id){
                    $trow +=1;
                    array_push($listpayment, [
                        'period_name'=>$val['period_name'],
                        'payments_code'=>$val['periodpayments']['payments_code'],
                        'staff_name'=>$val['periodstaff']['staff_name'],
                        'period_amount'=>$val['period_amount'],
                        'period_amount_batch'=>$val['period_amount_batch'],
                        'period_date'=>$val['period_date'],
                        'period_date_reality'=>$val['period_date_reality'],
                        'period_status'=>$val['period_status'],
                    ]);
                }
            }
            array_push($listPaymentPeriod,[
                'stock_code'=>$key['periodstock']['stock_code'],
                'trow'=>$trow,
                'listpayment'=>$listpayment
            ]);
        }
        return view("customers.update",compact('data','Period','customer_name','StaffCustomerBrand','customerChildrent','count','customer_id','periodPayment','listPaymentPeriod'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "customers_address_edit"=>$request->customers_address,
            "customers_name_edit"=>$request->customers_name,
            "customers_email_edit"=>$request->customers_email,
            "car_edit"=>$request->car,
            "house_type_edit"=>$request->house_type,
            "level_edit"=>$request->level,
            "customers_phone_edit"=>$request->customers_phone,
        ];
        $asc = CustomerService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function staffMeetCustomersList(){
        $date_start = date('Y-m-01');
        $date_end = date('Y-m-t');
        $param=[
            'fromDate'=>$date_start,
            'toDate'=>$date_end,
        ];
        $results = CustomerService::getByParamaterMeet($param);
        $list = [];
        $stt = 0;
        foreach ($results as $key){
            $stt +=1;
            $count = CustomerService::getByCountCustomers($param,$key->customers_staffs_id);
            array_push($list,[
                'stt'=>$stt,
                'staff_name'=>$key['customerStaffs']['staff_name'],
                'customers_staffs_id'=>$key['customers_staffs_id'],
                'counts_customers'=>$count,
                'created_at'=>$key['created_at']
            ]);
        }
        return view("customers.staff_meet_customers_list",compact('list','results'));
    }
    function staffMeetCustomersEdit($id){
        $date_start = date('Y-m-01');
        $date_end = date('Y-m-t');
        $param=[
            'fromDate'=>$date_start,
            'toDate'=>$date_end,
        ];
        $results = CustomerService::getByCustomersofStaff($param,$id);
        return view("customers.staff_meet_customers_edit",compact('results'));
    }
}
