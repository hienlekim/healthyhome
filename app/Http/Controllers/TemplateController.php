<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\UnitService;
use App\Services\AbstractService;
use App\Services\DM\GeneralService;
use DB;
class TemplateController extends Controller
{
    public function index()
    {
        $id_user =\Illuminate\Support\Facades\Auth::user()->id;
        $result =  $staffs = Staff::select("staff.*")
            ->with('brands','brands.currencies')
            ->with('Users')
            ->with('decenTLizaTion','decenTLizaTion.positions')
            ->where('staff.staff_user_id',$id_user)->first();
        return view("template",compact('result'));
    }
    /** khi nhân viên sale chọn sp thì được load lên */
    public function indexNotification(Request $request)
    {
        $notification = DB::table('receipts')->select('receipts.receipts_code','receipts.id as id','b.customers_name','c.staff_name')
            ->leftjoin('customers as b','receipts.receipts_customer_id','=','b.id')
            ->leftjoin('staff as c','receipts.receipts_staff_id','=','c.id')
            ->where('receipts.receipts_status','=','2')->groupBy('receipts.receipts_code')->get();
        $count = $notification->count();
        $data =[
            'notification'=>$notification,
            'count'=>$count,
        ];
        return AbstractService::DataSuccess($data);
    }
}
