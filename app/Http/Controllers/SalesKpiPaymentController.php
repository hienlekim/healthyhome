<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\Stock\salesKpiPaymentService;
use App\Services\AbstractService;
use App\Services\Stock\CustomerService;
use App\Services\DM\GeneralService;
use App\Services\Stock\PeriodService;
use function GuzzleHttp\Psr7\str;

class SalesKpiPaymentController extends Controller
{
    public function index(Request $request)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = salesKpiPaymentService::getModelByParamater($param);
        $listAll =[];
        $countCustomers =0;
        $countOders =0;
        $countKPI =0;
        foreach ($results as $val){
            /** lấy ds customers thuộc nv cấp sales */
            $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSales($val['staff_kpi_sales_id']);
            $listCustomer =[];
            $trowParent=0;
            $countRowsCustomers=0;
            foreach ($dbCustomers as $key){
                $countCustomers +=1;
                $countRowsCustomers +=1;
                /** lấy ds order thuộc customers nv cấp sales */
                $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSales($key['staff_kpi_customers_id']);
                $listOrders =[];
                $trowOrders =0;
                $countRowsOders =0;
                foreach ($dbOrders as $ks){
                    $countOders +=1;
                    $countRowsOders +=1;
                    /** lấy ds kpoi_code thuộc stock_code nv cấp sales */
                    $listKPICode =[];
                    $trowKPICode =0;
                    $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSales($ks['staff_kpi_period_stock_id']);
                    $countRowsKPI=0;
                    foreach ($dbKPICode as $ki){
                        $trowKPICode +=1;
                        $trowOrders +=1;
                        $trowParent +=1;
                        $countKPI +=1;
                        $countRowsKPI +=1;
                        array_push($listKPICode,[
                            'id'=>$ki['id'],
                            'staff_kpi_code'=>$ki['staff_kpi_code'],
                            'brand_name'=>$ki['kpiBrand']['brand_name'],
                            'staff_kpi_sales_status'=>$ki['staff_kpi_sales_status']
                        ]);
                    }
                    /** end */
                    array_push($listOrders,[
                        'stock_code'=>$ks['kpiStocks']['stock_code'],
                        'trowKPICode'=>$trowKPICode,
                        'listKPICode'=>$listKPICode,
                        'countRowsKPI'=>$countRowsKPI,
                    ]);
                }
                /** end */
                /** lấy ds customers thuộc nv cấp sales */
                array_push($listCustomer,[
                    'customers_name'=>$key['kpiCustomers']['customers_name'],
                    'trowOrders'=>$trowOrders,
                    'listOrders'=>$listOrders,
                    'countRowsOders'=>$countRowsOders,
                ]);
            }
            if($val['kpiStaffSales']['ranks']==null){
                $ranks = "Sales";
            }else{
                $ranks = $val['kpiStaffSales']['ranks']['rank_name'];
            }
            array_push($listAll,[
                'staffs_name'=>$val['kpiStaffSales']['staff_name'],
                'ranks'=>$ranks,
                'trowParent'=>$trowParent,
                'listCustomer'=>$listCustomer,
                'countCustomers'=>$countCustomers,
                'countOders'=>$countOders,
                'countRowsCustomers'=>$countRowsCustomers,
            ]);
        }
        $count = $results->count();
        $branch = GeneralService::getBrand();
        $Userbranch = GeneralService::getBrandUser();
        $Staffs = GeneralService::getStaffRankKpi();
//        return AbstractService::DataSuccess($listAll);
        return view("KPISales.index",compact('date_end','date_start','results','count','branch','Userbranch','Staffs','listAll'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $salesKpi = GeneralService::getSalesKpiPayment();
            $listAllPaymentPeriod=[];
            $Brand = CustomerService::getStaffCustomerBrand();
            $sales ='';
            return view('KPISales.create',compact('salesKpi','listAllPaymentPeriod','Brand','sales'))->render();
        }
    }
    // lấy ds customer của nv sales cần thanh toán kpi
    public function customersPeriodKpi(Request $request){
        //lấy ds khách hàng
        $customersPeriodKpi = GeneralService::getcustomersPeriodKpi($request->sales_kpi);
        //ds mảng show phiếu thanh toán
        $listAllPaymentPeriod =[];
        $stt=0;
        foreach ($customersPeriodKpi as $key){
            //gom ds đơn hàng theo khách hàng
            $PeriodPayment =  GeneralService::getCustomerPaymentKpi($key['id']);
            $listOdersPayment =[];
            $trowParent=0;
            foreach ($PeriodPayment as $val){
                //hiển thị ds kh có đơn hàng
                $PeriodPaymentCustomers =  GeneralService::getPeriodPaymentCustomersAll($val['period_customers_id'],$val['period_stock_id']);
                $trowChildrent = 0;
                $listOdersPeriod = [];
                foreach ($PeriodPaymentCustomers as $k){
                    $trowChildrent +=1;
                    $trowParent +=1;
                    $stt +=1;
                    if($k['period_status']==0){
                        $period_status="Đã thanh toán";
                    }else{
                        $period_status="Chưa thanh toán";
                    }
                    if($k['period_kpi_status']==1){
                        $period_kpi_status="Đã thanh toán";
                    }else{
                        $period_kpi_status="Chưa thanh toán";
                    }
                    array_push($listOdersPeriod,[
                        'id'=>$k['id'],
                        'stt'=>$stt,
                        'period_customers_id'=>$k['period_customers_id'],
                        'period_stock_id'=>$k['period_stock_id'],
                        'period_name'=>$k['period_name'],
                        'period_status'=>$period_status,
                        'period_kpi_status'=>$period_kpi_status
                    ]);
                }
                array_push($listOdersPayment, [
                    'stock_code'=>$val['periodstock']['stock_code'],
                    'trowChildrent'=>$trowChildrent,
                    'listOdersPeriod'=>$listOdersPeriod,
                ]);
            }
            array_push($listAllPaymentPeriod,[
                'customer_name'=>$key['customers_name'],
                'trowParent'=>$trowParent,
                'listOdersPayment'=>$listOdersPayment
            ]);
        }
        $salesKpi = GeneralService::getSalesKpiPayment();
        $Brand = CustomerService::getStaffCustomerBrand();
        $sales = $request->sales_kpi;
        return view('KPISales.create',compact('listAllPaymentPeriod','salesKpi','stt','Brand','sales'))->render();
    }
    public function create(Request $request){
        $periodCustomersId = array_unique(explode(",",$request->periodCustomersId));
        $periodId = explode(",",$request->periodId);
        $arrayperiodId= [];
        foreach ($periodId as $p){
            if($p){
                array_push($arrayperiodId,$p);
            }
        }
        /**  list ds ma phieu KPI */
        $staffKpiCode = GeneralService::getStaffKpiCodeQuery();
        /**  lấy ds customer*/
        $stt=0;
        foreach ($periodCustomersId as $val){
            /**lấy được stock_id thuộc customer SELECT `period_stock_id` FROM `period` WHERE `period_customers_id`=16 and `id` in (1,2,3,5,6) group by `period_stock_id` */
            if($val){
                $listPeriodStock = salesKpiPaymentService::getListPeriodGrouByStockId($val,$arrayperiodId);
                /**tiến ành thêm mới kpi sales đã tạo*/
                foreach($listPeriodStock as $ks) {
                    $stt +=1;
                    $t=1;
                    if($staffKpiCode){
                        $str = explode("KPI",$staffKpiCode);
                        $sum = $str[1]+$stt;
                        if($sum <10){
                            $subString = 'KPI0000'.$sum;
                        }elseif ($sum >9 && $sum < 100){
                            $subString = 'KPI000'.$sum;
                        }elseif ($sum >99 && $sum < 1000){
                            $subString = 'KPI00'.$sum;
                        }elseif ($sum >999 && $sum < 10000){
                            $subString = 'KPI0'.$sum;
                        }
                    }else{
                        $sum = $stt;
                        $subString = 'KPI0000'.$sum;
                    }
                    $periodStock = salesKpiPaymentService::getListPeriodStockId($val,$arrayperiodId,$ks['period_stock_id']);
                    $arrayId =[];
                    //  lấy id của ds theo stock customer
                    foreach ($periodStock as $k){
                        array_push($arrayId,$k['id']);
                    }
                    $IdPeriod = implode(",", $arrayId);
                    $data = [
                        'staff_kpi_code'=>$subString,
                        'staff_kpi_order_brand_id'=>$request->staff_kpi_order_brand_id,
                        'staff_kpi_period_stock_id'=>$ks['period_stock_id'],
                        'staff_kpi_customers_id'=>$val,
                        'staff_kpi_sales_id'=>$request->staff_kpi_sales_id,
                        'staff_kpi_sales_status'=>1,
                        'staff_kpi_order_period_id'=>$IdPeriod,
                    ];
                    $ascCreate = salesKpiPaymentService::createModel($data);
                }
            }
        }
        if($ascCreate==true){
            /** sau khi thêm thành công thì cập nhật staus_kpi  Period*/
            $asc = salesKpiPaymentService::updatePeriodStockKPIModel($periodId);
        }
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function edit($id)
    {
        $data = salesKpiPaymentService::getModelById($id);
        $salesKpi = GeneralService::getSalesKpiPaymentid($data->staff_kpi_sales_id);
        $Brand = CustomerService::getStaffCustomerBrand();
        //lấy ds khách hàng
        $kpi_order_period_id =explode(',', $data->staff_kpi_order_period_id);
        $customersPeriodKpi = GeneralService::getcustomersPeriodKpiEdit($kpi_order_period_id);
        //ds mảng show phiếu thanh toán
        $listAllPaymentPeriod =[];
        $stt=0;
        foreach ($customersPeriodKpi as $key){
            //gom ds đơn hàng theo khách hàng
            $PeriodPayment =  GeneralService::getCustomerPaymentKpiEdit($kpi_order_period_id);
            $listOdersPayment =[];
            $trowParent=0;
            foreach ($PeriodPayment as $val){
                //hiển thị ds kh có đơn hàng
                $PeriodPaymentCustomers =  GeneralService::getPeriodPaymentCustomersAllEdit($data->staff_kpi_order_period_id);
                $trowChildrent = 0;
                $listOdersPeriod = [];
                foreach ($PeriodPaymentCustomers as $k){
                    $trowChildrent +=1;
                    $trowParent +=1;
                    $stt +=1;
                    if($k['period_status']==0){
                        $period_status="Đã thanh toán";
                    }else{
                        $period_status="Chưa thanh toán";
                    }
                    if($k['period_kpi_status']==1){
                        $period_kpi_status="Đã thanh toán";
                    }else{
                        $period_kpi_status="Chưa thanh toán";
                    }
                    array_push($listOdersPeriod,[
                        'id'=>$k['id'],
                        'stt'=>$stt,
                        'period_customers_id'=>$k['period_customers_id'],
                        'period_stock_id'=>$k['period_stock_id'],
                        'period_name'=>$k['period_name'],
                        'period_status'=>$period_status,
                        'period_kpi_status'=>$period_kpi_status
                    ]);
                }
                array_push($listOdersPayment, [
                    'stock_code'=>$val['periodstock']['stock_code'],
                    'trowChildrent'=>$trowChildrent,
                    'listOdersPeriod'=>$listOdersPeriod,
                ]);
            }
            array_push($listAllPaymentPeriod,[
                'customer_name'=>$key['customers_name'],
                'trowParent'=>$trowParent,
                'listOdersPayment'=>$listOdersPayment
            ]);
        }
        return view("KPISales.update",compact('data','salesKpi','listAllPaymentPeriod','Brand'));
    }
    public function update(Request $request, $id)
    {
        $data = salesKpiPaymentService::getModelById($id);
        $kpi_order_period_id = explode(',',$data->staff_kpi_order_period_id);
        $asc = salesKpiPaymentService::updatePeriodStockKPIModelEdit($kpi_order_period_id);
        if ($asc) {
            $ascs = salesKpiPaymentService::removeModel($id);
            return AbstractService::ResultSuccess($ascs);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    /** ds công nợ của khách hàng */
    public function debtCustomer(Request $request)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $branch = GeneralService::getBrand();
        $Userbranch = GeneralService::getBrandUser();
        //lấy ds khách hàng
        $customersPeriodKpi = salesKpiPaymentService::getModelPeriodByParamater($param);
        $count = $customersPeriodKpi->count();
        //ds mảng show phiếu thanh toán
        $listAllPaymentPeriod =[];
        $stt=0;
        foreach ($customersPeriodKpi as $key){
            //gom ds đơn hàng theo khách hàng
            $PeriodPayment =  GeneralService::getCustomerPayment($key['period_customers_id']);
            $listOdersPayment =[];
            $trowParent=0;
            foreach ($PeriodPayment as $val){
                //hiển thị ds kh có đơn hàng
                $PeriodPaymentCustomers =  GeneralService::getPeriodPayments($val['period_customers_id'],$val['period_stock_id']);
                $trowChildrent = 0;
                $listOdersPeriod = [];
                foreach ($PeriodPaymentCustomers as $k){
                    $trowChildrent +=1;
                    $trowParent +=1;
                    $stt +=1;
                    array_push($listOdersPeriod,[
                        'id'=>$k['id'],
                        'period_customers_id'=>$k['period_customers_id'],
                        'period_stock_id'=>$k['period_stock_id'],
                        'period_name'=>$k['period_name'],
                        'period_status'=>$k['period_status'],
                        'period_date'=>$k['period_date'],
                        'period_date_reality'=>$k['period_date_reality'],
                    ]);
                }
                array_push($listOdersPayment, [
                    'stock_code'=>$val['periodstock']['stock_code'],
                    'trowChildrent'=>$trowChildrent,
                    'listOdersPeriod'=>$listOdersPeriod,
                ]);
            }
            array_push($listAllPaymentPeriod,[
                'customer_name'=>$key['periodcustomer']['customers_name'],
                'brand'=>$key['periodbranch']['brand_name'],
                'staffs_name'=>$key['periodstaff']['staff_name'],
                'trowParent'=>$trowParent,
                'listOdersPayment'=>$listOdersPayment
            ]);
        }
        //lấy ds khách hàng
        $customers = salesKpiPaymentService::getListGroupCustomers();
        return view('KPISales.debtCustomer',compact('date_start','date_end','count','listAllPaymentPeriod','Userbranch','branch','customers','customersPeriodKpi'))->render();
    }
}
