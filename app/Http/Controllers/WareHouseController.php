<?php
/**
 * date:10/08/2021
 * content: kho hàng mỗi chi nhánh có 1 hoặc 2 kho
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\WareHouseService;
use App\Services\AbstractService;
class WareHouseController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = WareHouseService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("warehouses.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('warehouses.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "ware_house_code"=>$request->ware_house_code,
            "ware_house_name"=>$request->ware_house_name,
        ];
        $query = WareHouseService::getByCode($request->ware_house_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = WareHouseService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = WareHouseService::getModelById($id);
        return view("warehouses.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "ware_house_name"=>$request->ware_house_name
        ];
        $asc = WareHouseService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
