<?php
/**
 * date:10/08/2021
 * content: khách hàng, nếu khách giới thiệu được khách hàng thì sẽ được tặng quà,
 * khi khách hàng chốt đơn sẽ được chăm sóc khách định kỳ 3 tháng 1 lần
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Services\DM\GeneralService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\Stock\CustomerService;
use App\Services\Stock\CustomerCareService;
use App\Services\AbstractService;

class CustomerCareController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = CustomerCareService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        $brandUser = GeneralService::getBrandUser();
        $date = date('Y-m-d');
        return view("customer Care.index",compact('brandUser','date','date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    public function customersCare($id)
    {
        $data =  CustomerCareService::getModelById($id);
        $customerCare = CustomerCareService::countTimeCustomer($data->customer_care_date_end);
        $dataCustomerCare =[
            'customer_care_date'=>$data->customer_care_date_end,
            'customer_care_date_end'=>$customerCare,
            'customer_care_status'=>2,
        ];
        $asc = CustomerCareService::updateModel($id,$dataCustomerCare);
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    //khách hàng nhận quà
    public function CustomerGifts(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = CustomerCareService::getModelCustomerGiftByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        $branchStaff = CustomerCareService::getbranchStaff();
        $brandUser = GeneralService::getBrandUser();
        $OdersGift = CustomerCareService::getQueryOdersGift();
        return view("customer Care.customerGift",compact('date_end','date_start','brandUser','branchStaff','results','count','StaffCustomerBrand','branch','OdersGift'));
    }
}
