<?php

namespace App\Http\Controllers;
use App\Services\Stock\CustomerService;
use App\Services\Stock\CustomerCareService;
use App\Services\Stock\ordersGiftService;
use App\Services\Stock\PeriodService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\GeneralService;
use App\Services\DM\HistoryService;
use App\Services\AbstractService;

class ordersGiftController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = ordersGiftService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("orderGift.index",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    /** khi nhân viên sale chọn sp thì được load lên */
    public function stockListPurchases(Request $request,$id)
    {
        $product = ordersGiftService::getQueryProduct($id);
        $productSum = ordersGiftService::getQuerySumSlProduct($id);
        $data = [ 'product'=>$product,
            'productSum'=>$productSum,
        ];
        return AbstractService::DataSuccess($data);
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $name_user = \Illuminate\Support\Facades\Auth::user()->name;
            $brandUser = GeneralService::getBrandUser();
            $Customer = GeneralService::getOdersGiftCustomer()->get();
            $products = GeneralService::getProductQuery();
            if($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators')
                $brandUsers = "";
            else
                $brandUsers = $brandUser['brands']['brand_ware_house_id'];
            $location = GeneralService::getLocationQuery($brandUsers);
            $unit = GeneralService::getUnitQuery();
            $payment = GeneralService::getPaymentQuery();
            $productType = GeneralService::getproductTypeQuery();
            $Stockcode = GeneralService::getStocksGiftTypeQuery();
            return view('orderGift.create',compact('Customer','products','location','brandUser','name_user','unit','productType','payment','Stockcode'))->render();
        }
    }
    public function create(Request $request){
        $brandUser = GeneralService::getBrandUser();
        $query = ordersGiftService::getByCode($request->stock_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            for($i=1;$i<5;$i++){
                $product = 'stock_product_id_'.$i;
                $unit = 'stock_product_unit_id_'.$i;
                $stock_quality = 'stock_quality_reality_'.$i;
                $stock_product_price_ex = 'product_price_ex_'.$i;
                $stock_amount = 'stock_amount_'.$i;
                if($request->$stock_quality > 0){
                    $data=[
                        "stock_code"=>$request->stock_code,
                        "stock_customer_id"=>$request->stock_customer_id,
                        "stock_location"=>$request->stock_location,
                        "stock_staff_id"=>$brandUser['id'],
                        "stock_date_start_stocker"=>date("Y-m-d H:i:s"),
                        "stock_brand_id"=>$brandUser['brands']['id'],
                        "stock_description"=>$request->stock_description,
                        'stock_currency_id'=>$request->stock_currency_id,
                        "stock_status"=>1,
                        "Type"=>"XUATKHOQUATANG",
                        "stock_type"=>$request->product_type_name,
                        "stock_product_id"=>$request->$product,
                        "stock_product_unit_id"=>$request->$unit,
//                        'stock_price_export'=>str_replace(".","",$request->$stock_product_price_ex),
                        "stock_quality_reality"=>-str_replace(".","",$request->$stock_quality),
//                        "stock_amount"=>-str_replace(".","",$request->$stock_amount),
                    ];
                    $asc = ordersGiftService::createModel($data);
                }
            }
            if($asc) {
//                cap nhaatj trang thai dang no cuar khach CustomerService
                $dataCustom = ['customers_status'=>2,'customers_stock_id'=>$asc->id];
                CustomerService::updateModel($asc->stock_customer_id,$dataCustom);
                $sumAmount = ordersGiftService::getCodeSumAmount($asc->stock_code);
                /**lich su*/
                $data_his = [
                    'history_code'=>$asc->stock_code,
                    'history_name'=>'Xuất kho quà tặng',
                    'history_staff_id'=>$asc->stock_staff_id,
                    'history_action'=>'tạo mới',
//                    'history_price'=>-$sumAmount
                ];
                HistoryService::createModel($data_his);
                /**kết thúc lịch sử*/
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = ordersGiftService::getModelById($id);
        $stockCode =  ordersGiftService::getByCode($data->stock_code)->Max('id');
        $dataProduct = ordersGiftService::getModelByProduct($data->stock_code);
        $brandUser = GeneralService::getBrandUser();
        $Customer = GeneralService::getOdersCustomerBrandAll();
        $products = GeneralService::getProductQuery();
        $location = GeneralService::getLocationAll();
        $unit = GeneralService::getUnitQuery();
        $payment = GeneralService::getPaymentQuery();
        $productType = GeneralService::getproductTypeQuery();
        return view('orderGift.approve', compact('dataProduct','stockCode','data','Customer', 'products', 'location', 'brandUser', 'unit', 'productType', 'payment'))->render();
    }
    public function update(Request $request, $id)
    {
        $data = ordersGiftService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = ordersGiftService::getModelByProduct($data->stock_code);
        $dataList = [
            'stock_status'=>0
        ];
        foreach ($dataProduct as $item){
            $asc = ordersGiftService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Hủy đơn hàng quà tặng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'CancelOderGift',
            ];
            HistoryService::createModel($data_his);
            return AbstractService::ResultSuccess($asc);

        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function sendApprove(Request $request, $id)
    {
//        $data = ordersGiftService::getModelById($id);
//        $brandUser = GeneralService::getBrandUser();
//        $dataProduct = ordersGiftService::getModelByProduct($data->stock_code);
//        $dataList = [
//            'stock_status'=>3
//        ];
//        foreach ($dataProduct as $item){
//            $asc = ordersGiftService::updateModel($item->id,$dataList);
//        }
//        if($asc) {
//            /**lich su*/
//            $data_his = [
//                'history_code'=>$data->stock_code,
//                'history_name'=>'Gửi duyệt đơn hàng quà tặng',
//                'history_staff_id'=>$brandUser['id'],
//                'history_action'=>'SendApproveOderGift',
//            ];
//            HistoryService::createModel($data_his);
//            //nếu khách đã nhận quà
//            $dataCustom = ['customers_status'=>2];
//            CustomerService::updateModel($data->stock_customer_id,$dataCustom);
//            return AbstractService::ResultSuccess($asc);
//        } else {
//            return AbstractService::ResultError("An error occurred, please try again");
//        }
        $data = ordersGiftService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = ordersGiftService::getModelByProduct($data->stock_code);
        $date = date('Y-m-d');
        $dataList = [
            'stock_status'=>3,
            'stock_staff_stocker_id'=>$brandUser['id'],
            'stock_date_start_stocker'=>$date
        ];
        foreach ($dataProduct as $item){
            $asc = ordersGiftService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Thủ kho duyệt đơn hàng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'ApproveOderGift',
            ];
            HistoryService::createModel($data_his);
            //nếu khách đã nhận quà
            $dataCustom = ['customers_status'=>2];
            CustomerService::updateModel($data->stock_customer_id,$dataCustom);
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function Approve(Request $request, $id)
    {
        $data = ordersGiftService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = ordersGiftService::getModelByProduct($data->stock_code);
        $date = date('Y-m-d');
        $dataList = [
            'stock_status'=>3,
            'stock_staff_stocker_id'=>$brandUser['id'],
            'stock_date_start_stocker'=>$date
        ];
        foreach ($dataProduct as $item){
            $asc = ordersGiftService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Thủ kho duyệt đơn hàng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'ApproveOderGift',
            ];
            HistoryService::createModel($data_his);
            //nếu khách đã nhận quà
            $dataCustom = ['customers_status'=>2];
            CustomerService::updateModel($data->stock_customer_id,$dataCustom);
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function cancelApprove(Request $request, $id)
    {
        $data = ordersGiftService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = ordersGiftService::getModelByProduct($data->stock_code);
        $dataList = [
            'stock_status'=>1
        ];
        foreach ($dataProduct as $item){
            $asc = ordersGiftService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Thủ kho hủy duyệt đơn hàng qùa tặng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'CancelApproveOderGift',
            ];
            HistoryService::createModel($data_his);
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
