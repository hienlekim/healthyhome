<?php
/**
 * date:10/08/2021
 * content: chi nhánh,sẽ có 2 chi nhánh
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\BrandService;
use App\Services\DM\GeneralService;
use App\Services\AbstractService;

class BrandController extends Controller
{

    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param = [
            'searchInput' => $request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = BrandService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("Brand.index",compact('results','count'));
    }

    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $dataWareHouse = GeneralService::getWareHouseQuery();
            $dataBank = GeneralService::getBankQuery();
            $dataCountry = GeneralService::getCountryQuery();
            $dataCurrency = GeneralService::getCurrencyQuery();
            return view('Brand.create',compact('dataWareHouse','dataBank','dataCountry','dataCurrency'))->render();
        }
    }
    public function create(Request $request){
        $data=[
            "brand_code"=>$request->brand_code,
            "brand_name"=>$request->brand_name,
            "brand_address"=>$request->brand_address,
            "brand_country_id"=>$request->brand_country_id,
            "brand_bank_id"=>$request->brand_bank_id,
            "brand_bank_id_1"=>$request->brand_bank_id_1,
            "brand_bank_id_2"=>$request->brand_bank_id_2,
            "brand_bank_id_3"=>$request->brand_bank_id_3,
            "brand_bank_id_4"=>$request->brand_bank_id_4,
            "brand_currency_id"=>$request->brand_currency_id,
            "brand_ware_house_id"=>$request->brand_ware_house_id
        ];
        $query = BrandService::getByCode($request->brand_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = BrandService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = BrandService::getModelById($id);
        $dataWareHouse = GeneralService::getWareHouseQuery();
        $dataBank = GeneralService::getBankQuery();
        $dataCountry = GeneralService::getCountryQuery();
        $dataCurrency = GeneralService::getCurrencyQuery();
        return view("Brand.update",compact('data','dataWareHouse','dataBank','dataCountry','dataCurrency'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "brand_name"=>$request->brand_name,
            "brand_address"=>$request->brand_address,
            "brand_country_id"=>$request->brand_country_id,
            "brand_bank_id"=>$request->brand_bank_id,
            "brand_bank_id_1"=>$request->brand_bank_id_1,
            "brand_bank_id_2"=>$request->brand_bank_id_2,
            "brand_bank_id_3"=>$request->brand_bank_id_3,
            "brand_bank_id_4"=>$request->brand_bank_id_4,
            "brand_currency_id"=>$request->brand_currency_id,
            "brand_ware_house_id"=>$request->brand_ware_house_id
        ];
        $asc = BrandService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
