<?php
/**
 * date:10/08/2021
 * content: bàn giao công việc,
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Services\DM\GeneralService;
use App\Services\Staff\StaffService;
use App\Services\Stock\PeriodService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\Stock\StockService;
use App\Services\Stock\CustomerCareService;
use App\Services\Stock\CustomerService;
use App\Services\Stock\ReceiptsService;
use App\Services\AbstractService;

class customerHandoverStaffController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = CustomerService::getModelCustomerHandoverByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("customerHandoverStaff.index",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $customers =CustomerService::getCustomersHandoverList();
            $customersCount = $customers->count();
            $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
            $Staffs = CustomerService::getStaff();
            $brandUser = GeneralService::getBrandUser();
            return view('customerHandoverStaff.create',compact('brandUser','Staffs','customersCount','StaffCustomerBrand','customers'))->render();
        }
    }
    public function create(Request $request){
        $date = date('Y-m-d');
        $data=[
            "customers_user_created"=>$request->customers_staff,
            "customers_staffs_id"=>$request->employee_takes_the_job,
            "customers_handover_status"=>1,
            "customers_handover_date_start"=>$date,
        ];
        $customerUpdate= CustomerService::getCustomerHandoverEdit($request->customers_staffs_id);
        if($customerUpdate){
            foreach ($customerUpdate as $key){
                $asc = CustomerService::updateModel($key->id,$data);
            }
            if($asc) {
                $PeriodUpdate= CustomerService::getPeriodHandoverEdit($request->customers_staffs_id);
                $dataPeriod=[
                    "period_user_created"=>$request->customers_staff,
                    "period_staff_id"=>$request->employee_takes_the_job
                ];
                if($PeriodUpdate->count()>0){
                    foreach ($PeriodUpdate as $item){
                        PeriodService::updateModel($item->id,$dataPeriod);
                    }
                }
                //stock orders
                $OrdersUpdate= CustomerService::getStockHandoverEdit($request->customers_staffs_id);
                $dataOrders=[
                    "stock_user_created"=>$request->customers_staff,
                    "stock_staff_id"=>$request->employee_takes_the_job
                ];
                if($OrdersUpdate->count()>0){
                    foreach ($OrdersUpdate as $item){
                        StockService::updateModel($item->id,$dataOrders);
                    }
                }
                //Customer care
                $customersCareUpdate= CustomerService::getCustomersCareHandoverEdit($request->customers_staffs_id);
                $datacustomers=[
                    "customer_care_user_created"=>$request->customers_staff,
                    "customer_staff_id"=>$request->employee_takes_the_job
                ];
                if($customersCareUpdate->count()>0){
                    foreach ($customersCareUpdate as $item){
                        CustomerCareService::updateModel($item->id,$datacustomers);
                    }
                }
                //phiếu thu
                $reciptsUpdate= CustomerService::getReciptskHandoverEdit($request->customers_staffs_id);
                $dataRecipts=[
                    "receipts_user_create"=>$request->customers_staff,
                    "receipts_staff_id"=>$request->employee_takes_the_job
                ];
                if($reciptsUpdate->count()>0){
                    foreach ($reciptsUpdate as $item){
                        ReceiptsService::updateModel($item->id,$dataRecipts);
                    }
                }
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }else{
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function edit($name,$id)
    {
        $dataName = $name;
        $dataId = $id;
        $customers =GeneralService::getCustomersHandoverApprove($dataId);
        $customersEdit =GeneralService::getCustomersHandoverEdit($dataId);
        $customersCount = $customers->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $Staffs = GeneralService::getUserEdit();
        $brandUser = GeneralService::getBrandUser();
        return view("customerHandoverStaff.approve",compact('customersEdit','dataId','dataName','brandUser','customers','customersCount','Staffs','customers','StaffCustomerBrand'));
    }
    public function approve(Request $request, $id)
    {
        $date = date('Y-m-d');
        $data=[
            "customers_handover_status"=>2,
            "customers_handover_date_approve"=>$date,
        ];
        $customerData= CustomerService::getCustomerHandEdit($id);
        $customerUpdate= CustomerService::getCustomerHandoverEdit($id);
        if($customerUpdate){
            foreach ($customerUpdate as $key){
                $asc = CustomerService::updateModel($key->id,$data);
            }
            if($asc) {
                $staffInfo = StaffService::getById($customerData->customers_user_created);
                $dataStaff = ['staff_status'=>2];
                StaffService::updateModel($staffInfo->id,$dataStaff);
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }else{
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
