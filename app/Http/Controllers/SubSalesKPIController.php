<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\Stock\salesKpiPaymentService;
use App\Services\Stock\subSalesKPIService;
use App\Services\AbstractService;
use App\Services\Stock\CustomerService;
use App\Services\DM\GeneralService;
use App\Services\Stock\PeriodService;
use function GuzzleHttp\Psr7\str;

class SubSalesKPIController extends Controller
{
    public function index(Request $request)
    {
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = subSalesKPIService::getModelByParamater($param);
        $listAll =[];
        foreach ($results as $val){
            /** lấy ds nv sales thuộc nv cấp sub sales */
            $countSales=0;
            $dbSalesOfSubsales = subSalesKPIService::getListsalesOffSubSales($val['staff_kpi_sales_sub_id']);
            $trowSales=0;
            $listSales =[];
            foreach ($dbSalesOfSubsales as $values){
                $countSales +=1;
                $trowCustomers = 0;
                $listCustomers =[];
                /** lấy ds customers thuộc nv cấp sales */
                $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSales($values['staff_kpi_sales_id']);
                foreach ($dbCustomers as $value){
                    $trowOrders = 0;
                    $listOrders =[];
                    /** lấy ds orders thuộc nv cấp sales */
                    $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSales($value['staff_kpi_customers_id']);
                    foreach ($dbOrders as $v){
                        $trowKPIOders = 0;
                        $listKPIOders =[];
                        /** lấy ds kpi orders thuộc nv cấp sales */
                        $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSales($v['staff_kpi_period_stock_id']);
                        foreach ($dbKPICode as $vs){
                            $trowKPIOders +=1;
                            $trowOrders +=1;
                            $trowCustomers +=1;
                            $trowSales +=1;
                            array_push($listKPIOders,[
                                'id'=>$vs['id'],
                                'staff_kpi_code'=>$vs['staff_kpi_code'],
                                'brand_name'=>$vs['kpiBrand']['brand_name'],
                                'staff_kpi_sales_sub_status'=>$vs['staff_kpi_sales_sub_status']
                            ]);
                        }
                        array_push($listOrders,[
                            'stock_code'=>$v['kpiStocks']['stock_code'],
                            'trowKPIOders'=>$trowKPIOders,
                            'listKPIOders'=>$listKPIOders
                        ]);
                    }
                    array_push($listCustomers,[
                        'customer_name'=>$value['kpiCustomers']['customers_name'],
                        'trowOrders'=>$trowOrders,
                        'listOrders'=>$listOrders
                    ]);
                }
                array_push($listSales,[
                    'sales_name'=>$values['kpiStaffSales']['staff_name'],
                    'listCustomers'=>$listCustomers,
                    'trowCustomers'=>$trowCustomers
                ]);
            }
            array_push($listAll,[
                'sales_sub_name'=>$val['kpiStaffSubSales']['staff_name'],
                'sales_sub_id'=>$val['staff_kpi_sales_sub_id'],
                'trowSales'=>$trowSales,
                'listSales'=>$listSales,
            ]);
        }
        $count = $results->count();
        $branch = GeneralService::getBrand();
        $Userbranch = GeneralService::getBrandUser();
        $Staffs = GeneralService::getStaffSubSalesKpi();
//        return AbstractService::ResultSuccess($listAll);
        return view("subSalesKPI.index",compact('date_end','date_start','results','count','branch','Userbranch','Staffs','listAll'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $salesSub = GeneralService::getSalesSubManagePayment();
            $listAllPaymentPeriod=[];
            $Brand = CustomerService::getStaffCustomerBrand();
            $sales ='';
            $count = 0;
            return view('subSalesKPI.create',compact('salesSub','listAllPaymentPeriod','Brand','sales','count'))->render();
        }
    }
    // lấy ds customer của nv sales cần thanh toán kpi
    public function customersPeriodKpi(Request $request){
        //lấy ds khách hàng
        $customersPeriodKpi = GeneralService::getStaffSubSalesPayment($request->sales_kpi);
        $array_id_sales = [];
        foreach ($customersPeriodKpi as $vs)
        {
            if($vs)
            {
                array_push($array_id_sales,$vs['id']);
            }
        }
        //ds mảng show phiếu thanh toán
        $results = subSalesKPIService::getModelBySub($array_id_sales);
        $listAll =[];
        $stt=0;
        foreach ($results as $val){
            /** lấy ds customers thuộc nv cấp sales */
            $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSubSalesEdit($val['staff_kpi_sales_id']);
            $listCustomer =[];
            $trowParent=0;
            $countRowsCustomers=0;
            foreach ($dbCustomers as $key){
                $countRowsCustomers +=1;
                /** lấy ds order thuộc customers nv cấp sales */
                $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSubSales($key['staff_kpi_customers_id']);
                $listOrders =[];
                $trowOrders =0;
                foreach ($dbOrders as $ks){
                    /** lấy ds kpoi_code thuộc stock_code nv cấp sales */
                    $listKPICode =[];
                    $trowKPICode =0;
                    $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSubSales($ks['staff_kpi_period_stock_id']);
                    foreach ($dbKPICode as $ki){
                        $trowKPICode +=1;
                        $trowOrders +=1;
                        $trowParent +=1;
                        $stt +=1;
                        array_push($listKPICode,[
                            'id'=>$ki['id'],
                            'stt'=>$stt,
                            'staff_kpi_code'=>$ki['staff_kpi_code'],
                            'brand_name'=>$ki['kpiBrand']['brand_name'],
                            'staff_kpi_sales_status'=>$ki['staff_kpi_sales_status'],
                            'staff_kpi_sales_sub_status'=>$ki['staff_kpi_sales_sub_status']
                        ]);
                    }
                    /** end */
                    array_push($listOrders,[
                        'stock_code'=>$ks['kpiStocks']['stock_code'],
                        'trowKPICode'=>$trowKPICode,
                        'listKPICode'=>$listKPICode,
                    ]);
                }
                /** end */
                /** lấy ds customers thuộc nv cấp sales */
                array_push($listCustomer,[
                    'customers_name'=>$key['kpiCustomers']['customers_name'],
                    'trowOrders'=>$trowOrders,
                    'listOrders'=>$listOrders,
                ]);
            }
            array_push($listAll,[
                'staffs_name'=>$val['kpiStaffSales']['staff_name'],
                'trowParent'=>$trowParent,
                'listCustomer'=>$listCustomer,
                'countRowsCustomers'=>$countRowsCustomers,
            ]);
        }
        $salesSub = GeneralService::getSalesSubManagePayment();
        $Brand = CustomerService::getStaffCustomerBrand();
        $sales = $request->sales_kpi;
        $count = $results->count();
        return view('subSalesKPI.create',compact('listAll','salesSub','stt','Brand','sales','count'))->render();
    }
    public function create(Request $request){
        $periodId = explode(",",$request->periodId);
        $arrayperiodId= [];
        foreach ($periodId as $p){
            if($p){
                array_push($arrayperiodId,$p);
            }
        }

        $data = [
            'staff_kpi_sales_sub_id'=>$request->staff_kpi_sales_id,
            'staff_kpi_sales_sub_status'=>1
        ];
        $asc = salesKpiPaymentService::updatePeriodStockKPIModelSubSales($arrayperiodId,$data);
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function edit($id)
    {

        $salesSub = GeneralService::getSalesSubManagePayment();
        //lấy ds khách hàng
        $customersPeriodKpi = GeneralService::getStaffSubSalesPayment($id);
        $array_id_sales = [];
        foreach ($customersPeriodKpi as $vs)
        {
            if($vs)
            {
               array_push($array_id_sales,$vs['id']);
            }
        }
        //ds mảng show phiếu thanh toán
        $results = subSalesKPIService::getModelByEdit($array_id_sales);
        $listAll =[];
        $stt=0;
        $count = $results->count();
        $sub_sales_status=0;
        foreach ($results as $val){
            if($val['staff_kpi_sales_manage_status']==null){
                $sub_sales_status=1;
            }
            /** lấy ds customers thuộc nv cấp sales */
            $dbCustomers = salesKpiPaymentService::getListCustomerOffStaffSalesEdit($val['staff_kpi_sales_id']);
            $listCustomer =[];
            $trowParent=0;
            $countRowsCustomers=0;
            foreach ($dbCustomers as $key){
                $countRowsCustomers +=1;
                /** lấy ds order thuộc customers nv cấp sales */
                $dbOrders = salesKpiPaymentService::getListOrdersOffStaffSalesEdit($key['staff_kpi_customers_id']);
                $listOrders =[];
                $trowOrders =0;
                foreach ($dbOrders as $ks){
                    /** lấy ds kpoi_code thuộc stock_code nv cấp sales */
                    $listKPICode =[];
                    $trowKPICode =0;
                    $dbKPICode = salesKpiPaymentService::getListKPICodeOffStaffSalesEdit($ks['staff_kpi_period_stock_id']);
                    foreach ($dbKPICode as $ki){
                        $trowKPICode +=1;
                        $trowOrders +=1;
                        $trowParent +=1;
                        $stt +=1;
                        array_push($listKPICode,[
                            'id'=>$ki['id'],
                            'stt'=>$stt,
                            'staff_kpi_code'=>$ki['staff_kpi_code'],
                            'brand_name'=>$ki['kpiBrand']['brand_name'],
                            'staff_kpi_sales_status'=>$ki['staff_kpi_sales_status'],
                            'staff_kpi_sales_sub_status'=>$ki['staff_kpi_sales_sub_status']
                        ]);
                    }
                    /** end */
                    array_push($listOrders,[
                        'stock_code'=>$ks['kpiStocks']['stock_code'],
                        'trowKPICode'=>$trowKPICode,
                        'listKPICode'=>$listKPICode,
                    ]);
                }
                /** end */
                /** lấy ds customers thuộc nv cấp sales */
                array_push($listCustomer,[
                    'customers_name'=>$key['kpiCustomers']['customers_name'],
                    'trowOrders'=>$trowOrders,
                    'listOrders'=>$listOrders,
                ]);
            }
            array_push($listAll,[
                'staffs_name'=>$val['kpiStaffSales']['staff_name'],
                'trowParent'=>$trowParent,
                'listCustomer'=>$listCustomer,
                'countRowsCustomers'=>$countRowsCustomers,
            ]);
        }
        $Brand = CustomerService::getStaffCustomerBrand();
        return view("subSalesKPI.update",compact('listAll','salesSub','id','count','Brand','sub_sales_status'));
    }
    public function update(Request $request, $id)
    {
//        $customersPeriodKpi = GeneralService::getStaffSubSalesPayment($id);
//        $array_id_sales = [];
//        foreach ($customersPeriodKpi as $vs)
//        {
//            if($vs)
//            {
//                array_push($array_id_sales,$vs['id']);
//            }
//        }
        //ds mảng show phiếu thanh toán
        $results = subSalesKPIService::getModelByEditSubManage($id);
        $periodId=[];
        foreach ($results as $val){
            array_push($periodId,$val->id);
        }
        $data = [
            'staff_kpi_sales_sub_id'=>null,
            'staff_kpi_sales_sub_status'=>null
        ];
        $asc = salesKpiPaymentService::updatePeriodStockKPIModelSubSales($periodId,$data);
        if($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }

}
