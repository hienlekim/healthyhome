<?php
/**
 * date:10/08/2021
 * content:nhà cung cấp sản phẩm
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\Stock\SupplierService;
use App\Services\DM\GeneralService;
use App\Services\AbstractService;
class SupplierController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results =  SupplierService::getModelByParamater($param, $rowsPerPage);
        $count = $results->count();
        return view("suppliers.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $country = GeneralService::getCountry();
            return view('suppliers.create',compact('country'))->render();
        }
    }
    public function create(Request $request){
        $data=[
            "supplier_code"=>$request->supplier_code,
            "supplier_name"=>$request->supplier_name,
            "supplier_address"=>$request->supplier_address,
            "supplier_country_id"=>$request->supplier_country_id
        ];
        $query = SupplierService::getByCode($request->supplier_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = SupplierService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = SupplierService::getModelById($id);
        $country = GeneralService::getCountry();
        return view("suppliers.update",compact('data','country'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "supplier_name"=>$request->supplier_name,
            "supplier_address"=>$request->supplier_address,
            "supplier_country_id"=>$request->supplier_country_id
        ];
        $asc = SupplierService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
