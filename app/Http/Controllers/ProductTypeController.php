<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\DM\ProductTypeService;
use App\Services\AbstractService;

class ProductTypeController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = ProductTypeService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("productGroups.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('productGroups.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "product_type_code"=>$request->product_type_code,
            "product_type_name"=>$request->product_type_name,
        ];
        $query = ProductTypeService::getByCode($request->product_type_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = ProductTypeService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = ProductTypeService::getModelById($id);
        return view("productGroups.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "product_type_name"=>$request->product_type_name
        ];
        $asc = ProductTypeService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
