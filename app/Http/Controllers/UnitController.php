<?php
/**
 * date:10/08/2021
 * content: đơn vị tính
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\UnitService;
use App\Services\AbstractService;
class UnitController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = UnitService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("unit.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('unit.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "unit_code"=>$request->unit_code,
            "unit_name"=>$request->unit_name
        ];
        $query = UnitService::getByCode($request->unit_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = UnitService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = UnitService::getModelById($id);
        return view("unit.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "unit_name"=>$request->unit_name
        ];
        $asc = UnitService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
