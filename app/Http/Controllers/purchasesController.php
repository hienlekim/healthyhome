<?php

namespace App\Http\Controllers;

use App\Services\AbstractService;
use App\Services\DM\HistoryService;
use App\Services\Stock\CustomerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\Stock\purchasesService;
use App\Services\Stock\StockService;
use App\Services\DM\GeneralService;
class purchasesController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = purchasesService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("purchases.index",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $name_user = \Illuminate\Support\Facades\Auth::user()->name;
            $brandUser = GeneralService::getBrandUser();
            $superliser = GeneralService::getSuperliserQuery();
            $products = GeneralService::getProductQuery();
            if($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators')
                $brandUsers = "";
            else
                $brandUsers = $brandUser['brands']['brand_ware_house_id'];
            $location = GeneralService::getLocationQuery($brandUsers);
            $unit = GeneralService::getUnitQuery();
            $currency = GeneralService::getCurrencyQuery();
            $productType = GeneralService::getproductTypeQuery();
            $Stocktype= GeneralService::getpurchasesTypeQuery();
            $date = date('Y-m-d');
            return view('purchases.create',compact('superliser','products','location','brandUser','name_user','unit','productType','currency','Stocktype','date'))->render();
        }
    }
    public function create(Request $request){
        $brandUser = GeneralService::getBrandUser();
        $query = purchasesService::getByCode($request->stock_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            for($i=1;$i<11;$i++){
                $product = 'stock_product_id_'.$i;
                $unit = 'stock_product_unit_id_'.$i;
                $stock_quality = 'stock_quality_reality_'.$i;
                if($request->$stock_quality > 0){
                    $data=[
                        "stock_code"=>$request->stock_code,
                        "stock_location"=>$request->stock_location,
                        "stock_currency_id"=>$request->stock_currency_id,
                        "stock_staff_id"=>$brandUser->id,
                        "stock_staff_stocker_id"=>$brandUser['id'],
                        "stock_date_start_stocker"=>date("Y-m-d H:i:s"),
                        "stock_brand_id"=>$brandUser['brands']['id'],
                        "stock_description"=>$request->stock_description,
                        "stock_receipt_date"=>$request->stock_receipt_date,
                        "stock_status"=>1,
                        "Type"=>"NHAPKHO",
                        "stock_type"=>$request->product_type_name,
                        "stock_product_id"=>$request->$product,
                        "stock_product_unit_id"=>$request->$unit,
                        "stock_quality_reality"=>str_replace(".","",$request->$stock_quality),
                    ];
                    $asc = purchasesService::createModel($data);
                }
            }
            if($asc) {
                /**lich su*/
                $data_his = [
                    'history_code'=>$asc->stock_code,
                    'history_name'=>'Nhập kho'.$asc->product_type_name,
                    'history_staff_id'=>$asc->stock_staff_id,
                    'history_action'=>'tạo mới',
                ];
                HistoryService::createModel($data_his);
                /**kết thúc lịch sử*/
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = purchasesService::getModelById($id);
        $dataProduct = purchasesService::getModelByProduct($data->stock_code);
        $name_user = \Illuminate\Support\Facades\Auth::user()->name;
        $brandUser = GeneralService::getBrandUser();
        $superliser = GeneralService::getSuperliserQuery();
        $products = GeneralService::getProductQuery();
        $location = GeneralService::getWareHouseQuery();
        $unit = GeneralService::getUnitQuery();
        $productType = GeneralService::getproductTypeQuery();
        return view("purchases.update",compact('data','superliser','products','location','brandUser','name_user','unit','productType','dataProduct'));
    }
    public function update(Request $request, $id)
    {

        $id_user =\Illuminate\Support\Facades\Auth::user()->id;
        $brandUser = GeneralService::getBrandUser($id_user);
        $dataProduct = purchasesService::getModelByProduct($request->stock_code);
        $i=0;
        foreach($dataProduct as $key){
            $i +=1;
            $product = 'stock_product_id_'.$i;
            $unit = 'stock_product_unit_id_'.$i;
            $stock_quality = 'stock_quality_reality_'.$i;
            if($request->$stock_quality > 0){
                $data=[
                    "stock_location"=>$request->stock_location,
                    "stock_staff_stocker_id"=>$brandUser['id'],
                    "stock_date_start_stocker"=>date("Y-m-d H:i:s"),
                    "stock_brand_id"=>$brandUser['brands']['id'],
                    "stock_description"=>$request->stock_description,
                    "stock_status"=>3,
                    "stock_product_id"=>$request->$product,
                    "stock_product_unit_id"=>$request->$unit,
                    "stock_quality_reality"=>str_replace(".","",$request->$stock_quality),
                ];
                $asc = purchasesService::updateModel($key->id, $data);
            }
        }
        if($asc) {
            $data = purchasesService::getModelById($id);
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Nhập kho'.$data->product_type_name,
                'history_staff_id'=>$data->stock_staff_id,
                'history_action'=>'Cập nhật',
            ];
            HistoryService::createModel($data_his);
            /**kết thúc lịch sử*/
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function listInventories(Request $request)
    {
//        $date_start = date('Y-m-01');
//        $date_end = date('Y-m-t');
        $rowsPerPage = $request->limit;
        $param=[
//            'fromDate'=>$date_start,
//            'toDate'=>$date_end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = purchasesService::getlistInventoriesParamater($param,$rowsPerPage);
        $count = $results->count();
        $list = [];
        $stt=0;
        foreach ($results as $item){
            $stt +=1;
            $productSum = StockService::getQuerySumSlProduct($item->stock_product_id);
            $data = [
                'stt'=>$stt,
                'product_name'=>$item['productStocks']['product_name'],
                'product_unit'=>$item['units']['unit_name'],
                'brand_name'=>$item['brands']['brand_name'],
                'product_quality'=>$productSum
                ];
            array_push($list,$data);
        }
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("purchases.Inventories",compact('results','StaffCustomerBrand','branch','list','count'));
    }
}
