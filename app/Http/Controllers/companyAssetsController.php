<?php

namespace App\Http\Controllers;

use App\Services\AbstractService;
use App\Services\DM\HistoryService;
use App\Services\Stock\companyAssetsService;
use App\Services\Stock\CustomerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\Stock\purchasesService;
use App\Services\DM\GeneralService;

class companyAssetsController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = companyAssetsService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("companyAssets.index",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    /** khi nhân viên sale chọn sp thì được load lên */
    public function stockListPurchases(Request $request,$id)
    {
        $product = companyAssetsService::getQueryProduct($id);
        $productSum = companyAssetsService::getQuerySumSlProduct($id);
        $data = [ 'product'=>$product,
            'productSum'=>$productSum,
        ];
        return AbstractService::DataSuccess($data);
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $name_user = \Illuminate\Support\Facades\Auth::user()->name;
            $brandUser = GeneralService::getBrandUser();
            $Customer = GeneralService::getOdersGiftCustomer()->get();
            $products = GeneralService::getProductQuery();
            if($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators')
                $brandUsers = "";
            else
                $brandUsers = $brandUser['brands']['brand_ware_house_id'];
            $location = GeneralService::getLocationQuery($brandUsers);
            $unit = GeneralService::getUnitQuery();
            $payment = GeneralService::getPaymentQuery();
            $productType = GeneralService::getproductTypeQuery();
            $StockCode= GeneralService::getCompanyAssetsTypeQuery();
            return view('companyAssets.create',compact('Customer','products','location','brandUser','name_user','unit','productType','payment','StockCode'))->render();
        }
    }
    public function create(Request $request){
        $brandUser = GeneralService::getBrandUser();
        $query = companyAssetsService::getByCode($request->stock_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            for($i=1;$i<5;$i++){
                $product = 'stock_product_id_'.$i;
                $unit = 'stock_product_unit_id_'.$i;
                $stock_quality = 'stock_quality_reality_'.$i;
                if($request->$stock_quality > 0){
                    $data=[
                        "stock_code"=>$request->stock_code,
                        "stock_location"=>$request->stock_location,
                        "stock_staff_id"=>$brandUser['id'],
                        "stock_brand_id"=>$brandUser['brands']['id'],
                        "stock_description"=>$request->stock_description,
                        "stock_status"=>1,
                        "Type"=>"XUATTAISANCONGTY",
                        "stock_type"=>$request->product_type_name,
                        "stock_product_id"=>$request->$product,
                        "stock_product_unit_id"=>$request->$unit,
                        "stock_quality_reality"=>-str_replace(".","",$request->$stock_quality),
                    ];
                    $asc = companyAssetsService::createModel($data);
                }
            }
            if($asc) {
                /**lich su*/
                $data_his = [
                    'history_code'=>$asc->stock_code,
                    'history_name'=>'Xuất kho tài sản công ty',
                    'history_staff_id'=>$asc->stock_staff_id,
                    'history_action'=>'tạo mới',
//                    'history_price'=>-$sumAmount
                ];
                HistoryService::createModel($data_his);
                /**kết thúc lịch sử*/
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = companyAssetsService::getModelById($id);
        $stockCode =  companyAssetsService::getByCode($data->stock_code)->Max('id');
        $dataProduct = companyAssetsService::getModelByProduct($data->stock_code);
        $brandUser = GeneralService::getBrandUser();
        $Customer = GeneralService::getOdersCustomerBrand();
        $products = GeneralService::getProductQuery();
        $location = GeneralService::getLocationQuery($brandUser['brands']['brand_ware_house_id']);
        $unit = GeneralService::getUnitQuery();
        $payment = GeneralService::getPaymentQuery();
        $productType = GeneralService::getproductTypeQuery();
        return view('companyAssets.approve', compact('dataProduct','stockCode','data','Customer', 'products', 'location', 'brandUser', 'unit', 'productType', 'payment'))->render();
    }
    public function update(Request $request, $id)
    {
        $data = companyAssetsService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = companyAssetsService::getModelByProduct($data->stock_code);
        $i=0;
        foreach ($dataProduct as $item){
            $i +=1;
            $product = 'stock_product_id_'.$i;
            $unit = 'stock_product_unit_id_'.$i;
            $stock_quality = 'stock_quality_reality_'.$i;
            if($request->$stock_quality > 0){
                $dataList=[
                    "stock_location"=>$request->stock_location,
                    "stock_staff_id"=>$brandUser['id'],
                    "stock_description"=>$request->stock_description,
                    "stock_product_id"=>$request->$product,
                    "stock_product_unit_id"=>$request->$unit,
                    "stock_quality_reality"=>-str_replace(".","",$request->$stock_quality),
                ];
                $asc = companyAssetsService::updateModel($item->id,$dataList);
            }
        }

        if($data) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Cập nhật tài sản chung',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'UpdateCompanyAssets',
            ];
            HistoryService::createModel($data_his);
            return AbstractService::ResultSuccess($data);

        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
