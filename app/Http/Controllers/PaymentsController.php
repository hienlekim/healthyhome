<?php
/**
 * date:10/08/2021
 * content:hình thức thanh toán trả 1 lần hay nhiều lần
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\PaymentsService;
use App\Services\AbstractService;
class PaymentsController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = PaymentsService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("payment.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            return view('payment.create')->render();
        }
    }
    public function create(Request $request){
        $data=[
            "payments_code"=>$request->payments_code,
            "payments_name"=>$request->payments_name,
            "payments_number"=>$request->payments_number
        ];
        $query = PaymentsService::getByCode($request->payments_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = PaymentsService::createModel($data);
            if($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = PaymentsService::getModelById($id);
        return view("payment.update",compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "payments_name"=>$request->payments_name,
            "payments_number"=>$request->payments_number
        ];
        $asc = PaymentsService::updateModel($id, $data);
        if ($asc) {
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
