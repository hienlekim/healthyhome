<?php
/**
 * date:10/08/2021
 * content:nếu nhân viên kho, thủ kho thì gắng liền với chức vụ,
 * nhân viên sales sẽ gắn liền với cấp bậc để hưởng hoa hồng
 * mỗi nhân viên sẽ thấy được thông tin đơn hàng của bản thân thôi.
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\Staff\StaffService;
use App\Services\AbstractService;
use Illuminate\Http\Request;
use App\Services\DM\GeneralService;
use Illuminate\Support\Facades\Hash;
use File;
use App\Services\Stock\CustomerService;
class StaffController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = StaffService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $brandUser = GeneralService::getBrandUser();
        return view("staff.index",compact('results','count','brandUser'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $dataBrand = GeneralService::getBrand();
            $dataPositionDecentral = GeneralService::getPositionDecentral();
            $Images="avatar-6.jpg";
            $code = GeneralService::getStafsTypeQuery();
            return view('staff.create',compact('dataBrand','dataPositionDecentral','Images','code'))->render();
        }
    }
    public function create(Request $request){
        $dataUsser = [
            "name"=>$request->name,
            "email"=>$request->emails,
            "password"=>Hash::make($request->passwords),
        ];
        $queryUser = StaffService::getByCodeUser($request->emails,$request->name);
        if($queryUser->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $ascUser = StaffService::createUserModel($dataUsser);
            if($ascUser) {
                //trường hợp đã thêm mới nhưng ko có images
                if($request->has('images_avata') ) {
                    $file_local =  $request->file('images_avata');
                    $extension = $request->file('images_avata')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_name = rand() . '.' . $extension;
                        $file_local->move(public_path('images/staff'), $new_name);
                    }else{
                        $new_name="";
                    }
                }else{
                    $new_name="";
                }
                $data = [
                    "staff_user_id" => $ascUser->id,
                    "staff_code" => $request->staff_code,
                    "staff_name" => $request->staff_name,
                    "staff_sex" => $request->staff_sex,
                    "staff_status_new" => $request->staff_status_new,
                    "staff_date_start" =>  $request->staff_date_start,
                    "staff_year_old" => $request->staff_year_old,
                    "staff_brand_id" => $request->staff_brand_id,
                    "staff_number_bank" => $request->staff_number_bank,
                    "staff_name_bank" => $request->staff_name_bank,
                    "staff_address" => $request->staff_address,
                    "staff_decentralization_id" => $request->staff_decentralization_id,
                    'staff_avatar'=>$new_name
                ];
                $query = StaffService::getByCode($request->staff_code);
                if ($query->count() > 0) {
                    return AbstractService::ResultErrors("Data registered before, please try again!");
                } else {
                    $asc = StaffService::createModel($data);;
                    if ($asc) {
                        return AbstractService::ResultSuccess($asc);
                    } else {
                        return AbstractService::ResultError("An error occurred, please try again");
                    }
                }
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = StaffService::getModelById($id);
        $users = StaffService::getModelUserId($data->staff_user_id);
        $dataBrand = GeneralService::getBrand();
        $dataPositionDecentral = GeneralService::getPositionDecentral();
        if($data->staff_avatar !=""){
            $Images = $data->staff_avatar;
        }else{
            $Images="avatar-6.jpg";
        }
        return view("staff.update",compact('data','Images','dataBrand','dataPositionDecentral','users'));
    }
    public function update(Request $request, $id)
    {
        $data = StaffService::getModelById($id);
        $new_name ="";
        if($data['staff_avatar'] == null){
            if($request->has('images_avata') ) {
                $file_local =  $request->file('images_avata');
                $extension = $request->file('images_avata')->getClientOriginalExtension();
                if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                    $new_name = rand() . '.' . $extension;
                    $file_local->move(public_path('images/staff'), $new_name);
                }else{
                    $new_name="";
                }
            }else{
                $new_name="";
            }
        }else{
            if($request->has('images_avata')!="" ) {
                $file_local =  $request->file('images_avata');
                $extension = $request->file('images_avata')->getClientOriginalExtension();
                if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                    if(file_exists(public_path("images/staff/". $data['staff_avatar']))){
                        unlink(public_path("images/staff/". $data['staff_avatar']));
                        $new_name = rand() . '.' . $extension;
                        $file_local->move(public_path('images/staff'), $new_name);
                    }
                }else{
                    $new_name="";
                }
            }else{
                $new_name=$data['staff_avatar'];
            }
        }
        $dataUsser = [
            "name"=>$request->name,
            "email"=>$request->emails,
            "password"=>Hash::make($request->passwords),
        ];
        $ascUser = StaffService::updateUserModel($data['staff_user_id'],$dataUsser);
        if($ascUser){
            $data = [
                "staff_name" => $request->staff_name,
                "staff_sex" => $request->staff_sex,
                "staff_status_new" => $request->staff_status_new,
                "staff_date_start" =>  $request->staff_date_start,
                "staff_year_old" => $request->staff_year_old,
                "staff_brand_id" => $request->staff_brand_id,
                "staff_number_bank" => $request->staff_number_bank,
                "staff_name_bank" => $request->staff_name_bank,
                "staff_address" => $request->staff_address,
                "staff_decentralization_id" => $request->staff_decentralization_id,
                'staff_avatar'=>$new_name
            ];
            $asc = StaffService::updateModel($id, $data);
            if ($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function delete(Request $request, $id)
    {
        $data = StaffService::getModelById($id);
        $ascUser = StaffService::RemoveUserModel($data['staff_user_id']);
        if($ascUser){
            $asc = StaffService::RemoveStaffModel($id);
            if ($asc) {
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
}
