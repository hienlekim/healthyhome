<?php
/**
 * date:10/08/2021
 * content:nhân viên sales lập đơn bán hàng, sau khi tạo đơn tiến hành gửi đến thủ kho, sau khi thủ kho duyệt
 * hoàn tất đơn sẽ tiến hành tạo phiếu thu
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Services\Stock\CustomerService;
use App\Services\Stock\CustomerCareService;
use App\Services\Stock\StockService;
use App\Services\Stock\PeriodService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Services\DM\GeneralService;
use App\Services\DM\HistoryService;
use App\Services\AbstractService;
class StockController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $start = date('Y-m-01');
        $end = date('Y-m-t');
        $param=[
            'fromDate'=>$request->input('fromDate')?? $start,
            'toDate'=>$request->input('toDate')?? $end,
            'searchBranch'=>$request->input('searchBranch'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $date_start = $param['fromDate'];
        $date_end = $param['toDate'];
        $results = StockService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        $StaffCustomerBrand = CustomerService::getStaffCustomerBrand();
        $branch = GeneralService::getBrand();
        return view("order.index",compact('date_end','date_start','results','count','StaffCustomerBrand','branch'));
    }
    /** khi nhân viên sale chọn sp thì được load lên */
    public function stockListPurchases(Request $request,$id)
    {
        $product = StockService::getQueryProduct($id);
        $productSum = StockService::getQuerySumSlProduct($id);
        $data = [ 'product'=>$product,
                    'productSum'=>$productSum,
            ];
        return AbstractService::DataSuccess($data);
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $name_user = \Illuminate\Support\Facades\Auth::user()->name;
            $brandUser = GeneralService::getBrandUser();
            $Customer = GeneralService::getOdersCustomerBrand();
            $products = GeneralService::getProductQuery();
            if($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators')
                $brandUsers = "";
            else
                $brandUsers = $brandUser['brands']['brand_ware_house_id'];
            $location = GeneralService::getLocationQuery($brandUsers);
            $unit = GeneralService::getUnitQuery();
            $payment = GeneralService::getPaymentQuery();
            $productType = GeneralService::getproductTypeQuery();
            $StockCode = GeneralService::getStocksTypeQuery();
            return view('order.create',compact('Customer','products','location','brandUser','name_user','unit','productType','payment','StockCode'))->render();
        }
    }
    public function create(Request $request){
        $brandUser = GeneralService::getBrandUser();
        $query = StockService::getByCode($request->stock_code);
        if($query->count() > 0){
            $StockCode = GeneralService::getStocksTypeQuery();
            $stock_code =$StockCode;
        }else{
            $stock_code =$request->stock_code;
        }
        for($i=1;$i<5;$i++){
            $product = 'stock_product_id_'.$i;
//                $unit = 'stock_product_unit_id_'.$i;
            $stock_quality = 'stock_quality_reality_'.$i;
            $stock_product_price_ex = 'product_price_ex_'.$i;
            $stock_amount = 'stock_amount_'.$i;
            if($request->$stock_quality > 0){
                $data=[
                    "stock_code"=>$stock_code,
                    "stock_customer_id"=>$request->stock_customer_id,
                    "stock_location"=>$request->stock_location,
                    "stock_staff_id"=>$brandUser['id'],
                    "stock_brand_id"=>$brandUser['brands']['id'],
                    "stock_description"=>$request->stock_description,
                    'stock_currency_id'=>$request->stock_currency_id,
                    'stock_seri_number'=>$request->stock_seri_number,
                    "stock_status"=>1,
                    "Type"=>"XUATKHO",
                    "stock_type"=>$request->product_type_name,
                    "stock_product_id"=>$request->$product,
//                        "stock_product_unit_id"=>$request->$unit,
                    'stock_price_export'=>str_replace(".","",$request->$stock_product_price_ex),
                    "stock_quality_reality"=>-str_replace(".","",$request->$stock_quality),
                    "stock_amount"=>-str_replace(".","",$request->$stock_amount),
                ];
                $asc = StockService::createModel($data);
            }
        }
        if($asc) {
//                cap nhaatj trang thai dang no cuar khach CustomerService
            $dataCustom = ['customers_payment_status'=>1];
            CustomerService::updateModel($asc->stock_customer_id,$dataCustom);
            $sumAmount = StockService::getCodeSumAmount($asc->stock_code);
//                /**lich su*/
            $data_his = [
                'history_code'=>$asc->stock_code,
                'history_name'=>'Xuất kho',
                'history_staff_id'=>$asc->stock_staff_id,
                'history_action'=>'tạo mới',
                'history_price'=>-$sumAmount
            ];
            HistoryService::createModel($data_his);
            /**kết thúc lịch sử*/
//                xử lý payment
            $period_payments = explode('-',$request->period_payments_id);
            if($period_payments[1]==0){
                $dataPeriod=[
                    "period_code"=>'PaymentFull',
                    "period_name"=>'Payments Full',
                    'period_customers_id'=>$asc->stock_customer_id,
                    "period_payments_id"=>$period_payments[0],
                    "period_brand_id"=>$brandUser['brands']['id'],
                    "period_stock_id"=>$asc->id,
                    'period_staff_id'=>$brandUser['id'],
                    "period_amount"=>abs($sumAmount),
                    "period_amount_batch"=>abs($sumAmount),
                    "period_date"=>date("Y-m-d"),
                    "period_status"=>1
                ];
                PeriodService::createModel($dataPeriod);
            }elseif($period_payments[1] > 0){
                $date = date('Y-m-d');
                $datcoctruoc = str_replace(".","",$request->first_payment);
                $sotienconlai = (int)$sumAmount + $datcoctruoc;
                $amount_batch =(int)$sotienconlai/($period_payments[1]-1);
                $array=[];
                for($i=1;$i<=$period_payments[1];$i++){
                    if($i==1){
                        $dataPeriod=[
                            "period_code"=>'PaymentNotFull',
                            "period_name"=>'Payment '.$i,
                            'period_customers_id'=>$asc->stock_customer_id,
                            "period_payments_id"=>$period_payments[0],
                            "period_brand_id"=>$brandUser['brands']['id'],
                            "period_stock_id"=>$asc->id,
                            'period_staff_id'=>$brandUser['id'],
                            "period_amount"=>abs($sumAmount),
                            "period_amount_batch"=>abs($datcoctruoc),
                            "period_date"=>$date,
                            "period_status"=>1
                        ];
                        PeriodService::createModel($dataPeriod);
                    }else{
                        $periodDate = strtotime('+'.($i-1).' month', strtotime(($date)));
                        $newdate = date('Y-m-j',$periodDate);
                        array_push($array,$newdate);
                        $dataPeriod=[
                            "period_code"=>'PaymentNotFull',
                            "period_name"=>'Payment '.$i,
                            'period_customers_id'=>$asc->stock_customer_id,
                            "period_payments_id"=>$period_payments[0],
                            "period_brand_id"=>$brandUser['brands']['id'],
                            "period_stock_id"=>$asc->id,
                            'period_staff_id'=>$brandUser['id'],
                            "period_amount"=>abs($sumAmount),
                            "period_amount_batch"=>abs($amount_batch),
                            "period_date"=>$newdate,
                            "period_status"=>1
                        ];
                        PeriodService::createModel($dataPeriod);
                    }
                }
            }
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function edit($id)
    {
        $data = StockService::getModelById($id);
        $stockCode =  StockService::getByCode($data->stock_code)->Max('id');
        $dataProduct = StockService::getModelByProduct($data->stock_code);
        $Period = PeriodService::getModelById($stockCode);
        $PeriodPayment = PeriodService::getModePaymentId($stockCode);
        $name_user = \Illuminate\Support\Facades\Auth::user()->name;
        $brandUser = GeneralService::getBrandUser();
        $Customer = GeneralService::getOdersCustomerBrand();
        $products = GeneralService::getProductQuery();
        $location = GeneralService::getWareHouseQuery();
        $unit = GeneralService::getUnitQuery();
        $payment = GeneralService::getPaymentQuery();
        $productType = GeneralService::getproductTypeQuery();
        return view('order.approve', compact('dataProduct','PeriodPayment','stockCode','Period','data','Customer', 'products', 'location', 'brandUser', 'name_user', 'unit', 'productType', 'payment'))->render();
    }
    public function update(Request $request, $id)
    {
        $data = StockService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = StockService::getModelByProduct($data->stock_code);
        $dataList = [
            'stock_status'=>0
        ];
        foreach ($dataProduct as $item){
            $asc = StockService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Hủy đơn hàng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'CancelOder',
            ];
            HistoryService::createModel($data_his);
            $Period = PeriodService::getModelById($id);
            $dataperiod = [
                'period_status'=>-1
            ];
            foreach ($Period as $items){
                $ascPeriod = PeriodService::updateModel($items->id,$dataperiod);
            }
            if($ascPeriod)
            {
                //cap nhaatj trang thai dang no cuar khach CustomerService
                $dataCustom = ['customers_payment_status'=>-1];
                CustomerService::updateModel($data->stock_customer_id,$dataCustom);
                return AbstractService::ResultSuccess($asc);
            }

        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function sendApprove(Request $request, $id)
    {
        $data = StockService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = StockService::getModelByProduct($data->stock_code);
        $dataList = [
            'stock_status'=>3
        ];
        foreach ($dataProduct as $item){
            $asc = StockService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $date = date('Y-m-d');
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Thủ kho duyệt đơn hàng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'ApproveOder',
            ];
            HistoryService::createModel($data_his);
            //bắt đầu đặt hẹn 3 tháng chăm sóc khách hàng
            $customerCare = CustomerCareService::countTimeCustomer($date);
            $dataCustomerCare = [
                'customer_staff_id'=>$data->stock_staff_id,
                'customer_id'=>$data->stock_customer_id,
                'customer_brand_id'=>$data->stock_brand_id,
                'customer_care_date'=>$date,
                'customer_care_date_end'=>$customerCare,
                'customer_care_status'=>1
            ];
            CustomerCareService::createModel($dataCustomerCare);
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function Approve(Request $request, $id)
    {
        $data = StockService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = StockService::getModelByProduct($data->stock_code);
        $date = date('Y-m-d');
        $dataList = [
            'stock_status'=>3,
            'stock_staff_stocker_id'=>$brandUser['id'],
            "stock_date_start_stocker"=>date("Y-m-d H:i:s")
        ];
        foreach ($dataProduct as $item){
            $asc = StockService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Thủ kho duyệt đơn hàng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'ApproveOder',
            ];
            HistoryService::createModel($data_his);
            //cập nhật trạng thái đang chờ tặng quà
            $dataCustom = ['customers_status'=>2];
            CustomerService::updateModel($data->stock_customer_id,$dataCustom);
            //bắt đầu đặt hẹn 3 tháng chăm sóc khách hàng
            $customerCare = CustomerCareService::countTimeCustomer($date);
            $dataCustomerCare = [
                'customer_staff_id'=>$data->stock_staff_id,
                'customer_id'=>$data->stock_customer_id,
                'customer_brand_id'=>$data->stock_brand_id,
                'customer_care_date'=>$date,
                'customer_care_date_end'=>$customerCare,
                'customer_care_status'=>1
            ];
            CustomerCareService::createModel($dataCustomerCare);
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
    public function cancelApprove(Request $request, $id)
    {
        $data = StockService::getModelById($id);
        $brandUser = GeneralService::getBrandUser();
        $dataProduct = StockService::getModelByProduct($data->stock_code);
        $dataList = [
            'stock_status'=>1
        ];
        foreach ($dataProduct as $item){
            $asc = StockService::updateModel($item->id,$dataList);
        }
        if($asc) {
            /**lich su*/
            $data_his = [
                'history_code'=>$data->stock_code,
                'history_name'=>'Thủ kho hủy duyệt đơn hàng',
                'history_staff_id'=>$brandUser['id'],
                'history_action'=>'CancelApproveOder',
            ];
            HistoryService::createModel($data_his);
            return AbstractService::ResultSuccess($asc);
        } else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
