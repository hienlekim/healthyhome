<?php
/**
 * date:10/08/2021
 * content:sản phẩm
 * developer: Lê Kim Hiển
 */
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Services\Stock\ProductService;
use App\Services\Stock\ImagesService;
use App\Services\AbstractService;
use File;
use App\Services\DM\GeneralService;
class ProductController extends Controller
{
    public function index(Request $request)
    {
        $rowsPerPage = $request->limit;
        $param=[
            'searchInput'=>$request->input('searchInput'),
            'orderBy' => $request->input('orderBy') ?? "asc",
        ];
        $results = ProductService::getModelByParamater($param,$rowsPerPage);
        $count = $results->count();
        return view("product.index",compact('results','count'));
    }
    public function store(Request $request)
    {
        if($request->isMethod("GET")){
            if($request->session()->token() != $request->_token){
                response()->json(["status"=>false, "message"=>"Dữ liệu không hợp lệ"]);
            }
            $Images="default-product-img.jpg";
            $origin = GeneralService::getOriginQuery();
            return view('product.create',compact('Images','origin'))->render();
        }
    }
    public function create(Request $request){
        $data=[
            "product_code"=>$request->product_code,
            "product_name"=>$request->product_name,
            "product_content"=>$request->product_content,
            "product_description"=>$request->product_description,
            "product_origin_id"=>$request->product_origin_id,
            "product_size"=>$request->product_size,
            "product_material"=>$request->product_material,
            "product_price_import"=>str_replace(".","",$request->product_price_import),
            "product_price_ex"=>str_replace(".","",$request->product_price_ex),
        ];
        $query = ProductService::getByCode($request->product_code);
        if($query->count() > 0){
            return AbstractService::ResultErrors("Data registered before, please try again!");
        }else{
            $asc = ProductService::createModel($data);
            if($asc) {
                //trường hợp đã thêm mới nhưng ko có images
                if($request->has('images_avata_1') ) {
                    $file_local =  $request->file('images_avata_1');
                    $extension = $request->file('images_avata_1')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$asc->id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                if($request->has('images_avata_2') ) {
                    $file_local =  $request->file('images_avata_2');
                    $extension = $request->file('images_avata_2')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$asc->id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                if($request->has('images_avata_3') ) {
                    $file_local =  $request->file('images_avata_3');
                    $extension = $request->file('images_avata_3')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$asc->id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                if($request->has('images_avata_4') ) {
                    $file_local =  $request->file('images_avata_4');
                    $extension = $request->file('images_avata_4')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$asc->id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                return AbstractService::ResultSuccess($asc);
            } else {
                return AbstractService::ResultError("An error occurred, please try again");
            }
        }
    }
    public function edit($id)
    {
        $data = ProductService::getModelById($id);
        $Images="default-product-img.jpg";
        $origin = GeneralService::getOriginQuery();
        $images = ImagesService::getModelById($id);
        if($images->count()==0){
            $img_1=$Images;
            $img_2=$Images;
            $img_3=$Images;
            $img_4=$Images;
        }
        if($images->count()==1){
            $img_1=$images[0]['images'];
            $img_2=$Images;
            $img_3=$Images;
            $img_4=$Images;
        }
        if($images->count()==2){
            $img_1=$images[0]['images'];
            $img_2=$images[1]['images'];
            $img_3=$Images;
            $img_4=$Images;
        }
        if($images->count()==3){
            $img_1=$images[0]['images'];
            $img_2=$images[1]['images'];
            $img_3=$images[2]['images'];
            $img_4=$Images;
        }
        if($images->count()==4){
            $img_1=$images[0]['images'];
            $img_2=$images[1]['images'];
            $img_3=$images[2]['images'];
            $img_4=$images[3]['images'];

        }
        return view("product.update",compact('data','img_1','img_2','img_3','img_4','origin'));
    }
    public function update(Request $request, $id)
    {
        $data=[
            "product_name"=>$request->product_name,
            "product_content"=>$request->product_content,
            "product_description"=>$request->product_description,
            "product_origin_id"=>$request->product_origin_id,
            "product_size"=>$request->product_size,
            "product_material"=>$request->product_material,
            "product_price_import"=>str_replace(".","",$request->product_price_import),
            "product_price_ex"=>str_replace(".","",$request->product_price_ex),
        ];
        $asc = ProductService::updateModel($id, $data);
        if($asc){
            $images = ImagesService::getModelById($id);
            if($images->count()<1){
                //trường hợp đã thêm mới nhưng ko có images
                if($request->has('images_avata_1')!="") {
                    $file_local =  $request->file('images_avata_1');
                    $extension = $request->file('images_avata_1')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                if($request->has('images_avata_2')!="" ) {
                    $file_local =  $request->file('images_avata_2');
                    $extension = $request->file('images_avata_2')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                if($request->has('images_avata_3') !="") {
                    $file_local =  $request->file('images_avata_3');
                    $extension = $request->file('images_avata_3')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                if($request->has('images_avata_4') !="") {
                    $file_local =  $request->file('images_avata_4');
                    $extension = $request->file('images_avata_4')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $new_images_product = rand() . '.' . $extension;
                        $file_local->move(public_path('images/product'), $new_images_product);
                        $dataImages=[
                            "images"=>$new_images_product,
                            "product_images_id"=>$id
                        ];
                        ImagesService::createModel($dataImages);
                    }
                }
                //kết thúc dk1
            }else{
                if($request->has('images_avata_1')!="") {
                    $file_local =  $request->file('images_avata_1');
                    $extension = $request->file('images_avata_1')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $idImeges = ImagesService::getModelByImages( $images[0]['images']);
                        if($idImeges){
                            if(file_exists(public_path("images/product/". $images[0]['images']))){
                                unlink(public_path("images/product/". $images[0]['images']));
                                $del = ImagesService::RemoveModel($idImeges->id);
                                if($del==true){
                                    $new_images_product = rand() . '.' . $extension;
                                    $file_local->move(public_path('images/product'), $new_images_product);
                                    $dataImages=[
                                        "images"=>$new_images_product,
                                        'product_images_id'=>$id
                                    ];
                                    ImagesService::createModel($dataImages);
                                }
                            }
                        } else{
                            $new_images_product = rand() . '.' . $extension;
                            $file_local->move(public_path('images/product'), $new_images_product);
                            $dataImages=[
                                "images"=>$new_images_product,
                                'product_images_id'=>$id
                            ];
                            ImagesService::createModel($dataImages);
                        }
                    }
                }
                if($request->has('images_avata_2')!="") {
                    $file_local =  $request->file('images_avata_2');
                    $extension = $request->file('images_avata_2')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $idImeges = ImagesService::getModelByImages( $images[1]['images']);
                        if(file_exists(public_path("images/product/". $images[1]['images']))){
                            unlink(public_path("images/product/". $images[1]['images']));
                            $del = ImagesService::RemoveModel($idImeges->id);
                            if($del==true){
                                $new_images_product = rand() . '.' . $extension;
                                $file_local->move(public_path('images/product'), $new_images_product);
                                $dataImages=[
                                    "images"=>$new_images_product,
                                    'product_images_id'=>$id
                                ];
                                ImagesService::createModel($dataImages);
                            }
                        }else{
                            $new_images_product = rand() . '.' . $extension;
                            $file_local->move(public_path('images/product'), $new_images_product);
                            $dataImages=[
                                "images"=>$new_images_product,
                                'product_images_id'=>$id
                            ];
                            ImagesService::createModel($dataImages);
                        }
                    }
                }
                if($request->has('images_avata_3')!="") {
                    $file_local =  $request->file('images_avata_3');
                    $extension = $request->file('images_avata_3')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $idImeges = ImagesService::getModelByImages( $images[2]['images']);
                        if(file_exists(public_path("images/product/". $images[2]['images']))){
                            unlink(public_path("images/product/". $images[2]['images']));
                            $del = ImagesService::RemoveModel($idImeges->id);
                            if($del){
                                $new_images_product = rand() . '.' . $extension;
                                $file_local->move(public_path('images/product'), $new_images_product);
                                $dataImages=[
                                    "images"=>$new_images_product,
                                    'product_images_id'=>$id
                                ];
                                ImagesService::createModel($dataImages);
                            }
                        }else{
                            $new_images_product = rand() . '.' . $extension;
                            $file_local->move(public_path('images/product'), $new_images_product);
                            $dataImages=[
                                "images"=>$new_images_product,
                                'product_images_id'=>$id
                            ];
                            ImagesService::createModel($dataImages);
                        }
                    }
                }
                if($request->has('images_avata_4')!="") {
                    $file_local =  $request->file('images_avata_4');
                    $extension = $request->file('images_avata_4')->getClientOriginalExtension();
                    if($extension == 'jpg' || $extension == 'png' || $extension == 'jpeg'){
                        $idImeges = ImagesService::getModelByImages( $images[3]['images']);
                        if(file_exists(public_path("images/product/". $images[3]['images']))){
                            unlink(public_path("images/product/". $images[3]['images']));
                            $del = ImagesService::RemoveModel($idImeges->id);
                            if($del){
                                $new_images_product = rand() . '.' . $extension;
                                $file_local->move(public_path('images/product'), $new_images_product);
                                $dataImages=[
                                    "images"=>$new_images_product,
                                    'product_images_id'=>$id
                                ];
                                ImagesService::createModel($dataImages);
                            }
                        }else{
                            $new_images_product = rand() . '.' . $extension;
                            $file_local->move(public_path('images/product'), $new_images_product);
                            $dataImages=[
                                "images"=>$new_images_product,
                                'product_images_id'=>$id
                            ];
                            ImagesService::createModel($dataImages);
                        }
                    }
                }
            }
            return AbstractService::ResultSuccess($asc);
        }else {
            return AbstractService::ResultError("An error occurred, please try again");
        }
    }
}
