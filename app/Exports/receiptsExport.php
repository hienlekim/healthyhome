<?php

namespace App\Exports;
use DB;
use Excel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class receiptsExport implements FromView
{
    public $customer;
    public $receipts_period_amount;
    public $phone;
    public $receipts_currency;
    public $customers_address;
    public $period_payments;
    public $receipts_period_date;
    public $interest_rate;
    public $period_name;
    public $period_date;
    public $receipts_amount_date;
    public $receipts_amount;
    public $price_lasts;
    public $number_days_overdue;
    public $overdue_fines;
    public $actual_amount;
    public $staff;
    public $date;
    public $brand;
    public function __construct($filter)
    {
        $this->customer = $filter['customer'];
        $this->receipts_period_amount = $filter['receipts_period_amount'];
        $this->phone = $filter['phone'];
        $this->receipts_currency = $filter['receipts_currency'];
        $this->customers_address = $filter['customers_address'];
        $this->period_payments = $filter['period_payments'];
        $this->receipts_period_date = $filter['receipts_period_date'];
        $this->interest_rate = $filter['interest_rate'];
        $this->period_name = $filter['period_name'];
        $this->period_date = $filter['period_date'];
        $this->receipts_amount_date = $filter['receipts_amount_date'];
        $this->receipts_amount = $filter['receipts_amount'];
        $this->price_lasts = $filter['price_lasts'];
        $this->number_days_overdue = $filter['number_days_overdue'];
        $this->overdue_fines = $filter['overdue_fines'];
        $this->actual_amount = $filter['actual_amount'];
        $this->staff = $filter['staff'];
        $this->date = $filter['date'];
        $this->brand = $filter['brand'];
    }
    public function view(): View
    {
        $customer =$this->customer;
        $receipts_period_amount =$this->receipts_period_amount;
        $phone =$this->phone;
//      $receipts_amount =$this->receipts_amount
        $receipts_currency =$this->receipts_currency;
        $customers_address =$this->customers_address;
        $period_payments =$this->period_payments;
        $receipts_period_date =$this->receipts_period_date;
        $interest_rate =$this->interest_rate;
        $period_name = $this->period_name;
        $period_date = $this->period_date;
        $receipts_amount_date =$this->receipts_amount_date;
        $receipts_amount = $this->receipts_amount;
        $price_lasts =$this->price_lasts;
        $number_days_overdue =$this->number_days_overdue;
        $overdue_fines = $this->overdue_fines;
        $actual_amount =$this->actual_amount;
        $staff =$this->staff;
        $date =$this->date;
        $brand = $this->brand;
        return view('receipts.export',compact('customer','receipts_period_amount','phone','receipts_currency',
        'customers_address','period_payments','receipts_period_date','interest_rate','period_name','period_date','receipts_amount_date','receipts_amount',
        'price_lasts','number_days_overdue','overdue_fines','actual_amount','staff','date','brand'));
    }
}
