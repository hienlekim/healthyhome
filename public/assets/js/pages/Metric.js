/*
|--------------------------------------------------------------------------
| developer: Lê kim Hiển
| date: 05-02-2020
| description: đếm số dòng đơn vị tính
|--------------------------------------------------------------------------
*/
$('#count_list_sreach').change(function(){
    var limit = this.value;
    if(limit>0){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'Metric/count_column/'+ limit,
            type:'GET',
            data:{'id':limit},
            success :function(data) {
                $('tbody#result_search').html(data);
            }
        });
    }
});
/*
|--------------------------------------------------------------------------
| developer: Lê kim Hiển
| date: 05-02-2020
| description: tìm kiếm theo tên đơn vị tính
|--------------------------------------------------------------------------
*/
$('#form-sreach').on('submit',function(event) {
    event.preventDefault();
    if($('#sreach').val()=='sreach'){
        var search = $('#search').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'Metric/count_sreach',
            type:'POST',
            data:{'search':search},
            success :function(data) {
                $('tbody#result_search').html(data);
            }
        });
    }
});
/*
|--------------------------------------------------------------------------
| developer: Lê kim Hiển
| date: 05-02-2020
| description: tìm kiếm theo tên đơn vị tính
|--------------------------------------------------------------------------
*/
$('#form_add_metric').on('submit',function(event) {
    event.preventDefault();
    if($('#insert').val()=='insert'){
        var symbol = $('#symbol').val();
        var name = $('#name').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'Metric/metrics_store',
            type:'POST',
            data:{'symbol':symbol,'name':name},
            success :function(data) {
                var html = '';
                if(data.success)
                {
                    html = '<div class="alert alert-success">' + data.success + '</div>';
                }
                $('#form_add_metric')[0].reset();
                $('#result').html(html);
            }
        });
    }
});
/*
|--------------------------------------------------------------------------
| developer: Lê kim Hiển
| date: 05-02-2020
| description: cập nhật thông tin đơn vị tính
|--------------------------------------------------------------------------
*/
$('a#edit').click(function(){
    $('div#miModal').modal('show');
    var id = $(this).data('id');
    if(id>0){
        $.get("Metric/metrics_edit/" + id , function (data) {
            console.log(data[0].id);
            $('input#id').val(data[0].id);
            $('input#names').val(data[0].name);
            $('input#symbols').val(data[0].symbol);
        });
    }
});
$('#form_edit_metric').on('submit',function(event) {
    event.preventDefault();
    if($('#update').val()=='update'){
        var symbol = $('#symbols').val();
        var name = $('#names').val();
        var id = $('#id').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'Metric/metrics_update',
            type:'POST',
            data:{'id':id,'symbol':symbol,'name':name},
            success :function(data) {
                var html = '';
                if(data.success)
                {
                    html = '<div class="alert alert-success">' + data.success + '</div>';
                }
                $('#result2').html(html);
            }
        });
    }
});
/*
|--------------------------------------------------------------------------
| developer: Lê kim Hiển
| date: 05-02-2020
| description: khi chọn đóng chức năng sẽ tự động load trang
|--------------------------------------------------------------------------
*/
$('.closed,.closeds').click(function(){
    window.location.reload('.display_metrics');
});
/*
|--------------------------------------------------------------------------
| developer: Lê kim Hiển
| date: 05-02-2020
| description: xóa thông tin đơn vị tính
|--------------------------------------------------------------------------
*/
$('a#delete').click(function(){
    var id = $(this).data('id');
    if(id>0){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url:'Metric/metrics_delete/'+ id,
            type:'GET',
            data:{'id':id},
            success :function(data) {
                window.location.reload('.display_metrics');
            }
        });
    }
});