-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th8 07, 2022 lúc 09:30 AM
-- Phiên bản máy phục vụ: 5.6.51-cll-lve
-- Phiên bản PHP: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_healthyhomes_reports`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bank_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã ngân hàng',
  `bank_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên ngân hàng',
  `bank_number` varchar(100) CHARACTER SET utf8 NOT NULL,
  `bank_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `bank_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bank`
--

INSERT INTO `bank` (`id`, `bank_code`, `bank_name`, `bank_number`, `bank_user_created`, `bank_user_updated`, `created_at`, `updated_at`) VALUES
(11, 'VCB C', 'Vietcombank C', '001223879656', NULL, NULL, '2021-10-23 09:26:58', '2022-02-25 11:36:04'),
(12, 'VCB', 'Vietcombank', '00100980078', NULL, NULL, '2021-10-23 09:27:34', '2021-10-23 09:27:34'),
(13, 'ACB', 'Ngân hàng ACB', '033477900988', NULL, NULL, '2021-10-23 09:29:07', '2022-02-25 11:22:23'),
(14, 'credit card', 'MPOS', '02233666987', NULL, NULL, '2021-10-23 09:32:21', '2022-02-25 11:35:03'),
(15, 'ABA', 'ABA', '00121', NULL, NULL, '2022-02-25 11:36:22', '2022-02-25 11:36:22'),
(16, 'MAYBANK C', 'MAYBANK C', '00321', NULL, NULL, '2022-02-25 11:36:45', '2022-02-25 11:36:45'),
(17, 'MAYBANK P', 'MAYBANK P', '02468', NULL, NULL, '2022-02-25 11:37:19', '2022-02-25 11:37:19'),
(18, 'ACLEDA', 'ACLEDA', '0987', NULL, NULL, '2022-02-25 11:43:39', '2022-02-25 11:43:39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã chi nhánh',
  `brand_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên chi nhánh',
  `brand_address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'địa chỉ',
  `brand_country_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'quốc gia',
  `brand_bank_id` int(11) DEFAULT NULL,
  `brand_bank_id_1` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `brand_bank_id_2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `brand_bank_id_3` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `brand_bank_id_4` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `brand_currency_id` int(11) DEFAULT NULL,
  `brand_ware_house_id` int(11) DEFAULT NULL,
  `brand_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `brand_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `brand`
--

INSERT INTO `brand` (`id`, `brand_code`, `brand_name`, `brand_address`, `brand_country_id`, `brand_bank_id`, `brand_bank_id_1`, `brand_bank_id_2`, `brand_bank_id_3`, `brand_bank_id_4`, `brand_currency_id`, `brand_ware_house_id`, `brand_user_created`, `brand_user_updated`, `created_at`, `updated_at`) VALUES
(3, 'cnvietnam', 'CN Việt Nam', 'cần thơ', '1', 11, '12', '13', NULL, NULL, 2, 1, NULL, NULL, '2021-10-23 09:51:05', '2021-11-25 09:32:57'),
(4, 'CNcampuchia', 'CN cambodia', 'Phnôm Pênh', '2', 14, NULL, NULL, NULL, NULL, 1, 2, NULL, NULL, '2021-10-23 09:54:37', '2021-12-02 11:01:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `commission`
--

CREATE TABLE `commission` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `commission_rank_id` int(11) DEFAULT NULL COMMENT 'cấp độ',
  `commission_currency_id` int(11) DEFAULT NULL COMMENT 'loại tiên tệ',
  `commission_brand_id` int(11) DEFAULT NULL COMMENT 'loại tiên tệ',
  `commission_amount` decimal(13,4) DEFAULT NULL COMMENT 'số tiền hoa hồng được nhận',
  `commission_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `commission_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `commission_rank`
--

CREATE TABLE `commission_rank` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `commission_rank_staff_parent_id` int(11) DEFAULT NULL COMMENT 'nhân viên cấp cha ',
  `commission_rank_staff_id` int(11) DEFAULT NULL COMMENT 'đơn hàng nhân viên cấp con',
  `commission_rank_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `commission_rank_period_id` int(11) DEFAULT NULL COMMENT 'hình thức thanh toán',
  `commission_rank_rank_id` int(11) DEFAULT NULL COMMENT 'Tên hình thức thanh toán',
  `commission_rank_commission` decimal(13,4) DEFAULT NULL COMMENT 'mức hoa hồng',
  `commission_rank_commission_amount` decimal(13,4) DEFAULT NULL COMMENT 'tiền thực lãnh',
  `commission_rank_actual_month` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tháng thực lãnh',
  `commission_rank_currency_id` int(11) DEFAULT NULL COMMENT 'mệnh giá tiền tệ',
  `commission_rank_status` tinyint(1) DEFAULT NULL COMMENT 'Trạng thái',
  `commission_rank_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `commission_rank_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `country`
--

CREATE TABLE `country` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã quốc gia',
  `country_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên quốc gia',
  `country_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `country_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `country`
--

INSERT INTO `country` (`id`, `country_code`, `country_name`, `country_user_created`, `country_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'VN', 'Việt Nam', NULL, NULL, '2021-10-19 09:35:19', '2021-10-19 09:35:19'),
(2, 'CPC', 'Campuchia', NULL, NULL, '2021-10-19 09:36:11', '2021-10-19 09:43:35');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `currency`
--

CREATE TABLE `currency` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `currency_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã tiền tệ',
  `currency_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên tiền tệ',
  `currency_symbol` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'kí hiệu',
  `currency_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `currency_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `currency`
--

INSERT INTO `currency` (`id`, `currency_code`, `currency_name`, `currency_symbol`, `currency_user_created`, `currency_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'USD', 'Dollar', '$', NULL, NULL, '2021-10-19 09:15:36', '2021-10-23 09:25:22'),
(2, 'VND', 'Việt Nam Đồng', 'VND', NULL, NULL, '2021-10-23 09:25:47', '2021-10-23 09:25:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customers_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã khách hàng',
  `customers_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên khách hàng',
  `customers_payment_status` tinyint(1) DEFAULT NULL COMMENT 'khách hàng nơ status=1,status=0 khách trả',
  `customers_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã khách hàng',
  `customers_address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'địa chỉ',
  `car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_type` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customers_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customers_country_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'quốc gia',
  `customers_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `customers_parent_id` int(11) DEFAULT NULL COMMENT 'khách hàng cấp cha',
  `customers_staffs_id` int(11) DEFAULT NULL,
  `customers_stock_id` int(11) DEFAULT NULL COMMENT 'nếu đã tặng quà thì gán id stock vào',
  `customers_handover_status` tinyint(4) DEFAULT NULL COMMENT 'trạng thái chuyển đổi giao việc',
  `customers_handover_date_start` date DEFAULT NULL COMMENT 'ngày tạo trạng thái chuyển đổi',
  `customers_handover_date_approve` date DEFAULT NULL COMMENT 'ngày nhân viên tiếp nhận duyệt',
  `customers_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `customers_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `customers_status` tinyint(1) DEFAULT NULL COMMENT 'trạng thái khách hàng',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `customers_code`, `customers_name`, `customers_payment_status`, `customers_email`, `customers_address`, `car`, `house_type`, `level`, `customers_phone`, `customers_country_id`, `customers_brand_id`, `customers_parent_id`, `customers_staffs_id`, `customers_stock_id`, `customers_handover_status`, `customers_handover_date_start`, `customers_handover_date_approve`, `customers_user_created`, `customers_user_updated`, `customers_status`, `created_at`, `updated_at`) VALUES
(16, 'KH00001', 'nguyễn văn a', 1, 'sales@gmail.com', NULL, NULL, NULL, NULL, '0888035604', NULL, 3, NULL, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-02-13 14:55:01', '2022-07-03 01:57:34'),
(17, 'KH00002', 'Nguyễn văn b', 1, 'hien.lekim@gmail.com', NULL, NULL, NULL, NULL, '0888035604', NULL, 3, NULL, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-02-26 14:38:43', '2022-07-03 01:57:34'),
(18, 'KH00003', 'lê kim hiển', 2, 'hien.lekim@gmail.com', NULL, NULL, 'Villa', NULL, '0888035604', NULL, 3, NULL, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, NULL, '2022-02-27 01:56:32', '2022-07-23 16:58:54'),
(19, 'KH00004', 'lê kim hiển child', 1, 'hien.lekim@gmail.com', NULL, NULL, 'Villa', NULL, '0888035604', NULL, 3, 18, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-02-27 01:59:37', '2022-07-03 01:57:34'),
(20, 'KH00005', 'sssssssss', 1, 'hien@gmail.com', NULL, NULL, 'Flat', NULL, 'ssssssssss', NULL, 3, 18, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-02-27 02:08:32', '2022-07-03 01:57:34'),
(21, 'KH00006', 'Trần nguyên văn A', 1, 'hien.lekim@gmail.com', NULL, NULL, 'Villa', 'Rich', '0888035604', NULL, 3, 18, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-02-27 23:41:13', '2022-07-03 01:57:34'),
(22, 'KH00007', 'Cô Tú', 1, 'Tu@gmail.com', 'B15 đường 5b phạm hữu lầu q7', 'No', 'Villa', 'Rich', '0909736338', NULL, 3, NULL, 95, 94, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 2, '2022-04-27 14:18:43', '2022-07-03 01:57:34'),
(23, 'KH00008', 'Thiên Di', 1, 'Phamthiendi@gmail.com', NULL, NULL, 'Villa', 'Rich', '0354088683', NULL, 3, NULL, 107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-27 14:23:25', '2022-04-28 09:17:44'),
(24, 'KH00009', 'Vũ', NULL, 'Dinhhoangvu2271997@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0397012302', NULL, 3, NULL, 108, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 14:25:23', '2022-04-27 14:25:23'),
(25, 'KH00010', 'Phạm Nhi', NULL, 'phamnhi@gmail.com', NULL, NULL, NULL, NULL, '01208122339', NULL, 3, NULL, 111, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 14:25:29', '2022-04-27 14:25:29'),
(26, 'KH00011', 'Cường', 1, 'Cuong@gmail.com', NULL, NULL, 'Small house', 'Poor', '0702327603', NULL, 3, NULL, 106, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 14:27:06', '2022-04-28 09:04:36'),
(27, 'KH00012', 'H’Ôni Niê Siêng', NULL, 'hnavynie0196@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0372478947', NULL, 3, NULL, 99, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 14:31:01', '2022-04-27 14:31:01'),
(28, 'KH00013', 'Ngọc Hằng', 1, 'Hang@gmail.com', 'Chung cư MOne 35/12 Bế Văn Cấm, Quận 7', NULL, 'Apartment', 'Medium', '0982094870', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 14:37:02', '2022-04-28 09:12:52'),
(29, 'KH00014', 'Hồ Lẹ Dung', NULL, 'holedung0196@gmail.com', NULL, NULL, 'Villa', 'Rich', '+84 90 3680722', NULL, 3, 27, 99, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 14:40:52', '2022-04-27 14:40:52'),
(30, 'KH00015', 'Vu', NULL, 'Vu@gmail.com', NULL, '100', 'Villa', 'Rich', '0913456789', NULL, 3, 28, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 14:41:29', '2022-04-27 14:41:29'),
(31, 'KH00016', 'Cô liên', 1, 'Lien@gmail.com', NULL, NULL, NULL, 'Medium', '0789231907', NULL, 3, 22, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-04-27 14:42:02', '2022-07-03 01:57:34'),
(32, 'KH00017', 'Cô Hồng', NULL, 'Hong@gmail.com', NULL, NULL, NULL, NULL, '0963025679', NULL, 3, 22, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-04-27 14:43:34', '2022-07-03 01:57:34'),
(33, 'KH00018', 'Nhật', 1, 'Nhat@gmail.com', NULL, '1', 'Apartment', 'Rich', '0918672976', NULL, 3, 28, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 15:00:57', '2022-04-28 09:06:34'),
(34, 'KH00019', 'Thiện', NULL, 'Thien@gmail.com', NULL, NULL, 'Flat', 'Medium', '0835852989', NULL, 3, NULL, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-04-27 15:30:20', '2022-07-03 01:57:34'),
(35, 'KH00020', 'Lai', NULL, 'hanvy123@gmail.com', NULL, '2', 'Villa', 'Rich', '0372478948', NULL, 3, 27, 99, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 15:35:35', '2022-04-27 15:35:35'),
(36, 'KH00021', 'Tuấn', NULL, 'tuanyeurb@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0835852989', NULL, 3, 34, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-04-27 22:14:03', '2022-07-03 01:57:34'),
(37, 'KH00022', 'thy', 1, 'abc@gmail.com', NULL, NULL, 'Villa', 'Medium', '0835852989', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-27 22:35:53', '2022-04-27 22:47:48'),
(38, 'KH00023', 'thien 1', 1, 'abc@gmail.com', NULL, NULL, NULL, NULL, '0835852989', NULL, 3, 37, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 22:45:32', '2022-04-27 22:46:26'),
(39, 'KH00024', 'Tùng', 1, 'abc@gmail.com', NULL, NULL, 'Flat', 'Medium', '123', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-27 23:41:02', '2022-04-28 09:19:36'),
(40, 'KH00024', 'Tùng', NULL, 'abc@gmail.com', NULL, NULL, 'Flat', 'Medium', '123', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 23:41:02', '2022-04-27 23:41:02'),
(41, 'KH00025', 'nhi', 1, 'abc@gmail.com', NULL, NULL, 'Villa', NULL, NULL, NULL, 3, 39, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-27 23:46:04', '2022-04-27 23:47:45'),
(42, 'KH00026', 'Thiến', 1, 'Thien@gmail.com', NULL, '0', 'Small house', 'Poor', '0909090909', NULL, 3, 26, 106, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-28 08:46:49', '2022-04-28 09:16:38'),
(43, 'KH00027', 'Dung', 1, 'Tung@gmail.com', NULL, NULL, NULL, NULL, '0835852989', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-28 08:47:51', '2022-04-30 14:20:25'),
(44, 'KH00028', 'Thiện Giàu', NULL, 'ABC@gmail.com', NULL, '10', 'Villa', 'Rich', '0372478947', NULL, 3, 27, 99, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-28 08:48:09', '2022-04-28 08:48:09'),
(45, 'KH00029', 'Nhi Nhi', NULL, 'abc@gmail.com', NULL, '1', 'Flat', 'Rich', '0354088683', NULL, 3, 23, 107, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-28 08:49:44', '2022-04-28 08:49:59'),
(46, 'KH00030', 'Thanh Quân', 1, 'Hu@gmail.com', NULL, '1', 'Villa', 'Rich', '373734', NULL, 3, 23, 107, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-28 09:15:20', '2022-04-28 09:16:28'),
(47, 'KH00031', 'huy', 1, 'abc@gmail.com', 'abc', NULL, NULL, NULL, NULL, NULL, 3, 37, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-28 18:17:48', '2022-04-28 18:25:09'),
(48, 'KH00032', 'khanh', 1, 'abc@gmail.com', NULL, NULL, NULL, NULL, '123', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2022-04-28 18:32:53', '2022-04-28 18:37:16'),
(49, 'KH00033', 'trieu', 1, 'abc@gmail.com', NULL, NULL, NULL, NULL, '123', NULL, 3, 48, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-04-28 18:33:37', '2022-04-28 18:34:34'),
(50, 'KH00034', 'nguyen van aaaaaaaa', NULL, 'hien.lekim@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 3, 18, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-05-03 13:42:58', '2022-07-03 01:57:34'),
(51, 'KH00035', 'Nguyen Van Lac', NULL, 'lac@gmail.com', NULL, NULL, 'Small house', 'Medium', '0908335537', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:36:10', '2022-05-05 14:36:10'),
(52, 'KH00036', 'Chi Thuy', NULL, 'thuy@gmail.com', NULL, NULL, NULL, NULL, '0908335537', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:40:41', '2022-05-05 14:40:41'),
(53, 'KH00037', 'chi thuy hc', NULL, 'thuyhc@gmail.com', NULL, NULL, NULL, NULL, '0902955583', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:41:57', '2022-05-05 14:41:57'),
(54, 'KH00038', 'Anh Qui', NULL, 'qui@gmail.com', NULL, NULL, NULL, NULL, '0903987515', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:42:47', '2022-05-05 14:42:47'),
(55, 'KH00039', 'Anh Binh', NULL, 'binh@gmail.com', NULL, NULL, NULL, NULL, '0906494967', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:43:24', '2022-05-05 14:43:24'),
(56, 'KH00040', 'Chi Uyen', NULL, 'uyen@gmail.com', NULL, NULL, NULL, NULL, '0903705769', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:44:01', '2022-05-05 14:44:01'),
(57, 'KH00041', 'Anh Dang', NULL, 'dang@gmail.com', NULL, NULL, NULL, NULL, '0903692084', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:44:42', '2022-05-05 14:44:42'),
(58, 'KH00042', 'Anh Hoang', NULL, 'hoang@gmail.com', NULL, NULL, NULL, NULL, '0908373697', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:45:17', '2022-05-05 14:45:17'),
(59, 'KH00043', 'Anh Hung', NULL, 'hung@gmail.com', NULL, NULL, NULL, NULL, '0908901330', NULL, 3, 51, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:45:52', '2022-05-05 14:45:52'),
(60, 'KH00044', 'Anh Quynh', NULL, 'quynh@gmail.com', NULL, NULL, NULL, NULL, '0906691338', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:47:09', '2022-05-05 14:47:09'),
(61, 'KH00045', 'Chi Linh', NULL, 'linh@gmail.com', NULL, NULL, NULL, NULL, '0903019017', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-05 14:47:48', '2022-05-05 14:47:48'),
(62, 'KH00046', 'Cô Tuyết', NULL, 'tuyetntk123@gmail.com', NULL, '0', 'Apartment', 'Medium', '0903760621', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 09:41:28', '2022-05-08 09:41:28'),
(63, 'KH00047', 'Chị Giang', NULL, 'Giang123@gmail.com', NULL, '0', 'Apartment', 'Medium', '0938271987', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:16:51', '2022-05-08 11:16:51'),
(64, 'KH00048', 'Chị Loan', NULL, 'Loan123@gmail.com', NULL, '0', 'Apartment', 'Medium', '0979071624', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:18:11', '2022-05-08 11:18:11'),
(65, 'KH00049', 'Cô Hường', NULL, 'Huong123@gmail.com', NULL, '0', NULL, NULL, '0909814594', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:19:44', '2022-05-08 11:19:44'),
(66, 'KH00050', 'Cô Thảo', NULL, 'Thao123@gmail.com', NULL, '0', NULL, NULL, '0984881826', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:22:33', '2022-05-08 11:22:33'),
(67, 'KH00051', 'Chị Khánh Hòa', NULL, 'Hoa123@gmai.com', NULL, NULL, NULL, NULL, '0903756956', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:23:37', '2022-05-08 11:23:37'),
(68, 'KH00052', 'Chị Thoa', NULL, 'Thoa123@gmail.com', NULL, NULL, NULL, NULL, '0902928496', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:24:39', '2022-05-08 11:24:39'),
(69, 'KH00053', 'Cô Thúy', NULL, 'Thuy123@gmail.com', NULL, NULL, NULL, NULL, '0985718378', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:25:33', '2022-05-08 11:25:33'),
(70, 'KH00054', 'Cô Nhĩ', NULL, 'Tuyet123@gmail.com', NULL, NULL, NULL, NULL, '0934826669', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:26:27', '2022-05-08 11:26:27'),
(71, 'KH00055', 'Chị Huyền', NULL, 'Huyen123@gmail.com', NULL, NULL, NULL, NULL, '0984783683', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:28:05', '2022-05-08 11:28:05'),
(72, 'KH00056', 'Cô Phúc', NULL, 'Phuc123@gmail.com', NULL, NULL, NULL, NULL, '0986101033', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:28:47', '2022-05-08 11:28:47'),
(73, 'KH00057', 'Cô Bảy', NULL, 'Bay123@gmail.com', NULL, NULL, NULL, NULL, '0913435988', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:29:30', '2022-05-08 11:29:30'),
(74, 'KH00058', 'Cô Phước Anh', NULL, 'Phuocanh123@gmail.com', NULL, NULL, NULL, NULL, '0813957283', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:30:14', '2022-05-08 11:30:14'),
(75, 'KH00059', 'Chị Thanh', NULL, 'Thanh123@gmail.com', NULL, NULL, NULL, NULL, '0908443839', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:30:56', '2022-05-08 11:30:56'),
(76, 'KH00060', 'Chị Thảo', NULL, 'Thao123@gmail.com', NULL, NULL, NULL, NULL, '0983666108', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:31:35', '2022-05-08 11:31:35'),
(77, 'KH00061', 'Chị Thảo (Đảng viên)', NULL, 'Thao123@gmail.com', NULL, NULL, NULL, NULL, '0972725787', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:32:27', '2022-05-08 11:32:27'),
(78, 'KH00062', 'Anh Hà', NULL, 'Ha123@gmaik.com', NULL, NULL, NULL, NULL, '0909599242', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-08 11:33:06', '2022-05-08 11:33:06'),
(79, 'KH00063', 'Hoàng', 1, 'Habilove@gmail.com', NULL, NULL, NULL, NULL, '123', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2022-05-11 11:27:48', '2022-05-11 11:47:51'),
(80, 'KH00064', 'A Mập', 1, 'Abc@gmail.com', NULL, NULL, 'Small house', 'Poor', '0908149064', NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2022-05-11 11:28:20', '2022-05-11 11:47:56'),
(81, 'KH00065', 'Cô Thuỳ', 1, 'abc@gmail.com', NULL, '1', 'Villa', 'Rich', '1234', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:28:27', '2022-05-11 11:40:38'),
(82, 'KH00066', 'Kim Anh', 1, 'Abc@gmail.com', NULL, NULL, NULL, 'Poor', NULL, NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:28:45', '2022-05-11 11:43:06'),
(83, 'KH00067', 'Bi', NULL, 'Abc12@gmail.com', NULL, NULL, NULL, NULL, '0966619058', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:29:25', '2022-05-11 11:29:25'),
(84, 'KH00068', 'Long', 1, 'Abc@gmail.com', NULL, NULL, NULL, NULL, '123', NULL, 3, 79, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:30:29', '2022-05-11 11:44:47'),
(85, 'KH00069', 'Dũng', NULL, 'Abc12@gmail.com', NULL, NULL, NULL, NULL, '0966619058', NULL, 3, 83, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:30:39', '2022-05-11 11:30:39'),
(86, 'KH00070', 'Chị Mai', 1, 'Abc@gmail.com', NULL, NULL, 'Small house', 'Poor', '0907202883', NULL, 3, 80, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:31:01', '2022-05-11 11:44:11'),
(87, 'KH00071', 'Cô Huệ', 1, 'abc@gmail.com', NULL, '1', 'Apartment', 'Rich', '5678', NULL, 3, 81, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:31:29', '2022-05-11 11:45:32'),
(88, 'KH00072', 'Nhi', NULL, 'Aaa@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0779141142', NULL, 3, 82, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-11 11:32:10', '2022-05-11 11:32:10'),
(89, 'KH00073', 'Thien', 1, 'Fahsh@gmail.com', NULL, 'Lambogini', 'Villa', 'Poor', '0907841815', NULL, 3, NULL, 104, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2022-05-12 09:51:43', '2022-05-12 10:04:18'),
(90, 'KH00074', 'Vũ', 1, 'Dgdjevdjd@gmail.com', NULL, 'Ferrary', 'Small house', 'Rich', '364829r6', NULL, 3, 89, 104, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-12 09:53:33', '2022-05-12 09:59:45'),
(91, 'KH00075', 'Chị Danh+Anh Quỳnh', NULL, 'quynh@gmail.com', NULL, '0', 'Small house', 'Medium', '0813555789', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:12:08', '2022-05-17 09:12:08'),
(92, 'KH00076', 'Cô Hạnh', NULL, 'Hanh1234@gmail.com', NULL, NULL, NULL, NULL, '0902971194', NULL, 3, 91, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:13:46', '2022-05-17 09:13:46'),
(93, 'KH00077', 'Chị Ngà', NULL, 'Nga@gmail.com', NULL, NULL, NULL, NULL, '0903383862', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:14:27', '2022-05-17 09:14:27'),
(94, 'KH00078', 'Chị Hoài', NULL, 'Aq2@gmail.com', NULL, NULL, NULL, NULL, '0938450208', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:15:17', '2022-05-17 09:15:17'),
(95, 'KH00079', 'Chị Linh', NULL, 'Linh23@gmail.com', NULL, NULL, NULL, NULL, '0938121266', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:16:18', '2022-05-17 09:16:18'),
(96, 'KH00080', 'Chị Tuyết', NULL, 'Tuyet13@gmail.com', NULL, NULL, NULL, NULL, '0909023685', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:18:23', '2022-05-17 09:18:23'),
(97, 'KH00081', 'Cô Phượng', NULL, 'Phuong12@gmail.com', NULL, NULL, NULL, NULL, '0918951559', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:19:37', '2022-05-17 09:19:37'),
(98, 'KH00082', 'Cô Hương', NULL, 'Huong12@gmail.com', NULL, NULL, NULL, NULL, '0903346377', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:21:37', '2022-05-17 09:21:37'),
(99, 'KH00083', 'Anh Nam', NULL, 'Nam@gmail.com', NULL, NULL, NULL, NULL, '0902928496', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:22:41', '2022-05-17 09:22:41'),
(100, 'KH00084', 'Anh Thạch Hùng', NULL, 'AHung1@gmail.com', NULL, NULL, NULL, NULL, '0774790828', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:23:44', '2022-05-17 09:23:44'),
(101, 'KH00085', 'Chị Linh', NULL, 'Linh34@gmail.com', NULL, NULL, NULL, NULL, '0974499959', NULL, 3, 99, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:25:16', '2022-05-17 09:25:16'),
(102, 'KH00086', 'Anh Long', NULL, 'Long12@gmail.com', NULL, NULL, NULL, NULL, '0937999257', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:26:30', '2022-05-17 09:26:30'),
(103, 'KH00087', 'Anh Lâm', NULL, 'Lam123@gmail.com', NULL, NULL, NULL, NULL, '0903558812', NULL, 3, 62, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:27:10', '2022-05-17 09:27:10'),
(104, 'KH00088', 'Chị Trang Belleza', NULL, 'tranghhh@gmail.com', NULL, NULL, NULL, NULL, '0939726345', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-05-17 09:28:23', '2022-05-17 09:28:23'),
(105, 'KH00089', 'Chị Hiền', NULL, 'As@123gmail.com', NULL, NULL, 'Flat', 'Medium', '0902435529', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-02 17:58:12', '2022-06-02 17:58:12'),
(106, 'KH00090', 'Chị Hảo', NULL, 'Hao@gmail.com', NULL, NULL, 'Villa', NULL, '0933846256', NULL, 3, 105, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-02 17:59:16', '2022-06-22 23:19:32'),
(107, 'KH00091', 'Chị Xuân', NULL, 'Xuan@gmail.com', NULL, NULL, NULL, NULL, '0902583472', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-02 18:00:17', '2022-06-02 18:00:17'),
(108, 'KH00092', 'Chị Vân', NULL, 'Van@gmail.com', NULL, NULL, NULL, NULL, '0938128601', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-02 18:00:57', '2022-06-02 18:00:57'),
(109, 'KH00093', 'Chị Nga Moscow Tower Q12', NULL, 'Aaaa@gmail.com', NULL, NULL, 'Apartment', 'Poor', '0972221503', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-06 10:49:09', '2022-06-06 10:49:09'),
(110, 'KH00094', 'Chị Ly', NULL, 'Lyyyy@gmail.com', NULL, NULL, 'Flat', 'Medium', '0973720730', NULL, 3, 109, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-06 10:51:10', '2022-06-06 10:51:10'),
(111, 'KH00095', 'Chị Yến', NULL, 'Yenn@gmail.com', NULL, NULL, NULL, NULL, '0975440995', NULL, 3, 110, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-06 10:52:55', '2022-06-06 10:52:55'),
(112, 'KH00096', 'Chị Oanh', NULL, 'Ly@gmail.com', NULL, NULL, NULL, NULL, '0912331288', NULL, 3, 110, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-06 10:56:02', '2022-06-06 10:56:02'),
(113, 'KH00097', 'Chị Thuý ( Chị Tâm)', NULL, 'thuy@gmail.com', NULL, '0', 'Small house', 'Medium', '0906656160 ( 0934166014)', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 09:51:02', '2022-06-15 09:51:02'),
(114, 'KH00098', 'Chị Bích', NULL, 'Bich@gmail.com', NULL, '1', 'Apartment', 'Medium', '0934109088', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 09:56:07', '2022-06-15 09:56:07'),
(115, 'KH00099', 'Cô Bằng', NULL, 'bang@gmail.com', NULL, '0', 'Small house', 'Medium', '0378855397', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 09:57:41', '2022-06-15 09:57:41'),
(116, 'KH00100', 'Bạch Dương', NULL, 'bachduong@gmail.com', NULL, '0', 'Small house', 'Medium', '0395180479', NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 10:07:26', '2022-06-15 10:07:26'),
(117, 'KH00101', 'Dì Trinh', NULL, 'Trinh@gmail.com', NULL, '0', 'Small house', 'Rich', '0975465353', NULL, 3, 116, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 10:08:54', '2022-06-15 10:08:54'),
(118, 'KH00102', 'Chị Trang', NULL, 'Chitrang@gmail.com', NULL, '0', 'Small house', 'Medium', '0903066607', NULL, 3, 116, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 10:10:56', '2022-06-15 10:10:56'),
(119, 'KH00103', 'Chị Thuận', NULL, 'Minhthuan@gmail.com', NULL, '1', 'Apartment', 'Rich', '093 3011087', NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 10:13:37', '2022-06-15 10:13:37'),
(120, 'KH00104', 'Chị Hằng', NULL, 'Abc@gmail.com', NULL, NULL, NULL, NULL, '0934069365', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 14:04:23', '2022-06-15 14:04:23'),
(121, 'KH00105', 'Anh Tuấn', NULL, 'Abc@gmail.com', NULL, NULL, NULL, NULL, '0378878879', NULL, 3, 120, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 14:05:53', '2022-06-15 14:05:53'),
(122, 'KH00106', 'Chị Hường', NULL, 'Abc@gmail.com', NULL, NULL, NULL, NULL, '0935502974', NULL, 3, 120, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 14:06:51', '2022-06-15 14:06:51'),
(123, 'KH00107', 'Cô Thiện', NULL, 'Abc@gmail.com', NULL, NULL, NULL, NULL, '0984016066', NULL, 3, 120, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 14:07:44', '2022-06-15 14:07:44'),
(124, 'KH00108', 'Hương', NULL, 'Huong@12gmail.com', NULL, NULL, NULL, NULL, '0913754773', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 14:09:30', '2022-06-15 14:09:30'),
(125, 'KH00109', 'Chị IVy', NULL, 'Ivy@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0908147568', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 15:01:52', '2022-06-15 15:01:52'),
(126, 'KH00110', 'Chị Mai', NULL, 'Mai@gmail.com', NULL, NULL, 'Villa', 'Rich', '0908384900', NULL, 3, 125, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 15:04:43', '2022-06-15 15:04:43'),
(127, 'KH00111', 'Anh MICHAEL', NULL, 'Anh@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0908889421', NULL, 3, 125, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:00:44', '2022-06-15 17:00:44'),
(128, 'KH00112', 'Chi Bich', NULL, 'bich@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0985250065', NULL, 3, 125, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:02:50', '2022-06-15 17:02:50'),
(129, 'KH00113', 'Chi Ngoc', NULL, 'ngoc@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0909057004', NULL, 3, 125, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:06:16', '2022-06-15 17:06:16'),
(130, 'KH00114', 'Chi Nga', NULL, 'nga@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0912155039', NULL, 3, 125, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:07:22', '2022-06-15 17:07:22'),
(131, 'KH00115', 'Chi Bao Anh', NULL, 'banh@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0906762568', NULL, 3, 125, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:08:45', '2022-06-15 17:08:45'),
(132, 'KH00116', 'Chi Ngoc', NULL, 'ngoc@gmail.com', NULL, '1', 'Flat', 'Rich', '0977703029', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:11:48', '2022-06-15 17:11:48'),
(133, 'KH00117', 'Chi Ngan', NULL, 'ngan@gmail.com', NULL, NULL, 'Flat', 'Rich', '0901951783', NULL, 3, 132, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:28:14', '2022-06-15 17:28:14'),
(134, 'KH00118', 'Co Lan', NULL, 'lan@gmail.com', NULL, NULL, NULL, 'Rich', '0975557137', NULL, 3, 132, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:31:12', '2022-06-15 17:31:12'),
(135, 'KH00119', 'Chi Mai', NULL, 'mai@gmail.com', NULL, NULL, NULL, NULL, '0938845388', NULL, 3, 132, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:32:04', '2022-06-15 17:32:04'),
(136, 'KH00120', 'Chi Trang', NULL, 'trang@gmail.com', NULL, NULL, NULL, NULL, '0983787275', NULL, 3, 132, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:33:10', '2022-06-15 17:33:10'),
(137, 'KH00121', 'Co My Anh', NULL, 'myanh@gmail.com', NULL, NULL, 'Small house', 'Medium', '0913752602', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:35:24', '2022-06-15 17:35:24'),
(138, 'KH00122', 'Co Phuong', NULL, 'phuong@gmail.com', NULL, NULL, NULL, NULL, '0903938183', NULL, 3, 137, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:36:08', '2022-06-15 17:36:08'),
(139, 'KH00123', 'Chi Ngoc', NULL, 'ngoc@gmail.com', NULL, NULL, 'Apartment', 'Rich', '0909057004', NULL, 3, 129, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:37:59', '2022-06-15 17:37:59'),
(140, 'KH00124', 'Chi Oanh', NULL, 'oanh@gmail.com', NULL, NULL, 'Small house', 'Rich', '0935501271', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:40:24', '2022-06-15 17:40:24'),
(141, 'KH00125', 'Chi Phuong Anh', 1, 'phuonganh@gmail.com', NULL, NULL, 'Small house', 'Rich', '0935501271', NULL, 3, 140, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-15 17:41:41', '2022-06-16 09:43:17'),
(142, 'KH00126', 'Chị Chinh', NULL, 'Chinh@gmail.com', NULL, 'Xe hơi', 'Villa', 'Rich', '0902770800', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:34:12', '2022-06-16 09:34:12'),
(143, 'KH00127', 'Chị Trâm', NULL, 'Tram@gmail.com', NULL, 'Xe máy', 'Small house', 'Medium', '0933829932', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:38:38', '2022-06-16 09:38:38'),
(144, 'KH00128', 'Chị Hạnh', NULL, 'Hanh@gmail.com', NULL, 'Xe máy', 'Flat', 'Rich', '0904125473', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:41:55', '2022-06-16 09:41:55'),
(145, 'KH00129', 'Chị Thảo', NULL, 'Thao@gmail.com', NULL, 'Xe máy', 'Flat', 'Rich', '0906309707', NULL, 3, 144, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:46:46', '2022-06-16 09:46:46'),
(146, 'KH00130', 'Chị Lệ', NULL, 'Le@gmail.com', NULL, 'Xe máy', 'Villa', 'Rich', '0903581061', NULL, 3, 142, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:48:05', '2022-06-16 09:48:05'),
(147, 'KH00131', 'Chị Oanh', NULL, 'Oanh@gmail.com', NULL, 'Xe hơi', 'Villa', 'Rich', '0903051066', NULL, 3, 142, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:49:01', '2022-06-16 09:49:01'),
(148, 'KH00132', 'Chị Trinh', NULL, 'Trinh@gmail.com', NULL, 'Xe hơi', 'Villa', 'Rich', '0903908377', NULL, 3, 142, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:50:15', '2022-06-16 09:50:15'),
(149, 'KH00133', 'Phuong Thao', NULL, 'thao@gmail.com', NULL, NULL, NULL, NULL, '0975920205', NULL, 3, 141, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:52:22', '2022-06-16 09:52:22'),
(150, 'KH00134', 'Chị Nga', NULL, 'Nga@gmail.com', NULL, 'Xe hơi', 'Villa', 'Rich', '0903908377', NULL, 3, 142, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:52:36', '2022-06-16 09:52:36'),
(151, 'KH00135', 'Co Thu', NULL, 'thu@gmail.com', NULL, NULL, NULL, NULL, '0903340369', NULL, 3, 141, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:53:07', '2022-06-16 09:53:07'),
(152, 'KH00136', 'Chị Thuỷ', NULL, 'Thuy@gmail.com', NULL, 'Xe hơi', 'Villa', 'Rich', '0988869880', NULL, 3, 142, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-16 09:53:27', '2022-06-16 09:53:27'),
(153, 'KH00137', 'Chị Nam', NULL, 'Ah@gmail.com', NULL, NULL, 'Small house', 'Medium', '0906365595', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-17 11:20:32', '2022-06-17 11:20:32'),
(154, 'KH00138', 'Cô Tiên', NULL, 'cotien@gmail.com', NULL, '0', 'Small house', 'Poor', '0973265740', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 09:43:44', '2022-06-20 09:43:44'),
(155, 'KH00139', 'Chị My', NULL, 'ChiMy@gmail.com', NULL, '1', 'Apartment', 'Medium', '0978037385', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 09:45:23', '2022-06-20 09:45:23'),
(156, 'KH00140', 'Chị Oanh', NULL, 'Oanh@gmail.com', NULL, '0', 'Apartment', 'Medium', '0938141828', NULL, 3, 117, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 09:53:30', '2022-06-20 09:53:30'),
(157, 'KH00141', 'Nguyen Thi Ky Thuy', NULL, 'kythuy123@gmail.com', NULL, '0', 'Flat', 'Rich', '0935301689', NULL, 3, NULL, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:04:39', '2022-06-20 10:04:39'),
(158, 'KH00142', 'Ngoc Lan', NULL, 'ngoclan123@gmail.com', NULL, '0', 'Apartment', 'Poor', '0918849905', NULL, 3, NULL, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:06:22', '2022-06-20 10:06:22'),
(159, 'KH00143', 'Anh Tam', NULL, 'tam123@gmail.com', NULL, '0', 'Villa', 'Rich', '0911880366', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:09:17', '2022-06-20 10:09:17'),
(160, 'KH00144', 'chi Dinh', NULL, 'dinh123@gmail.com', NULL, '0', 'Flat', 'Medium', '081436774', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:10:57', '2022-06-20 10:10:57'),
(161, 'KH00145', 'Chị Hiền', NULL, 'Hien123@gmail.com', NULL, '0', 'Flat', 'Medium', '0774983498', NULL, 3, 113, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:11:01', '2022-06-20 10:11:01'),
(162, 'KH00146', 'chi Nuong', NULL, 'nuong123@gmail.com', NULL, '0', 'Flat', 'Medium', '0901486069', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:12:03', '2022-06-20 10:12:03'),
(163, 'KH00147', 'Chị Thảo', NULL, 'thao345@gmail.com', NULL, '0', 'Flat', 'Rich', '0933105278', NULL, 3, 113, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:12:22', '2022-06-20 10:12:22'),
(164, 'KH00148', 'chi Mong Ha', NULL, 'mongha123@gmail.com', NULL, '0', 'Flat', 'Medium', '0915777238', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:13:05', '2022-06-20 10:13:05'),
(165, 'KH00149', 'Chị Vy', NULL, 'vy456@gmail.com', NULL, '0', 'Flat', 'Medium', '0792776248', NULL, 3, 113, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:13:31', '2022-06-20 10:13:31'),
(166, 'KH00150', 'chi Phuong', NULL, 'phuong123@gmail.com', NULL, '0', 'Flat', 'Medium', '0983540666', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:14:05', '2022-06-20 10:14:05'),
(167, 'KH00151', 'chi Thuy', NULL, 'thuy123@gmail.com', NULL, '0', 'Flat', 'Medium', '0902877108', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:14:52', '2022-06-20 10:14:52'),
(168, 'KH00152', 'Cô Lộc', NULL, 'coloc123@gmail.com', NULL, '0', 'Flat', 'Medium', '0974565304', NULL, 3, 115, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:16:11', '2022-06-20 10:16:11'),
(169, 'KH00153', 'anh Son', NULL, 'son123@gmail.com', NULL, '0', 'Flat', 'Medium', '0968835678', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:16:28', '2022-06-20 10:16:28'),
(170, 'KH00154', 'chi Hanh', NULL, 'hanh123@gmail.com', NULL, '0', 'Flat', 'Medium', '0357704111', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:17:27', '2022-06-20 10:17:27'),
(171, 'KH00155', 'Cô Tuyết', NULL, 'cotuyet345@gmail.com', NULL, '0', 'Flat', 'Medium', '0903934588', NULL, 3, 115, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:18:06', '2022-06-20 10:18:06'),
(172, 'KH00156', 'chi Thuy', NULL, 'thuy1234@gmail.com', NULL, '0', 'Flat', 'Medium', '0909146481', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:18:34', '2022-06-20 10:18:34'),
(173, 'KH00157', 'anh Phong', NULL, 'phong123@gmail.com', NULL, '0', 'Flat', 'Medium', '0983865387', NULL, 3, 157, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:19:37', '2022-06-20 10:19:37'),
(174, 'KH00158', 'Chị Lâm', NULL, 'chilam456@gmail.com', NULL, '0', 'Flat', 'Rich', '0332323326', NULL, 3, 115, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:21:07', '2022-06-20 10:21:07'),
(175, 'KH00159', 'chi thuy', NULL, 'thuy0123@gmail.com', NULL, 'co', 'Villa', 'Rich', '0939219168', NULL, 3, 158, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:21:20', '2022-06-20 10:21:20'),
(176, 'KH00160', 'Anh Tùng', NULL, 'anhtung456@gmail.com', NULL, '0', 'Flat', 'Medium', '0983355174', NULL, 3, 115, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:22:18', '2022-06-20 10:22:18'),
(177, 'KH00161', 'chi Linh', NULL, 'linh123@gmail.com', NULL, '0', 'Apartment', 'Medium', '0906660073', NULL, 3, 158, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:22:22', '2022-06-20 10:22:22'),
(178, 'KH00162', 'Chị Tú', NULL, 'Nam@123gmail.com', NULL, NULL, NULL, NULL, '0978172330', NULL, 3, 153, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:22:33', '2022-06-20 10:22:33'),
(179, 'KH00163', 'chi Hanh', NULL, 'hanh0123@gmail.com', NULL, '0', 'Flat', 'Medium', '0909538299', NULL, 3, 158, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:23:16', '2022-06-20 10:23:16'),
(180, 'KH00164', 'Chị Thủy', NULL, 'Thuy123@gmail.com', NULL, NULL, NULL, NULL, '0789648730', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:23:28', '2022-06-20 10:23:28'),
(181, 'KH00165', 'Chị Lâm', NULL, 'lam123@gmail.com', NULL, '0', 'Flat', 'Rich', '0908007756', NULL, 3, 115, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:23:38', '2022-06-20 10:23:38'),
(182, 'KH00166', 'chi Hong', NULL, 'hong0123@gmail.com', NULL, '0', 'Flat', 'Medium', '0902659870', NULL, 3, 158, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:24:05', '2022-06-20 10:24:05'),
(183, 'KH00167', 'chi HA', NULL, 'ha0123@gmail.com', NULL, '0', 'Flat', 'Medium', '0908497676', NULL, 3, 158, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:24:50', '2022-06-20 10:24:50'),
(184, 'KH00168', 'Chị Tuyết', NULL, 'tuyet123@gmail.com', NULL, '0', 'Flat', 'Rich', '0899473706', NULL, 3, 114, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:25:02', '2022-06-20 10:25:02'),
(185, 'KH00169', 'Chị Lài', NULL, 'Lai123@gmail.com', NULL, NULL, NULL, NULL, '0909459500', NULL, 3, 153, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:25:10', '2022-06-20 10:25:10'),
(186, 'KH00170', 'Anh Hải', NULL, 'Hai123@gmail.com', NULL, NULL, NULL, NULL, '0919488731', NULL, 3, 153, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:25:54', '2022-06-20 10:25:54'),
(187, 'KH00171', 'Chị Tiên', NULL, 'tien123@gmail.com', NULL, '0', NULL, NULL, '0982462734', NULL, 3, 114, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:26:23', '2022-06-20 10:26:23'),
(188, 'KH00172', 'Chị Phong phú', NULL, 'Phongphu@gmail.com', NULL, NULL, NULL, NULL, '0908601174', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:26:39', '2022-06-20 10:26:39'),
(189, 'KH00173', 'Chị Hoa', NULL, 'Hoa123@gmail.com', NULL, NULL, NULL, NULL, '0362779754', NULL, 3, 153, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:27:25', '2022-06-20 10:27:25'),
(190, 'KH00174', 'Chị Ngọc anh', NULL, 'ngocanh@gmail.com', NULL, '0', NULL, NULL, '0907730707', NULL, 3, 114, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:27:38', '2022-06-20 10:27:38'),
(191, 'KH00175', 'A.Hưng+c Trang', NULL, 'hungtrang@gmail.com', NULL, NULL, NULL, NULL, '0949809406', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:28:35', '2022-06-20 10:28:35'),
(192, 'KH00176', 'Chị Thảo', NULL, 'thao123@gmail.com', NULL, '0', NULL, NULL, '0909000630', NULL, 3, 114, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:28:45', '2022-06-20 10:28:45'),
(193, 'KH00177', 'Anh Nam', NULL, 'Nam@gmail.com', NULL, NULL, NULL, NULL, '0976452426', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:29:18', '2022-06-20 10:29:18'),
(194, 'KH00178', 'Chị Thư', NULL, 'thu1234@gmail.com', NULL, '0', NULL, NULL, '0976645129', NULL, 3, 114, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:30:08', '2022-06-20 10:30:08'),
(195, 'KH00179', 'Chị Lê Thảo', NULL, 'lethao@gmail.com', NULL, '0', NULL, NULL, '0978709618', NULL, 3, 155, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:31:40', '2022-06-20 10:31:40'),
(196, 'KH00180', 'Chị Linh', NULL, 'linh@gmail.com', NULL, '0', NULL, NULL, '0948549668', NULL, 3, 155, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:32:35', '2022-06-20 10:32:35'),
(197, 'KH00181', 'Chị Vy', NULL, 'vy1234@gmail.com', NULL, '0', NULL, NULL, '0944451356', NULL, 3, 155, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:33:38', '2022-06-20 10:33:38'),
(198, 'KH00182', 'Chị Nương', NULL, 'nuong123@gmail.com', NULL, '0', NULL, NULL, '0938912918', NULL, 3, 155, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:36:00', '2022-06-20 10:36:00'),
(199, 'KH00183', 'Chị Trang', NULL, 'trang1234@gmail.com', NULL, '0', NULL, NULL, '0996978798', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:37:07', '2022-06-20 10:37:07'),
(200, 'KH00184', 'Phuong Ngoc', NULL, 'ngoc2@GMAIL.COM', NULL, NULL, 'Apartment', 'Rich', '0935501270', NULL, 3, 141, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:37:57', '2022-06-20 10:37:57'),
(201, 'KH00185', 'Chị Hồng', NULL, 'hong1234@gmail.com', NULL, '0', NULL, NULL, '0902469398', NULL, 3, 155, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:38:07', '2022-06-20 10:38:07'),
(202, 'KH00186', 'Chị Oanh', NULL, 'oanh1234@gmail.com', NULL, '0', NULL, NULL, '0934930700', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:38:45', '2022-06-20 10:38:45'),
(203, 'KH00187', 'Anh Tuấn', NULL, 'anhtuan@gmail.com', NULL, '0', NULL, NULL, '0984659644', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:39:28', '2022-06-20 10:39:28'),
(204, 'KH00188', 'Chị Tuyên', NULL, 'tuyen1234@gmail.com', NULL, '0', NULL, NULL, '0912982015', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:40:33', '2022-06-20 10:40:33'),
(205, 'KH00189', 'Chi Hanh', NULL, 'hanh@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0902550891', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:41:06', '2022-06-20 10:41:06'),
(206, 'KH00190', 'Chị Thiên Nhiên', NULL, 'thiennhien@gmail.com', NULL, '0', NULL, NULL, '0938278148', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:41:26', '2022-06-20 10:41:26'),
(207, 'KH00191', 'Chi Ngan', NULL, 'ngan@gmail.com', NULL, NULL, NULL, NULL, '0902144509', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:42:12', '2022-06-20 10:42:12'),
(208, 'KH00192', 'Chị Linh', NULL, 'Linh789@gmail.com', NULL, '0', NULL, NULL, '0907508939', NULL, 3, 154, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:43:06', '2022-06-20 10:43:06'),
(209, 'KH00193', 'Chị Ánh', NULL, 'chianh897@gmail.com', NULL, '0', NULL, NULL, '0986677123', NULL, 3, 154, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:44:17', '2022-06-20 10:44:17'),
(210, 'KH00194', 'Chi Ngan', NULL, 'nganc@gmail.com', NULL, NULL, 'Apartment', NULL, '0931793693', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:44:32', '2022-06-20 10:44:32'),
(211, 'KH00195', 'Anh Thành', NULL, 'anhthanh1234@gmail.com', NULL, '0', NULL, NULL, '0367001655', NULL, 3, 154, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:45:43', '2022-06-20 10:45:43'),
(212, 'KH00196', 'Chi Tuyen', NULL, 'TUYENM@GMAIL.COM', NULL, NULL, 'Apartment', NULL, '0933719669', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:46:08', '2022-06-20 10:46:08'),
(213, 'KH00197', 'Cô Thắm', NULL, 'tham123@gmail.com', NULL, '0', NULL, NULL, '0384785860', NULL, 3, 154, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:46:45', '2022-06-20 10:46:45'),
(214, 'KH00198', 'Chi Trang', NULL, 'trang@gmail.com', NULL, NULL, NULL, NULL, '0937007330', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:47:10', '2022-06-20 10:47:10'),
(215, 'KH00199', 'Chị Hoa', NULL, 'chihoa123@gmail.com', NULL, '0', NULL, NULL, '0935146384', NULL, 3, 154, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:47:45', '2022-06-20 10:47:45'),
(216, 'KH00200', 'Chi Ly', NULL, 'ly@gmail.com', NULL, NULL, NULL, NULL, '0908041847', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:48:08', '2022-06-20 10:48:08'),
(217, 'KH00201', 'Chi Nhu', NULL, 'nhu@gmail.com', NULL, NULL, NULL, NULL, '0937436256', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:49:41', '2022-06-20 10:49:41'),
(218, 'KH00202', 'Anh Hoang', NULL, 'anhhoang@gmail.com', NULL, NULL, NULL, NULL, '0986818285', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:51:50', '2022-06-20 10:51:50'),
(219, 'KH00203', 'Chi Thao', NULL, 'thao@gmail.com', NULL, NULL, NULL, NULL, '0907998172', NULL, 3, 205, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-20 10:54:09', '2022-06-20 10:54:09'),
(220, 'KH00204', 'lê kim hiển', NULL, 'hien.lekim@gmail.com', NULL, NULL, 'Villa', NULL, '0888035604', NULL, 3, 18, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-06-22 23:34:21', '2022-07-03 01:57:34'),
(221, 'KH00205', 'lê kim hiển', NULL, 'hien.lekim@gmail.com', NULL, NULL, 'Villa', NULL, '0888035604', NULL, 3, 18, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-06-22 23:35:39', '2022-07-03 01:57:34'),
(222, 'KH00206', 'hien hoa hau', NULL, 'dddd@gmail.com', NULL, 'ssssss', 'Villa', 'Medium', '0888035604', NULL, 3, 221, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-06-22 23:40:51', '2022-07-03 01:57:34'),
(223, 'KH00207', 'anh Thy', NULL, 'abc@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0933123468', NULL, 3, NULL, 122, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:18:00', '2022-06-23 13:18:00'),
(224, 'KH00208', 'Yến', 1, 'Yen123@gmail.com', NULL, NULL, NULL, NULL, '0918504448', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2022-06-23 13:19:35', '2022-06-23 13:32:27'),
(225, 'KH00209', 'Vân', 1, 'Van123@gmail.com', NULL, NULL, NULL, NULL, '0903159696', NULL, 3, 224, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:21:35', '2022-06-23 13:30:37'),
(226, 'KH00210', 'Chị Vân', NULL, 'Van123@gmail.com', NULL, NULL, NULL, NULL, '0903159696', NULL, 3, 224, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:24:00', '2022-06-23 13:24:00'),
(227, 'KH00211', 'Ninh', NULL, 'abc@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0909095093', NULL, 3, 223, 122, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:24:25', '2022-06-23 13:24:25'),
(228, 'KH00212', 'Chị Hiền', NULL, 'Hien123@gmail.com', NULL, NULL, NULL, NULL, '0908991068', NULL, 3, 224, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:34:47', '2022-06-23 13:34:47'),
(229, 'KH00213', 'Ba chị Yến', NULL, 'Bayen123@gmail.com', NULL, NULL, NULL, NULL, '0935343101', NULL, 3, 224, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:35:51', '2022-06-23 13:35:51'),
(230, 'KH00214', 'Chị Hà', NULL, 'Ha123@gmail.com', NULL, NULL, NULL, NULL, '0903399567', NULL, 3, 224, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:36:35', '2022-06-23 13:36:35'),
(231, 'KH00215', 'Chị Liên', NULL, 'Lien123@gmail.com', NULL, NULL, NULL, NULL, '0906738886', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:38:21', '2022-06-23 13:38:21'),
(232, 'KH00216', 'Chị Trâm', NULL, 'Tram123@gmail.com', NULL, NULL, NULL, NULL, '0907594385', NULL, 3, 231, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:39:22', '2022-06-23 13:39:22'),
(233, 'KH00217', 'Chị Hạnh', NULL, 'Hanh123@gmail.com', NULL, NULL, NULL, NULL, '0383511935', NULL, 3, 231, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:40:19', '2022-06-23 13:40:19'),
(234, 'KH00218', 'Anh Bình', NULL, 'Binh123@gmail.com', NULL, NULL, NULL, NULL, '0913838645', NULL, 3, 231, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:41:21', '2022-06-23 13:41:21'),
(235, 'KH00219', 'Cô Ôn', NULL, 'On123@gmail.com', NULL, NULL, NULL, NULL, '0905167763', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:43:06', '2022-06-23 13:43:06'),
(236, 'KH00220', 'Chị Tuyên', NULL, 'Tuyen123@gmail.com', NULL, NULL, NULL, NULL, '0912982015', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:44:25', '2022-06-23 13:44:25'),
(237, 'KH00221', 'Chị My', NULL, 'My123@gmail.com', NULL, NULL, NULL, NULL, '0978037385', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:45:38', '2022-06-23 13:45:38'),
(238, 'KH00222', 'Cô Ngo', NULL, 'Ngo123@gmail.com', NULL, NULL, NULL, NULL, '0973726504', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:47:26', '2022-06-23 13:47:26'),
(239, 'KH00223', 'Cô Lụa', NULL, 'Lua123@gmail.com', NULL, NULL, NULL, NULL, '0983732374', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:48:16', '2022-06-23 13:48:16'),
(240, 'KH00224', 'Chị Châu', NULL, 'Chau123@gmail.com', NULL, NULL, NULL, NULL, '0359180894', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:49:45', '2022-06-23 13:49:45'),
(241, 'KH00225', 'Cô Ánh', NULL, 'Anh123@gmail.com', NULL, NULL, NULL, NULL, '0767659159', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:50:52', '2022-06-23 13:50:52'),
(242, 'KH00226', 'Cô Phúc', NULL, 'Phuc123@gmail.com', NULL, NULL, NULL, NULL, '0962256754', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:51:55', '2022-06-23 13:51:55'),
(243, 'KH00227', 'Cô Dung', NULL, 'Dung123@gmail.com', NULL, NULL, NULL, NULL, '0379252236', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:53:11', '2022-06-23 13:53:11'),
(244, 'KH00228', 'Cô Hậu', NULL, 'Hau123@gmail.com', NULL, NULL, NULL, NULL, '0942085943', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:56:28', '2022-06-23 13:56:28'),
(245, 'KH00229', 'Cô Sen', NULL, 'Sen123@gmail.com', NULL, NULL, NULL, NULL, '0979449614', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 13:57:18', '2022-06-23 13:57:18'),
(246, 'KH00230', 'Chị Thủy', NULL, 'Thuy123@gamil.com', NULL, NULL, NULL, NULL, '0765699129', NULL, 3, 235, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:02:38', '2022-06-23 14:02:38'),
(247, 'KH00231', 'Anh Minh', NULL, 'Minh123@gmail.com', NULL, NULL, NULL, NULL, '0989936270', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:04:37', '2022-06-23 14:04:37'),
(248, 'KH00232', 'Anh Hiếu', NULL, 'Hieu123@gmail.com', NULL, NULL, NULL, NULL, '0909088018', NULL, 3, 247, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:05:36', '2022-06-23 14:05:36'),
(249, 'KH00233', 'Anh Phương', NULL, 'Phuong123@gmail.com', NULL, NULL, NULL, NULL, '0906302494', NULL, 3, 247, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:06:27', '2022-06-23 14:06:27'),
(250, 'KH00234', 'Chị Thủy', NULL, 'Thuy123@gmail.com', NULL, NULL, NULL, NULL, '0902909593', NULL, 3, 247, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:07:37', '2022-06-23 14:07:37'),
(251, 'KH00235', 'Chị Hoa', NULL, 'Hoa123@gmail.com', NULL, NULL, NULL, NULL, '0934857239', NULL, 3, 247, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:08:34', '2022-06-23 14:08:34'),
(252, 'KH00236', 'Anh Chí', NULL, 'Chi123@gmail.com', NULL, NULL, NULL, NULL, '0908541453', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:10:20', '2022-06-23 14:10:20'),
(253, 'KH00237', 'Chị Nga', NULL, 'Nga123@gmail.com', NULL, NULL, NULL, NULL, '0913803557', NULL, 3, 252, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:11:43', '2022-06-23 14:11:43'),
(254, 'KH00238', 'Anh Minh', NULL, 'Minh123@gmail.com', NULL, NULL, NULL, NULL, '0989936270', NULL, 3, 252, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:12:52', '2022-06-23 14:12:52'),
(255, 'KH00239', 'Anh Bằng', NULL, 'Bang123@gmail.com', NULL, NULL, NULL, NULL, '0909910334', NULL, 3, 252, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:14:01', '2022-06-23 14:14:01'),
(256, 'KH00240', 'Anh Thắng', NULL, 'Thang123@gmail.com', NULL, NULL, NULL, NULL, '0903759766', NULL, 3, 252, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:14:52', '2022-06-23 14:14:52'),
(257, 'KH00241', 'Anh Sơn', NULL, 'Son123@gmail.com', NULL, NULL, NULL, NULL, '0903275513', NULL, 3, 252, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:15:40', '2022-06-23 14:15:40'),
(258, 'KH00242', 'Anh Dũng', NULL, 'Dung123@gmail.com', NULL, NULL, NULL, NULL, '0902144888', NULL, 3, 252, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:16:31', '2022-06-23 14:16:31'),
(259, 'KH00243', 'Anh Thắng', NULL, 'Thang1234@gmail.com', NULL, NULL, NULL, NULL, '0903839966', NULL, 3, 252, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:17:18', '2022-06-23 14:17:18'),
(260, 'KH00244', 'Chị Quy', NULL, 'Quy123@gmail.com', NULL, NULL, NULL, NULL, '0906555739', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:18:35', '2022-06-23 14:18:35'),
(261, 'KH00245', 'Chị Ngọc', NULL, 'Ngoc123@gmail.com', NULL, NULL, NULL, NULL, '0945525782', NULL, 3, 260, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:19:29', '2022-06-23 14:19:29'),
(262, 'KH00246', 'Anh Long', NULL, 'Long123@gmail.com', NULL, NULL, NULL, NULL, '0915310140', NULL, 3, 260, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:20:14', '2022-06-23 14:20:14');
INSERT INTO `customers` (`id`, `customers_code`, `customers_name`, `customers_payment_status`, `customers_email`, `customers_address`, `car`, `house_type`, `level`, `customers_phone`, `customers_country_id`, `customers_brand_id`, `customers_parent_id`, `customers_staffs_id`, `customers_stock_id`, `customers_handover_status`, `customers_handover_date_start`, `customers_handover_date_approve`, `customers_user_created`, `customers_user_updated`, `customers_status`, `created_at`, `updated_at`) VALUES
(263, 'KH00247', 'Chị Nguyệt', NULL, 'Nguyet123@gmail.com', NULL, NULL, NULL, NULL, '0932652819', NULL, 3, 260, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:21:08', '2022-06-23 14:21:08'),
(264, 'KH00248', 'Anh Phong', NULL, 'Phong123@gmail.com', NULL, NULL, NULL, NULL, '0968583688', NULL, 3, 260, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:22:02', '2022-06-23 14:22:02'),
(265, 'KH00249', 'Chị Kim Anh', NULL, 'Anh123@gmail.com', NULL, NULL, NULL, NULL, '0902303490', NULL, 3, 260, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:22:56', '2022-06-23 14:22:56'),
(266, 'KH00250', 'Anh Tùng', NULL, 'Tung123@gmail.com', NULL, NULL, NULL, NULL, '0901306016', NULL, 3, 260, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:24:26', '2022-06-23 14:24:26'),
(267, 'KH00251', 'Chị Linh', NULL, 'Linh123@gmail.com', NULL, NULL, NULL, NULL, '0903699450', NULL, 3, 260, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-23 14:25:23', '2022-06-23 14:25:23'),
(268, 'KH00252', 'trần như ánh', NULL, 'hien@gnmail.com', NULL, 'dđ', 'Villa', 'Rich', '0888035604', NULL, 3, 220, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-06-23 22:29:11', '2022-07-03 01:57:34'),
(269, 'KH00253', 'ddddddddddddaaaaaaaaaaaaaaa', NULL, 'asaa@gmail.com', NULL, NULL, 'Flat', 'Rich', '0888035604', NULL, 3, NULL, 95, NULL, 2, '2022-07-02', '2022-07-02', 'sales02', NULL, 0, '2022-06-26 13:13:54', '2022-07-03 01:57:34'),
(270, 'KH00254', 'Chị Nga', NULL, 'Fg@gmail.com', NULL, NULL, NULL, NULL, '0368919780', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:04:08', '2022-06-27 09:04:08'),
(271, 'KH00255', 'Chị Hồng', NULL, 'nhi@gmail.com', NULL, NULL, NULL, NULL, '0902951395', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:07:10', '2022-06-27 09:07:10'),
(272, 'KH00256', 'Chị Nguyệt', NULL, 'nhi@gmail.com', NULL, NULL, NULL, 'Rich', '0902381371', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:08:32', '2022-06-27 09:08:32'),
(273, 'KH00257', 'Chị Nữ', NULL, 'Nu@gmail.com', NULL, NULL, NULL, NULL, '0916178762', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:34:22', '2022-06-27 09:34:22'),
(274, 'KH00258', 'Chị Phúc', NULL, 'Phuc@gmail.com', NULL, NULL, NULL, NULL, '0938614456', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:35:07', '2022-06-27 09:35:07'),
(275, 'KH00259', 'Chị Quỳnh', NULL, 'Quynh@12gmail.com', NULL, NULL, NULL, NULL, '0901209440', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:38:23', '2022-06-27 09:38:23'),
(276, 'KH00260', 'Dì 6', NULL, 'Di6@gmail.com', NULL, NULL, NULL, NULL, '0782981282', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:39:15', '2022-06-27 09:39:15'),
(277, 'KH00261', 'Chị Quệ', NULL, 'Que@gmail.com', NULL, NULL, NULL, NULL, '0943976568', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:41:28', '2022-06-27 09:41:28'),
(278, 'KH00262', 'Cô Xuân', NULL, 'Xuan@gmail.com', NULL, NULL, NULL, NULL, '0902420924', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 09:42:33', '2022-06-27 09:42:33'),
(279, 'KH00263', 'Long', NULL, 'abc@gmail.com', NULL, NULL, 'Small house', 'Medium', '0903157918', NULL, 3, NULL, 122, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:10:35', '2022-06-27 10:10:35'),
(280, 'KH00264', 'Thai', NULL, 'abc@gmail.com', NULL, NULL, 'Flat', 'Medium', '0908227627', NULL, 3, NULL, 122, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:12:50', '2022-06-27 10:12:50'),
(281, 'KH00265', 'Tuan', NULL, 'abc@gmail.com', NULL, NULL, 'Flat', 'Medium', '0903944770', NULL, 3, 280, 122, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:13:59', '2022-06-27 10:13:59'),
(282, 'KH00266', 'Hung', NULL, 'abc@gmail.com', NULL, NULL, 'Flat', 'Medium', '0939582779', NULL, 3, 280, 122, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:15:01', '2022-06-27 10:15:01'),
(283, 'KH00267', 'anh Tam', NULL, 'tam123@gmail.com', NULL, 'yes', 'Villa', 'Rich', '0983976221', NULL, 3, 159, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:15:31', '2022-06-27 10:15:31'),
(284, 'KH00268', 'chi Vy', NULL, 'vy123@gmail.com', NULL, 'yes', 'Villa', 'Rich', '0986256626', NULL, 3, 159, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:16:35', '2022-06-27 10:16:35'),
(285, 'KH00269', 'chi Thao', NULL, 'thao123456@gmail.com', NULL, 'yes', 'Villa', 'Rich', '0983976253', NULL, 3, 159, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:17:56', '2022-06-27 10:17:56'),
(286, 'KH00270', 'co Hong', NULL, 'hong123456@gmail.com', NULL, 'no', 'Apartment', 'Rich', '0918385097', NULL, 3, NULL, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:19:59', '2022-06-27 10:19:59'),
(287, 'KH00271', 'co xa', NULL, 'xa123456@gmail.com', NULL, 'no', 'Apartment', 'Rich', '0986781167', NULL, 3, NULL, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:22:08', '2022-06-27 10:22:08'),
(288, 'KH00272', 'co Bach Long', NULL, 'bachlong123@gmail.com', NULL, 'no', 'Flat', 'Rich', '0908126123', NULL, 3, NULL, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:23:22', '2022-06-27 10:23:22'),
(289, 'KH00273', 'co Doan- chu Bien', NULL, 'doanbien123@gmail.com', NULL, 'no', 'Apartment', 'Rich', '0384402300', NULL, 3, NULL, 121, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:24:57', '2022-06-27 10:24:57'),
(290, 'KH00274', 'Chị Phương', NULL, 'phuong@gmail.com', NULL, '0', 'Apartment', 'Medium', '0972541581', NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:28:40', '2022-06-27 10:28:40'),
(291, 'KH00275', 'Chị Diệu Linh', NULL, 'Linh@gmail.com', NULL, '1', 'Apartment', 'Rich', NULL, NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:32:22', '2022-06-27 10:32:22'),
(292, 'KH00276', 'Chị Ngân', NULL, 'Ngan@gmail.com', NULL, '0', 'Small house', 'Medium', '0909656065', NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:33:48', '2022-06-27 10:33:48'),
(293, 'KH00277', 'Chị Ly', NULL, 'Ly@gmail.com', NULL, 'Xe máy', 'Apartment', 'Medium', '0908041847', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:56:42', '2022-06-27 10:56:42'),
(294, 'KH00278', 'Chị Uyên', NULL, 'Uyen@gmail.com', NULL, 'Xe máy', 'Apartment', 'Medium', '0979731040', NULL, 3, 293, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:58:33', '2022-06-27 10:58:33'),
(295, 'KH00279', 'Chị Hằng', NULL, 'Hang@gmail.com', NULL, 'Xe máy', 'Apartment', 'Medium', '0938249444', NULL, 3, 293, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 10:59:39', '2022-06-27 10:59:39'),
(296, 'KH00280', 'A Triều', NULL, 'Trieu@gmail.com', NULL, 'Xe hơi', 'Apartment', 'Medium', '0908365165', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:01:49', '2022-06-27 11:01:49'),
(297, 'KH00281', 'Anh Thuật', NULL, 'Thuat@gmail.com', NULL, 'Xe máy', 'Apartment', 'Medium', '0989328388', NULL, 3, 296, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:03:05', '2022-06-27 11:03:05'),
(298, 'KH00282', 'Anh Hùng', NULL, 'Hung@gmail.com', NULL, 'Xe máy', 'Apartment', 'Medium', '0919181914', NULL, 3, 296, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:04:18', '2022-06-27 11:04:18'),
(299, 'KH00283', 'Ms Trúc', NULL, '1234@gmail.com', NULL, NULL, 'Small house', 'Medium', '0707669510', NULL, 3, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:10:17', '2022-06-27 11:10:17'),
(300, 'KH00284', 'Ms.Phượng (Thu)', NULL, '1234@gmail.com', NULL, 'No', 'Small house', 'Medium', '0906097833', NULL, 3, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:12:24', '2022-06-27 11:12:24'),
(301, 'KH00285', 'Ms.Hồng', NULL, '1234@gmai.com', NULL, NULL, 'Small house', 'Rich', '0988324570', NULL, 3, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:14:17', '2022-06-27 11:14:17'),
(302, 'KH00286', 'Ms Loan ( Hằng)', NULL, '1234@gmail.com', NULL, NULL, 'Small house', 'Poor', '0903155245', NULL, 3, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:15:45', '2022-06-27 11:15:45'),
(303, 'KH00287', 'Ms Trang', NULL, '1234@gmail.com', NULL, NULL, 'Small house', 'Medium', '0902325476', NULL, 3, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:17:32', '2022-06-27 11:17:32'),
(304, 'KH00288', 'Ms Thúy', NULL, '1234@gmail.com', NULL, NULL, 'Small house', 'Medium', '0939068499', NULL, 3, NULL, 124, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 11:19:42', '2022-06-27 11:19:42'),
(305, 'KH00289', 'Chị Nhung', NULL, 'Nhung@gmail.com', NULL, '0', 'Apartment', 'Medium', '0979056515', NULL, 3, 290, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 17:39:08', '2022-06-27 17:39:08'),
(306, 'KH00290', 'Cô Trắng', NULL, 'Trang@gmail.com', NULL, '0', 'Small house', 'Medium', '0908311370', NULL, 3, 292, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 17:40:42', '2022-06-27 17:40:42'),
(307, 'KH00291', 'Chị Linh', NULL, 'Linh@gmail.com', NULL, '0', NULL, NULL, NULL, NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 17:43:06', '2022-06-27 17:43:06'),
(308, 'KH00292', 'Cô Hiền', NULL, 'Hien@gmail.com', NULL, '0', 'Flat', 'Medium', '0905585462', NULL, 3, 307, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 17:44:39', '2022-06-27 17:44:39'),
(309, 'KH00293', 'Chị Thuận', NULL, 'Thuan@gmail.com', NULL, '1', 'Flat', 'Rich', '0933011087', NULL, 3, NULL, 118, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-27 17:46:42', '2022-06-27 17:46:42'),
(310, 'KH00294', 'Chị Vân', NULL, 'van12345@gmail.com', NULL, '2', 'Villa', 'Rich', '0989193999', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-28 10:07:35', '2022-06-28 10:07:35'),
(311, 'KH00295', 'Chị Ngọc', NULL, 'Ngoc1234@gmail.com', NULL, '1', 'Flat', 'Rich', '0937075674', NULL, 3, 155, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-29 17:37:06', '2022-06-29 17:37:06'),
(312, 'KH00296', 'Chị Lai', NULL, 'lai1234@gmail.com', NULL, '1', 'Flat', 'Rich', '0355203245', NULL, 3, 155, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-29 17:38:14', '2022-06-29 17:38:14'),
(313, 'KH00297', 'Chị Lan Hương', NULL, 'Huong@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0903606083', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 09:22:01', '2022-06-30 09:22:01'),
(314, 'KH00298', 'Chị Bích', NULL, 'Bich@gmail.com', NULL, NULL, NULL, NULL, '0903691404', NULL, 3, 313, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 09:23:03', '2022-06-30 09:23:03'),
(315, 'KH00299', 'Chị Diễm', NULL, 'Diem@gmail.com', NULL, NULL, NULL, NULL, '0903923786', NULL, 3, 313, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 09:24:08', '2022-06-30 09:24:08'),
(316, 'KH00300', 'Chị Mỹ', NULL, 'My@gmail.com', NULL, NULL, NULL, NULL, '0903923786', NULL, 3, 313, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 09:24:56', '2022-06-30 09:24:56'),
(317, 'KH00301', 'Chị Quỳnh', NULL, 'Quynh@gmail.com', NULL, NULL, NULL, NULL, '0918479474', NULL, 3, 313, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 09:25:44', '2022-06-30 09:25:44'),
(318, 'KH00302', 'Chị Trang', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0935135439', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:11:16', '2022-06-30 14:11:16'),
(319, 'KH00303', 'Chị Hạnh', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0907767358', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:11:54', '2022-06-30 14:11:54'),
(320, 'KH00304', 'Chị Thảo', NULL, 'Thao@gmail.com', NULL, 'Xe máy', 'Apartment', 'Medium', '0947261187', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:12:25', '2022-06-30 14:12:25'),
(321, 'KH00305', 'Anh Hải', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0903649202', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:13:21', '2022-06-30 14:13:21'),
(322, 'KH00306', 'Chị Ánh', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0903649202', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:13:54', '2022-06-30 14:13:54'),
(323, 'KH00307', 'Anh Bân', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0903134881', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:14:24', '2022-06-30 14:14:24'),
(324, 'KH00308', 'Bà Hà', NULL, 'Ha@gmail.com', NULL, 'K biết', 'Apartment', 'Medium', '0786758478', NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:14:32', '2022-06-30 14:14:32'),
(325, 'KH00309', 'Chủ Rau Câu', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0909138899', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:15:04', '2022-06-30 14:15:04'),
(326, 'KH00310', 'Cô Kim', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0937672359', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:15:37', '2022-06-30 14:15:37'),
(327, 'KH00311', 'Chị Thuỷ', NULL, 'Thuy@gmail.com', NULL, 'Không biết', 'Flat', 'Medium', '0909931355', NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:15:47', '2022-06-30 14:15:47'),
(328, 'KH00312', 'Anh Tỉnh', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0938034661', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:16:36', '2022-06-30 14:16:36'),
(329, 'KH00313', 'Anh Sơn', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0902838185', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:17:10', '2022-06-30 14:17:10'),
(330, 'KH00314', 'Chị Linh', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0829977379', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:17:42', '2022-06-30 14:17:42'),
(331, 'KH00315', 'Chị Thuỷ', NULL, 'Thuy2@gmail.com', NULL, 'K biết', 'Flat', 'Medium', '0937740474', NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:17:55', '2022-06-30 14:17:55'),
(332, 'KH00316', 'Chị Thành', NULL, 'Trang@gmail.com', NULL, NULL, NULL, NULL, '0986128638', NULL, 3, 318, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:18:21', '2022-06-30 14:18:21'),
(333, 'KH00317', 'Chị Tiên', NULL, 'Tien@gmail.com', NULL, 'K biết', 'Flat', 'Medium', '0905982558', NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:18:54', '2022-06-30 14:18:54'),
(334, 'KH00318', 'Bà Chin', NULL, 'Chi@gmail.com', NULL, 'K biết', 'Apartment', NULL, NULL, NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:20:19', '2022-06-30 14:20:19'),
(335, 'KH00319', 'Chị Vi', NULL, 'Vi@gmail.com', NULL, NULL, NULL, NULL, '0988610334', NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:22:03', '2022-06-30 14:22:03'),
(336, 'KH00320', 'Chị Hằng', NULL, 'Hang@gmail.com', NULL, NULL, NULL, NULL, '0938200878', NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:23:03', '2022-06-30 14:23:03'),
(337, 'KH00321', 'Chị Thu', NULL, 'Thu@gmail.com', NULL, NULL, NULL, NULL, '0902781456', NULL, 3, 320, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 14:23:49', '2022-06-30 14:23:49'),
(338, 'KH00322', 'C.Ngân', NULL, 'Ngan@gmail.com', NULL, '1', 'Apartment', 'Medium', '0902144509', NULL, 3, NULL, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:03:53', '2022-06-30 15:03:53'),
(339, 'KH00323', 'C.Loan', NULL, 'Loan@gmail.com', NULL, '1', 'Apartment', 'Medium', '0938801039', NULL, 3, 338, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:05:41', '2022-06-30 15:05:41'),
(340, 'KH00324', 'C.Quỳnh Anh', NULL, 'Oanh@gmail.com', NULL, '1', 'Apartment', 'Medium', '0938174597', NULL, 3, NULL, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:13:28', '2022-06-30 15:13:28'),
(341, 'KH00325', 'C.Nương', NULL, 'Nuong@gmail.com', NULL, '1', 'Villa', 'Rich', '0968840719', NULL, 3, NULL, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:18:44', '2022-06-30 15:18:44'),
(342, 'KH00326', 'Cô Lanh', NULL, 'Lanh@gmail.com', NULL, '1', 'Flat', 'Rich', '0332525379', NULL, 3, NULL, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:25:03', '2022-06-30 15:25:03'),
(343, 'KH00327', 'C.Tuyến', NULL, 'Tuyen@gmail.com', NULL, '1', 'Apartment', 'Rich', '0962962676', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:30:39', '2022-06-30 15:30:39'),
(344, 'KH00328', 'Chị Tuyền', NULL, 'Tuyen@gmail.com', NULL, '1', 'Apartment', 'Rich', '0909390118', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:32:49', '2022-06-30 15:32:49'),
(345, 'KH00329', 'A Chương', NULL, 'Chuong@gmail.com', NULL, '1', 'Apartment', 'Rich', '0918795566', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:34:19', '2022-06-30 15:34:19'),
(346, 'KH00330', 'C.Trang', NULL, 'Trang@gmail.com', NULL, '1', 'Apartment', 'Rich', '0935135439', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:39:11', '2022-06-30 15:39:11'),
(347, 'KH00331', 'C.Châu', NULL, 'Chau@gmail.com', NULL, '1', 'Apartment', 'Medium', '0903777541', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:40:39', '2022-06-30 15:40:39'),
(348, 'KH00332', 'C.Tuyết', NULL, 'Tuyet@mail.com', NULL, '1', 'Apartment', 'Rich', '0948239759', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:42:28', '2022-06-30 15:42:28'),
(349, 'KH00333', 'C.Hạnh', NULL, 'Hanh@gmail.com', NULL, '1', 'Apartment', 'Rich', '0896464874', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:44:07', '2022-06-30 15:44:07'),
(350, 'KH00334', 'C.hạnh Lưu', NULL, 'Luu@gmail.com', NULL, '1', 'Apartment', 'Rich', '0907767358', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:46:03', '2022-06-30 15:46:03'),
(351, 'KH00335', 'C.Hạnh Huế', NULL, 'Hue@gmail.com', NULL, '1', 'Apartment', 'Rich', '0931430384', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 15:54:32', '2022-06-30 15:54:32'),
(352, 'KH00336', 'A.Phương', NULL, 'Phuong@gmail.com', NULL, '1', 'Apartment', 'Rich', '097171926', NULL, 3, 342, 114, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 16:01:13', '2022-06-30 16:01:13'),
(353, 'KH00337', 'Chị Lài+Anh Ninh', NULL, 'LaiNinh@gmail.com', NULL, '1', 'Apartment', 'Medium', '0906305884', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:29:22', '2022-06-30 18:29:22'),
(354, 'KH00338', 'Anh Đăng', NULL, 'Dang@gmail.com', NULL, NULL, NULL, NULL, '0902299365', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:35:33', '2022-06-30 18:35:33'),
(355, 'KH00339', 'Chị Thảo', NULL, 'Thao@gmail.com', NULL, NULL, NULL, NULL, '0916000963', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:38:37', '2022-06-30 18:38:37'),
(356, 'KH00340', 'Tú', NULL, 'Tu@gmail.com', NULL, NULL, NULL, NULL, '0937970971', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:41:12', '2022-06-30 18:41:12'),
(357, 'KH00341', 'Anh Thìn', NULL, 'Thin@gmail.com', NULL, NULL, NULL, NULL, '0937305999', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:42:46', '2022-06-30 18:42:46'),
(358, 'KH00342', 'Anh Hùng', NULL, 'Hung@gmail.com', NULL, NULL, NULL, NULL, '0938129982', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:45:02', '2022-06-30 18:45:02'),
(359, 'KH00343', 'Anh Trúc', NULL, 'Truc@gmail.com', NULL, NULL, NULL, NULL, '0909286875', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:45:50', '2022-06-30 18:45:50'),
(360, 'KH00344', 'Anh Thới, C. Hải', NULL, 'Thoi@gmail.com', NULL, NULL, NULL, NULL, '0908421852', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:46:37', '2022-06-30 18:46:37'),
(361, 'KH00345', 'Anh Nhá', NULL, 'Nha@gmail.com', NULL, NULL, NULL, NULL, '0903624120', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:48:19', '2022-06-30 18:48:19'),
(362, 'KH00346', 'Cô Hoàng', NULL, 'Hoang@gmail.com', NULL, NULL, NULL, NULL, '0983454259', NULL, 3, 353, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 18:58:02', '2022-06-30 18:58:02'),
(363, 'KH00347', 'Chị Hiếu', NULL, 'Hieu123@gmail.com', NULL, NULL, NULL, NULL, '0906208365', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:17:11', '2022-06-30 21:17:11'),
(364, 'KH00348', 'Minh', NULL, '1@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0785812309', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:17:29', '2022-06-30 21:17:29'),
(365, 'KH00349', 'Chị Chi', NULL, 'Chi123@gmail.com', NULL, NULL, NULL, NULL, '0908279977', NULL, 3, 363, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:18:00', '2022-06-30 21:18:00'),
(366, 'KH00350', 'Chị Hạnh', NULL, 'Hanh123@gmail.com', NULL, NULL, NULL, NULL, '0903929499', NULL, 3, 363, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:18:46', '2022-06-30 21:18:46'),
(367, 'KH00351', 'Thịnh', NULL, '2@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0908532189', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:19:03', '2022-06-30 21:19:03'),
(368, 'KH00352', 'Huyền', NULL, '3@gmail.com', NULL, NULL, 'Small house', 'Medium', '0939101997', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:20:27', '2022-06-30 21:20:27'),
(369, 'KH00353', 'Chị Hoa', NULL, 'Hoa123@gmail.com', NULL, NULL, NULL, NULL, '0935146384', NULL, 3, NULL, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:21:16', '2022-06-30 21:21:16'),
(370, 'KH00354', 'Cô Ngân', NULL, '4@gmail.com', NULL, NULL, 'Small house', 'Rich', '0909539449', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:22:47', '2022-06-30 21:22:47'),
(371, 'KH00355', 'Chị Dung', NULL, 'Dung123@gmail.com', NULL, NULL, NULL, NULL, '0938794477', NULL, 3, 369, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:23:02', '2022-06-30 21:23:02'),
(372, 'KH00356', 'Chị Thịnh', NULL, 'Thinh123@gmail.com', NULL, NULL, NULL, NULL, '0933095933', NULL, 3, 369, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:23:57', '2022-06-30 21:23:57'),
(373, 'KH00357', 'Chị Tuyền', NULL, '5@gmail.com', NULL, '1', 'Flat', 'Rich', '0933759079', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:24:18', '2022-06-30 21:24:18'),
(374, 'KH00358', 'Chị Hà', NULL, 'Ha123@gmail.com', NULL, NULL, NULL, NULL, '0906354820', NULL, 3, 369, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:25:10', '2022-06-30 21:25:10'),
(375, 'KH00359', 'Chị Linh', NULL, '6@gmail.com', NULL, '1', 'Small house', 'Rich', '0906783558', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:25:45', '2022-06-30 21:25:45'),
(376, 'KH00360', 'Chị Trang', NULL, 'Trang123@gmail.com', NULL, NULL, NULL, NULL, '0939327599', NULL, 3, 369, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:26:07', '2022-06-30 21:26:07'),
(377, 'KH00361', 'Chú Bình', NULL, '7@gmail.com', NULL, '0', 'Small house', 'Rich', '0906841763', NULL, 3, NULL, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:26:59', '2022-06-30 21:26:59'),
(378, 'KH00362', 'Cô Vy', NULL, 'Vy123@gmail.com', NULL, NULL, NULL, NULL, '0914721721', NULL, 3, 369, 123, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:27:30', '2022-06-30 21:27:30'),
(379, 'KH00363', 'A.Phong', NULL, '1@gmail.com', NULL, NULL, NULL, NULL, '0395162519', NULL, 3, 364, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:29:13', '2022-06-30 21:29:13'),
(380, 'KH00364', 'A.Bình', NULL, '1@gmail.com', NULL, NULL, NULL, NULL, '0769682049', NULL, 3, 364, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:30:14', '2022-06-30 21:30:14'),
(381, 'KH00365', 'C.Hiếu', NULL, '1@gmail.com', NULL, NULL, NULL, NULL, '0911521798', NULL, 3, 364, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:31:07', '2022-06-30 21:31:07'),
(382, 'KH00366', 'Hiệp Hình sự Q.8', NULL, '1@gmail.com', NULL, NULL, NULL, NULL, '0938511226', NULL, 3, 364, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:31:46', '2022-06-30 21:31:46'),
(383, 'KH00367', 'C Thanh', NULL, '2@gmail.com', NULL, NULL, NULL, NULL, '0899320998', NULL, 3, 367, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:33:33', '2022-06-30 21:33:33'),
(384, 'KH00368', 'C.Thi', NULL, '2@gmail.com', NULL, NULL, NULL, NULL, '0922080856', NULL, 3, 367, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:34:09', '2022-06-30 21:34:09'),
(385, 'KH00369', 'C.Jodie', NULL, '2@gmail.com', NULL, NULL, NULL, NULL, '09869197393', NULL, 3, 367, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:34:53', '2022-06-30 21:34:53'),
(386, 'KH00370', 'C. Ashlee', NULL, '2@gmail.com', NULL, NULL, NULL, NULL, '0987198369', NULL, 3, 367, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:36:13', '2022-06-30 21:36:13'),
(387, 'KH00371', 'A.Thành', NULL, '2@gmail.com', NULL, NULL, NULL, NULL, '0933402576', NULL, 3, 367, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:36:52', '2022-06-30 21:36:52'),
(388, 'KH00372', 'A.Hải', NULL, '2@gmail.com', NULL, NULL, NULL, NULL, '0903949067', NULL, 3, 367, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:37:18', '2022-06-30 21:37:18'),
(389, 'KH00373', 'Dì Phương', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0932011699', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:40:25', '2022-06-30 21:40:25'),
(390, 'KH00374', 'Bác Huyền', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0933250569', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:41:03', '2022-06-30 21:41:03'),
(391, 'KH00375', 'A.Duy', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0908301705', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:41:52', '2022-06-30 21:41:52'),
(392, 'KH00376', 'A.Thịnh', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0937889972', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:42:22', '2022-06-30 21:42:22'),
(393, 'KH00377', 'C.Tiên', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0909057107', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:43:31', '2022-06-30 21:43:31'),
(394, 'KH00378', 'C.Thảo Linh', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0902772862', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:44:12', '2022-06-30 21:44:12'),
(395, 'KH00379', 'C.Ngân', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0342225333', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:45:00', '2022-06-30 21:45:00'),
(396, 'KH00380', 'C.Phương Anh', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0934668393', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:45:39', '2022-06-30 21:45:39'),
(397, 'KH00381', 'C.Dung', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0909262337', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:46:11', '2022-06-30 21:46:11'),
(398, 'KH00382', 'C.Lê Thu', NULL, '3@gmail.com', NULL, NULL, NULL, NULL, '0345783760', NULL, 3, 368, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:46:46', '2022-06-30 21:46:46'),
(399, 'KH00383', 'Chú Lượng', NULL, '4@gmail.com', NULL, NULL, NULL, NULL, '09122592679', NULL, 3, 370, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:47:30', '2022-06-30 21:47:30'),
(400, 'KH00384', 'Chú Hùng', NULL, '4@gmail.com', NULL, NULL, NULL, NULL, '0917275063', NULL, 3, 370, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:48:39', '2022-06-30 21:48:39'),
(401, 'KH00385', 'Chú Bảy', NULL, '4@gmail.com', NULL, NULL, NULL, NULL, '0903942085', NULL, 3, 370, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:49:07', '2022-06-30 21:49:07'),
(402, 'KH00386', 'Chú Tuấn', NULL, '4@gmail.com', NULL, NULL, NULL, NULL, '0913613274', NULL, 3, 370, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:49:35', '2022-06-30 21:49:35'),
(403, 'KH00387', 'Cô Kiêm (A Tư)', NULL, '4@gmail.com', NULL, NULL, NULL, NULL, '0919590010', NULL, 3, 370, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:50:11', '2022-06-30 21:50:11'),
(404, 'KH00388', 'Cô Lan', NULL, '4@gmail.com', NULL, NULL, NULL, NULL, '0937828823', NULL, 3, 370, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:51:06', '2022-06-30 21:51:06'),
(405, 'KH00389', 'C.Hương', NULL, '5@gmail.com', NULL, NULL, NULL, NULL, '0983415256', NULL, 3, 373, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:52:28', '2022-06-30 21:52:28'),
(406, 'KH00390', 'C.Ngọc', NULL, '5@gmail.com', NULL, NULL, NULL, NULL, '0903865220', NULL, 3, 373, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:53:11', '2022-06-30 21:53:11'),
(407, 'KH00391', 'A.Khánh', NULL, '5@gmail.com', NULL, NULL, NULL, NULL, '0918643406', NULL, 3, 373, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:53:43', '2022-06-30 21:53:43'),
(408, 'KH00392', 'C.Lan', NULL, '5@gmail.com', NULL, NULL, NULL, NULL, '0987371907', NULL, 3, 373, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:55:04', '2022-06-30 21:55:04'),
(409, 'KH00393', 'C.Bảo', NULL, '5@gmail.com', NULL, NULL, NULL, NULL, '0986073973', NULL, 3, 373, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:55:36', '2022-06-30 21:55:36'),
(410, 'KH00394', 'A.Nhân', NULL, '6@gmail.com', NULL, NULL, NULL, NULL, '0937995980', NULL, 3, 375, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:57:40', '2022-06-30 21:57:40'),
(411, 'KH00395', 'Chú Bình hàng xóm', NULL, '7@gmail.com', NULL, NULL, NULL, NULL, '0909902135', NULL, 3, 377, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:58:54', '2022-06-30 21:58:54'),
(412, 'KH00396', 'Chú Yến', NULL, '7@gmail.com', NULL, NULL, NULL, NULL, '0986687579', NULL, 3, 377, 119, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-06-30 21:59:54', '2022-06-30 21:59:54'),
(413, 'KH00397', 'Cô Xuân', NULL, 'Xuan@gmail.com', NULL, NULL, NULL, NULL, '0902420924', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-04 14:53:02', '2022-07-04 14:53:02'),
(414, 'KH00398', 'Cô Hà', NULL, 'Ha@gmail.com', NULL, 'Xe máy', 'Apartment', 'Medium', '0786758478', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-08 14:22:58', '2022-07-08 14:22:58'),
(415, 'KH00399', 'Chị Quyên', NULL, 'Quyen@gmail.com', NULL, 'Xe hơi', 'Apartment', 'Rich', '093265144', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-08 14:33:34', '2022-07-08 14:33:34'),
(416, 'KH00400', 'Chị Chi', NULL, 'Chi@gmail.com', NULL, 'Xe máy', 'Flat', 'Medium', '09833993438', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-11 13:49:22', '2022-07-11 13:49:22'),
(417, 'KH00401', 'Chị Hân', NULL, 'Han@gmail.com', NULL, 'Xe hơi', 'Apartment', 'Medium', '0938734711', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-11 13:51:12', '2022-07-11 13:51:12'),
(418, 'KH00402', 'Chị Thủy', NULL, 'Thuy@gmail.com', NULL, NULL, 'Small house', 'Rich', '0909328499', NULL, 3, NULL, 115, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-14 10:43:36', '2022-07-14 10:43:36'),
(419, 'KH00403', 'Chị Phương', NULL, 'phuongjamona@gmail.com', NULL, '0', 'Apartment', 'Medium', '0983540666', NULL, 3, NULL, 116, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-18 09:16:36', '2022-07-18 09:16:36'),
(420, 'KH00404', 'Anh Lê', NULL, 'Le@gmail.com', NULL, NULL, NULL, 'Rich', '0909347744', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-18 15:36:31', '2022-07-18 15:36:31'),
(421, 'KH00405', 'trần thanh mai', 2, 'mai@gmail.com', NULL, NULL, 'Villa', 'Rich', '08889035604', NULL, 3, NULL, 95, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-23 15:38:57', '2022-07-23 23:08:55'),
(422, 'KH00406', 'Cô mai', NULL, 'Abc@gmai.com', NULL, NULL, NULL, NULL, '0909494186', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-25 09:38:35', '2022-07-25 09:38:35'),
(423, 'KH00407', 'Chị Trinh', NULL, 'Abc@gmail.com', NULL, NULL, NULL, NULL, '0908845657', NULL, 3, NULL, 109, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-25 09:39:42', '2022-07-25 09:39:42'),
(424, 'KH00408', 'Chị Chi', NULL, 'Chi@gmail.com', NULL, NULL, 'Small house', 'Medium', '0907122733', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-25 14:56:54', '2022-07-25 14:56:54'),
(425, 'KH00409', 'Nguyễn Thuỷ Tiên', NULL, 'Thoa@gmail.com', NULL, 'Xe máy', 'Small house', 'Medium', '0908030755', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-27 09:47:47', '2022-07-27 09:47:47'),
(426, 'KH00410', 'Chị Hiếu', NULL, 'Hieu@gmail.com', NULL, NULL, NULL, NULL, '0909528895', NULL, 3, 425, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-27 09:53:37', '2022-07-27 09:53:37'),
(427, 'KH00411', 'Chị Thoa', NULL, 'Thoa1@gmail.com', NULL, 'Xe máy', 'Small house', 'Medium', '0908030755', NULL, 3, NULL, 120, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-27 09:56:37', '2022-07-27 09:56:37'),
(428, 'KH00412', 'Chi Diem', NULL, 'diem@gmail.com', NULL, NULL, NULL, NULL, '0932822568', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:49:39', '2022-07-28 16:49:39'),
(429, 'KH00413', 'Chi Hien', NULL, 'hien@gmail.com', NULL, NULL, 'Flat', 'Medium', '0772222074', NULL, 3, 428, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:51:19', '2022-07-28 16:51:19'),
(430, 'KH00414', 'Chi Nhi', NULL, 'nhi@gmail.com', NULL, NULL, NULL, NULL, '0938047296', NULL, 3, 429, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:52:33', '2022-07-28 16:52:33'),
(431, 'KH00415', 'Chi Nhi(Bao Nhi)', NULL, 'nhi@gmail.com', NULL, NULL, NULL, NULL, '0932742362', NULL, 3, 429, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:53:53', '2022-07-28 16:53:53'),
(432, 'KH00416', 'Chi Ngan', NULL, 'ngan@gmail.com', NULL, NULL, NULL, NULL, '0938585157', NULL, 3, 429, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:54:39', '2022-07-28 16:54:39'),
(433, 'KH00417', 'Chi Truc', NULL, 'truc@gmail.com', NULL, NULL, NULL, NULL, '0798393779', NULL, 3, 429, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:55:11', '2022-07-28 16:55:11'),
(434, 'KH00418', 'Anh Nhi', NULL, 'nhi@gmail.com', NULL, NULL, NULL, NULL, '0931789692', NULL, 3, 430, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:57:28', '2022-07-28 16:57:28'),
(435, 'KH00419', 'Chi Thuong', NULL, 'thuong@gmail.com', NULL, NULL, NULL, NULL, '0974073615', NULL, 3, 430, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:58:10', '2022-07-28 16:58:10'),
(436, 'KH00420', 'Anh Long', NULL, 'long@gmail.com', NULL, NULL, NULL, NULL, '0362583025', NULL, 3, 430, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:59:06', '2022-07-28 16:59:06'),
(437, 'KH00421', 'Anh Thoai', NULL, 'thoai@gmail.com', NULL, NULL, NULL, NULL, '0902103937', NULL, 3, 430, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 16:59:38', '2022-07-28 16:59:38'),
(438, 'KH00422', 'Anh Minh', NULL, 'minh@gmail.com', NULL, NULL, NULL, NULL, '0908046736', NULL, 3, 430, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:00:16', '2022-07-28 17:00:16'),
(439, 'KH00423', 'Ms Quyen', NULL, 'quyen@gmail.com', NULL, '1', 'Villa', 'Rich', '0983324150', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:01:44', '2022-07-28 17:01:44'),
(440, 'KH00424', 'Co Khue', NULL, 'khue@gmail.com', NULL, NULL, NULL, NULL, '0981237676', NULL, 3, 439, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:02:33', '2022-07-28 17:02:33'),
(441, 'KH00425', 'Co Lan', NULL, 'lan@gmail.com', NULL, NULL, NULL, NULL, '0909428560', NULL, 3, 440, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:06:04', '2022-07-28 17:06:04'),
(442, 'KH00426', 'co dien', NULL, 'dien@gmail.com', NULL, NULL, NULL, NULL, '0932029843', NULL, 3, 440, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:06:34', '2022-07-28 17:06:34'),
(443, 'KH00427', 'anh ha chi van', NULL, 'ha@gmail.com', NULL, NULL, NULL, NULL, '0901319123', NULL, 3, 440, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:07:01', '2022-07-28 17:07:01'),
(444, 'KH00428', 'chi quynh', NULL, 'quynh@gmail.com', NULL, NULL, NULL, NULL, '0973678468', NULL, 3, 440, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:08:05', '2022-07-28 17:08:05'),
(445, 'KH00429', 'Chi Dao', NULL, 'dao@gmail.com', NULL, NULL, 'Flat', 'Rich', '0908709939', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:11:23', '2022-07-28 17:11:23'),
(446, 'KH00430', 'Anh HA', NULL, 'Ha@gmail.com', NULL, NULL, 'Apartment', 'Medium', '0379519843', NULL, 3, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2022-07-28 17:14:23', '2022-07-28 17:14:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_care`
--

CREATE TABLE `customer_care` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_staff_id` int(11) DEFAULT NULL COMMENT 'Nhân sự tiếp nhận',
  `customer_id` int(11) DEFAULT NULL COMMENT 'khách hàng cần chăm sóc',
  `customer_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `customer_care_date` date DEFAULT NULL COMMENT 'ngày chăm sóc khách hàng',
  `customer_care_date_end` date NOT NULL COMMENT 'ngày kết thúc',
  `customer_care_status` tinyint(1) DEFAULT NULL COMMENT 'trạng thái',
  `customer_care_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `customer_care_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer_care`
--

INSERT INTO `customer_care` (`id`, `customer_staff_id`, `customer_id`, `customer_brand_id`, `customer_care_date`, `customer_care_date_end`, `customer_care_status`, `customer_care_user_created`, `customer_care_user_updated`, `created_at`, `updated_at`) VALUES
(1, 95, 16, 3, '2022-05-26', '2022-08-26', 2, NULL, NULL, '2022-02-26 14:43:13', '2022-07-03 10:23:57'),
(2, 95, 17, 3, '2022-05-26', '2022-08-26', 2, NULL, NULL, '2022-02-26 14:47:46', '2022-07-03 10:24:37'),
(3, 95, 18, 3, '2022-05-26', '2022-08-26', 2, NULL, NULL, '2022-02-27 01:57:06', '2022-07-03 10:25:10'),
(4, 95, 19, 3, '2022-05-26', '2022-08-26', 2, NULL, NULL, '2022-02-27 02:00:19', '2022-07-03 10:26:05'),
(5, 95, 20, 3, '2022-05-26', '2022-08-26', 2, NULL, NULL, '2022-02-27 02:17:35', '2022-07-03 10:26:48'),
(6, 95, 20, 3, '2022-05-26', '2022-08-26', 2, NULL, NULL, '2022-02-27 02:20:09', '2022-07-03 10:27:10'),
(7, 95, 21, 3, '2022-05-27', '2022-08-27', 2, NULL, NULL, '2022-02-28 00:09:24', '2022-07-03 10:27:53'),
(8, 109, 22, 3, '2022-04-27', '2022-07-27', 1, NULL, NULL, '2022-04-27 15:17:52', '2022-04-27 15:17:52'),
(9, 109, 31, 3, '2022-04-27', '2022-07-27', 1, NULL, NULL, '2022-04-27 15:23:04', '2022-04-27 15:23:04'),
(10, 109, 37, 3, '2022-04-27', '2022-07-27', 1, NULL, NULL, '2022-04-27 22:39:34', '2022-04-27 22:39:34'),
(11, 109, 38, 3, '2022-04-27', '2022-07-27', 1, NULL, NULL, '2022-04-27 22:46:36', '2022-04-27 22:46:36'),
(12, 109, 39, 3, '2022-04-27', '2022-07-27', 1, NULL, NULL, '2022-04-27 23:42:38', '2022-04-27 23:42:38'),
(13, 109, 41, 3, '2022-04-27', '2022-07-27', 1, NULL, NULL, '2022-04-27 23:50:44', '2022-04-27 23:50:44'),
(14, 103, 33, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 09:09:19', '2022-04-28 09:09:19'),
(15, 109, 41, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 09:09:24', '2022-04-28 09:09:24'),
(16, 107, 23, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 09:09:24', '2022-04-28 09:09:24'),
(17, 106, 26, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 09:12:46', '2022-04-28 09:12:46'),
(18, 107, 46, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 09:16:45', '2022-04-28 09:16:45'),
(19, 106, 42, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 09:18:58', '2022-04-28 09:18:58'),
(20, 109, 47, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 18:25:34', '2022-04-28 18:25:34'),
(21, 109, 49, 3, '2022-04-28', '2022-07-28', 1, NULL, NULL, '2022-04-28 18:35:17', '2022-04-28 18:35:17'),
(22, 109, 43, 3, '2022-04-30', '2022-07-30', 1, NULL, NULL, '2022-04-30 14:20:35', '2022-04-30 14:20:35'),
(23, 109, 79, 3, '2022-05-11', '2022-08-11', 1, NULL, NULL, '2022-05-11 11:40:17', '2022-05-11 11:40:17'),
(24, 118, 80, 3, '2022-05-11', '2022-08-11', 1, NULL, NULL, '2022-05-11 11:40:19', '2022-05-11 11:40:19'),
(25, 118, 86, 3, '2022-05-11', '2022-08-11', 1, NULL, NULL, '2022-05-11 11:44:23', '2022-05-11 11:44:23'),
(26, 109, 84, 3, '2022-05-11', '2022-08-11', 1, NULL, NULL, '2022-05-11 11:44:54', '2022-05-11 11:44:54'),
(27, 104, 89, 3, '2022-05-12', '2022-08-12', 1, NULL, NULL, '2022-05-12 09:57:36', '2022-05-12 09:57:36'),
(28, 104, 90, 3, '2022-05-12', '2022-08-12', 1, NULL, NULL, '2022-05-12 09:59:55', '2022-05-12 09:59:55'),
(29, 103, 141, 3, '2022-06-16', '2022-09-16', 1, NULL, NULL, '2022-06-16 09:43:31', '2022-06-16 09:43:31'),
(30, 123, 225, 3, '2022-06-23', '2022-09-23', 1, NULL, NULL, '2022-06-23 13:30:53', '2022-06-23 13:30:53'),
(31, 95, 421, 3, '2022-07-23', '2022-10-23', 1, NULL, NULL, '2022-07-23 16:38:39', '2022-07-23 16:38:39'),
(32, 95, 421, 3, '2022-07-23', '2022-10-23', 1, NULL, NULL, '2022-07-23 16:40:33', '2022-07-23 16:40:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `decentralization`
--

CREATE TABLE `decentralization` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `decen_position_id` int(11) DEFAULT NULL COMMENT 'chức vụ',
  `decen_general_directory` tinyint(1) DEFAULT NULL COMMENT 'Cấu hình chung',
  `decen_history_system` tinyint(1) DEFAULT NULL COMMENT 'Lịch sử',
  `decen_formality_payment` tinyint(1) DEFAULT NULL COMMENT 'thanh toán chính thức',
  `decen_receipts` tinyint(1) DEFAULT NULL COMMENT 'phiếu thu',
  `decen_payment` tinyint(1) DEFAULT NULL COMMENT 'thanh toán',
  `decen_approve_receipts` tinyint(1) DEFAULT NULL COMMENT 'duyệt phiếu thu',
  `decen_approve_payment` tinyint(1) DEFAULT NULL COMMENT 'duyệt thanh toán',
  `decen_Staff` tinyint(1) DEFAULT NULL COMMENT 'nhân viên',
  `decen_kpi_sales` tinyint(1) DEFAULT NULL COMMENT 'kpi nhân viên sales',
  `decen_position` tinyint(1) DEFAULT NULL COMMENT 'chức vụ',
  `decen_rank` tinyint(1) DEFAULT NULL COMMENT 'cấp bậc',
  `decen_commission_rank` tinyint(1) DEFAULT NULL COMMENT 'cấp bậc hoa hồng',
  `decen_commission` tinyint(1) DEFAULT NULL COMMENT 'hoa hồng',
  `decen_supplier` tinyint(1) DEFAULT NULL COMMENT 'nhà cung cấp',
  `decen_supplier_order_list` tinyint(1) DEFAULT NULL COMMENT 'danh sách đơn hàng nhà cung cấp',
  `decen_supplier_debt_list` tinyint(1) DEFAULT NULL COMMENT 'danh sách công nợ nahf cung cấp',
  `decen_customers` tinyint(1) DEFAULT NULL COMMENT 'Khách hàng',
  `decen_customer_debt_list` tinyint(1) DEFAULT NULL COMMENT 'danh sách công nợ khách hàng',
  `decen_customers_care` tinyint(1) DEFAULT NULL COMMENT 'danh sách khách hàng cần quan tâm',
  `decen_customers_receive_gifts` tinyint(1) DEFAULT NULL,
  `decen_product` tinyint(1) DEFAULT NULL COMMENT 'sản phẩm',
  `decen_product_cate` tinyint(1) DEFAULT NULL COMMENT 'loại sản phẩm',
  `decen_purchases` tinyint(1) DEFAULT NULL COMMENT 'nhập kho',
  `decen_successful_order` tinyint(1) DEFAULT NULL COMMENT 'danh sách đơn hàng nhà cung cấp',
  `decen_inventory` tinyint(1) DEFAULT NULL COMMENT 'kiểm kho',
  `decen_synthesis_report` tinyint(1) DEFAULT NULL COMMENT 'báo cáo',
  `decen_Order` tinyint(1) DEFAULT NULL COMMENT 'sản phẩm',
  `decen_installment_orders` tinyint(1) DEFAULT NULL COMMENT 'loại sản phẩm',
  `decen_full_payment_order` tinyint(1) DEFAULT NULL COMMENT 'nhập kho',
  `decen_gift_order` tinyint(1) DEFAULT NULL COMMENT 'danh sách đơn hàng nhà cung cấp',
  `decen_payment_methods` tinyint(1) DEFAULT NULL COMMENT 'kiểm kho',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `decentralization`
--

INSERT INTO `decentralization` (`id`, `decen_position_id`, `decen_general_directory`, `decen_history_system`, `decen_formality_payment`, `decen_receipts`, `decen_payment`, `decen_approve_receipts`, `decen_approve_payment`, `decen_Staff`, `decen_kpi_sales`, `decen_position`, `decen_rank`, `decen_commission_rank`, `decen_commission`, `decen_supplier`, `decen_supplier_order_list`, `decen_supplier_debt_list`, `decen_customers`, `decen_customer_debt_list`, `decen_customers_care`, `decen_customers_receive_gifts`, `decen_product`, `decen_product_cate`, `decen_purchases`, `decen_successful_order`, `decen_inventory`, `decen_synthesis_report`, `decen_Order`, `decen_installment_orders`, `decen_full_payment_order`, `decen_gift_order`, `decen_payment_methods`, `created_at`, `updated_at`) VALUES
(2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, '2021-10-24 00:29:09', '2021-10-24 00:29:09'),
(3, 2, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, '2021-11-26 20:53:20', '2021-11-26 20:53:20'),
(4, 3, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, '2021-11-26 20:54:00', '2021-11-26 20:54:00'),
(5, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-11-26 23:57:49', '2021-11-26 23:57:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `history`
--

CREATE TABLE `history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `history_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã phiếu',
  `history_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên phiếu',
  `history_content` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'nội dung',
  `history_staff_id` int(11) DEFAULT NULL COMMENT 'Nhân viên thực hiện',
  `history_action` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'hành động',
  `history_price` decimal(13,4) DEFAULT NULL COMMENT 'số tiền nếu có',
  `history_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `history_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `history`
--

INSERT INTO `history` (`id`, `history_code`, `history_name`, `history_content`, `history_staff_id`, `history_action`, `history_price`, `history_user_created`, `history_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'PN00001', 'Nhập kho', NULL, 94, 'tạo mới', NULL, NULL, NULL, '2022-02-26 14:36:28', '2022-02-26 14:36:28'),
(2, 'PN00001', 'Nhập kho', NULL, 94, 'Cập nhật', NULL, NULL, NULL, '2022-02-26 14:36:37', '2022-02-26 14:36:37'),
(3, 'PX00001', 'Xuất kho', NULL, 95, 'tạo mới', 900000.0000, NULL, NULL, '2022-02-26 14:43:05', '2022-02-26 14:43:05'),
(4, 'PX00001', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-02-26 14:43:13', '2022-02-26 14:43:13'),
(5, 'PX00002', 'Xuất kho', NULL, 95, 'tạo mới', 2700000.0000, NULL, NULL, '2022-02-26 14:47:39', '2022-02-26 14:47:39'),
(6, 'PX00002', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-02-26 14:47:46', '2022-02-26 14:47:46'),
(7, 'PT00001', 'Phiếu thu Tiền -PT00001', NULL, 95, 'tạo mới', 100000.0000, NULL, NULL, '2022-02-26 14:51:36', '2022-02-26 14:51:36'),
(8, 'PT00001', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-26 14:53:03', '2022-02-26 14:53:03'),
(9, 'PT00001', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-02-26 14:54:09', '2022-02-26 14:54:09'),
(10, 'PX00003', 'Xuất kho', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-02-27 01:56:59', '2022-02-27 01:56:59'),
(11, 'PX00003', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-02-27 01:57:06', '2022-02-27 01:57:06'),
(12, 'PX00004', 'Xuất kho', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-02-27 02:00:11', '2022-02-27 02:00:11'),
(13, 'PX00004', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-02-27 02:00:19', '2022-02-27 02:00:19'),
(14, 'PT00002', 'Phiếu thu Tiền -PT00002', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-02-27 02:01:05', '2022-02-27 02:01:05'),
(15, 'PT00002', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-27 02:01:23', '2022-02-27 02:01:23'),
(16, 'PT00003', 'Phiếu thu Tiền -PT00003', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-02-27 02:09:34', '2022-02-27 02:09:34'),
(17, 'PT00003', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-27 02:09:59', '2022-02-27 02:09:59'),
(18, 'PX00005', 'Xuất kho', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-02-27 02:17:21', '2022-02-27 02:17:21'),
(19, 'PX00005', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-02-27 02:17:35', '2022-02-27 02:17:35'),
(20, 'PX00006', 'Xuất kho', NULL, 95, 'tạo mới', 2700000.0000, NULL, NULL, '2022-02-27 02:19:58', '2022-02-27 02:19:58'),
(21, 'PX00006', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-02-27 02:20:09', '2022-02-27 02:20:09'),
(22, 'PN00002', 'Nhập kho', NULL, 94, 'tạo mới', NULL, NULL, NULL, '2022-02-27 16:10:00', '2022-02-27 16:10:00'),
(23, 'PN00002', 'Nhập kho', NULL, 94, 'Cập nhật', NULL, NULL, NULL, '2022-02-27 16:15:00', '2022-02-27 16:15:00'),
(24, 'PT00004', 'Phiếu thu Tiền -PT00004', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-02-27 22:36:43', '2022-02-27 22:36:43'),
(25, 'PT00004', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-27 22:37:02', '2022-02-27 22:37:02'),
(26, 'PT00005', 'Phiếu thu Tiền -PT00005', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-02-27 23:01:36', '2022-02-27 23:01:36'),
(27, 'PT00005', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-27 23:01:59', '2022-02-27 23:01:59'),
(28, 'PT00002', 'Admin hủy duyệt phiếu thu', NULL, 1, 'CancelOderGift', NULL, NULL, NULL, '2022-02-27 23:11:37', '2022-02-27 23:11:37'),
(29, 'PT00003', 'Admin hủy duyệt phiếu thu', NULL, 1, 'CancelOderGift', NULL, NULL, NULL, '2022-02-27 23:11:46', '2022-02-27 23:11:46'),
(30, 'PT00002', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-27 23:12:15', '2022-02-27 23:12:15'),
(31, 'PT00002', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-02-27 23:12:40', '2022-02-27 23:12:40'),
(32, 'PT00004', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-02-27 23:14:00', '2022-02-27 23:14:00'),
(33, 'PT00005', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-02-27 23:15:23', '2022-02-27 23:15:23'),
(34, 'PT00003', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-27 23:16:43', '2022-02-27 23:16:43'),
(35, 'PT00003', 'Admin hủy duyệt phiếu thu', NULL, 1, 'CancelOderGift', NULL, NULL, NULL, '2022-02-27 23:17:07', '2022-02-27 23:17:07'),
(36, 'PX00007', 'Xuất kho', NULL, 95, 'tạo mới', 86800000.0000, NULL, NULL, '2022-02-28 00:09:06', '2022-02-28 00:09:06'),
(37, 'PX00007', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-02-28 00:09:24', '2022-02-28 00:09:24'),
(38, 'PXQT00001', 'Xuất kho quà tặng', NULL, 95, 'tạo mới', NULL, NULL, NULL, '2022-02-28 00:21:02', '2022-02-28 00:21:02'),
(39, 'PXQT00001', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOderGift', NULL, NULL, NULL, '2022-02-28 00:21:09', '2022-02-28 00:21:09'),
(40, 'PT00006', 'Phiếu thu Tiền -PT00006', NULL, 95, 'tạo mới', 10000000.0000, NULL, NULL, '2022-02-28 01:01:44', '2022-02-28 01:01:44'),
(41, 'PT00006', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-02-28 01:02:57', '2022-02-28 01:02:57'),
(42, 'PT00006', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-03-06 16:17:45', '2022-03-06 16:17:45'),
(43, 'PT00007', 'Phiếu thu Tiền -PT00007', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-03-06 16:19:55', '2022-03-06 16:19:55'),
(44, 'PT00007', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-03-06 16:20:13', '2022-03-06 16:20:13'),
(45, 'PT00007', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-03-06 16:20:58', '2022-03-06 16:20:58'),
(46, 'TSC00001', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 14:47:05', '2022-04-27 14:47:05'),
(47, 'TSC00002', 'Xuất kho tài sản công ty', NULL, 111, 'tạo mới', NULL, NULL, NULL, '2022-04-27 14:47:20', '2022-04-27 14:47:20'),
(48, 'PX00008', 'Xuất kho', NULL, 109, 'tạo mới', 1800000.0000, NULL, NULL, '2022-04-27 15:17:22', '2022-04-27 15:17:22'),
(49, 'PX00008', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-27 15:17:52', '2022-04-27 15:17:52'),
(50, 'PX00009', 'Xuất kho', NULL, 109, 'tạo mới', 900000.0000, NULL, NULL, '2022-04-27 15:19:45', '2022-04-27 15:19:45'),
(51, 'PX00009', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-27 15:23:04', '2022-04-27 15:23:04'),
(52, 'PXQT00002', 'Xuất kho quà tặng', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 22:15:02', '2022-04-27 22:15:02'),
(53, 'TSC00003', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 22:18:21', '2022-04-27 22:18:21'),
(54, 'TSC00004', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 22:23:20', '2022-04-27 22:23:20'),
(55, 'PX00010', 'Xuất kho', NULL, 109, 'tạo mới', 17000000.0000, NULL, NULL, '2022-04-27 22:38:07', '2022-04-27 22:38:07'),
(56, 'PX00010', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-27 22:39:34', '2022-04-27 22:39:34'),
(57, 'TSC00005', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 22:42:11', '2022-04-27 22:42:11'),
(58, 'TSC00006', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 22:42:54', '2022-04-27 22:42:54'),
(59, 'PX00011', 'Xuất kho', NULL, 109, 'tạo mới', 17000000.0000, NULL, NULL, '2022-04-27 22:46:26', '2022-04-27 22:46:26'),
(60, 'PX00011', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-27 22:46:36', '2022-04-27 22:46:36'),
(61, 'PXQT00003', 'Xuất kho quà tặng', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 22:47:37', '2022-04-27 22:47:37'),
(62, 'PXQT00003', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOderGift', NULL, NULL, NULL, '2022-04-27 22:47:48', '2022-04-27 22:47:48'),
(63, 'PX00012', 'Xuất kho', NULL, 109, 'tạo mới', 34000000.0000, NULL, NULL, '2022-04-27 23:42:05', '2022-04-27 23:42:05'),
(64, 'PX00012', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-27 23:42:38', '2022-04-27 23:42:38'),
(65, 'PX00013', 'Xuất kho', NULL, 109, 'tạo mới', 188000000.0000, NULL, NULL, '2022-04-27 23:47:45', '2022-04-27 23:47:45'),
(66, 'PX00013', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-27 23:50:44', '2022-04-27 23:50:44'),
(67, 'PT00008', 'Phiếu thu Tiền -PT00008', NULL, 109, 'tạo mới', 5000000.0000, NULL, NULL, '2022-04-27 23:57:17', '2022-04-27 23:57:17'),
(68, 'PT00008', 'Gửi duyệt phiếu thu', NULL, 109, 'Send Approve', NULL, NULL, NULL, '2022-04-27 23:58:03', '2022-04-27 23:58:03'),
(69, 'PT00009', 'Phiếu thu Tiền -PT00009', NULL, 109, 'tạo mới', 34000000.0000, NULL, NULL, '2022-04-27 23:58:22', '2022-04-27 23:58:22'),
(70, 'TSC00007', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-27 23:59:19', '2022-04-27 23:59:19'),
(71, 'TSC00008', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 00:10:27', '2022-04-28 00:10:27'),
(72, 'TSC00009', 'Xuất kho tài sản công ty', NULL, 107, 'tạo mới', NULL, NULL, NULL, '2022-04-28 08:52:46', '2022-04-28 08:52:46'),
(73, 'TSC00010', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 08:52:58', '2022-04-28 08:52:58'),
(74, 'TSC00011', 'Xuất kho tài sản công ty', NULL, 103, 'tạo mới', NULL, NULL, NULL, '2022-04-28 08:53:00', '2022-04-28 08:53:00'),
(75, 'TSC00012', 'Xuất kho tài sản công ty', NULL, 106, 'tạo mới', NULL, NULL, NULL, '2022-04-28 08:53:34', '2022-04-28 08:53:34'),
(76, 'TSC00012', 'Cập nhật tài sản chung', NULL, 106, 'UpdateCompanyAssets', NULL, NULL, NULL, '2022-04-28 08:55:18', '2022-04-28 08:55:18'),
(77, 'TSC00013', 'Xuất kho tài sản công ty', NULL, 99, 'tạo mới', NULL, NULL, NULL, '2022-04-28 08:58:34', '2022-04-28 08:58:34'),
(78, 'PX00014', 'Xuất kho', NULL, 106, 'tạo mới', 79800000.0000, NULL, NULL, '2022-04-28 09:04:36', '2022-04-28 09:04:36'),
(79, 'PX00015', 'Xuất kho', NULL, 103, 'tạo mới', 900000.0000, NULL, NULL, '2022-04-28 09:06:34', '2022-04-28 09:06:34'),
(80, 'PX00016', 'Xuất kho', NULL, 107, 'tạo mới', 2700000.0000, NULL, NULL, '2022-04-28 09:07:50', '2022-04-28 09:07:50'),
(81, 'PX00017', 'Xuất kho', NULL, 109, 'tạo mới', 17000000.0000, NULL, NULL, '2022-04-28 09:08:06', '2022-04-28 09:08:06'),
(82, 'PX00015', 'Thủ kho duyệt đơn hàng', NULL, 103, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 09:09:19', '2022-04-28 09:09:19'),
(83, 'PX00017', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 09:09:24', '2022-04-28 09:09:24'),
(84, 'PX00016', 'Thủ kho duyệt đơn hàng', NULL, 107, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 09:09:24', '2022-04-28 09:09:24'),
(85, 'PX00014', 'Thủ kho duyệt đơn hàng', NULL, 106, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 09:12:46', '2022-04-28 09:12:46'),
(86, 'PXQT00004', 'Xuất kho quà tặng', NULL, 103, 'tạo mới', NULL, NULL, NULL, '2022-04-28 09:12:52', '2022-04-28 09:12:52'),
(87, 'PXQT00005', 'Xuất kho quà tặng', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 09:13:17', '2022-04-28 09:13:17'),
(88, 'PX00018', 'Xuất kho', NULL, 107, 'tạo mới', 900000.0000, NULL, NULL, '2022-04-28 09:16:28', '2022-04-28 09:16:28'),
(89, 'PX00019', 'Xuất kho', NULL, 106, 'tạo mới', 78900000.0000, NULL, NULL, '2022-04-28 09:16:38', '2022-04-28 09:16:38'),
(90, 'PX00018', 'Thủ kho duyệt đơn hàng', NULL, 107, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 09:16:45', '2022-04-28 09:16:45'),
(91, 'PXQT00006', 'Xuất kho quà tặng', NULL, 107, 'tạo mới', NULL, NULL, NULL, '2022-04-28 09:17:30', '2022-04-28 09:17:30'),
(92, 'PXQT00006', 'Thủ kho duyệt đơn hàng', NULL, 107, 'ApproveOderGift', NULL, NULL, NULL, '2022-04-28 09:17:44', '2022-04-28 09:17:44'),
(93, 'PX00019', 'Thủ kho duyệt đơn hàng', NULL, 106, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 09:18:58', '2022-04-28 09:18:58'),
(94, 'PXQT00005', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOderGift', NULL, NULL, NULL, '2022-04-28 09:19:36', '2022-04-28 09:19:36'),
(95, 'PXQT00007', 'Xuất kho quà tặng', NULL, 106, 'tạo mới', NULL, NULL, NULL, '2022-04-28 09:20:05', '2022-04-28 09:20:05'),
(96, 'TSC00014', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 09:35:18', '2022-04-28 09:35:18'),
(97, 'TSC00015', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 09:36:04', '2022-04-28 09:36:04'),
(98, 'TSC00016', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 09:36:45', '2022-04-28 09:36:45'),
(99, 'TSC00017', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 18:19:36', '2022-04-28 18:19:36'),
(100, 'PX00020', 'Xuất kho', NULL, 109, 'tạo mới', 17000000.0000, NULL, NULL, '2022-04-28 18:25:09', '2022-04-28 18:25:09'),
(101, 'PX00020', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 18:25:34', '2022-04-28 18:25:34'),
(102, 'PT00010', 'Phiếu thu Tiền -PT00010', NULL, 109, 'tạo mới', 7000000.0000, NULL, NULL, '2022-04-28 18:30:32', '2022-04-28 18:30:32'),
(103, 'PT00010', 'Gửi duyệt phiếu thu', NULL, 109, 'Send Approve', NULL, NULL, NULL, '2022-04-28 18:31:03', '2022-04-28 18:31:03'),
(104, 'PX00021', 'Xuất kho', NULL, 109, 'tạo mới', 85000000.0000, NULL, NULL, '2022-04-28 18:34:34', '2022-04-28 18:34:34'),
(105, 'PX00021', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-28 18:35:17', '2022-04-28 18:35:17'),
(106, 'PXQT00008', 'Xuất kho quà tặng', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-28 18:36:16', '2022-04-28 18:36:16'),
(107, 'PXQT00008', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOderGift', NULL, NULL, NULL, '2022-04-28 18:37:16', '2022-04-28 18:37:16'),
(108, 'PXQT00009', 'Xuất kho quà tặng', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-04-29 09:12:07', '2022-04-29 09:12:07'),
(109, 'PXQT00009', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOderGift', NULL, NULL, NULL, '2022-04-29 09:12:17', '2022-04-29 09:12:17'),
(110, 'PX00022', 'Xuất kho', NULL, 109, 'tạo mới', 17000000.0000, NULL, NULL, '2022-04-30 14:20:25', '2022-04-30 14:20:25'),
(111, 'PX00022', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-04-30 14:20:35', '2022-04-30 14:20:35'),
(112, 'TSC00001', 'Cập nhật tài sản chung', NULL, 95, 'UpdateCompanyAssets', NULL, NULL, NULL, '2022-05-10 20:59:49', '2022-05-10 20:59:49'),
(113, 'TSC00004', 'Cập nhật tài sản chung', NULL, 95, 'UpdateCompanyAssets', NULL, NULL, NULL, '2022-05-10 21:08:40', '2022-05-10 21:08:40'),
(114, 'TSC00018', 'Xuất kho tài sản công ty', NULL, 116, 'tạo mới', NULL, NULL, NULL, '2022-05-11 11:33:44', '2022-05-11 11:33:44'),
(115, 'TSC00019', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-05-11 11:34:09', '2022-05-11 11:34:09'),
(116, 'TSC00020', 'Xuất kho tài sản công ty', NULL, 118, 'tạo mới', NULL, NULL, NULL, '2022-05-11 11:34:35', '2022-05-11 11:34:35'),
(117, 'PX00023', 'Xuất kho', NULL, 118, 'tạo mới', 900000.0000, NULL, NULL, '2022-05-11 11:39:28', '2022-05-11 11:39:28'),
(118, 'PX00024', 'Xuất kho', NULL, 109, 'tạo mới', 17000000.0000, NULL, NULL, '2022-05-11 11:39:54', '2022-05-11 11:39:54'),
(119, 'PX00024', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-05-11 11:40:17', '2022-05-11 11:40:17'),
(120, 'PX00023', 'Thủ kho duyệt đơn hàng', NULL, 118, 'ApproveOder', NULL, NULL, NULL, '2022-05-11 11:40:19', '2022-05-11 11:40:19'),
(121, 'PX00025', 'Xuất kho', NULL, 116, 'tạo mới', 900000.0000, NULL, NULL, '2022-05-11 11:40:38', '2022-05-11 11:40:38'),
(122, 'PX00026', 'Xuất kho', NULL, 120, 'tạo mới', 900000.0000, NULL, NULL, '2022-05-11 11:43:06', '2022-05-11 11:43:06'),
(123, 'PX00027', 'Xuất kho', NULL, 118, 'tạo mới', 85000000.0000, NULL, NULL, '2022-05-11 11:44:11', '2022-05-11 11:44:11'),
(124, 'PX00027', 'Thủ kho duyệt đơn hàng', NULL, 118, 'ApproveOder', NULL, NULL, NULL, '2022-05-11 11:44:23', '2022-05-11 11:44:23'),
(125, 'PX00028', 'Xuất kho', NULL, 109, 'tạo mới', 85000000.0000, NULL, NULL, '2022-05-11 11:44:47', '2022-05-11 11:44:47'),
(126, 'PX00028', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOder', NULL, NULL, NULL, '2022-05-11 11:44:54', '2022-05-11 11:44:54'),
(127, 'PX00029', 'Xuất kho', NULL, 116, 'tạo mới', 68900000.0000, NULL, NULL, '2022-05-11 11:45:32', '2022-05-11 11:45:32'),
(128, 'PXQT00010', 'Xuất kho quà tặng', NULL, 118, 'tạo mới', NULL, NULL, NULL, '2022-05-11 11:46:18', '2022-05-11 11:46:18'),
(129, 'PXQT00011', 'Xuất kho quà tặng', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-05-11 11:47:44', '2022-05-11 11:47:44'),
(130, 'PXQT00011', 'Thủ kho duyệt đơn hàng', NULL, 109, 'ApproveOderGift', NULL, NULL, NULL, '2022-05-11 11:47:51', '2022-05-11 11:47:51'),
(131, 'PXQT00010', 'Thủ kho duyệt đơn hàng', NULL, 118, 'ApproveOderGift', NULL, NULL, NULL, '2022-05-11 11:47:56', '2022-05-11 11:47:56'),
(132, 'TSC00021', 'Xuất kho tài sản công ty', NULL, 104, 'tạo mới', NULL, NULL, NULL, '2022-05-12 09:54:46', '2022-05-12 09:54:46'),
(133, 'PX00030', 'Xuất kho', NULL, 104, 'tạo mới', 85000000.0000, NULL, NULL, '2022-05-12 09:57:27', '2022-05-12 09:57:27'),
(134, 'PX00030', 'Thủ kho duyệt đơn hàng', NULL, 104, 'ApproveOder', NULL, NULL, NULL, '2022-05-12 09:57:36', '2022-05-12 09:57:36'),
(135, 'PX00031', 'Xuất kho', NULL, 104, 'tạo mới', 85000000.0000, NULL, NULL, '2022-05-12 09:59:45', '2022-05-12 09:59:45'),
(136, 'PX00031', 'Thủ kho duyệt đơn hàng', NULL, 104, 'ApproveOder', NULL, NULL, NULL, '2022-05-12 09:59:55', '2022-05-12 09:59:55'),
(137, 'PXQT00012', 'Xuất kho quà tặng', NULL, 104, 'tạo mới', NULL, NULL, NULL, '2022-05-12 10:03:41', '2022-05-12 10:03:41'),
(138, 'PXQT00012', 'Thủ kho duyệt đơn hàng', NULL, 104, 'ApproveOderGift', NULL, NULL, NULL, '2022-05-12 10:04:18', '2022-05-12 10:04:18'),
(139, 'PX00032', 'Xuất kho', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-06-08 23:33:01', '2022-06-08 23:33:01'),
(140, 'PX00033', 'Xuất kho', NULL, 95, 'tạo mới', 51000000.0000, NULL, NULL, '2022-06-08 23:33:41', '2022-06-08 23:33:41'),
(141, 'PX00034', 'Xuất kho', NULL, 95, 'tạo mới', 34000000.0000, NULL, NULL, '2022-06-08 23:34:25', '2022-06-08 23:34:25'),
(142, 'PX00035', 'Xuất kho', NULL, 103, 'tạo mới', 79800000.0000, NULL, NULL, '2022-06-16 09:43:17', '2022-06-16 09:43:17'),
(143, 'PX00035', 'Thủ kho duyệt đơn hàng', NULL, 103, 'ApproveOder', NULL, NULL, NULL, '2022-06-16 09:43:31', '2022-06-16 09:43:31'),
(144, 'PT00011', 'Phiếu thu Tiền -PT00011', NULL, 103, 'tạo mới', 79800000.0000, NULL, NULL, '2022-06-16 09:46:36', '2022-06-16 09:46:36'),
(145, 'PT00011', 'Gửi duyệt phiếu thu', NULL, 103, 'Send Approve', NULL, NULL, NULL, '2022-06-16 09:46:56', '2022-06-16 09:46:56'),
(146, 'TSC00022', 'Xuất kho tài sản công ty', NULL, 103, 'tạo mới', NULL, NULL, NULL, '2022-06-16 15:54:24', '2022-06-16 15:54:24'),
(147, 'PX00036', 'Xuất kho', NULL, 95, 'tạo mới', 90000000.0000, NULL, NULL, '2022-06-19 17:49:36', '2022-06-19 17:49:36'),
(148, 'PX00037', 'Xuất kho', NULL, 95, 'tạo mới', 900000.0000, NULL, NULL, '2022-06-19 18:00:05', '2022-06-19 18:00:05'),
(149, 'PT00008', 'Admin hủy duyệt phiếu thu', NULL, 1, 'CancelOderGift', NULL, NULL, NULL, '2022-06-20 15:45:37', '2022-06-20 15:45:37'),
(150, 'PT00010', 'Admin hủy duyệt phiếu thu', NULL, 1, 'CancelOderGift', NULL, NULL, NULL, '2022-06-20 15:45:46', '2022-06-20 15:45:46'),
(151, 'PT00011', 'Admin hủy duyệt phiếu thu', NULL, 1, 'CancelOderGift', NULL, NULL, NULL, '2022-06-20 15:45:52', '2022-06-20 15:45:52'),
(152, 'TSC00023', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-06-23 13:26:55', '2022-06-23 13:26:55'),
(153, 'TSC00024', 'Xuất kho tài sản công ty', NULL, 123, 'tạo mới', NULL, NULL, NULL, '2022-06-23 13:27:16', '2022-06-23 13:27:16'),
(154, 'TSC00025', 'Xuất kho tài sản công ty', NULL, 122, 'tạo mới', NULL, NULL, NULL, '2022-06-23 13:29:01', '2022-06-23 13:29:01'),
(155, 'PX00038', 'Xuất kho', NULL, 123, 'tạo mới', 85000000.0000, NULL, NULL, '2022-06-23 13:30:37', '2022-06-23 13:30:37'),
(156, 'PX00038', 'Thủ kho duyệt đơn hàng', NULL, 123, 'ApproveOder', NULL, NULL, NULL, '2022-06-23 13:30:53', '2022-06-23 13:30:53'),
(157, 'PXQT00013', 'Xuất kho quà tặng', NULL, 123, 'tạo mới', NULL, NULL, NULL, '2022-06-23 13:32:19', '2022-06-23 13:32:19'),
(158, 'PXQT00013', 'Thủ kho duyệt đơn hàng', NULL, 123, 'ApproveOderGift', NULL, NULL, NULL, '2022-06-23 13:32:27', '2022-06-23 13:32:27'),
(159, 'PXQT00014', 'Xuất kho quà tặng', NULL, 95, 'tạo mới', NULL, NULL, NULL, '2022-07-02 21:41:34', '2022-07-02 21:41:34'),
(160, 'PXQT00014', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOderGift', NULL, NULL, NULL, '2022-07-02 21:41:44', '2022-07-02 21:41:44'),
(161, 'PXQT00001', 'Xuất kho quà tặng', NULL, 95, 'tạo mới', NULL, NULL, NULL, '2022-07-03 00:10:56', '2022-07-03 00:10:56'),
(162, 'PXQT00001', 'Xuất kho quà tặng', NULL, 95, 'tạo mới', NULL, NULL, NULL, '2022-07-03 00:14:02', '2022-07-03 00:14:02'),
(163, 'PXQT00001', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOderGift', NULL, NULL, NULL, '2022-07-03 00:14:10', '2022-07-03 00:14:10'),
(164, 'TSC00026', 'Xuất kho tài sản công ty', NULL, 103, 'tạo mới', NULL, NULL, NULL, '2022-07-04 14:49:53', '2022-07-04 14:49:53'),
(165, 'PT00012', 'Phiếu thu Tiền -PT00012', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-07-10 17:53:38', '2022-07-10 17:53:38'),
(166, 'PT00012', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-07-10 17:54:02', '2022-07-10 17:54:02'),
(167, 'TSC00027', 'Xuất kho tài sản công ty', NULL, 109, 'tạo mới', NULL, NULL, NULL, '2022-07-20 16:33:47', '2022-07-20 16:33:47'),
(168, 'PN00003', 'Nhập kho', NULL, 94, 'tạo mới', NULL, NULL, NULL, '2022-07-23 16:23:59', '2022-07-23 16:23:59'),
(169, 'PN00003', 'Nhập kho', NULL, 94, 'Cập nhật', NULL, NULL, NULL, '2022-07-23 16:24:39', '2022-07-23 16:24:39'),
(170, 'PX00039', 'Xuất kho', NULL, 95, 'tạo mới', 5400000.0000, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 16:38:18'),
(171, 'PX00039', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-07-23 16:38:39', '2022-07-23 16:38:39'),
(172, 'PX00040', 'Xuất kho', NULL, 95, 'tạo mới', 1800000.0000, NULL, NULL, '2022-07-23 16:40:04', '2022-07-23 16:40:04'),
(173, 'PX00040', 'Thủ kho duyệt đơn hàng', NULL, 95, 'ApproveOder', NULL, NULL, NULL, '2022-07-23 16:40:33', '2022-07-23 16:40:33'),
(174, 'PT00013', 'Phiếu thu Tiền -PT00013', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 16:54:02', '2022-07-23 16:54:02'),
(175, 'PT00013', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-07-23 16:55:51', '2022-07-23 16:55:51'),
(176, 'PT00012', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-07-23 16:58:54', '2022-07-23 16:58:54'),
(177, 'PT00014', 'Phiếu thu Tiền -PT00014', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 17:00:42', '2022-07-23 17:00:42'),
(178, 'PT00015', 'Phiếu thu Tiền -PT00015', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:08:17', '2022-07-23 23:08:17'),
(179, 'PT00013', 'Duyệt phiếu thu', NULL, 1, 'Send Approve', NULL, NULL, NULL, '2022-07-23 23:08:55', '2022-07-23 23:08:55'),
(180, 'PT00012', 'Phiếu thu Tiền -PT00012', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:15:57', '2022-07-23 23:15:57'),
(181, 'PT00012', 'Gửi duyệt phiếu thu', NULL, 95, 'Send Approve', NULL, NULL, NULL, '2022-07-23 23:16:14', '2022-07-23 23:16:14'),
(182, 'PT00012', 'Phiếu thu Tiền -PT00012', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:22:12', '2022-07-23 23:22:12'),
(183, 'PT00013', 'Phiếu thu Tiền -PT00013', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:22:31', '2022-07-23 23:22:31'),
(184, 'PT00014', 'Phiếu thu Tiền -PT00014', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:23:06', '2022-07-23 23:23:06'),
(185, 'PT00012', 'Phiếu thu Tiền -PT00012', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:24:17', '2022-07-23 23:24:17'),
(186, 'PT00012', 'Phiếu thu Tiền -PT00012', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:25:51', '2022-07-23 23:25:51'),
(187, 'PT00013', 'Phiếu thu Tiền -PT00013', NULL, 95, 'tạo mới', 880000.0000, NULL, NULL, '2022-07-23 23:27:36', '2022-07-23 23:27:36'),
(188, 'PT00013', 'Phiếu thu Tiền -PT00013', NULL, 95, 'tạo mới', 880000.0000, NULL, NULL, '2022-07-23 23:34:28', '2022-07-23 23:34:28'),
(189, 'PT00014', 'Phiếu thu Tiền -PT00014', NULL, 95, 'tạo mới', 880000.0000, NULL, NULL, '2022-07-23 23:36:39', '2022-07-23 23:36:39'),
(190, 'PT00015', 'Phiếu thu Tiền -PT00015', NULL, 95, 'tạo mới', 880000.0000, NULL, NULL, '2022-07-23 23:37:23', '2022-07-23 23:37:23'),
(191, 'PT00016', 'Phiếu thu Tiền -PT00016', NULL, 95, 'tạo mới', 880000.0000, NULL, NULL, '2022-07-23 23:37:55', '2022-07-23 23:37:55'),
(192, 'PT00017', 'Phiếu thu Tiền -PT00017', NULL, 95, 'tạo mới', 880000.0000, NULL, NULL, '2022-07-23 23:38:32', '2022-07-23 23:38:32'),
(193, 'PT00013', 'Phiếu thu Tiền -PT00013', NULL, 95, 'tạo mới', 880000.0000, NULL, NULL, '2022-07-23 23:43:02', '2022-07-23 23:43:02'),
(194, 'PT00012', 'Phiếu thu Tiền -PT00012', NULL, 95, 'tạo mới', 1000000.0000, NULL, NULL, '2022-07-23 23:51:40', '2022-07-23 23:51:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_images_id` int(11) DEFAULT NULL COMMENT 'sản phẩm',
  `receipts_images_id` int(11) DEFAULT NULL COMMENT 'Phiếu thu',
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'hình ảnh',
  `images_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `images_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `product_images_id`, `receipts_images_id`, `images`, `images_user_created`, `images_user_updated`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, '131378592.jpg', NULL, NULL, '2022-02-27 15:52:37', '2022-02-27 15:52:37');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_03_153147_create_province_table', 1),
(5, '2021_09_03_185919_create_country_table', 1),
(6, '2021_09_03_185940_create_staff_table', 1),
(7, '2021_09_03_190018_create_brand_table', 1),
(8, '2021_09_03_190039_create_position_table', 1),
(9, '2021_09_03_190101_create_product_table', 1),
(10, '2021_09_03_190124_create_unit_table', 1),
(11, '2021_09_03_190156_create_origin_table', 1),
(12, '2021_09_03_190221_create_supplier_table', 1),
(13, '2021_09_03_190354_create_customers_table', 1),
(14, '2021_09_03_190413_create_stock_table', 1),
(15, '2021_09_03_190440_create_bank_table', 1),
(16, '2021_09_03_190517_create_rank_table', 1),
(17, '2021_09_03_190539_create_commission_rank_table', 1),
(18, '2021_09_03_190558_create_history_table', 1),
(19, '2021_09_03_190626_create_images_table', 1),
(20, '2021_09_03_190644_create_commission_table', 1),
(21, '2021_09_03_190708_create_receipts_table', 1),
(22, '2021_09_03_190812_create_period_table', 1),
(23, '2021_09_03_190916_create_staff_kpi_order_table', 1),
(24, '2021_09_03_191436_create_currency_table', 1),
(25, '2021_09_03_201005_create_tax_table', 1),
(26, '2021_09_10_172317_create_warranty_form_table', 1),
(27, '2021_09_10_172558_create_customer_care_table', 1),
(28, '2021_09_10_172628_create_payments_table', 1),
(29, '2021_09_10_193354_create_ware_house_table', 1),
(30, '2021_10_23_183548_create_decentralization_table', 2),
(31, '2021_10_30_095946_create_product_type_table', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `origin`
--

CREATE TABLE `origin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `origin_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã xuất xứ',
  `origin_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên xuất xứ',
  `origin_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `origin_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `origin`
--

INSERT INTO `origin` (`id`, `origin_code`, `origin_name`, `origin_user_created`, `origin_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'USa', 'Mỹ', NULL, NULL, '2021-10-30 03:40:45', '2021-10-30 03:40:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payments_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã hình thức thanh toán',
  `payments_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên hình thức thanh toán',
  `payments_number` text COLLATE utf8mb4_unicode_ci COMMENT 'con số kì trả cụ thể',
  `payments_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `payments_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `payments_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `payments`
--

INSERT INTO `payments` (`id`, `payments_code`, `payments_name`, `payments_number`, `payments_brand_id`, `payments_user_created`, `payments_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'full', 'Toàn Bộ', '0', NULL, NULL, NULL, '2021-11-27 21:58:21', '2021-11-27 21:58:21'),
(2, '4', 'Trả góp 4M', '4', NULL, NULL, NULL, '2021-11-27 21:59:01', '2021-11-27 21:59:01'),
(3, '6', 'Trả góp 6M', '6', NULL, NULL, NULL, '2021-12-05 01:21:21', '2021-12-05 01:21:21'),
(4, '9', 'Trả góp 9M', '9', NULL, NULL, NULL, '2021-12-05 01:21:40', '2021-12-05 01:21:40'),
(5, '12', 'Trả góp 12M', '12', NULL, NULL, NULL, '2021-12-05 01:22:03', '2021-12-05 01:22:03'),
(6, '6Mcredit', '6M credit', '6', NULL, NULL, NULL, '2021-12-05 01:22:15', '2021-12-05 01:22:15'),
(7, '9Mcredit', '9M credit', '9', NULL, NULL, NULL, '2021-12-05 01:22:59', '2021-12-05 01:22:59'),
(8, '12Mcredit', '12M credit', '12', NULL, NULL, NULL, '2021-12-05 01:23:25', '2021-12-05 01:23:25'),
(9, 'Deposit', 'Deposit', '0', NULL, NULL, NULL, '2021-12-05 01:23:41', '2021-12-05 01:23:41'),
(10, 'Smell', 'Smell', '0', NULL, NULL, NULL, '2021-12-05 01:24:05', '2021-12-05 01:24:05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `period`
--

CREATE TABLE `period` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `period_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã đợt thanh toán',
  `period_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Đợt thanh toán',
  `period_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `period_payments_id` int(11) DEFAULT NULL COMMENT 'hình thức thanh toán',
  `period_receipts_id` int(11) NOT NULL COMMENT 'phiếu thu',
  `period_customers_id` int(11) NOT NULL COMMENT 'khách hàng',
  `period_staff_id` int(11) NOT NULL COMMENT 'nhân viên thực hiện',
  `period_stock_id` int(11) DEFAULT NULL COMMENT 'Đơn hàng',
  `period_amount` decimal(10,0) DEFAULT NULL COMMENT 'số tiền thanh toán',
  `period_amount_batch` decimal(11,0) NOT NULL COMMENT 'số tiền trả từng đợt',
  `period_receipts_actual_amount` decimal(10,0) DEFAULT NULL COMMENT 'số tiền thực tế khi thanh toán',
  `period_date` date DEFAULT NULL COMMENT 'ngày thanh toán theo hợp đồng',
  `period_date_reality` date DEFAULT NULL COMMENT 'ngày khách hàng thanh toán',
  `period_status` tinyint(1) DEFAULT NULL COMMENT 'Trạng thái',
  `period_kpi_status` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'trạng thái thanh toán kpi cho nv sales',
  `period_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Nội dung',
  `period_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `period_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `period`
--

INSERT INTO `period` (`id`, `period_code`, `period_name`, `period_brand_id`, `period_payments_id`, `period_receipts_id`, `period_customers_id`, `period_staff_id`, `period_stock_id`, `period_amount`, `period_amount_batch`, `period_receipts_actual_amount`, `period_date`, `period_date_reality`, `period_status`, `period_kpi_status`, `period_description`, `period_user_created`, `period_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'PaymentNotFull', 'Payment 1', 3, 2, 1, 16, 95, 2, 900000, 100000, 1134400168, '2022-02-26', '2022-02-26', 1, '1', NULL, 'sales02', NULL, '2022-02-26 14:43:05', '2022-07-14 18:42:48'),
(2, 'PaymentNotFull', 'Payment 2', 3, 2, 0, 16, 95, 2, 900000, 266667, NULL, '2022-03-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:43:05', '2022-07-14 18:42:48'),
(3, 'PaymentNotFull', 'Payment 3', 3, 2, 0, 16, 95, 2, 900000, 266667, NULL, '2022-04-26', NULL, 1, '1', NULL, 'sales02', NULL, '2022-02-26 14:43:05', '2022-07-14 18:45:59'),
(4, 'PaymentNotFull', 'Payment 4', 3, 2, 0, 16, 95, 2, 900000, 266667, NULL, '2022-05-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:43:05', '2022-07-14 18:45:59'),
(5, 'PaymentNotFull', 'Payment 1', 3, 3, 0, 17, 95, 3, 2700000, 100000, NULL, '2022-02-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:47:39', '2022-07-14 18:42:48'),
(6, 'PaymentNotFull', 'Payment 2', 3, 3, 0, 17, 95, 3, 2700000, 520000, NULL, '2022-03-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:47:39', '2022-07-14 18:45:59'),
(7, 'PaymentNotFull', 'Payment 3', 3, 3, 0, 17, 95, 3, 2700000, 520000, NULL, '2022-04-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:47:39', '2022-07-14 18:45:59'),
(8, 'PaymentNotFull', 'Payment 4', 3, 3, 0, 17, 95, 3, 2700000, 520000, NULL, '2022-05-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:47:39', '2022-07-14 18:45:59'),
(9, 'PaymentNotFull', 'Payment 5', 3, 3, 0, 17, 95, 3, 2700000, 520000, NULL, '2022-06-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:47:39', '2022-07-19 00:34:37'),
(10, 'PaymentNotFull', 'Payment 6', 3, 3, 0, 17, 95, 3, 2700000, 520000, NULL, '2022-07-26', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-26 14:47:39', '2022-07-19 00:34:37'),
(11, 'PaymentFull', '', 3, 1, 7, 18, 95, 4, 1800000, 1800000, 1800000, '2022-02-26', '2022-03-06', 0, '1', NULL, 'sales02', NULL, '2022-02-27 01:56:59', '2022-07-14 18:42:48'),
(12, 'PaymentFull', 'Payments Full', 3, 1, 5, 19, 95, 5, 1800000, 1800000, 1800000, '2022-02-26', '2022-02-27', 0, '1', NULL, 'sales02', NULL, '2022-02-27 02:00:11', '2022-07-14 18:45:59'),
(13, 'PaymentFull', 'Payments Full', 3, 1, 4, 20, 95, 6, 1800000, 1800000, 1800000, '2022-02-26', '2022-02-27', 0, '1', NULL, 'sales02', NULL, '2022-02-27 02:17:21', '2022-07-14 18:45:59'),
(15, 'PaymentNotFull', 'Payment 1', 3, 2, 6, 21, 95, 10, 86800000, 10000000, 11290240, '2022-02-27', '2022-02-27', 0, '1', NULL, 'sales02', NULL, '2022-02-28 00:09:06', '2022-07-19 00:34:37'),
(16, 'PaymentNotFull', 'Payment 2', 3, 2, 0, 21, 95, 10, 86800000, 25600000, NULL, '2022-03-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-28 00:09:06', '2022-07-19 00:34:37'),
(17, 'PaymentNotFull', 'Payment 3', 3, 2, 0, 21, 95, 10, 86800000, 25600000, NULL, '2022-04-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-28 00:09:06', '2022-07-19 00:34:37'),
(18, 'PaymentNotFull', 'Payment 4', 3, 2, 0, 21, 95, 10, 86800000, 25600000, NULL, '2022-05-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-02-28 00:09:06', '2022-07-19 00:34:37'),
(19, 'PaymentFull', 'Payments Full', 3, 1, 0, 22, 95, 17, 1800000, 1800000, NULL, '2022-04-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-04-27 15:17:22', '2022-07-19 00:34:37'),
(20, 'PaymentNotFull', 'Payment 1', 3, 2, 0, 31, 95, 18, 900000, 12000000, NULL, '2022-04-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-04-27 15:19:45', '2022-07-19 00:34:37'),
(21, 'PaymentNotFull', 'Payment 2', 3, 2, 0, 31, 95, 18, 900000, -3700000, NULL, '2022-05-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-04-27 15:19:45', '2022-07-19 00:34:37'),
(22, 'PaymentNotFull', 'Payment 3', 3, 2, 0, 31, 95, 18, 900000, -3700000, NULL, '2022-06-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-04-27 15:19:45', '2022-07-19 00:34:37'),
(23, 'PaymentNotFull', 'Payment 4', 3, 2, 0, 31, 95, 18, 900000, -3700000, NULL, '2022-07-27', NULL, 0, '1', NULL, 'sales02', NULL, '2022-04-27 15:19:45', '2022-07-19 00:34:37'),
(24, 'PaymentFull', 'Payments Full', 3, 1, 0, 37, 109, 22, 17000000, 17000000, NULL, '2022-04-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:38:07', '2022-07-19 00:37:29'),
(25, 'PaymentNotFull', 'Payment 1', 3, 5, 0, 38, 109, 25, 17000000, 5000000, 0, '2022-04-27', '2022-04-27', 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(26, 'PaymentNotFull', 'Payment 2', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-05-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(27, 'PaymentNotFull', 'Payment 3', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-06-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(28, 'PaymentNotFull', 'Payment 4', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-07-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(29, 'PaymentNotFull', 'Payment 5', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-08-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(30, 'PaymentNotFull', 'Payment 6', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-09-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(31, 'PaymentNotFull', 'Payment 7', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-10-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(32, 'PaymentNotFull', 'Payment 8', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-11-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(33, 'PaymentNotFull', 'Payment 9', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2022-12-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(34, 'PaymentNotFull', 'Payment 10', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2023-01-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(35, 'PaymentNotFull', 'Payment 11', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2023-02-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(36, 'PaymentNotFull', 'Payment 12', 3, 5, 0, 38, 109, 25, 17000000, 1090909, NULL, '2023-03-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-07-19 00:37:29'),
(37, 'PaymentFull', 'Payments Full', 3, 1, 0, 39, 109, 27, 34000000, 34000000, 34000000, '2022-04-27', '2022-04-27', 0, '1', NULL, NULL, NULL, '2022-04-27 23:42:05', '2022-07-19 00:37:29'),
(38, 'PaymentNotFull', 'Payment 1', 3, 8, 0, 41, 109, 29, 188000000, 5000000, NULL, '2022-04-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(39, 'PaymentNotFull', 'Payment 2', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-05-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(40, 'PaymentNotFull', 'Payment 3', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-06-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(41, 'PaymentNotFull', 'Payment 4', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-07-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(42, 'PaymentNotFull', 'Payment 5', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-08-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(43, 'PaymentNotFull', 'Payment 6', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-09-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(44, 'PaymentNotFull', 'Payment 7', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-10-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(45, 'PaymentNotFull', 'Payment 8', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-11-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(46, 'PaymentNotFull', 'Payment 9', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2022-12-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(47, 'PaymentNotFull', 'Payment 10', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2023-01-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(48, 'PaymentNotFull', 'Payment 11', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2023-02-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(49, 'PaymentNotFull', 'Payment 12', 3, 8, 0, 41, 109, 29, 188000000, 16636364, NULL, '2023-03-27', NULL, 0, '1', NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-07-19 00:37:29'),
(50, 'PaymentFull', 'Payments Full', 3, 1, 0, 26, 106, 40, 79800000, 79800000, NULL, '2022-04-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:04:36', '2022-07-16 04:57:42'),
(51, 'PaymentNotFull', 'Payment 1', 3, 8, 0, 33, 103, 41, 900000, 4800000, NULL, '2022-04-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(52, 'PaymentNotFull', 'Payment 2', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-05-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(53, 'PaymentNotFull', 'Payment 3', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-06-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(54, 'PaymentNotFull', 'Payment 4', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-07-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(55, 'PaymentNotFull', 'Payment 5', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-08-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(56, 'PaymentNotFull', 'Payment 6', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-09-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(57, 'PaymentNotFull', 'Payment 7', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-10-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(58, 'PaymentNotFull', 'Payment 8', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-11-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(59, 'PaymentNotFull', 'Payment 9', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2022-12-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(60, 'PaymentNotFull', 'Payment 10', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2023-01-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(61, 'PaymentNotFull', 'Payment 11', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2023-02-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(62, 'PaymentNotFull', 'Payment 12', 3, 8, 0, 33, 103, 41, 900000, -354545, NULL, '2023-03-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-07-19 00:30:22'),
(63, 'PaymentNotFull', 'Payment 1', 3, 2, 0, 23, 107, 43, 2700000, 5000000, NULL, '2022-04-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:07:50', '2022-07-16 04:58:48'),
(64, 'PaymentNotFull', 'Payment 2', 3, 2, 0, 23, 107, 43, 2700000, -766667, NULL, '2022-05-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:07:50', '2022-07-16 04:58:48'),
(65, 'PaymentNotFull', 'Payment 3', 3, 2, 0, 23, 107, 43, 2700000, -766667, NULL, '2022-06-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:07:50', '2022-07-16 04:59:03'),
(66, 'PaymentNotFull', 'Payment 4', 3, 2, 0, 23, 107, 43, 2700000, -766667, NULL, '2022-07-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:07:50', '2022-07-16 04:59:03'),
(67, 'PaymentNotFull', 'Payment 1', 3, 2, 0, 41, 109, 44, 17000000, 500000, NULL, '2022-04-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:08:06', '2022-07-19 00:37:29'),
(68, 'PaymentNotFull', 'Payment 2', 3, 2, 0, 41, 109, 44, 17000000, 5500000, NULL, '2022-05-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:08:06', '2022-07-19 00:37:29'),
(69, 'PaymentNotFull', 'Payment 3', 3, 2, 0, 41, 109, 44, 17000000, 5500000, NULL, '2022-06-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:08:06', '2022-07-19 00:37:29'),
(70, 'PaymentNotFull', 'Payment 4', 3, 2, 0, 41, 109, 44, 17000000, 5500000, NULL, '2022-07-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:08:06', '2022-07-19 00:37:29'),
(71, 'PaymentFull', 'Payments Full', 3, 1, 0, 46, 107, 47, 900000, 900000, NULL, '2022-04-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:28', '2022-07-16 04:58:48'),
(72, 'PaymentNotFull', 'Payment 1', 3, 5, 0, 42, 106, 48, 78900000, 30000000, NULL, '2022-04-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-18 23:41:46'),
(73, 'PaymentNotFull', 'Payment 2', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-05-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-18 23:41:46'),
(74, 'PaymentNotFull', 'Payment 3', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-06-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-18 23:41:46'),
(75, 'PaymentNotFull', 'Payment 4', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-07-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-18 23:41:46'),
(76, 'PaymentNotFull', 'Payment 5', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-08-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-18 23:41:46'),
(77, 'PaymentNotFull', 'Payment 6', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-09-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-18 23:41:46'),
(78, 'PaymentNotFull', 'Payment 7', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-10-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-19 00:29:21'),
(79, 'PaymentNotFull', 'Payment 8', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-11-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-19 00:29:21'),
(80, 'PaymentNotFull', 'Payment 9', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2022-12-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-19 00:29:21'),
(81, 'PaymentNotFull', 'Payment 10', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2023-01-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-19 00:29:21'),
(82, 'PaymentNotFull', 'Payment 11', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2023-02-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-19 00:29:21'),
(83, 'PaymentNotFull', 'Payment 12', 3, 5, 0, 42, 106, 48, 78900000, 4445455, NULL, '2023-03-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-07-19 00:29:21'),
(84, 'PaymentNotFull', 'Payment 1', 3, 4, 0, 47, 109, 56, 17000000, 7000000, 0, '2022-04-28', '2022-04-28', 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(85, 'PaymentNotFull', 'Payment 2', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-05-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(86, 'PaymentNotFull', 'Payment 3', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-06-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(87, 'PaymentNotFull', 'Payment 4', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-07-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(88, 'PaymentNotFull', 'Payment 5', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-08-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(89, 'PaymentNotFull', 'Payment 6', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-09-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(90, 'PaymentNotFull', 'Payment 7', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-10-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(91, 'PaymentNotFull', 'Payment 8', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-11-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(92, 'PaymentNotFull', 'Payment 9', 3, 4, 0, 47, 109, 56, 17000000, 1250000, NULL, '2022-12-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-07-19 00:37:29'),
(93, 'PaymentNotFull', 'Payment 1', 3, 8, 0, 49, 109, 57, 85000000, 30000000, NULL, '2022-04-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(94, 'PaymentNotFull', 'Payment 2', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-05-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(95, 'PaymentNotFull', 'Payment 3', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-06-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(96, 'PaymentNotFull', 'Payment 4', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-07-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(97, 'PaymentNotFull', 'Payment 5', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-08-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(98, 'PaymentNotFull', 'Payment 6', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-09-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(99, 'PaymentNotFull', 'Payment 7', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-10-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(100, 'PaymentNotFull', 'Payment 8', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-11-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(101, 'PaymentNotFull', 'Payment 9', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2022-12-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(102, 'PaymentNotFull', 'Payment 10', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2023-01-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(103, 'PaymentNotFull', 'Payment 11', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2023-02-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(104, 'PaymentNotFull', 'Payment 12', 3, 8, 0, 49, 109, 57, 85000000, 5000000, NULL, '2023-03-28', NULL, 0, '1', NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-07-19 00:37:29'),
(105, 'PaymentNotFull', 'Payment 1', 3, 6, 0, 43, 109, 60, 17000000, 2000000, NULL, '2022-04-30', NULL, 0, '1', NULL, NULL, NULL, '2022-04-30 14:20:25', '2022-07-19 00:37:29'),
(106, 'PaymentNotFull', 'Payment 2', 3, 6, 0, 43, 109, 60, 17000000, 3000000, NULL, '2022-05-30', NULL, 0, '1', NULL, NULL, NULL, '2022-04-30 14:20:25', '2022-07-19 00:37:29'),
(107, 'PaymentNotFull', 'Payment 3', 3, 6, 0, 43, 109, 60, 17000000, 3000000, NULL, '2022-06-30', NULL, 0, '1', NULL, NULL, NULL, '2022-04-30 14:20:25', '2022-07-19 00:37:29'),
(108, 'PaymentNotFull', 'Payment 4', 3, 6, 0, 43, 109, 60, 17000000, 3000000, NULL, '2022-07-30', NULL, 0, '1', NULL, NULL, NULL, '2022-04-30 14:20:25', '2022-07-19 00:37:29'),
(109, 'PaymentNotFull', 'Payment 5', 3, 6, 0, 43, 109, 60, 17000000, 3000000, NULL, '2022-08-30', NULL, 0, '1', NULL, NULL, NULL, '2022-04-30 14:20:25', '2022-07-19 00:37:29'),
(110, 'PaymentNotFull', 'Payment 6', 3, 6, 0, 43, 109, 60, 17000000, 3000000, NULL, '2022-09-30', NULL, 0, '1', NULL, NULL, NULL, '2022-04-30 14:20:25', '2022-07-19 00:37:29'),
(111, 'PaymentFull', 'Payments Full', 3, 1, 0, 80, 118, 64, 900000, 900000, NULL, '2022-05-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:39:28', '2022-07-19 00:33:52'),
(112, 'PaymentFull', 'Payments Full', 3, 1, 0, 79, 109, 65, 17000000, 17000000, NULL, '2022-05-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:39:54', '2022-07-19 00:37:29'),
(113, 'PaymentFull', 'Payments Full', 3, 1, 0, 81, 116, 66, 900000, 900000, NULL, '2022-05-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:40:38', '2022-07-19 00:31:42'),
(114, 'PaymentFull', 'Payments Full', 3, 1, 0, 82, 120, 67, 900000, 900000, NULL, '2022-05-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:43:06', '2022-07-19 00:33:31'),
(115, 'PaymentNotFull', 'Payment 1', 3, 5, 0, 86, 118, 68, 85000000, 30000000, NULL, '2022-05-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(116, 'PaymentNotFull', 'Payment 2', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2022-06-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(117, 'PaymentNotFull', 'Payment 3', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2022-07-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(118, 'PaymentNotFull', 'Payment 4', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2022-08-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(119, 'PaymentNotFull', 'Payment 5', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2022-09-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(120, 'PaymentNotFull', 'Payment 6', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2022-10-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(121, 'PaymentNotFull', 'Payment 7', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2022-11-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(122, 'PaymentNotFull', 'Payment 8', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2022-12-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(123, 'PaymentNotFull', 'Payment 9', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2023-01-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(124, 'PaymentNotFull', 'Payment 10', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2023-02-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(125, 'PaymentNotFull', 'Payment 11', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2023-03-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(126, 'PaymentNotFull', 'Payment 12', 3, 5, 0, 86, 118, 68, 85000000, 5000000, NULL, '2023-04-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-07-19 00:33:52'),
(127, 'PaymentNotFull', 'Payment 1', 3, 5, 0, 84, 109, 69, 85000000, 30000000, NULL, '2022-05-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(128, 'PaymentNotFull', 'Payment 2', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2022-06-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(129, 'PaymentNotFull', 'Payment 3', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2022-07-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(130, 'PaymentNotFull', 'Payment 4', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2022-08-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(131, 'PaymentNotFull', 'Payment 5', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2022-09-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(132, 'PaymentNotFull', 'Payment 6', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2022-10-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(133, 'PaymentNotFull', 'Payment 7', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2022-11-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(134, 'PaymentNotFull', 'Payment 8', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2022-12-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(135, 'PaymentNotFull', 'Payment 9', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2023-01-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(136, 'PaymentNotFull', 'Payment 10', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2023-02-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(137, 'PaymentNotFull', 'Payment 11', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2023-03-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(138, 'PaymentNotFull', 'Payment 12', 3, 5, 0, 84, 109, 69, 85000000, 5000000, NULL, '2023-04-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-07-19 00:37:29'),
(139, 'PaymentNotFull', 'Payment 1', 3, 5, 0, 87, 116, 71, 68900000, 30000000, NULL, '2022-05-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(140, 'PaymentNotFull', 'Payment 2', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2022-06-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(141, 'PaymentNotFull', 'Payment 3', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2022-07-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(142, 'PaymentNotFull', 'Payment 4', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2022-08-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(143, 'PaymentNotFull', 'Payment 5', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2022-09-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(144, 'PaymentNotFull', 'Payment 6', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2022-10-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(145, 'PaymentNotFull', 'Payment 7', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2022-11-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(146, 'PaymentNotFull', 'Payment 8', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2022-12-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(147, 'PaymentNotFull', 'Payment 9', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2023-01-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(148, 'PaymentNotFull', 'Payment 10', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2023-02-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(149, 'PaymentNotFull', 'Payment 11', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2023-03-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(150, 'PaymentNotFull', 'Payment 12', 3, 5, 0, 87, 116, 71, 68900000, 3536364, NULL, '2023-04-11', NULL, 0, '1', NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-07-19 00:31:42'),
(151, 'PaymentFull', 'Payments Full', 3, 1, 0, 89, 104, 75, 85000000, 85000000, NULL, '2022-05-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:57:27', '2022-07-23 02:21:16'),
(152, 'PaymentNotFull', 'Payment 1', 3, 5, 0, 90, 104, 76, 85000000, 30000000, NULL, '2022-05-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(153, 'PaymentNotFull', 'Payment 2', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2022-06-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(154, 'PaymentNotFull', 'Payment 3', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2022-07-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(155, 'PaymentNotFull', 'Payment 4', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2022-08-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(156, 'PaymentNotFull', 'Payment 5', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2022-09-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(157, 'PaymentNotFull', 'Payment 6', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2022-10-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(158, 'PaymentNotFull', 'Payment 7', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2022-11-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(159, 'PaymentNotFull', 'Payment 8', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2022-12-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(160, 'PaymentNotFull', 'Payment 9', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2023-01-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(161, 'PaymentNotFull', 'Payment 10', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2023-02-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(162, 'PaymentNotFull', 'Payment 11', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2023-03-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(163, 'PaymentNotFull', 'Payment 12', 3, 5, 0, 90, 104, 76, 85000000, 5000000, NULL, '2023-04-12', NULL, 0, '1', NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-07-23 02:21:16'),
(164, 'PaymentFull', 'Payments Full', 3, 1, 12, 18, 95, 78, 1800000, 1800000, 1800000, '2022-06-08', '2022-07-10', 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:33:01', '2022-07-23 16:58:54'),
(165, 'PaymentNotFull', 'Payment 1', 3, 2, 0, 18, 95, 79, 51000000, 10000000, NULL, '2022-06-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:33:41', '2022-07-14 18:42:48'),
(166, 'PaymentNotFull', 'Payment 2', 3, 2, 0, 18, 95, 79, 51000000, 13666667, NULL, '2022-07-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:33:41', '2022-07-14 18:42:48'),
(167, 'PaymentNotFull', 'Payment 3', 3, 2, 0, 18, 95, 79, 51000000, 13666667, NULL, '2022-08-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:33:41', '2022-07-14 18:42:48'),
(168, 'PaymentNotFull', 'Payment 4', 3, 2, 0, 18, 95, 79, 51000000, 13666667, NULL, '2022-09-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:33:41', '2022-07-14 18:45:59'),
(169, 'PaymentNotFull', 'Payment 1', 3, 3, 0, 21, 95, 80, 34000000, 10000000, NULL, '2022-06-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:34:25', '2022-07-19 00:34:37'),
(170, 'PaymentNotFull', 'Payment 2', 3, 3, 0, 21, 95, 80, 34000000, 4800000, NULL, '2022-07-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:34:25', '2022-07-19 00:34:37'),
(171, 'PaymentNotFull', 'Payment 3', 3, 3, 0, 21, 95, 80, 34000000, 4800000, NULL, '2022-08-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:34:25', '2022-07-19 00:34:37'),
(172, 'PaymentNotFull', 'Payment 4', 3, 3, 0, 21, 95, 80, 34000000, 4800000, NULL, '2022-09-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:34:25', '2022-07-19 00:34:37'),
(173, 'PaymentNotFull', 'Payment 5', 3, 3, 0, 21, 95, 80, 34000000, 4800000, NULL, '2022-10-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:34:25', '2022-07-19 00:34:37'),
(174, 'PaymentNotFull', 'Payment 6', 3, 3, 0, 21, 95, 80, 34000000, 4800000, NULL, '2022-11-08', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-08 23:34:25', '2022-07-19 00:34:37'),
(175, 'PaymentNotFull', 'Payment 1', 3, 8, 0, 141, 103, 81, 79800000, 79800000, 0, '2022-06-16', '2022-06-16', 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(176, 'PaymentNotFull', 'Payment 2', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2022-07-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(177, 'PaymentNotFull', 'Payment 3', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2022-08-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(178, 'PaymentNotFull', 'Payment 4', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2022-09-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(179, 'PaymentNotFull', 'Payment 5', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2022-10-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(180, 'PaymentNotFull', 'Payment 6', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2022-11-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(181, 'PaymentNotFull', 'Payment 7', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2022-12-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(182, 'PaymentNotFull', 'Payment 8', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2023-01-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(183, 'PaymentNotFull', 'Payment 9', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2023-02-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(184, 'PaymentNotFull', 'Payment 10', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2023-03-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(185, 'PaymentNotFull', 'Payment 11', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2023-04-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(186, 'PaymentNotFull', 'Payment 12', 3, 8, 0, 141, 103, 81, 79800000, 0, NULL, '2023-05-16', NULL, 0, '1', NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-07-19 00:30:22'),
(187, 'PaymentNotFull', 'Payment 1', 3, 8, 0, 22, 95, 85, 90000000, 10000000, NULL, '2022-06-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-14 18:05:11'),
(188, 'PaymentNotFull', 'Payment 2', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2022-07-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(189, 'PaymentNotFull', 'Payment 3', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2022-08-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(190, 'PaymentNotFull', 'Payment 4', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2022-09-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(191, 'PaymentNotFull', 'Payment 5', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2022-10-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(192, 'PaymentNotFull', 'Payment 6', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2022-11-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(193, 'PaymentNotFull', 'Payment 7', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2022-12-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(194, 'PaymentNotFull', 'Payment 8', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2023-01-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(195, 'PaymentNotFull', 'Payment 9', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2023-02-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(196, 'PaymentNotFull', 'Payment 10', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2023-03-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(197, 'PaymentNotFull', 'Payment 11', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2023-04-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(198, 'PaymentNotFull', 'Payment 12', 3, 8, 0, 22, 95, 85, 90000000, 7272727, NULL, '2023-05-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-19 00:34:37'),
(199, 'PaymentNotFull', 'Payment 1', 3, 8, 0, 20, 95, 86, 900000, 400000, NULL, '2022-06-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-14 18:45:59'),
(200, 'PaymentNotFull', 'Payment 2', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2022-07-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(201, 'PaymentNotFull', 'Payment 3', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2022-08-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(202, 'PaymentNotFull', 'Payment 4', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2022-09-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(203, 'PaymentNotFull', 'Payment 5', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2022-10-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(204, 'PaymentNotFull', 'Payment 6', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2022-11-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(205, 'PaymentNotFull', 'Payment 7', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2022-12-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(206, 'PaymentNotFull', 'Payment 8', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2023-01-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(207, 'PaymentNotFull', 'Payment 9', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2023-02-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(208, 'PaymentNotFull', 'Payment 10', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2023-03-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(209, 'PaymentNotFull', 'Payment 11', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2023-04-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(210, 'PaymentNotFull', 'Payment 12', 3, 8, 0, 20, 95, 86, 900000, 45455, NULL, '2023-05-19', NULL, 0, '1', NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-19 00:34:37'),
(211, 'PaymentNotFull', 'Payment 1', 3, 2, 0, 225, 123, 90, 85000000, 30000000, NULL, '2022-06-23', NULL, 0, '1', NULL, NULL, NULL, '2022-06-23 13:30:37', '2022-07-19 00:33:18'),
(212, 'PaymentNotFull', 'Payment 2', 3, 2, 0, 225, 123, 90, 85000000, 18333333, NULL, '2022-07-23', NULL, 0, '1', NULL, NULL, NULL, '2022-06-23 13:30:37', '2022-07-19 00:33:18'),
(213, 'PaymentNotFull', 'Payment 3', 3, 2, 0, 225, 123, 90, 85000000, 18333333, NULL, '2022-08-23', NULL, 0, '1', NULL, NULL, NULL, '2022-06-23 13:30:37', '2022-07-19 00:33:18'),
(214, 'PaymentNotFull', 'Payment 4', 3, 2, 0, 225, 123, 90, 85000000, 18333333, NULL, '2022-09-23', NULL, 0, '1', NULL, NULL, NULL, '2022-06-23 13:30:37', '2022-07-19 00:33:18'),
(215, 'PaymentNotFull', 'Payment 1', 3, 3, 29, 421, 95, 98, 5400000, 1000000, 1000000, '2022-07-23', '2022-07-23', 2, NULL, NULL, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 23:51:40'),
(216, 'PaymentNotFull', 'Payment 2', 3, 3, 28, 421, 95, 98, 5400000, 880000, NULL, '2022-08-23', NULL, 1, NULL, NULL, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 23:43:02'),
(217, 'PaymentNotFull', 'Payment 3', 3, 3, 24, 421, 95, 98, 5400000, 880000, NULL, '2022-09-23', NULL, 1, NULL, NULL, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 23:36:39'),
(218, 'PaymentNotFull', 'Payment 4', 3, 3, 25, 421, 95, 98, 5400000, 880000, NULL, '2022-10-23', NULL, 1, NULL, NULL, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 23:37:23'),
(219, 'PaymentNotFull', 'Payment 5', 3, 3, 26, 421, 95, 98, 5400000, 880000, NULL, '2022-11-23', NULL, 1, NULL, NULL, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 23:37:55'),
(220, 'PaymentNotFull', 'Payment 6', 3, 3, 27, 421, 95, 98, 5400000, 880000, NULL, '2022-12-23', NULL, 1, NULL, NULL, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 23:38:32'),
(221, 'PaymentFull', 'Payments Full', 3, 1, 0, 421, 95, 99, 1800000, 1800000, NULL, '2022-07-23', NULL, 1, NULL, NULL, NULL, NULL, '2022-07-23 16:40:04', '2022-07-23 16:40:04');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `position`
--

CREATE TABLE `position` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `position_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã chức vụ',
  `position_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên chức vụ',
  `position_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `position_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `position`
--

INSERT INTO `position` (`id`, `position_code`, `position_name`, `position_user_created`, `position_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'stocker', 'Stocker', NULL, NULL, '2021-10-23 11:32:03', '2021-10-23 11:32:03'),
(2, 'sales', 'Sales', NULL, NULL, '2021-10-23 11:32:33', '2021-10-23 11:32:33'),
(3, 'headbranch', 'Head branch', NULL, NULL, '2021-11-26 20:52:15', '2021-11-26 20:52:15'),
(4, 'administrators', 'Administrators', NULL, NULL, '2021-11-26 23:57:22', '2021-11-26 23:57:22'),
(5, 'Manager', 'Manager', NULL, NULL, '2022-02-25 12:16:11', '2022-02-25 12:16:11');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã sản phẩm',
  `product_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên sản phẩm',
  `product_content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'nội dung ngắn',
  `product_description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'nội dung mô tả',
  `product_price_import` decimal(11,0) DEFAULT NULL,
  `product_price_ex` decimal(11,0) DEFAULT NULL,
  `product_origin_id` int(11) DEFAULT NULL COMMENT 'xuất xứ',
  `product_size` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'kích thước',
  `product_material` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'chất liệu',
  `product_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `product_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `product_code`, `product_name`, `product_content`, `product_description`, `product_price_import`, `product_price_ex`, `product_origin_id`, `product_size`, `product_material`, `product_user_created`, `product_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'SPA', 'Sản phẩm a', NULL, 'ffffff', 900000, 900000, 1, '38', NULL, NULL, NULL, '2022-02-26 14:13:56', '2022-02-26 14:13:56'),
(2, 'sP001', 'Ipad', 'ipda product Origin.', NULL, 15000000, 17000000, 1, '32', NULL, NULL, NULL, '2022-02-27 15:52:37', '2022-02-27 15:52:37');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_type`
--

CREATE TABLE `product_type` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_type_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã loại nhập kho',
  `product_type_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên loại nhập kho',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_type`
--

INSERT INTO `product_type` (`id`, `product_type_code`, `product_type_name`, `created_at`, `updated_at`) VALUES
(1, 'quatang', 'Quà Tặng', '2021-10-30 03:21:29', '2021-10-30 03:21:29'),
(2, 'nhapkhobanhang', 'nhập kho bán hàng', '2022-02-27 15:54:39', '2022-02-27 15:54:39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `province`
--

CREATE TABLE `province` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `prov_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_prefix` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_region` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_position` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_meta` text COLLATE utf8mb4_unicode_ci,
  `prov_map_sharp` text COLLATE utf8mb4_unicode_ci,
  `prov_status` tinyint(1) DEFAULT NULL,
  `prov_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prov_created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `prov_updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `rank`
--

CREATE TABLE `rank` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rank_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã cấp độ',
  `rank_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên cấp độ',
  `rank_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `rank_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `rank`
--

INSERT INTO `rank` (`id`, `rank_code`, `rank_name`, `rank_user_created`, `rank_user_updated`, `created_at`, `updated_at`) VALUES
(2, 'SalesManage', 'Sales Manage', NULL, NULL, '2021-10-23 11:13:24', '2021-10-23 11:13:24'),
(3, 'SalesSubManage', 'Sales Sub Manage', NULL, NULL, '2021-10-23 11:19:25', '2021-10-23 11:19:25');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `receipts`
--

CREATE TABLE `receipts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `receipts_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'mã phiếu',
  `receipts_type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Loại phiếu',
  `receipts_stock_id` int(11) DEFAULT NULL COMMENT 'đơn hàng',
  `receipts_customer_id` int(11) DEFAULT NULL COMMENT 'khách hàng',
  `receipts_bank_id` int(11) DEFAULT NULL COMMENT 'ngân hàng',
  `receipts_staff_id` int(11) DEFAULT NULL COMMENT 'nhân viên lập phiếu',
  `receipts_staff_approve_id` int(11) DEFAULT NULL COMMENT 'nhân viên duyệt phiếu',
  `receipts_staff_approve_date` datetime DEFAULT NULL COMMENT 'ngày giờ duyệt',
  `receipts_period_id` int(11) DEFAULT NULL COMMENT 'Lần thanh toán',
  `receipts_supplier_id` int(11) DEFAULT NULL COMMENT 'nhà cung cấp',
  `receipts_images` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `receipts_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `receipts_currency_id` int(11) DEFAULT NULL COMMENT 'Tiền tệ sử dụng',
  `receipts_period_payments_id` int(11) DEFAULT NULL COMMENT 'số lần thanh toán',
  `receipts_amount` decimal(13,4) DEFAULT NULL COMMENT 'số tiền thu ',
  `receipts_period_amount` decimal(10,0) DEFAULT NULL COMMENT 'tổng giá trị đơn hàng',
  `interest_rate` text COLLATE utf8mb4_unicode_ci COMMENT 'lãi xuất trả góp',
  `receipts_amount_date` date DEFAULT NULL COMMENT 'ngày thu',
  `receipts_period_date` date DEFAULT NULL COMMENT 'ngày thanh toán theo hợp đồng',
  `number_days_overdue` float NOT NULL COMMENT 'số ngày quá hạn',
  `fines` text COLLATE utf8mb4_unicode_ci COMMENT 'mức phạt vd 0.05%',
  `actual_amount` decimal(10,0) DEFAULT NULL COMMENT 'số tiền thuc tế',
  `overdue_fines` decimal(10,0) DEFAULT NULL COMMENT 'tiền phạt quá hạn',
  `receipts_description` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `receipts_status` text COLLATE utf8mb4_unicode_ci COMMENT 'Trạng thái',
  `receipts_user_create` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `receipts`
--

INSERT INTO `receipts` (`id`, `receipts_code`, `receipts_type`, `receipts_stock_id`, `receipts_customer_id`, `receipts_bank_id`, `receipts_staff_id`, `receipts_staff_approve_id`, `receipts_staff_approve_date`, `receipts_period_id`, `receipts_supplier_id`, `receipts_images`, `receipts_brand_id`, `receipts_currency_id`, `receipts_period_payments_id`, `receipts_amount`, `receipts_period_amount`, `interest_rate`, `receipts_amount_date`, `receipts_period_date`, `number_days_overdue`, `fines`, `actual_amount`, `overdue_fines`, `receipts_description`, `receipts_status`, `receipts_user_create`, `created_at`, `updated_at`) VALUES
(1, 'PT00001', 'PHIEUTHUKHACHHANG', 2, 16, 11, 95, 1, '0000-00-00 00:00:00', 1, NULL, '727849031.jpg', 3, 2, 2, 100000.0000, 900000, '1.68', '2022-02-26', '2022-02-26', 0, '0', 1134400168, 0, 'dddd', '3', NULL, '2022-02-26 14:51:36', '2022-02-26 14:54:09'),
(2, 'PT00002', 'PHIEUTHUKHACHHANG', 5, 19, 12, 95, 1, '0000-00-00 00:00:00', 12, NULL, '969916902.jpg', 3, 2, 1, 1800000.0000, 1800000, '1.68', '2022-02-26', '2022-02-26', 0, '0', 1800000, 0, 'hhhhh', '3', NULL, '2022-02-27 02:01:05', '2022-02-27 23:12:40'),
(3, 'PT00003', 'PHIEUTHUKHACHHANG', 4, 18, 11, 95, NULL, NULL, 11, NULL, '1836884799.jpg', 3, 2, 1, 1800000.0000, 1800000, '1.68', '2022-02-26', '2022-02-26', 0, '0', 1800000, 0, NULL, '1', NULL, '2022-02-27 02:09:34', '2022-02-27 23:17:07'),
(4, 'PT00004', 'PHIEUTHUKHACHHANG', 6, 20, 13, 95, 1, '0000-00-00 00:00:00', 13, NULL, '1985118758.jpg', 3, 2, 1, 1800000.0000, 1800000, '1.68', '2022-02-27', '2022-02-26', -1, '0', 1800000, 0, NULL, '3', NULL, '2022-02-27 22:36:43', '2022-02-27 23:14:00'),
(5, 'PT00005', 'PHIEUTHUKHACHHANG', 5, 19, 11, 95, 1, '0000-00-00 00:00:00', 12, NULL, '536261328.jpg', 3, 2, 1, 1800000.0000, 1800000, '1.68', '2022-02-27', '2022-02-26', -1, '0', 1800000, 0, 'ff', '3', NULL, '2022-02-27 23:01:36', '2022-02-27 23:15:23'),
(6, 'PT00006', 'PHIEUTHUKHACHHANG', 10, 21, 11, 95, 1, '0000-00-00 00:00:00', 15, NULL, '1154808162.jpg', 3, 2, 2, 10000000.0000, 86800000, '1.68', '2022-02-27', '2022-02-27', 0, '0', 11290240, 0, NULL, '3', NULL, '2022-02-28 01:01:44', '2022-03-06 16:17:45'),
(7, 'PT00007', 'PHIEUTHUKHACHHANG', 4, 18, 12, 95, 1, '0000-00-00 00:00:00', 11, NULL, '1145648284.jpg', 3, 2, 1, 1800000.0000, 1800000, '1.68', '2022-03-06', '2022-02-26', -8, '0', 1800000, 0, NULL, '3', NULL, '2022-03-06 16:19:55', '2022-03-06 16:20:58'),
(8, 'PT00008', 'PHIEUTHUKHACHHANG', 25, 38, 11, 109, NULL, NULL, 25, NULL, NULL, 3, 2, 5, 5000000.0000, 17000000, '0', '2022-04-27', '2022-04-27', 0, '0', 0, 0, NULL, '1', NULL, '2022-04-27 23:57:17', '2022-06-20 15:45:37'),
(9, 'PT00009', 'PHIEUTHUKHACHHANG', 27, 39, NULL, 109, NULL, NULL, 37, NULL, NULL, 3, 2, 1, 34000000.0000, 34000000, '0', '2022-04-27', '2022-04-27', 0, '0', 34000000, 0, NULL, '1', NULL, '2022-04-27 23:58:22', '2022-04-27 23:58:22'),
(10, 'PT00010', 'PHIEUTHUKHACHHANG', 56, 47, 13, 109, NULL, NULL, 84, NULL, NULL, 3, 2, 4, 7000000.0000, 17000000, '0', '2022-04-28', '2022-04-28', 0, '0', 0, 0, 'thang ong noi doi hoa don', '1', NULL, '2022-04-28 18:30:32', '2022-06-20 15:45:46'),
(11, 'PT00011', 'PHIEUTHUKHACHHANG', 81, 141, 12, 103, NULL, NULL, 175, NULL, NULL, 3, 2, 8, 79800000.0000, 79800000, '0', '2022-06-16', '2022-06-16', 0, '0', 0, 0, NULL, '1', NULL, '2022-06-16 09:46:36', '2022-06-20 15:45:52'),
(29, 'PT00012', 'PHIEUTHUKHACHHANG', 98, 421, NULL, 95, NULL, NULL, 215, NULL, NULL, 3, 2, 3, 1000000.0000, 5400000, '0', '2022-07-23', '2022-07-23', 0, '0', 1000000, 0, NULL, '1', NULL, '2022-07-23 23:51:40', '2022-07-23 23:51:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã nhân viên',
  `staff_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên nhân viên',
  `staff_sex` tinyint(1) NOT NULL COMMENT 'Giới tính',
  `staff_status_new` tinyint(1) DEFAULT NULL COMMENT 'nếu là nhân viên mới thì status_new=true',
  `staff_avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ảnh nhân viên',
  `staff_year_old` date DEFAULT NULL COMMENT 'ngày tháng năm sinh',
  `staff_brand_id` int(11) DEFAULT NULL COMMENT 'thuộc chi nhánh',
  `staff_rank_id` int(11) DEFAULT NULL COMMENT 'cấp bậc nhân viên đối với thủ kho hay trưởng chi nhánh sẽ không có',
  `staff_date_start` date DEFAULT NULL COMMENT 'Ngày bắt đầu vào làm',
  `staff_number_bank` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Số tài khoản ngân hàng',
  `staff_name_bank` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `staff_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'địa chỉ nhà',
  `staff_user_id` int(11) DEFAULT NULL COMMENT 'tài khoản đăng nhập',
  `staff_decentralization_id` int(11) DEFAULT NULL COMMENT 'chức vụ đảm nhận và quyền hạn',
  `staff_parent_id` int(11) DEFAULT NULL COMMENT 'nhân viên cấp cha',
  `staff_status_ranks` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `staff_status` tinyint(1) DEFAULT NULL COMMENT 'trạng thái',
  `staff_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `staff_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `staff`
--

INSERT INTO `staff` (`id`, `staff_code`, `staff_name`, `staff_sex`, `staff_status_new`, `staff_avatar`, `staff_year_old`, `staff_brand_id`, `staff_rank_id`, `staff_date_start`, `staff_number_bank`, `staff_name_bank`, `staff_address`, `staff_user_id`, `staff_decentralization_id`, `staff_parent_id`, `staff_status_ranks`, `staff_status`, `staff_user_created`, `staff_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'NV00001', 'Admin', 0, NULL, '', '2014-06-09', 0, NULL, '2021-11-06', '11000111100011', 'Seabank', 'Cần thơ', 2, 5, NULL, NULL, NULL, NULL, NULL, '2021-11-05 11:57:46', '2021-11-26 23:59:57'),
(2, 'NV00002', 'administrator', 0, NULL, '', '2021-11-27', 3, NULL, '2021-11-27', NULL, NULL, NULL, 1, 5, NULL, NULL, NULL, NULL, NULL, '2021-11-27 01:18:46', '2021-11-27 01:18:46'),
(94, 'NV00003', 'Thủ kho', 0, NULL, '', '2022-02-13', 3, NULL, '2022-02-13', NULL, NULL, NULL, 3, 2, NULL, NULL, NULL, NULL, NULL, '2022-02-13 04:08:11', '2022-02-13 04:08:11'),
(95, 'NV00004', 'sales', 0, NULL, '', '2022-02-13', 3, NULL, '2022-02-13', NULL, NULL, NULL, 5, 3, 106, NULL, 2, NULL, NULL, '2022-02-13 04:29:27', '2022-06-29 13:48:53'),
(97, 'NV00005', 'Chi nhánh trưởng', 0, NULL, '', '2022-02-13', 3, NULL, '2022-02-13', NULL, NULL, NULL, 7, 4, NULL, NULL, NULL, NULL, NULL, '2022-02-13 04:46:12', '2022-02-13 04:46:12'),
(98, 'NV00006', 'sales02', 0, NULL, '', '2022-02-26', 3, NULL, '2022-02-26', NULL, NULL, NULL, 8, 3, 106, NULL, 2, NULL, NULL, '2022-02-26 15:24:03', '2022-07-03 01:57:34'),
(99, 'NV00007', 'Đặng Văn Triệu', 0, NULL, '', '2022-02-16', 3, NULL, '2022-02-02', NULL, NULL, NULL, 9, 3, 106, NULL, NULL, NULL, NULL, '2022-02-27 10:50:52', '2022-06-29 13:48:53'),
(100, 'NV00008', 'Nguyễn Thị Thành', 1, 1, '', '2022-02-11', 3, NULL, '2022-02-23', NULL, NULL, NULL, 10, 3, 107, NULL, NULL, NULL, NULL, '2022-02-27 10:51:47', '2022-06-29 13:52:10'),
(101, 'NV00009', 'Phúc', 0, 1, '', '2022-02-04', 3, NULL, '2022-02-04', NULL, NULL, NULL, 11, 3, 107, NULL, NULL, NULL, NULL, '2022-02-27 10:52:26', '2022-06-29 13:52:10'),
(102, 'NV00010', 'Nguyễn Ngọc Toàn', 0, 1, '', '2022-02-03', 3, NULL, '2022-02-18', NULL, NULL, NULL, 12, 3, 107, NULL, NULL, NULL, NULL, '2022-02-27 10:53:19', '2022-06-29 13:52:10'),
(103, 'NV00011', 'Dung', 1, 1, '', '2022-02-02', 3, 3, '2022-02-11', NULL, NULL, NULL, 13, 3, 106, NULL, NULL, NULL, NULL, '2022-02-27 10:54:01', '2022-08-07 23:27:01'),
(104, 'NV00012', 'Tùng', 0, 1, '', '2022-02-04', 3, NULL, '2022-02-03', NULL, NULL, NULL, 14, 3, 103, NULL, NULL, NULL, NULL, '2022-02-27 10:55:39', '2022-06-29 13:52:31'),
(105, 'NV00013', 'Võ Bích Thuỷ', 1, NULL, '', '2022-02-03', 3, NULL, '2022-02-02', NULL, NULL, NULL, 15, 4, NULL, NULL, NULL, NULL, NULL, '2022-02-27 11:22:16', '2022-02-27 11:22:16'),
(106, 'NV00014', 'Cường 1', 0, 1, '', '2022-02-04', 3, 2, '2022-02-03', NULL, NULL, NULL, 16, 3, 0, NULL, NULL, NULL, NULL, '2022-02-27 11:23:08', '2022-08-07 23:23:51'),
(107, 'NV00015', 'Cường 2', 0, 1, '', '2022-02-08', 3, 3, '2022-02-10', NULL, NULL, NULL, 17, 3, NULL, NULL, NULL, NULL, NULL, '2022-02-27 11:23:52', '2022-08-07 23:27:01'),
(108, 'NV00016', 'Lâm', 0, 1, '', '2022-02-02', 3, NULL, '2022-02-08', NULL, NULL, NULL, 18, 3, 119, NULL, NULL, NULL, NULL, '2022-02-27 11:24:31', '2022-06-29 13:52:51'),
(109, 'NV00017', 'Thien', 0, 1, '1647161735.jpeg', '1999-08-25', 3, NULL, '2022-02-16', NULL, NULL, NULL, 19, 3, 119, NULL, NULL, NULL, NULL, '2022-02-27 11:25:43', '2022-06-29 13:52:51'),
(110, 'NV00018', 'Trường', 0, 1, '', '2022-02-11', 3, NULL, '2022-02-09', NULL, NULL, NULL, 20, 3, 119, NULL, NULL, NULL, NULL, '2022-02-27 11:26:27', '2022-06-29 13:52:51'),
(111, 'NV00019', 'Quang', 0, 1, '', '2022-02-04', 3, NULL, '2022-02-15', NULL, NULL, NULL, 21, 3, 119, NULL, NULL, NULL, NULL, '2022-02-27 11:27:54', '2022-06-29 13:52:51'),
(112, 'NV00020', 'nguyễn văn b', 0, NULL, '', NULL, 3, NULL, NULL, NULL, NULL, NULL, 22, 3, 119, NULL, NULL, NULL, NULL, '2022-03-06 16:40:33', '2022-06-29 13:52:51'),
(113, 'NV00021', 'Thuỷ Sales', 1, NULL, '', '2022-05-09', 3, NULL, '2021-11-04', NULL, NULL, NULL, 23, 3, 116, NULL, NULL, NULL, NULL, '2022-05-07 14:36:10', '2022-06-29 13:53:08'),
(114, 'NV00022', 'Đinh Hoàng Vũ', 0, 1, '', '2022-03-09', 3, NULL, '2022-03-09', NULL, NULL, NULL, 24, 3, 116, NULL, NULL, NULL, NULL, '2022-05-07 14:37:45', '2022-06-29 13:53:08'),
(115, 'NV00023', 'Phạm Thị Yến Nhi', 1, 1, '', '2022-04-13', 3, NULL, '2022-04-14', NULL, NULL, NULL, 25, 3, 116, NULL, NULL, NULL, NULL, '2022-05-07 14:38:41', '2022-06-29 13:53:08'),
(116, 'NV00024', 'H\' Ô Ni Niê Siêng', 1, 1, '', '2022-04-06', 3, 3, '2022-04-05', NULL, NULL, NULL, 26, 3, 119, NULL, NULL, NULL, NULL, '2022-05-07 14:39:29', '2022-06-29 19:31:04'),
(117, 'NV00025', 'Nguyễn Đức Thiện', 0, 1, '', '2022-04-07', 3, NULL, '2022-04-06', NULL, NULL, NULL, 27, 3, 116, NULL, NULL, NULL, NULL, '2022-05-07 14:40:17', '2022-06-29 13:53:08'),
(118, 'NV00026', 'Nguyễn Trường Khánh Đoan', 0, 1, '', '2022-04-07', 3, NULL, '2022-05-01', NULL, NULL, NULL, 28, 3, 123, '1', NULL, NULL, NULL, '2022-05-07 14:41:22', '2022-08-07 22:57:26'),
(119, 'NV00027', 'Hoàng Anh Tuấn', 0, 1, '', '2022-03-09', 3, 2, '2022-05-01', NULL, NULL, NULL, 29, 3, NULL, NULL, NULL, NULL, NULL, '2022-05-07 14:42:23', '2022-06-29 19:21:16'),
(120, 'NV00028', 'Nguyễn Thuỷ Tiên', 1, 1, '', '2022-03-23', 3, NULL, '2022-05-01', NULL, NULL, NULL, 30, 3, 123, '1', NULL, NULL, NULL, '2022-05-07 14:43:20', '2022-08-07 22:57:26'),
(121, 'NV00029', 'Đoàn Thị Bích Hiền', 1, 1, '', '2021-12-14', 3, NULL, '2022-05-01', NULL, NULL, NULL, 31, 3, 123, '1', NULL, NULL, NULL, '2022-05-07 14:44:24', '2022-08-07 22:57:26'),
(122, 'NV00030', 'Phan Đức Thành', 0, 1, '', NULL, 3, NULL, NULL, NULL, NULL, NULL, 32, 3, 123, '1', NULL, NULL, NULL, '2022-06-20 14:00:43', '2022-08-07 22:57:26'),
(123, 'NV00031', 'Lê Thị Mỹ Hảo', 1, 1, '', NULL, 3, 3, NULL, NULL, NULL, NULL, 33, 3, 106, NULL, NULL, NULL, NULL, '2022-06-20 14:17:32', '2022-08-07 23:27:01'),
(124, 'NV00032', 'Nguyễn Thị Bích Trâm', 0, 1, '', NULL, 3, NULL, NULL, NULL, NULL, NULL, 34, 3, 123, '1', NULL, NULL, NULL, '2022-06-20 14:21:28', '2022-08-07 22:57:26');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `staff_kpi_order`
--

CREATE TABLE `staff_kpi_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_kpi_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'mã phiếu tính kpi',
  `staff_kpi_period_stock_id` int(11) DEFAULT NULL COMMENT 'đơn hàng',
  `staff_kpi_order_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `staff_kpi_order_period_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'lây ds id period cần thanh toán',
  `staff_kpi_customers_id` int(11) DEFAULT NULL,
  `staff_kpi_sales_id` int(11) DEFAULT NULL,
  `staff_kpi_sales_status` tinyint(1) DEFAULT NULL,
  `staff_kpi_sales_sub_id` int(11) DEFAULT NULL,
  `staff_kpi_sales_sub_status` tinyint(1) DEFAULT NULL,
  `staff_kpi_sales_manage_id` int(11) DEFAULT NULL,
  `staff_kpi_sales_manage_status` tinyint(1) DEFAULT NULL,
  `staff_kpi_order_status` tinyint(1) DEFAULT NULL COMMENT 'Trạng thái',
  `staff_kpi_order_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `staff_kpi_order_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `staff_kpi_order`
--

INSERT INTO `staff_kpi_order` (`id`, `staff_kpi_code`, `staff_kpi_period_stock_id`, `staff_kpi_order_brand_id`, `staff_kpi_order_period_id`, `staff_kpi_customers_id`, `staff_kpi_sales_id`, `staff_kpi_sales_status`, `staff_kpi_sales_sub_id`, `staff_kpi_sales_sub_status`, `staff_kpi_sales_manage_id`, `staff_kpi_sales_manage_status`, `staff_kpi_order_status`, `staff_kpi_order_user_created`, `staff_kpi_order_user_updated`, `created_at`, `updated_at`) VALUES
(46, 'KPI00001', 2, 3, '1,2', 16, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:42:48', '2022-08-07 15:36:56'),
(47, 'KPI00002', 3, 3, '5', 17, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:42:48', '2022-08-07 15:36:56'),
(48, 'KPI00003', 4, 3, '11', 18, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:42:48', '2022-08-07 15:36:56'),
(49, 'KPI00004', 78, 3, '164', 18, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:42:48', '2022-08-07 15:36:56'),
(50, 'KPI00005', 79, 3, '165,166,167', 18, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:42:48', '2022-08-07 15:36:56'),
(51, 'KPI00006', 2, 3, '3,4', 16, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:45:59', '2022-08-07 15:36:56'),
(52, 'KPI00007', 3, 3, '6,7,8', 17, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:45:59', '2022-08-07 15:36:56'),
(53, 'KPI00008', 79, 3, '168', 18, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:45:59', '2022-08-07 15:36:56'),
(54, 'KPI00009', 5, 3, '12', 19, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:45:59', '2022-08-07 15:36:56'),
(55, 'KPI00010', 6, 3, '13', 20, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:45:59', '2022-08-07 15:36:56'),
(56, 'KPI00011', 86, 3, '199', 20, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-14 18:45:59', '2022-08-07 15:36:56'),
(57, 'KPI00012', 40, 3, '50', 26, 106, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-16 04:57:42', '2022-07-16 04:57:42'),
(58, 'KPI00013', 43, 3, '63,64', 23, 107, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-16 04:58:48', '2022-07-16 04:58:48'),
(59, 'KPI00014', 47, 3, '71', 46, 107, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-16 04:58:48', '2022-07-16 04:58:48'),
(60, 'KPI00015', 43, 3, '65,66', 23, 107, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-16 04:59:03', '2022-07-16 04:59:03'),
(61, 'KPI00016', 48, 3, '72,73,74,75,76,77', 42, 106, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-18 23:41:46', '2022-07-18 23:41:46'),
(62, 'KPI00017', 48, 3, '78,79,80,81,82,83', 42, 106, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:29:21', '2022-07-19 00:29:21'),
(63, 'KPI00018', 41, 3, '51,52,53,54,55,56,57,58,59,60,61,62', 33, 103, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:30:22', '2022-07-19 00:30:22'),
(64, 'KPI00019', 81, 3, '175,176,177,178,179,180,181,182,183,184,185,186', 141, 103, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:30:22', '2022-07-19 00:30:22'),
(65, 'KPI00020', 66, 3, '113', 81, 116, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:31:42', '2022-07-19 00:31:42'),
(66, 'KPI00021', 71, 3, '139,140,141,142,143,144,145,146,147,148,149,150', 87, 116, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:31:42', '2022-07-19 00:31:42'),
(67, 'KPI00022', 90, 3, '211,212,213,214', 225, 123, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:33:18', '2022-07-19 00:33:18'),
(68, 'KPI00023', 67, 3, '114', 82, 120, 1, 123, 1, 106, 1, NULL, NULL, NULL, '2022-07-19 00:33:31', '2022-07-31 18:09:34'),
(69, 'KPI00024', 64, 3, '111', 80, 118, 1, 123, 1, 106, 1, NULL, NULL, NULL, '2022-07-19 00:33:51', '2022-07-31 18:09:34'),
(70, 'KPI00025', 68, 3, '115,116,117,118,119,120,121,122,123,124,125,126', 86, 118, 1, 123, 1, 106, 1, NULL, NULL, NULL, '2022-07-19 00:33:52', '2022-07-31 18:09:34'),
(71, 'KPI00026', 3, 3, '9,10', 17, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:34:37', '2022-08-07 15:36:56'),
(72, 'KPI00027', 86, 3, '200,201,202,203,204,205,206,207,208,209,210', 20, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:34:37', '2022-08-07 15:36:56'),
(73, 'KPI00028', 10, 3, '15,16,17,18', 21, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:34:37', '2022-08-07 15:36:56'),
(74, 'KPI00029', 80, 3, '169,170,171,172,173,174', 21, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:34:37', '2022-08-07 15:36:56'),
(75, 'KPI00030', 17, 3, '19', 22, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:34:37', '2022-08-07 15:36:56'),
(76, 'KPI00031', 85, 3, '188,189,190,191,192,193,194,195,196,197,198', 22, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:34:37', '2022-08-07 15:36:56'),
(77, 'KPI00032', 18, 3, '20,21,22,23', 31, 95, 1, 106, 1, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:34:37', '2022-08-07 15:36:56'),
(78, 'KPI00033', 22, 3, '24', 37, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(79, 'KPI00034', 25, 3, '25,26,27,28,29,30,31,32,33,34,35,36', 38, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(80, 'KPI00035', 27, 3, '37', 39, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(81, 'KPI00036', 29, 3, '38,39,40,41,42,43,44,45,46,47,48,49', 41, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(82, 'KPI00037', 44, 3, '67,68,69,70', 41, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(83, 'KPI00038', 60, 3, '105,106,107,108,109,110', 43, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(84, 'KPI00039', 56, 3, '84,85,86,87,88,89,90,91,92', 47, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(85, 'KPI00040', 57, 3, '93,94,95,96,97,98,99,100,101,102,103,104', 49, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(86, 'KPI00041', 65, 3, '112', 79, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(87, 'KPI00042', 69, 3, '127,128,129,130,131,132,133,134,135,136,137,138', 84, 109, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-19 00:37:29', '2022-07-19 00:37:29'),
(88, 'KPI00043', 75, 3, '151', 89, 104, 1, 103, 1, 106, 1, NULL, NULL, NULL, '2022-07-23 02:21:16', '2022-07-31 18:09:34'),
(89, 'KPI00044', 76, 3, '152,153,154,155,156,157,158,159,160,161,162,163', 90, 104, 1, 103, 1, 106, 1, NULL, NULL, NULL, '2022-07-23 02:21:16', '2022-07-31 18:09:34');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `stock`
--

CREATE TABLE `stock` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `stock_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã đơn hàng',
  `stock_seri_number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'seri để bảo hành',
  `stock_type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'kiểu đơn hàng',
  `Type` varchar(255) CHARACTER SET utf8 NOT NULL,
  `stock_product_id` int(11) DEFAULT NULL COMMENT 'sản phẩm',
  `stock_supplier_id` int(11) DEFAULT NULL COMMENT 'nhà cung cấp',
  `stock_customer_id` int(11) DEFAULT NULL COMMENT 'khách hàng',
  `stock_payment_form_id` int(11) DEFAULT NULL COMMENT 'Hình thức thanh toán',
  `stock_product_unit_id` int(11) DEFAULT NULL COMMENT 'đơn vị tính',
  `stock_product_unit_covert_id` int(11) DEFAULT NULL COMMENT 'đơn vị tính quy đổi',
  `stock_quality_reality` decimal(10,0) DEFAULT NULL COMMENT 'số lượng thực tế',
  `stock_quality_convert` decimal(10,0) DEFAULT NULL COMMENT 'số lượng đã đổi',
  `stock_price` decimal(10,0) DEFAULT NULL COMMENT 'giá sản phẩm',
  `stock_price_export` decimal(10,0) DEFAULT NULL COMMENT 'giá xuất bán',
  `stock_currency_id` int(11) DEFAULT NULL COMMENT 'đơn vị tiền tệ',
  `stock_amount_tax_id` int(11) DEFAULT NULL COMMENT 'thuế',
  `stock_amount_tax` int(11) DEFAULT NULL COMMENT 'mức thuế',
  `stock_location_Out` int(11) DEFAULT NULL COMMENT 'Từ kho',
  `stock_location_Int` int(11) DEFAULT NULL COMMENT 'Đến kho',
  `stock_location` int(11) DEFAULT NULL COMMENT 'kho',
  `stock_amount` decimal(10,0) DEFAULT NULL COMMENT 'thành tiền',
  `stock_costs_incurred` decimal(10,0) DEFAULT NULL COMMENT 'chi phí phát sinh',
  `stock_amount_total` decimal(10,0) DEFAULT NULL COMMENT 'tổng tiền',
  `stock_description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'nội dung mô tả',
  `stock_staff_id` int(11) DEFAULT NULL COMMENT 'nhân viên thực hiện xuất đơn',
  `stock_staff_stocker_id` int(11) DEFAULT NULL COMMENT 'thủ kho duyệt xuất',
  `stock_date_start_stocker` datetime DEFAULT NULL COMMENT 'ngày thủ kho duyệt',
  `stock_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `stock_status` int(11) DEFAULT NULL COMMENT 'trạng thái đơn hàng, status=1 tạo đơn nhập hoặc xuất,status=2 thủ kho duyệt',
  `stock_receipt_date` date DEFAULT NULL COMMENT 'ngày lập phiếu',
  `stock_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `stock_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `stock`
--

INSERT INTO `stock` (`id`, `stock_code`, `stock_seri_number`, `stock_type`, `Type`, `stock_product_id`, `stock_supplier_id`, `stock_customer_id`, `stock_payment_form_id`, `stock_product_unit_id`, `stock_product_unit_covert_id`, `stock_quality_reality`, `stock_quality_convert`, `stock_price`, `stock_price_export`, `stock_currency_id`, `stock_amount_tax_id`, `stock_amount_tax`, `stock_location_Out`, `stock_location_Int`, `stock_location`, `stock_amount`, `stock_costs_incurred`, `stock_amount_total`, `stock_description`, `stock_staff_id`, `stock_staff_stocker_id`, `stock_date_start_stocker`, `stock_brand_id`, `stock_status`, `stock_receipt_date`, `stock_user_created`, `stock_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'PN00001', NULL, 'quatang', 'NHAPKHO', 1, NULL, NULL, NULL, 1, NULL, 1000, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'dddđ', 94, 94, '2022-02-26 07:36:37', 3, 3, NULL, NULL, NULL, '2022-02-26 14:36:28', '2022-02-26 14:36:37'),
(2, 'PX00001', '222222222', 'quatang', 'XUATKHO', 1, NULL, 16, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'wwwwwwwwww', 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-26 14:43:05', '2022-07-03 01:56:50'),
(3, 'PX00002', 'aaaaaaaaaaa', 'quatang', 'XUATKHO', 1, NULL, 17, NULL, NULL, NULL, -3, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -2700000, NULL, NULL, NULL, 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-26 14:47:39', '2022-07-03 01:56:50'),
(4, 'PX00003', 'ffffff', 'quatang', 'XUATKHO', 1, NULL, 18, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, 'ffff', 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-27 01:56:59', '2022-07-03 01:56:50'),
(5, 'PX00004', '4444444', 'quatang', 'XUATKHO', 1, NULL, 19, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, 'ccccccccc', 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-27 02:00:11', '2022-07-03 01:56:50'),
(6, 'PX00005', 'dddd', 'quatang', 'XUATKHO', 1, NULL, 20, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, 'dddd', 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-27 02:17:21', '2022-07-03 01:56:50'),
(7, 'PX00006', 'dddd', 'quatang', 'XUATKHO', 1, NULL, 20, NULL, NULL, NULL, -3, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -2700000, NULL, NULL, 'đ', 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-27 02:19:58', '2022-07-03 01:56:50'),
(8, 'PN00002', NULL, 'nhapkhobanhang', 'NHAPKHO', 2, NULL, NULL, NULL, 1, NULL, 1000, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 94, 94, '2022-02-27 09:15:00', 3, 3, NULL, NULL, NULL, '2022-02-27 16:10:00', '2022-02-27 16:15:00'),
(9, 'PX00007', '22222222', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 21, NULL, NULL, NULL, -5, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -85000000, NULL, NULL, NULL, 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-28 00:09:06', '2022-07-03 01:56:50'),
(10, 'PX00007', '22222222', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 21, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, NULL, 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-02-28 00:09:06', '2022-07-03 01:56:50'),
(12, 'TSC00001', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'fffffff', 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-04-27 14:47:05', '2022-07-03 01:56:50'),
(13, 'TSC00002', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 111, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-27 14:47:20', '2022-04-27 14:47:20'),
(14, 'TSC00002', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 111, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-27 14:47:20', '2022-04-27 14:47:20'),
(15, 'TSC00002', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 111, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-27 14:47:20', '2022-04-27 14:47:20'),
(16, 'TSC00002', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 111, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-27 14:47:20', '2022-04-27 14:47:20'),
(17, 'PX00008', '01', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 22, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, NULL, 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-04-27 15:17:22', '2022-07-03 01:56:50'),
(18, 'PX00009', '02', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 31, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, NULL, 95, NULL, NULL, 3, 3, NULL, 'sales02', NULL, '2022-04-27 15:19:45', '2022-07-03 01:56:50'),
(20, 'TSC00003', NULL, 'quatang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'smell 2 tặng ông nội khó tính', 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-04-27 22:18:21', '2022-07-03 01:56:50'),
(21, 'TSC00004', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-04-27 22:23:20', '2022-07-03 01:56:50'),
(22, 'PX00010', '01', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 37, NULL, NULL, NULL, -1, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -17000000, NULL, NULL, 'mua 1 rb', 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-27 22:38:07', '2022-04-27 22:39:34'),
(23, 'TSC00005', NULL, 'quatang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'quà tặng chị Thy', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-27 22:42:11', '2022-04-27 22:42:11'),
(24, 'TSC00006', NULL, 'quatang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, 1, NULL, -2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'quà tặng chị thy lần nữa', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-27 22:42:54', '2022-04-27 22:42:54'),
(25, 'PX00011', '01', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 38, NULL, NULL, NULL, -1, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -17000000, NULL, NULL, NULL, 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-27 22:46:26', '2022-04-27 22:46:36'),
(27, 'PX00012', '03', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 39, NULL, NULL, NULL, -2, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -34000000, NULL, NULL, NULL, 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-27 23:42:05', '2022-04-27 23:42:38'),
(28, 'PX00013', '04', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 41, NULL, NULL, NULL, -10, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -170000000, NULL, NULL, 'xin bố đường mua 10 RB', 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-04-27 23:50:44'),
(29, 'PX00013', '04', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 41, NULL, NULL, NULL, -20, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -18000000, NULL, NULL, 'xin bố đường mua 10 RB', 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-27 23:47:45', '2022-04-27 23:50:44'),
(30, 'TSC00007', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'hết smell set 6', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-27 23:59:19', '2022-04-27 23:59:19'),
(31, 'TSC00008', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Set 1 bỏ mất smell', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 00:10:27', '2022-04-28 00:10:27'),
(32, 'TSC00009', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Nhi hết smell', 107, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:52:46', '2022-04-28 08:52:46'),
(33, 'TSC00010', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Thiện mất smell. Xin cấp 1 smell', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:52:58', '2022-04-28 08:52:58'),
(34, 'TSC00011', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Bổ sung khuynh diệp', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:53:00', '2022-04-28 08:53:00'),
(35, 'TSC00011', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Bổ sung khuynh diệp', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:53:00', '2022-04-28 08:53:00'),
(36, 'TSC00011', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Bổ sung khuynh diệp', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:53:00', '2022-04-28 08:53:00'),
(37, 'TSC00011', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Bổ sung khuynh diệp', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:53:00', '2022-04-28 08:53:00'),
(38, 'TSC00012', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 106, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:53:34', '2022-04-28 08:55:18'),
(39, 'TSC00013', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Thiện lấy', 99, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 08:58:34', '2022-04-28 08:58:34'),
(40, 'PX00014', '01', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 26, NULL, NULL, NULL, -1, NULL, NULL, 79800000, 2, NULL, NULL, NULL, NULL, 1, -79800000, NULL, NULL, NULL, 106, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 09:04:36', '2022-04-28 09:12:46'),
(41, 'PX00015', '22310208', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 33, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'Mua 1 máy', 103, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 09:06:34', '2022-04-28 09:09:19'),
(42, 'PX00016', '01', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 23, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'Nhà giàu con đẹp trai kiếm tiền giỏi', 107, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 09:07:50', '2022-04-28 09:09:24'),
(43, 'PX00016', '01', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 23, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, 'Nhà giàu con đẹp trai kiếm tiền giỏi', 107, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 09:07:50', '2022-04-28 09:09:24'),
(44, 'PX00017', '03', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 41, NULL, NULL, NULL, -1, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -17000000, NULL, NULL, NULL, 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 09:08:06', '2022-04-28 09:09:24'),
(47, 'PX00018', '02', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 46, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'Nhà giàu', 107, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 09:16:28', '2022-04-28 09:16:45'),
(48, 'PX00019', '01', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 42, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -78900000, NULL, NULL, NULL, 106, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 09:16:38', '2022-04-28 09:18:58'),
(52, 'TSC00014', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 09:35:18', '2022-04-28 09:35:18'),
(53, 'TSC00015', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Lỗi smell', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 09:36:04', '2022-04-28 09:36:04'),
(54, 'TSC00016', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 09:36:45', '2022-04-28 09:36:45'),
(55, 'TSC00017', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'nhi an cap mui huong', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-04-28 18:19:36', '2022-04-28 18:19:36'),
(56, 'PX00020', '022222', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 47, NULL, NULL, NULL, -1, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -17000000, NULL, NULL, 'huy luom duoc tien mua rb', 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 18:25:09', '2022-04-28 18:25:34'),
(57, 'PX00021', '222', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 49, NULL, NULL, NULL, -5, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -85000000, NULL, NULL, NULL, 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-28 18:34:34', '2022-04-28 18:35:17'),
(60, 'PX00022', '06', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 43, NULL, NULL, NULL, -1, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -17000000, NULL, NULL, NULL, 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-04-30 14:20:25', '2022-04-30 14:20:35'),
(61, 'TSC00018', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Ny hết mùi hương', 116, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-11 11:33:44', '2022-05-11 11:33:44'),
(62, 'TSC00019', NULL, 'quatang', 'XUATTAISANCONGTY', 2, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Ni hết mùi hương', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-11 11:34:09', '2022-05-11 11:34:09'),
(63, 'TSC00020', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Đoan hết mùi hương', 118, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-11 11:34:35', '2022-05-11 11:34:35'),
(64, 'PX00023', '12345', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 80, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'Abc', 118, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-05-11 11:39:28', '2022-05-11 11:40:19'),
(65, 'PX00024', '0222', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 79, NULL, NULL, NULL, -1, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -17000000, NULL, NULL, 'Ăn cướp mua rb', 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-05-11 11:39:54', '2022-05-11 11:40:17'),
(66, 'PX00025', '22222', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 81, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'Khách mua Rain bow', 116, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-11 11:40:38', '2022-05-11 11:40:38'),
(67, 'PX00026', 'Gsghdjc', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 82, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'Abc', 120, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-11 11:43:06', '2022-05-11 11:43:06'),
(68, 'PX00027', '0908149064', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 86, NULL, NULL, NULL, -5, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -85000000, NULL, NULL, NULL, 118, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-05-11 11:44:11', '2022-05-11 11:44:23'),
(69, 'PX00028', '022', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 84, NULL, NULL, NULL, -5, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -85000000, NULL, NULL, 'Abc', 109, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-05-11 11:44:47', '2022-05-11 11:44:54'),
(70, 'PX00029', '333333', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 87, NULL, NULL, NULL, -4, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -68000000, NULL, NULL, 'Khách Giới thiệu mua', 116, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-05-11 11:45:32'),
(71, 'PX00029', '333333', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 87, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'Khách Giới thiệu mua', 116, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-11 11:45:32', '2022-05-11 11:45:32'),
(74, 'TSC00021', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Uống hết', 104, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-05-12 09:54:46', '2022-05-12 09:54:46'),
(75, 'PX00030', '1223', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 89, NULL, NULL, NULL, -5, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -85000000, NULL, NULL, 'Mới cướp ngân hàng', 104, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-05-12 09:57:27', '2022-05-12 09:57:36'),
(76, 'PX00031', '1222', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 90, NULL, NULL, NULL, -5, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -85000000, NULL, NULL, 'Ngủ với bồ, bồ cho tiền mua', 104, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-05-12 09:59:45', '2022-05-12 09:59:55'),
(78, 'PX00032', '2222', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 18, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, 'dd', 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-06-08 23:33:01', '2022-07-03 01:56:50'),
(79, 'PX00033', 'dddd', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 18, NULL, NULL, NULL, -3, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -51000000, NULL, NULL, 'dđ', 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-06-08 23:33:41', '2022-07-03 01:56:50'),
(80, 'PX00034', 'ffffffffff', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 21, NULL, NULL, NULL, -2, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -34000000, NULL, NULL, 'ffffff', 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-06-08 23:34:25', '2022-07-03 01:56:50'),
(81, 'PX00035', '22367567', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 141, NULL, NULL, NULL, -1, NULL, NULL, 79800000, 2, NULL, NULL, NULL, NULL, 1, -79800000, NULL, NULL, 'mua may', 103, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-06-16 09:43:17', '2022-06-16 09:43:31'),
(82, 'TSC00022', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Lấy smell đi Demo(Cam,Thông,Khuynh Diệp)', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-06-16 15:54:24', '2022-06-16 15:54:24'),
(83, 'TSC00022', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Lấy smell đi Demo(Cam,Thông,Khuynh Diệp)', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-06-16 15:54:24', '2022-06-16 15:54:24'),
(84, 'TSC00022', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Lấy smell đi Demo(Cam,Thông,Khuynh Diệp)', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-06-16 15:54:24', '2022-06-16 15:54:24'),
(85, 'PX00036', 'ggggggggggg', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 22, NULL, NULL, NULL, -100, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -90000000, NULL, NULL, 'ggg', 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-06-19 17:49:36', '2022-07-03 01:56:50'),
(86, 'PX00037', 'rrrrrrrrr', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 20, NULL, NULL, NULL, -1, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -900000, NULL, NULL, 'rrrrrrrrr', 95, NULL, NULL, 3, 1, NULL, 'sales02', NULL, '2022-06-19 18:00:05', '2022-07-03 01:56:50'),
(87, 'TSC00023', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Hết smell', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-06-23 13:26:55', '2022-06-23 13:26:55'),
(88, 'TSC00024', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Hết smell', 123, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-06-23 13:27:16', '2022-06-23 13:27:16'),
(89, 'TSC00025', NULL, 'quatang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, NULL, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'hêt smell', 122, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-06-23 13:29:01', '2022-06-23 13:29:01'),
(90, 'PX00038', '22355655', 'nhapkhobanhang', 'XUATKHO', 2, NULL, 225, NULL, NULL, NULL, -5, NULL, NULL, 17000000, 2, NULL, NULL, NULL, NULL, 1, -85000000, NULL, NULL, 'Mua may', 123, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-06-23 13:30:37', '2022-06-23 13:30:53'),
(94, 'PXQT00001', NULL, 'quatang', 'XUATKHOQUATANG', 1, NULL, 22, NULL, 1, NULL, -2, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'yhjyt', 95, 95, '2022-07-02 00:00:00', 3, 3, NULL, 'sales02', NULL, '2022-07-03 00:14:02', '2022-07-03 01:56:50'),
(95, 'TSC00026', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Fresh Air set 1', 103, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-07-04 14:49:53', '2022-07-04 14:49:53'),
(96, 'TSC00027', NULL, 'nhapkhobanhang', 'XUATTAISANCONGTY', 1, NULL, NULL, NULL, 1, NULL, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'Thiện báo hết mùi hương', 109, NULL, NULL, 3, 1, NULL, NULL, NULL, '2022-07-20 16:33:47', '2022-07-20 16:33:47'),
(97, 'PN00003', NULL, 'nhapkhobanhang', 'NHAPKHO', 1, NULL, NULL, NULL, 1, NULL, 120, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 94, 94, '2022-07-23 09:24:39', 3, 3, NULL, NULL, NULL, '2022-07-23 16:23:59', '2022-07-23 16:24:39'),
(98, 'PX00039', '1111111', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 421, NULL, NULL, NULL, -6, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -5400000, NULL, NULL, NULL, 95, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-07-23 16:38:18', '2022-07-23 16:38:39'),
(99, 'PX00040', 'rrrr', 'nhapkhobanhang', 'XUATKHO', 1, NULL, 421, NULL, NULL, NULL, -2, NULL, NULL, 900000, 2, NULL, NULL, NULL, NULL, 1, -1800000, NULL, NULL, NULL, 95, NULL, NULL, 3, 3, NULL, NULL, NULL, '2022-07-23 16:40:04', '2022-07-23 16:40:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `supplier`
--

CREATE TABLE `supplier` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã ncc',
  `supplier_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên ncc',
  `supplier_address` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'địa chỉ',
  `supplier_country_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'quốc gia',
  `supplier_brand_id` int(11) DEFAULT NULL COMMENT 'chi nhánh',
  `supplier_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `supplier_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `supplier`
--

INSERT INTO `supplier` (`id`, `supplier_code`, `supplier_name`, `supplier_address`, `supplier_country_id`, `supplier_brand_id`, `supplier_user_created`, `supplier_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'NCC001', 'nhà cung cấp điện máy A', 'cần thơ', '2', NULL, NULL, NULL, '2021-10-30 02:12:21', '2021-10-30 02:13:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tax`
--

CREATE TABLE `tax` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tax_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã thuế',
  `tax_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên thuế',
  `tax_level` float DEFAULT NULL COMMENT 'mức thuế (%)',
  `tax_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tax`
--

INSERT INTO `tax` (`id`, `tax_code`, `tax_name`, `tax_level`, `tax_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'gtgt', 'thuế giá trị gia tăng', 10, NULL, '2021-10-23 01:05:32', '2021-10-23 01:25:08');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `unit`
--

CREATE TABLE `unit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unit_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã đơn vị',
  `unit_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên đơn vị',
  `unit_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `unit_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `unit`
--

INSERT INTO `unit` (`id`, `unit_code`, `unit_name`, `unit_user_created`, `unit_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'c', 'cái', NULL, NULL, '2021-10-30 02:46:28', '2021-10-30 02:46:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'administrator@gmail.com', NULL, '$2y$10$2.C3j0PEBBG7M4mquBBJ.uslaWu7VsuY201/Rz6tf3E47mHGWeGR2', NULL, NULL, NULL),
(2, 'admin', 'admin@gmail.com', NULL, '$2y$10$8aL2j6wgq9nQNhIf.xQkj.QOYZFF63U3Ws/fvlKRRI0jy2MFJ9QU2', NULL, NULL, NULL),
(3, 'thukho', 'thukho@gmail.com', NULL, '$2y$10$EgCoYJazjOaQ4/c.bfq.HOuVwuTgcwtRbKo9UL4AnKof7u/ZBnkXu', NULL, '2022-02-13 04:08:11', '2022-02-13 04:08:11'),
(5, 'sales', 'sales@gmail.com', NULL, '$2y$10$Ib7CIE3l879Mnz7zbnu0mOvWmno3ZTRM6b8E1.0OwJJnbMRA2tu.W', NULL, '2022-02-13 04:29:27', '2022-02-13 04:29:27'),
(7, 'chinhanhtruong', 'chinhanhtruong@gmail.com', NULL, '$2y$10$EJiIOoXuPLceANgM4Lxn2OzsMNYCKhldHmnIGOop2VES8vBqEws2q', NULL, '2022-02-13 04:46:12', '2022-02-13 04:46:12'),
(8, 'sales02', 'sales02@gmail.com', NULL, '$2y$10$MKcaHk.1.y/XPRgDLNKrcuDfOuzOIu0mFkxjB98gNhoz3GuRYNaVO', NULL, '2022-02-26 15:24:03', '2022-02-26 15:24:03'),
(9, 'trieuyeurainbow658', 'Vantrieu1208@gmail.com', NULL, '$2y$10$edNt/rE4ZZnTfluKFZFNb.rKx30q3Wj2.njDpM5kosUTPq8VMbIrK', NULL, '2022-02-27 10:50:52', '2022-02-27 10:50:52'),
(10, 'thanhyeurainbow857', 'Thanhnguyenthi0977@gmail.com', NULL, '$2y$10$SUEfWZWV2R4Du6rqhu.Ps.5aFgt/iQzWvw/SN8MM3cqV8VWg9nnPK', NULL, '2022-02-27 10:51:47', '2022-02-27 10:51:47'),
(11, 'phucyeurainbow863', 'Phuc131293@gmail.com', NULL, '$2y$10$sxKgpoKoIlk3uv5mpe9PC.tztWduaRJhE5D87sCJouu7qa9ouWEpe', NULL, '2022-02-27 10:52:26', '2022-02-27 10:52:26'),
(12, 'toanyeurainbow784', 'Ngoctoan057@gmail.com', NULL, '$2y$10$ArfGyz057hjXwybXW134OuwnboT0uXiZLNqwGGFRvB6ggiJMok6Xy', NULL, '2022-02-27 10:53:19', '2022-02-27 10:53:19'),
(13, 'dungyeurainbow153', 'htledung@gmail.com', NULL, '$2y$10$8aL2j6wgq9nQNhIf.xQkj.QOYZFF63U3Ws/fvlKRRI0jy2MFJ9QU2', NULL, '2022-02-27 10:54:01', '2022-02-27 10:54:01'),
(14, 'tungyeurainbow854', 'Nguyenhoangtungkp@gmail.com', NULL, '$2y$10$v70bzxo5F5ykAql7LfpB1edIO1vjsq7CCA3MkXpct1NzOD3omRpLO', NULL, '2022-02-27 10:55:39', '2022-02-27 10:55:39'),
(15, 'thuyyeurainbow734', 'thuy@healthyhomes.com.vn', NULL, '$2y$10$AZ.PeWC2tfMKZKtsDjIqCeeGlmtfwK7E6/MgPWgMl3/5NeOOpP1NC', NULL, '2022-02-27 11:22:16', '2022-02-27 11:22:16'),
(16, 'cuongyeurainbow472', 'cuongphucton@gmail.com', NULL, '$2y$10$2.C3j0PEBBG7M4mquBBJ.uslaWu7VsuY201/Rz6tf3E47mHGWeGR2', NULL, '2022-02-27 11:23:08', '2022-02-27 11:23:08'),
(17, 'cuongyeurainbow981', 'Huycuongng94@gmail.com', NULL, '$2y$10$tyORdYkXoKGpDBMMUTHF3eFc3GDumkQAZN7ceVp7Z0wYi3IjQKC6i', NULL, '2022-02-27 11:23:52', '2022-02-27 11:23:52'),
(18, 'lamyeurainbow754', 'kimylam.z7@gmail.com', NULL, '$2y$10$nWZ89p4Bm21B4x4UWiK/7eQV99MKFQ7jhIu7kd5k6/SjUphtuuP5m', NULL, '2022-02-27 11:24:31', '2022-02-27 11:24:31'),
(19, 'thienyeurb99', 'habilovee28@gmail.com', NULL, '$2y$10$.g58a6Q14iinXdEu4fn5J.etso5EM.gK9j4ByBSnfz9ztUFFsB0e6', NULL, '2022-02-27 11:25:43', '2022-04-27 14:54:00'),
(20, 'truongyeurainbow983', 'duongkhuongduy10@gmail.com', NULL, '$2y$10$BbD75uwwFUl07pautkAMnOi8tx5AhqA4qvV8ZgJ2AuvTmeWGZhkXu', NULL, '2022-02-27 11:26:27', '2022-02-27 11:26:27'),
(21, 'quangyeurainbow765', 'quangdang1111@gmail.com', NULL, '$2y$10$q4fdPaC4wudxGBzMOZxgMO12HTKPFAdc/u4JipR5MK6NUsiY7t1Ee', NULL, '2022-02-27 11:27:54', '2022-02-27 11:27:54'),
(22, 'nguyenvanb', 'nguyenvanb@gmail.com', NULL, '$2y$10$63UmAk4eDCe7Z0juEEdtz.KBV88X4UScukuiDNod5Qih9qpBAxgXS', NULL, '2022-03-06 16:40:33', '2022-03-06 16:40:33'),
(23, 'thuyyeurainbow735', 'bichthuy.inthy@gmail.com', NULL, '$2y$10$mBsaVVLGXkk81mRfBMdQNud54YycVseg/8EDbdbYPov04YP0nK/V6', NULL, '2022-05-07 14:36:10', '2022-05-07 14:36:10'),
(24, 'vuyeurainbow753', 'dinhhoangvu2271997@gmail.com', NULL, '$2y$10$W1kd4J48QJhA5wfxVMrScebcLFHt0I25BlkpvC46pbQ/rcgUTWEbW', NULL, '2022-05-07 14:37:45', '2022-05-07 14:37:45'),
(25, 'nhiloverainbow851', 'nhipty0601@gmail.com', NULL, '$2y$10$2LDItqRhv3n4h0cruiG81ulgJ4TfjHDOwZg/epS0fd8v9VSur5vhS', NULL, '2022-05-07 14:38:41', '2022-05-07 14:38:41'),
(26, 'nyloverainbow874', 'hnavynie0196@gmail.com', NULL, '$2y$10$r8Gnips/d4pN9HaY6wXjMu6Ywa728CSU6RvwSpR5LTIv4iBdtJtEm', NULL, '2022-05-07 14:39:29', '2022-05-07 14:39:29'),
(27, 'thienloverainbow742', 'habilovee28@gmail.com', NULL, '$2y$10$3PPsj2VRpVgd27cUg3HijeHJsIKyZYuFzNzLtxCWZ6rIqSDWrdWVe', NULL, '2022-05-07 14:40:17', '2022-05-07 14:40:17'),
(28, 'doanloverainbow863', 'khanhdoan.nguyen0707@gmail.com', NULL, '$2y$10$Ro.bZpy7rv3U.r0nBahJsee0VSQBaXai6yuPocorDQiHR3vnUkdHG', NULL, '2022-05-07 14:41:22', '2022-05-07 14:41:22'),
(29, 'tuanloverainbow745', 'monkey000291@gmail.com', NULL, '$2y$10$ZJA9EVwq9jZCAC9rA3JKhuQzJidle6OBM2ujHthbdsLKN8cl4rpmy', NULL, '2022-05-07 14:42:23', '2022-05-07 14:42:23'),
(30, 'tienloverainbow852', 'Tientienfly@gmail.com', NULL, '$2y$10$rv1GrHXfN9UlvOLptCzsWOrkeyTQB4zf6HiDePNlrr0Jx4555iENK', NULL, '2022-05-07 14:43:20', '2022-05-07 14:43:20'),
(31, 'hienloverainbow749', 'doanthibichhien123@gmail.com', NULL, '$2y$10$DCvxgYP.iXSecZr1PPHJnO3/me1pWS5M.3DeV4eB0vUHY3C/Xbjum', NULL, '2022-05-07 14:44:24', '2022-05-07 14:44:24'),
(32, 'Phanducthanh', 'Phanducthanhh999@gmail.com', NULL, '$2y$10$qulpaq4hEtIuy9.PpisWTOIhZP7JX8rXP4jSqlxe2/2usqa5M5arO', NULL, '2022-06-20 14:00:43', '2022-06-20 14:00:43'),
(33, 'Lethimyhao', 'Myhaolethi409@gmail.com', NULL, '$2y$10$2.C3j0PEBBG7M4mquBBJ.uslaWu7VsuY201/Rz6tf3E47mHGWeGR2', NULL, '2022-06-20 14:17:32', '2022-06-20 14:17:32'),
(34, 'Nguyenthibichtram', 'trammaibot@gmail.com', NULL, '$2y$10$De4Zb3tQm2fn/0Bw.rc0Qupb.6PxqI8VHm9m1uqPQthd1yPjZT0RC', NULL, '2022-06-20 14:21:28', '2022-06-20 14:21:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ware_house`
--

CREATE TABLE `ware_house` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ware_house_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã kho',
  `ware_house_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên kho',
  `ware_house_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `ware_house_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ware_house`
--

INSERT INTO `ware_house` (`id`, `ware_house_code`, `ware_house_name`, `ware_house_user_created`, `ware_house_user_updated`, `created_at`, `updated_at`) VALUES
(1, 'khovn', 'kho Việt Nam', NULL, NULL, '2021-10-23 01:50:36', '2021-10-23 01:50:36'),
(2, 'khocampuchia', 'kho Cambodia', NULL, NULL, '2021-12-02 11:01:41', '2021-12-02 11:01:41');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `warranty_form`
--

CREATE TABLE `warranty_form` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `warranty_form_code` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Mã hình thức bảo hành',
  `warranty_form_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Tên hình thức bảo hành',
  `warranty_form_user_created` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người tạo',
  `warranty_form_user_updated` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Người cập nhât',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `commission`
--
ALTER TABLE `commission`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `commission_rank`
--
ALTER TABLE `commission_rank`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer_care`
--
ALTER TABLE `customer_care`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `decentralization`
--
ALTER TABLE `decentralization`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `origin`
--
ALTER TABLE `origin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Chỉ mục cho bảng `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prov_code` (`prov_code`),
  ADD KEY `prov_name` (`prov_name`);

--
-- Chỉ mục cho bảng `rank`
--
ALTER TABLE `rank`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `staff_kpi_order`
--
ALTER TABLE `staff_kpi_order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ware_house`
--
ALTER TABLE `ware_house`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `commission`
--
ALTER TABLE `commission`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `commission_rank`
--
ALTER TABLE `commission_rank`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `currency`
--
ALTER TABLE `currency`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=447;

--
-- AUTO_INCREMENT cho bảng `customer_care`
--
ALTER TABLE `customer_care`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT cho bảng `decentralization`
--
ALTER TABLE `decentralization`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `history`
--
ALTER TABLE `history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT cho bảng `origin`
--
ALTER TABLE `origin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `period`
--
ALTER TABLE `period`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT cho bảng `position`
--
ALTER TABLE `position`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `province`
--
ALTER TABLE `province`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `rank`
--
ALTER TABLE `rank`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT cho bảng `staff_kpi_order`
--
ALTER TABLE `staff_kpi_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT cho bảng `stock`
--
ALTER TABLE `stock`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT cho bảng `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tax`
--
ALTER TABLE `tax`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `unit`
--
ALTER TABLE `unit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT cho bảng `ware_house`
--
ALTER TABLE `ware_house`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
