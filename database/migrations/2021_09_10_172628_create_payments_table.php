<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('payments_code', 200)->nullable()->comment('Mã hình thức thanh toán');
            $table->string('payments_name', 200)->nullable()->comment('Tên hình thức thanh toán');
            $table->integer('payments_brand_id')->nullable()->comment('chi nhánh');
            $table->string('payments_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('payments_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('payments_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('payments_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
