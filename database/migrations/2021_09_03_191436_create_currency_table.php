<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('currency_code', 200)->nullable()->comment('Mã tiền tệ');
            $table->string('currency_name', 200)->nullable()->comment('Tên tiền tệ');
            $table->string('currency_symbol', 50)->nullable()->comment('kí hiệu');
            $table->string('currency_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('currency_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('currency_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('currency_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currency');
    }
}
