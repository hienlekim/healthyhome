<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('staff_code', 200)->nullable()->comment('Mã nhân viên');
            $table->string('staff_name', 200)->nullable()->comment('Tên nhân viên');
            $table->boolean('staff_sex')->comment('Giới tính');
            $table->string('staff_avatar', 255)->nullable()->comment('ảnh nhân viên');
            $table->date('staff_year_old')->nullable()->comment('ngày tháng năm sinh');
            $table->integer('staff_brand_id')->nullable()->comment('thuộc chi nhánh');
            $table->integer('staff_rank_id')->nullable()->comment('cấp bậc nhân viên đối với thủ kho hay trưởng chi nhánh sẽ không có');
            $table->date('staff_date_start')->nullable()->comment('Ngày bắt đầu vào làm');
            $table->string('staff_number_bank',100)->nullable()->comment('Số tài khoản ngân hàng');
            $table->string('staff_address',255)->nullable()->comment('địa chỉ nhà');
            $table->integer('staff_user_id')->nullable()->comment('tài khoản đăng nhập');
            $table->integer('staff_position_id')->nullable()->comment('chức vụ đảm nhận');
            $table->integer('staff_parent_id')->nullable()->comment('nhân viên cấp cha');
            $table->boolean('staff_status')->nullable()->comment('trạng thái');
            $table->string('staff_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('staff_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('staff_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('staff_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
