<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffKpiOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_kpi_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('staff_kpi_order_staff_id')->nullable()->comment('nhân viên');
            $table->integer('staff_kpi_order_stock_id')->nullable()->comment('đơn hàng');
            $table->integer('staff_kpi_order_brand_id')->nullable()->comment('chi nhánh');
            $table->integer('staff_kpi_order_period_id')->nullable()->comment('hình thức thanh toán');
            $table->integer('staff_kpi_order_rank_id')->nullable()->comment('Tên hình thức thanh toán');
            $table->decimal('staff_kpi_order_commission',13, 4)->nullable()->comment('mức hoa hồng');
            $table->decimal('staff_kpi_order_commission_amount',13, 4)->nullable()->comment('tiền thực lãnh');
            $table->string('staff_kpi_order_actual_month', 200)->nullable()->comment('Tháng thực lãnh');
            $table->integer('staff_kpi_order_currency_id')->nullable()->comment('mệnh giá tiền tệ');
            $table->boolean('staff_kpi_order_status')->nullable()->comment('Trạng thái');
            $table->string('staff_kpi_order_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('staff_kpi_order_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('staff_kpi_order_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('staff_kpi_order_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_kpi_order');
    }
}
