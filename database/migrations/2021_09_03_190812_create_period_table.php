<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('period_code',100)->nullable()->comment('Mã đợt thanh toán');
            $table->string('period_name',255)->nullable()->comment('Đợt thanh toán');
            $table->integer('period_brand_id')->nullable()->comment('chi nhánh');
            $table->integer('period_payments_id')->nullable()->comment('hình thức thanh toán');
            $table->integer('period_stock_id')->nullable()->comment('Đơn hàng');
            $table->decimal('period_amount',13, 4)->nullable()->comment('số tiền thanh toán');
            $table->date('period_date')->nullable()->comment('ngày thanh toán theo hợp đồng');
            $table->date('period_date_reality')->nullable()->comment('ngày khách hàng thanh toán');
            $table->boolean('period_status')->nullable()->comment('Trạng thái');
            $table->string('period_description',255)->nullable()->comment('Nội dung');
            $table->string('period_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('period_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('period_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('period_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('period');
    }
}
