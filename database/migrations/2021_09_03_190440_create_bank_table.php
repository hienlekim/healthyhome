<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bank_code', 200)->nullable()->comment('Mã ngân hàng');
            $table->string('bank_name', 200)->nullable()->comment('Tên ngân hàng');
            $table->string('bank_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('bank_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('bank_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('bank_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank');
    }
}
