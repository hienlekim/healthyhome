<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_images_id')->nullable()->comment('sản phẩm');
            $table->integer('receipts_images_id')->nullable()->comment('Phiếu thu');
            $table->string('images',255)->nullable()->comment('hình ảnh');
            $table->string('images_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('images_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('images_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('images_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
