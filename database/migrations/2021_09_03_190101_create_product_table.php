<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->string('product_code', 200)->nullable()->comment('Mã sản phẩm');
            $table->string('product_name', 200)->nullable()->comment('Tên sản phẩm');
            $table->string('product_content', 255)->nullable()->comment('nội dung ngắn');
            $table->string('product_description', 2000)->nullable()->comment('nội dung mô tả');
            $table->integer('product_origin_id')->nullable()->comment('xuất xứ');
            $table->string('product_size', 100)->nullable()->comment('kích thước');
            $table->string('product_material',255)->nullable()->comment('chất liệu');
            $table->integer('product_brand_id')->nullable()->comment('chi nhánh');
            $table->string('product_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('product_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('product_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('product_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
