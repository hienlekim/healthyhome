<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customers_code', 200)->nullable()->comment('Mã khách hàng');
            $table->string('customers_name', 200)->nullable()->comment('Tên khách hàng');
            $table->string('customers_email', 255)->nullable()->comment('Mã khách hàng');
            $table->string('customers_address', 200)->nullable()->comment('địa chỉ');
//            $table->string('customers_village', 200)->nullable()->comment('xã');
//            $table->string('customers_district', 200)->nullable()->comment('huyện');
//            $table->string('customers_province', 200)->nullable()->comment('Tỉnh/thành phố');
            $table->string('customers_country_id', 200)->nullable()->comment('quốc gia');
            $table->integer('customers_brand_id')->nullable()->comment('chi nhánh');
            $table->integer('customers_parent_id')->nullable()->comment('khách hàng cấp cha');
            $table->string('customers_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('customers_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->boolean('customers_status')->nullable()->comment('trạng thái khách hàng');
            $table->timestamp('customers_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('customers_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
