<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('commission_rank_id')->nullable()->comment('cấp độ');
            $table->integer('commission_currency_id')->nullable()->comment('loại tiên tệ');
            $table->integer('commission_brand_id')->nullable()->comment('loại tiên tệ');
            $table->decimal('commission_amount',13, 4)->nullable()->comment('số tiền hoa hồng được nhận');
            $table->string('commission_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('commission_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('commission_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('commission_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission');
    }
}
