<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvinceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province', function (Blueprint $table) {
            $table->bigIncrements('prov_id');
            $table->string('prov_code', 5)->nullable()->index('prov_code');
            $table->string('prov_prefix', 50)->nullable();
            $table->string('prov_name', 100)->nullable()->index('prov_name');
            $table->string('prov_type', 30)->nullable();
            $table->string('prov_region', 20)->nullable();
            $table->string('prov_position', 50)->nullable();
            $table->text('prov_meta')->nullable();
            $table->text('prov_map_sharp')->nullable();
            $table->boolean('prov_status')->nullable();
            $table->string('prov_user_created', 200)->nullable();
            $table->string('prov_user_updated', 200)->nullable();
            $table->timestamp('prov_created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('prov_updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('province');
    }
}
