<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionRankTable extends Migration
{
    /**
     * Run the migrations.
     * salses manage
     * @return void
     */
    public function up()
    {
        Schema::create('commission_rank', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('commission_rank_staff_parent_id')->nullable()->comment('nhân viên cấp quản lý ');
            $table->integer('commission_rank_staff_id')->nullable()->comment('đơn hàng nhân viên cấp con');
            $table->integer('commission_rank_brand_id')->nullable()->comment('chi nhánh');
            $table->integer('commission_rank_period_id')->nullable()->comment('hình thức thanh toán');
            $table->integer('commission_rank_rank_id')->nullable()->comment('Tên hình thức thanh toán');
            $table->decimal('commission_rank_commission',13, 4)->nullable()->comment('mức hoa hồng');
            $table->decimal('commission_rank_commission_amount',13, 4)->nullable()->comment('tiền thực lãnh');
            $table->string('commission_rank_actual_month', 200)->nullable()->comment('Tháng thực lãnh');
            $table->integer('commission_rank_currency_id')->nullable()->comment('mệnh giá tiền tệ');
            $table->boolean('commission_rank_status')->nullable()->comment('Trạng thái');
            $table->string('commission_rank_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('commission_rank_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('commission_rank_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('commission_rank_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_rank');
    }
}
