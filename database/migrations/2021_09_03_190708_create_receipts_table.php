<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('receipts_code', 200)->nullable()->comment('mã phiếu');
            $table->string('receipts_type', 200)->nullable()->comment('Loại phiếu');
            $table->integer('receipts_stock_id')->nullable()->comment('đơn hàng');
            $table->integer('receipts_customer_id')->nullable()->comment('khách hàng');
            $table->integer('receipts_bank_id')->nullable()->comment('ngân hàng');
            $table->integer('receipts_staff_id')->nullable()->comment('nhân viên lập phiếu');
            $table->integer('receipts_staff_approve_id')->nullable()->comment('nhân viên duyệt phiếu');
            $table->dateTime('receipts_staff_approve_date')->nullable()->comment('ngày giờ duyệt');
            $table->integer('receipts_period_id')->nullable()->comment('hình thức thanh toán');
            $table->integer('receipts_supplier_id')->nullable()->comment('nhà cung cấp');
            $table->integer('receipts_brand_id')->nullable()->comment('chi nhánh');
            $table->integer('receipts_currency_id')->nullable()->comment('Tiền tệ sử dụng');
            $table->decimal('receipts_amount',13, 4)->nullable()->comment('số tiền thu ');
            $table->string('receipts_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('receipts_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('receipts_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('receipts_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
