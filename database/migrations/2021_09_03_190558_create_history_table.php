<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('history_code', 200)->nullable()->comment('Mã phiếu');
            $table->string('history_name', 200)->nullable()->comment('Tên phiếu');
            $table->string('history_content', 200)->nullable()->comment('nội dung');
            $table->integer('history_staff_id')->nullable()->comment('Nhân viên thực hiện');
            $table->string('history_action', 200)->nullable()->comment('hành động');
            $table->decimal('history_price',13, 4)->nullable()->comment('số tiền nếu có');
            $table->string('history_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('history_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('history_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('history_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history');
    }
}
