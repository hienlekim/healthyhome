<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('supplier_code', 200)->nullable()->comment('Mã ncc');
            $table->string('supplier_name', 200)->nullable()->comment('Tên ncc');
            $table->string('supplier_address', 200)->nullable()->comment('địa chỉ');
//            $table->string('supplier_village', 200)->nullable()->comment('xã');
//            $table->string('supplier_district', 200)->nullable()->comment('huyện');
//            $table->string('supplier_province', 200)->nullable()->comment('Tỉnh/thành phố');
            $table->string('supplier_country_id', 200)->nullable()->comment('quốc gia');
            $table->integer('supplier_brand_id')->nullable()->comment('chi nhánh');
            $table->string('supplier_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('supplier_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('supplier_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('supplier_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
