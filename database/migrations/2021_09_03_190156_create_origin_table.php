<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOriginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('origin', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('origin_code', 200)->nullable()->comment('Mã xuất xứ');
            $table->string('origin_name', 200)->nullable()->comment('Tên xuất xứ');
            $table->string('origin_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('origin_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('origin_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('origin_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('origin');
    }
}
