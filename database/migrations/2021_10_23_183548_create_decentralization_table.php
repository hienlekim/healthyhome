<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecentralizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decentralization', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('decen_position_id')->nullable()->comment('chức vụ');
            $table->boolean('decen_general_directory')->nullable()->comment('Cấu hình chung');
            $table->boolean('decen_history_system')->nullable()->comment('Lịch sử');
            $table->boolean('decen_formality_payment')->nullable()->comment('thanh toán chính thức');
            $table->boolean('decen_receipts')->nullable()->comment('phiếu thu');
            $table->boolean('decen_payment')->nullable()->comment('thanh toán');
            $table->boolean('decen_approve_receipts')->nullable()->comment('duyệt phiếu thu');
            $table->boolean('decen_approve_payment')->nullable()->comment('duyệt thanh toán');
            $table->boolean('decen_Staff')->nullable()->comment('nhân viên');
            $table->boolean('decen_kpi_sales')->nullable()->comment('kpi nhân viên sales');
            $table->boolean('decen_position')->nullable()->comment('chức vụ');
            $table->boolean('decen_rank')->nullable()->comment('cấp bậc');
            $table->boolean('decen_commission_rank')->nullable()->comment('cấp bậc hoa hồng');
            $table->boolean('decen_commission')->nullable()->comment('hoa hồng');
            $table->boolean('decen_supplier')->nullable()->comment('nhà cung cấp');
            $table->boolean('decen_supplier_order_list')->nullable()->comment('danh sách đơn hàng nhà cung cấp');
            $table->boolean('decen_supplier_debt_list')->nullable()->comment('danh sách công nợ nahf cung cấp');
            $table->boolean('decen_customers')->nullable()->comment('Khách hàng');
            $table->boolean('decen_customer_debt_list')->nullable()->comment('danh sách công nợ khách hàng');
            $table->boolean('decen_customers_care')->nullable()->comment('danh sách khách hàng cần quan tâm');
            $table->boolean('decen_customers_receive_gifts')->nullable()->comment('danh sách khách hàng nhận quà');
            $table->boolean('decen_product')->nullable()->comment('sản phẩm');
            $table->boolean('decen_product_cate')->nullable()->comment('loại sản phẩm');
            $table->boolean('decen_purchases')->nullable()->comment('nhập kho');
            $table->boolean('decen_successful_order')->nullable()->comment('danh sách đơn hàng nhà cung cấp');
            $table->boolean('decen_inventory')->nullable()->comment('kiểm kho');
            $table->boolean('decen_synthesis_report')->nullable()->comment('báo cáo');
            $table->boolean('decen_Order')->nullable()->comment('sản phẩm');
            $table->boolean('decen_installment_orders')->nullable()->comment('loại sản phẩm');
            $table->boolean('decen_full_payment_order')->nullable()->comment('nhập kho');
            $table->boolean('decen_gift_order')->nullable()->comment('danh sách đơn hàng nhà cung cấp');
            $table->boolean('decen_payment_methods')->nullable()->comment('kiểm kho');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decentralization');
    }
}
