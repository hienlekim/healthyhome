<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unit_code', 200)->nullable()->comment('Mã đơn vị');
            $table->string('unit_name', 200)->nullable()->comment('Tên đơn vị');
            $table->string('unit_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('unit_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('unit_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('unit_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit');
    }
}
