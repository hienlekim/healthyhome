<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('position_code', 200)->nullable()->comment('Mã chức vụ');
            $table->string('position_name', 200)->nullable()->comment('Tên chức vụ');
            $table->string('position_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('position_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('position_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('position_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position');
    }
}
