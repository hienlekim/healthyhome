<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock_code', 200)->nullable()->comment('Mã đơn hàng');
            $table->string('stock_seri_number', 200)->nullable()->comment('seri để bảo hành');
            $table->string('stock_type', 200)->nullable()->comment('kiểu đơn hàng');
            $table->integer('stock_product_id')->nullable()->comment('sản phẩm');
            $table->integer('stock_supplier_id')->nullable()->comment('nhà cung cấp');
            $table->integer('stock_customer_id')->nullable()->comment('khách hàng');
            $table->integer('stock_warranty_form_id')->nullable()->comment('Hình thức bảo hành');
            $table->integer('stock_product_unit_id')->nullable()->comment('đơn vị tính');
            $table->integer('stock_product_unit_covert_id')->nullable()->comment('đơn vị tính quy đổi');
            $table->decimal('stock_quality_reality',13, 4)->nullable()->comment('số lượng thực tế');
            $table->decimal('stock_quality_convert',13, 4)->nullable()->comment('số lượng đã đổi');
            $table->decimal('stock_price',13, 4)->nullable()->comment('giá sản phẩm');
            $table->decimal('stock_price_export',13, 4)->nullable()->comment('giá xuất bán');
            $table->integer('stock_currency_id')->nullable()->comment('đơn vị tiền tệ');
            $table->integer('stock_amount_tax_id')->nullable()->comment('thuế');
            $table->integer('stock_amount_tax')->nullable()->comment('mức thuế');
            $table->integer('stock_location_Out')->nullable()->comment('Từ kho');
            $table->integer('stock_location_Int')->nullable()->comment('Đến kho');
            $table->integer('stock_location')->nullable()->comment('kho');
            $table->decimal('stock_amount',13, 4)->nullable()->comment('thành tiền');
            $table->decimal('stock_costs_incurred',13, 4)->nullable()->comment('chi phí phát sinh');
            $table->decimal('stock_amount_total',13, 4)->nullable()->comment('tổng tiền');
            $table->string('stock_description', 200)->nullable()->comment('nội dung mô tả');
            $table->integer('stock_staff_id')->nullable()->comment('nhân viên thực hiện xuất đơn');
            $table->integer('stock_staff_stocker_id')->nullable()->comment('thủ kho duyệt xuất');
            $table->dateTime('stock_date_start_stocker')->nullable()->comment('ngày thủ kho duyệt');
            $table->integer('stock_brand_id')->nullable()->comment('chi nhánh');
            $table->integer('stock_status')->nullable()->comment('trạng thái đơn hàng, status=1 tạo đơn nhập hoặc xuất,status=2 thủ kho duyệt');
            $table->string('stock_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('stock_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('stock_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('stock_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
