<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank', function (Blueprint $table) {
            $table->bigIncrements('rank_id');
            $table->string('rank_code', 200)->nullable()->comment('Mã cấp bậc');
            $table->string('rank_name', 200)->nullable()->comment('Tên cấp độ');
            $table->string('rank_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('rank_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('rank_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('rank_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank');
    }
}
