<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarrantyFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warranty_form', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('warranty_form_code', 200)->nullable()->comment('Mã hình thức bảo hành');
            $table->string('warranty_form_name', 200)->nullable()->comment('Tên hình thức bảo hành');
            $table->string('warranty_form_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('warranty_form_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('warranty_form_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('warranty_form_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warranty_form');
    }
}
