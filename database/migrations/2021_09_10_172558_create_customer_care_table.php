<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerCareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_care', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_staff_id')->nullable()->comment('Nhân sự tiếp nhận');
            $table->integer('customer_id')->nullable()->comment('khách hàng cần chăm sóc');
            $table->integer('customer_brand_id')->nullable()->comment('chi nhánh');
            $table->date('customer_care_date')->nullable()->comment('ngày chăm sóc khách hàng');
            $table->boolean('customer_care_status')->nullable()->comment('trạng thái');
            $table->string('warranty_form_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('warranty_form_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('warranty_form_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('warranty_form_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_care');
    }
}
