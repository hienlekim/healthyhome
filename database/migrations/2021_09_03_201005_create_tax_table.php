<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tax_code', 200)->nullable()->comment('Mã thuế');
            $table->string('tax_name', 200)->nullable()->comment('Tên thuế');
            $table->string('tax_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('tax_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('tax_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('tax_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax');
    }
}
