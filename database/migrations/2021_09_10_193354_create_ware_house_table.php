<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWareHouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ware_house', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ware_house_code', 200)->nullable()->comment('Mã kho');
            $table->string('ware_house_name', 200)->nullable()->comment('Tên kho');
            $table->integer('ware_house_brand_id')->nullable()->comment('chi nhánh');
            $table->string('ware_house_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('ware_house_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('ware_house_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('ware_house_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ware_house');
    }
}
