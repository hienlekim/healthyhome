<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand_code', 200)->nullable()->comment('Mã chi nhánh');
            $table->string('brand_name', 200)->nullable()->comment('Tên chi nhánh');
            $table->string('brand_address', 200)->nullable()->comment('địa chỉ');
//            $table->string('brand_village', 200)->nullable()->comment('xã');
//            $table->string('brand_district', 200)->nullable()->comment('huyện');
//            $table->string('brand_province', 200)->nullable()->comment('Tỉnh/thành phố');
            $table->integer('brand_ware_house_id')->nullable()->comment('mỗi chi nhánh có kho riêng');
            $table->integer('brand_currency_id')->nullable()->comment('mỗi chi nhánh có quy định tiền tệ riêng');
            $table->string('brand_country_id', 200)->nullable()->comment('quốc gia');
            $table->string('brand_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('brand_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('brand_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('brand_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand');
    }
}
