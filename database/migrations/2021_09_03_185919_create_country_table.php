<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('country_code', 200)->nullable()->comment('Mã quốc gia');
            $table->string('country_name', 200)->nullable()->comment('Tên quốc gia');
            $table->string('country_user_created', 200)->nullable()->comment('Người tạo');
            $table->string('country_user_updated', 200)->nullable()->comment('Người cập nhât');
            $table->timestamp('country_created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('country_updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
    }
}
