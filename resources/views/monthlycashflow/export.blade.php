<table>
    <tr>
        <td colspan="10" style="text-align: center;font-weight: bold;font-size: 15px">
            <span>Monthly Cash Flow</span>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: left;font-size: 13px">
            <span>Branch : {{$branch}}</span>
        </td>
        <td colspan="2" style="text-align: left;font-size: 13px">
            <span>Currency : {{$currency}}</span>
        </td>
        <td colspan="5" style="text-align: right;font-size: 13px">
            <span>{{$date}}</span>
        </td>
    </tr>
    <tr>
        <td colspan="5" style="text-align: left;font-size: 13px">
            <span>implementation staff :  {{$staff_name}}</span>
        </td>
        <td colspan="5" style="text-align: right;font-size: 13px">
            <span>Position : {{$position}}</span>
        </td>
    </tr>
    <tr style="background: #3bafda;color: white">
        <th style="border: 1px solid #dee2e6;width: 20px;background: #3bafda;color: #FFFFFF" >Month</th>
        <th style="border: 1px solid #dee2e6;width: 20px;background: #3bafda;color: #FFFFFF" >Dealer</th>
        <th style="border: 1px solid #dee2e6;width: 25px;background: #3bafda;color: #FFFFFF" >Customer</th>
        <th style="border: 1px solid #dee2e6;width: 25px;background: #3bafda;color: #FFFFFF" >Date</th>
        <th style="border: 1px solid #dee2e6;width: 10px;background: #3bafda;color: #FFFFFF" >Orders Code</th>
        <th style="border: 1px solid #dee2e6;width: 10px;background: #3bafda;color: #FFFFFF" >Payment Type</th>
        <th style="border: 1px solid #dee2e6;width: 60px;background: #3bafda;color: #FFFFFF">Note</th>
        @foreach($listmonthlYearycash as $k)
            <th style="border: 1px solid #dee2e6;width: 25px;background: #3bafda;color: #FFFFFF">{{$k['monthyear']}}</th>
        @endforeach
    </tr>
    @if($listAll)
        @foreach($listAll as $k=>$key)
            @if($key['tRows']==1)
                @foreach($key['data'] as $ks1)
                    @if($ks1['tRowsChild']==1)
                        @foreach($ks1['listChildrent_2'] as $vs)
                            <tr>
                                <td style="border: 1px solid #343a40">{{$key['months']}}</td>
                                <td style="border: 1px solid #343a40">{{$ks1['staff']}}</td>
                                <td style="border: 1px solid #343a40">{{$ks1['customer']}}</td>
                                <td style="border: 1px solid #343a40">{{date_format($vs['date'],"d/m/Y")}}</td>
                                <td style="border: 1px solid #343a40">{{$vs['ordersCode']}}</td>
                                <td style="border: 1px solid #343a40">{{$vs['payment']}}</td>
                                <td style="border: 1px solid #343a40"></td>
                                @foreach($vs['customersPeriodDate'] as $v)
                                    @if($v==0)
                                        <td style="border: 1px solid #343a40"></td>
                                    @else
                                        <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                    @endif
                                @endforeach
                            </tr>
                        @endforeach
                    @elseif($ks1['tRowsChild']>1)
                        <?php $t=0; ?>
                        @foreach($ks1['listChildrent_2'] as $ks)
                            <?php $t+=1; ?>
                            @if($t==1)
                                <tr>
                                    <td style="border: 1px solid #343a40" rowspan="{{$ks['tRowsChild']+$ks['tRowsChild']}}">{{$key['months']}}</td>
                                    <td style="border: 1px solid #343a40" rowspan="{{$ks['tRowsChild']+$ks['tRowsChild']}}">{{$ks1['staff']}}</td>
                                    <td style="border: 1px solid #343a40" rowspan="{{$ks['tRowsChild']+$ks['tRowsChild']}}">{{$ks1['customer']}}</td>
                                    <td style="border: 1px solid #343a40">{{date_format($ks['date'],"d/m/Y")}}</td>
                                    <td style="border: 1px solid #343a40">{{$ks['ordersCode']}}</td>
                                    <td style="border: 1px solid #343a40">{{$ks['payment']}}</td>
                                    <td style="border: 1px solid #343a40"></td>
                                    @foreach($ks['customersPeriodDate'] as $v)
                                        @if($v==0)
                                            <td style="border: 1px solid #343a40"></td>
                                        @else
                                            <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @else
                                <tr>
                                    <td style="border: 1px solid #343a40">{{date_format($ks['date'],"d/m/Y")}}</td>
                                    <td style="border: 1px solid #343a40">{{$ks['ordersCode']}}</td>
                                    <td style="border: 1px solid #343a40">{{$ks['payment']}}</td>
                                    <td style="border: 1px solid #343a40"></td>
                                    @foreach($ks['customersPeriodDate'] as $v)
                                        @if($v==0)
                                            <td style="border: 1px solid #343a40"></td>
                                        @else
                                            <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endif
                        @endforeach
                    @endif
                @endforeach
                {{--  kết thúc dk 1--}}
            @elseif($key['tRows']>1)
                <?php $t=0; ?>
                @foreach($key['data'] as $ks1)
                    <?php $t +=1; ?>
                    <?php $t2=0; ?>
                    @foreach($ks1['listChildrent_2'] as $vs)
                        @if($t==1 && $ks1['tRowsChild']==1)
                            @if($ks1['tRowsChild']==1)
                                <tr>
                                    <td style="border: 1px solid #343a40" rowspan="{{$key['tRows']}}">{{$key['months']}}</td>
                                    <td style="border: 1px solid #343a40">{{$ks1['staff']}}</td>
                                    <td style="border: 1px solid #343a40">{{$ks1['customer']}}</td>
                                    <td style="border: 1px solid #343a40">{{date_format($vs['date'],"d/m/Y")}}</td>
                                    <td style="border: 1px solid #343a40">{{$vs['ordersCode']}}</td>
                                    <td style="border: 1px solid #343a40">{{$vs['payment']}}</td>
                                    <td style="border: 1px solid #343a40"></td>
                                    @foreach($vs['customersPeriodDate'] as $v)
                                        @if($v==0)
                                            <td style="border: 1px solid #343a40"></td>
                                        @else
                                            <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endif
                        @elseif($t==1 && $ks1['tRowsChild']>1)
                            <?php $t2+=1; ?>
                            @if($ks1['tRowsChild']>1)
                                @if($t2==1)
                                    <tr>
                                        <td style="border: 1px solid #343a40" rowspan="{{$key['tRows']}}">{{$key['months']}}</td>
                                        <td style="border: 1px solid #343a40" rowspan="{{$ks1['tRowsChild']}}">{{$ks1['staff']}}</td>
                                        <td style="border: 1px solid #343a40" rowspan="{{$ks1['tRowsChild']}}">{{$ks1['customer']}}</td>
                                        <td style="border: 1px solid #343a40">{{date_format($vs['date'],"d/m/Y")}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['ordersCode']}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['payment']}}</td>
                                        <td style="border: 1px solid #343a40"></td>
                                        @foreach($vs['customersPeriodDate'] as $v)
                                            @if($v==0)
                                                <td style="border: 1px solid #343a40"></td>
                                            @else
                                                <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @else
                                    <tr>
                                        <td style="border: 1px solid #343a40">{{date_format($vs['date'],"d/m/Y")}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['ordersCode']}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['payment']}}</td>
                                        <td style="border: 1px solid #343a40"></td>
                                        @foreach($vs['customersPeriodDate'] as $v)
                                            @if($v==0)
                                                <td style="border: 1px solid #343a40"></td>
                                            @else
                                                <td  @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endif
                            @endif
                        @else
                            <?php $t2+=1; ?>
                            @if($ks1['tRowsChild']>1)
                                @if($t2==1)
                                    <tr>
                                        <td style="border: 1px solid #343a40" rowspan="{{$ks1['tRowsChild']}}">{{$ks1['staff']}}</td>
                                        <td style="border: 1px solid #343a40" rowspan="{{$ks1['tRowsChild']}}">{{$ks1['customer']}}</td>
                                        <td style="border: 1px solid #343a40">{{date_format($vs['date'],"d/m/Y")}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['ordersCode']}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['payment']}}</td>
                                        <td style="border: 1px solid #343a40"></td>
                                        @foreach($vs['customersPeriodDate'] as $v)
                                            @if($v==0)
                                                <td style="border: 1px solid #343a40"></td>
                                            @else
                                                <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @else
                                    <tr>
                                        <td style="border: 1px solid #343a40">{{date_format($vs['date'],"d/m/Y")}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['ordersCode']}}</td>
                                        <td style="border: 1px solid #343a40">{{$vs['payment']}}</td>
                                        <td style="border: 1px solid #343a40"></td>
                                        @foreach($vs['customersPeriodDate'] as $v)
                                            @if($v==0)
                                                <td style="border: 1px solid #dee2e6"></td>
                                            @else
                                                <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                @endif
                            @elseif($ks1['tRowsChild']==1)
                                <tr>
                                    <td style="border: 1px solid #343a40">{{$ks1['staff']}}</td>
                                    <td style="border: 1px solid #343a40">{{$ks1['customer']}}</td>
                                    <td style="border: 1px solid #343a40">{{date_format($vs['date'],"d/m/Y")}}</td>
                                    <td style="border: 1px solid #343a40">{{$vs['ordersCode']}}</td>
                                    <td style="border: 1px solid #343a40">{{$vs['payment']}}</td>
                                    <td style="border: 1px solid #343a40"></td>
                                    @foreach($vs['customersPeriodDate'] as $v)
                                        @if($v==0)
                                            <td style="border: 1px solid #343a40"></td>
                                        @else
                                            <td @if($v['period_status']==1) style="background: yellow;border: 1px solid #343a40" @else style="background:#1abc9c;border: 1px solid #343a40" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endif
                        @endif
                    @endforeach
                    {{--kết thúc--}}
                @endforeach
            @endif
        @endforeach
    @else
        <tr>
            <td colspan="6" class="text-center">Content is not available or does not exist</td>
        </tr>
    @endif
{{--    <tr>--}}
{{--        <td colspan="3" style="text-align: left;font-size: 13px;border: 1px solid #6f7071;">--}}
{{--            Tổng (Đơn vị tính: {{$receipts_currency}})--}}
{{--        </td>--}}
{{--        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"> </td>--}}
{{--        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"></td>--}}
{{--        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"></td>--}}
{{--        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"></td>--}}
{{--        <td colspan="3" style="text-align: left;font-size: 13px;border: 1px solid #6f7071;">--}}
{{--            <span>Nhân Viên : {{$staff}}</span>--}}
{{--        </td>--}}
{{--    </tr>--}}
</table>
