@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">Monthly Cash Flow</h2>
                </div>
            </td>
            <td>
{{--                <div class="col-xl-12 text-right">--}}
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/customers/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
{{--                </div>--}}
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/monthlycashflow")}}">
                <div class="row">
                    <div class="col-xl-3 text-left">
{{--                        <div class="dataTables_length" id="products-datatable_length"><label>Display--}}
{{--                                <select class="form-select form-select-sm mx-1" id="limit" name="limit">--}}
{{--                                    <option value="10">10</option>--}}
{{--                                    <option value="20">20</option>--}}
{{--                                    <option value="30">30</option>--}}
{{--                                    <option value="50">50</option>--}}
{{--                                    <option value="100">100</option>--}}
{{--                                    <option value="all">all</option>--}}
{{--                                </select>column</label>--}}
{{--                        </div>--}}
                    </div>

                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                                <button class="btn btn-primary waves-effect waves-light mr-1" id="searchButton"
                                        type="submit"><i class="fe-search"></i>Search</button>
                                <button class="btn btn-success waves-effect waves-light" id="exportButton"
                                        type="button"><i class="fe-align-center"></i> Export</button>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered" id="tablemonthlycashflow" style="min-width: 2500px">
                        <thead style="border-top: none!important; ">
                        <tr style="background: #3bafda;color: white">
                            <th>Month</th>
                            <th>Dealer</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Orders Code</th>
                            <th>Payment Type</th>
                            @foreach($listmonthlYearycash as $k)
                                <th>{{$k['monthyear']}}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        @if($listAll)
                            @foreach($listAll as $k=>$key)
                                @if($key['tRows']==1)
                                    @foreach($key['data'] as $ks1)
                                        @if($ks1['tRowsChild']==1)
                                            @foreach($ks1['listChildrent_2'] as $vs)
                                                <tr>
                                                    <td>{{$key['months']}}</td>
                                                    <td>{{$ks1['staff']}}</td>
                                                    <td>{{$ks1['customer']}}</td>
                                                    <td>{{date_format($vs['date'],"d/m/Y")}}</td>
                                                    <td>{{$vs['ordersCode']}}</td>
                                                    <td>{{$vs['payment']}}</td>
                                                    @foreach($vs['customersPeriodDate'] as $v)
                                                        @if($v==0)
                                                            <td></td>
                                                        @else
                                                            <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                        @endif
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        @elseif($ks1['tRowsChild']>1)
                                            <?php $t=0; ?>
                                            @foreach($ks1['listChildrent_2'] as $ks)
                                                <?php $t+=1; ?>
                                                @if($t==1)
                                                    <tr>
                                                        <td rowspan="{{$ks['tRowsChild']+$ks['tRowsChild']}}">{{$key['months']}}</td>
                                                        <td rowspan="{{$ks['tRowsChild']+$ks['tRowsChild']}}">{{$ks1['staff']}}</td>
                                                        <td rowspan="{{$ks['tRowsChild']+$ks['tRowsChild']}}">{{$ks1['customer']}}</td>
                                                        <td>{{date_format($ks['date'],"d/m/Y")}}</td>
                                                        <td>{{$ks['ordersCode']}}</td>
                                                        <td>{{$ks['payment']}}</td>
                                                        @foreach($ks['customersPeriodDate'] as $v)
                                                            @if($v==0)
                                                                <td></td>
                                                            @else
                                                                <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{date_format($ks['date'],"d/m/Y")}}</td>
                                                        <td>{{$ks['ordersCode']}}</td>
                                                        <td>{{$ks['payment']}}</td>
                                                        @foreach($ks['customersPeriodDate'] as $v)
                                                            @if($v==0)
                                                                <td></td>
                                                            @else
                                                                <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                    {{--                       kết thúc dk 1--}}
                                @elseif($key['tRows']>1)
                                    <?php $t=0; ?>
                                    @foreach($key['data'] as $ks1)
                                        <?php $t +=1; ?>
                                        <?php $t2=0; ?>
                                        @foreach($ks1['listChildrent_2'] as $vs)
                                            @if($t==1 && $ks1['tRowsChild']==1)
                                                @if($ks1['tRowsChild']==1)
                                                    <tr>
                                                        <td rowspan="{{$key['tRows']}}">{{$key['months']}}</td>
                                                        <td>{{$ks1['staff']}}</td>
                                                        <td>{{$ks1['customer']}}</td>
                                                        <td>{{date_format($vs['date'],"d/m/Y")}}</td>
                                                        <td>{{$vs['ordersCode']}}</td>
                                                        <td>{{$vs['payment']}}</td>
                                                        @foreach($vs['customersPeriodDate'] as $v)
                                                            @if($v==0)
                                                                <td></td>
                                                            @else
                                                                <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @elseif($t==1 && $ks1['tRowsChild']>1)
                                                <?php $t2+=1; ?>
                                                @if($ks1['tRowsChild']>1)
                                                    @if($t2==1)
                                                        <tr>
                                                            <td rowspan="{{$key['tRows']}}">{{$key['months']}}</td>
                                                            <td rowspan="{{$ks1['tRowsChild']}}">{{$ks1['staff']}}</td>
                                                            <td rowspan="{{$ks1['tRowsChild']}}">{{$ks1['customer']}}</td>
                                                            <td>{{date_format($vs['date'],"d/m/Y")}}</td>
                                                            <td>{{$vs['ordersCode']}}</td>
                                                            <td>{{$vs['payment']}}</td>
                                                            @foreach($vs['customersPeriodDate'] as $v)
                                                                @if($v==0)
                                                                    <td></td>
                                                                @elseif($v=='x')
                                                                    <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{$v}}-{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                                @else
                                                                    <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                                @endif
                                                            @endforeach
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td>{{date_format($vs['date'],"d/m/Y")}}</td>
                                                            <td>{{$vs['ordersCode']}}</td>
                                                            <td>{{$vs['payment']}}</td>
                                                            @foreach($vs['customersPeriodDate'] as $v)
                                                                @if($v==0)
                                                                    <td></td>
                                                                @elseif($v=='x')
                                                                    <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{$v}}-{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                                @else
                                                                    <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                                @endif
                                                            @endforeach
                                                        </tr>
                                                    @endif
                                                @endif
                                            @else
                                                <?php $t2+=1; ?>
                                                @if($ks1['tRowsChild']>1)
                                                    @if($t2==1)
                                                        <tr>
                                                            <td rowspan="{{$ks1['tRowsChild']}}">{{$ks1['staff']}}</td>
                                                            <td rowspan="{{$ks1['tRowsChild']}}">{{$ks1['customer']}}</td>
                                                            <td>{{date_format($vs['date'],"d/m/Y")}}</td>
                                                            <td>{{$vs['ordersCode']}}</td>
                                                            <td>{{$vs['payment']}}</td>
                                                            @foreach($vs['customersPeriodDate'] as $v)
                                                                @if($v==0)
                                                                    <td></td>
                                                                @else
                                                                    <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                                @endif
                                                            @endforeach
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td>{{date_format($vs['date'],"d/m/Y")}}</td>
                                                            <td>{{$vs['ordersCode']}}</td>
                                                            <td>{{$vs['payment']}}</td>
                                                            @foreach($vs['customersPeriodDate'] as $v)
                                                                @if($v==0)
                                                                    <td></td>
                                                                @else
                                                                    <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                                @endif
                                                            @endforeach
                                                        </tr>
                                                    @endif
                                                @elseif($ks1['tRowsChild']==1)
                                                    <tr>
                                                        <td>{{$ks1['staff']}}</td>
                                                        <td>{{$ks1['customer']}}</td>
                                                        <td>{{date_format($vs['date'],"d/m/Y")}}</td>
                                                        <td>{{$vs['ordersCode']}}</td>
                                                        <td>{{$vs['payment']}}</td>
                                                        @foreach($vs['customersPeriodDate'] as $v)
                                                            @if($v==0)
                                                                <td></td>
                                                            @else
                                                                <td @if($v['period_status']==1) style="background: yellow" @else style="background:#1abc9c" @endif>{{number_format($v['period_amount_batch'],2,",",".")}} {{$v['currency']}}</td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                        {{--kết thúc--}}
                                    @endforeach
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </form>
        <form class="mb-1" method="post" id="formmonthlycashflow" action="{{url("api/monthlycashflow/export")}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-xl-3 pr-3">
                    <div class="mb-2 row">
                        <div class="input-group">
{{--                            <label class="col-form-label pr-2" for="simpleinput">Date Start </label>--}}
                            <input type="hidden" class="form-control" id="fromDates" name="fromDate"
                                   autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 text-right">
                    <div class="mb-2 row">
                        <div class="input-group">
{{--                            <label class="col-form-label pr-1" for="simpleinput">Date End </label>--}}
                            <input type="hidden" class="form-control" id="toDates" name="toDate"
                                   autocomplete="off">
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script>
    $('#exportButton').click(function(){
        let fromDate = $('#fromDate').val();
        let toDate = $('#toDate').val();
        $('#fromDates').val(fromDate);
        $('#toDates').val(toDate);
        $("#formmonthlycashflow").submit();
    })
</script>
@endsection


