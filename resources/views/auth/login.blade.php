<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Đăng nhập - Healthyhome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{asset('favicon.ico') }}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

</head>

<style>

    @media only screen and (min-device-width: 768px){
        #imagesLefft{
            display: inline-block;
        }
    }
    @media only screen and (min-device-width: 200px) and (max-device-width: 767px){
        #imagesLefft{
            display: none;
        }
    }
    .has-error input[type="text"], .has-error input[type="password"]{
        border: 1px solid red;
    }
    body  {
        /*background:  url('http://localhost/healthyhomes/public/assets/image/R.jpg');*/
        /*background-repeat: no-repeat, no-repeat;*/
    }
    .imagesBackground.ribbon-box {
        background: #f7f7f7c9;
    }
    #btnPasswordControl{
        float: right;
        margin-top: -35px;
        margin-right: 10px;
        font-size: 20px;
        cursor: pointer;
    }
    .startcharater{
        color: red;
    }
    #btnPasswordControl:hover{
        color: deepskyblue;
    }
    body.enlarged {
         min-height: 0px!important;
    }
</style>

<body id="body-content" class="page fresh-ui" data-trekkie-device-id="11f36f9c-b7fd-4e8b-acc9-a640bca35be1" style="padding-bottom: 0px!important;">
<style>          html, body {
        background-color: rgb(246, 246, 247);
        color: rgb(32, 34, 35);
    }
</style>
<div style="--p-background:rgb(246, 246, 247);--p-background-hovered:rgb(241, 242, 243);--p-background-pressed:rgb(237, 238, 239);--p-background-selected:rgb(237, 238, 239);--p-surface:rgb(255, 255, 255);--p-surface-neutral:rgb(228, 229, 231);--p-surface-neutral-hovered:rgb(219, 221, 223);--p-surface-neutral-pressed:rgb(201, 204, 208);--p-surface-neutral-disabled:rgb(241, 242, 243);--p-surface-neutral-subdued:rgb(246, 246, 247);--p-surface-subdued:rgb(250, 251, 251);--p-surface-disabled:rgb(250, 251, 251);--p-surface-hovered:rgb(246, 246, 247);--p-surface-pressed:rgb(241, 242, 243);--p-surface-depressed:rgb(237, 238, 239);--p-backdrop:rgba(0, 0, 0, 0.5);--p-overlay:rgba(255, 255, 255, 0.5);--p-shadow-from-dim-light:rgba(0, 0, 0, 0.2);--p-shadow-from-ambient-light:rgba(23, 24, 24, 0.05);--p-shadow-from-direct-light:rgba(0, 0, 0, 0.15);--p-hint-from-direct-light:rgba(0, 0, 0, 0.15);--p-on-surface-background:rgb(241, 242, 243);--p-border:rgb(140, 145, 150);--p-border-neutral-subdued:rgb(186, 191, 195);--p-border-hovered:rgb(153, 158, 164);--p-border-disabled:rgb(210, 213, 216);--p-border-subdued:rgb(201, 204, 207);--p-border-depressed:rgb(87, 89, 89);--p-border-shadow:rgb(174, 180, 185);--p-border-shadow-subdued:rgb(186, 191, 196);--p-divider:rgb(225, 227, 229);--p-icon:rgb(92, 95, 98);--p-icon-hovered:rgb(26, 28, 29);--p-icon-pressed:rgb(68, 71, 74);--p-icon-disabled:rgb(186, 190, 195);--p-icon-subdued:rgb(140, 145, 150);--p-text:rgb(32, 34, 35);--p-text-disabled:rgb(140, 145, 150);--p-text-subdued:rgb(109, 113, 117);--p-interactive:rgb(44, 110, 203);--p-interactive-disabled:rgb(189, 193, 204);--p-interactive-hovered:rgb(31, 81, 153);--p-interactive-pressed:rgb(16, 50, 98);--p-focused:rgb(69, 143, 255);--p-surface-selected:rgb(242, 247, 254);--p-surface-selected-hovered:rgb(237, 244, 254);--p-surface-selected-pressed:rgb(229, 239, 253);--p-icon-on-interactive:rgb(255, 255, 255);--p-text-on-interactive:rgb(255, 255, 255);--p-action-secondary:rgb(255, 255, 255);--p-action-secondary-disabled:rgb(255, 255, 255);--p-action-secondary-hovered:rgb(246, 246, 247);--p-action-secondary-pressed:rgb(241, 242, 243);--p-action-secondary-depressed:rgb(109, 113, 117);--p-action-primary:rgb(0, 128, 96);--p-action-primary-disabled:rgb(241, 241, 241);--p-action-primary-hovered:rgb(0, 110, 82);--p-action-primary-pressed:rgb(0, 94, 70);--p-action-primary-depressed:rgb(0, 61, 44);--p-icon-on-primary:rgb(255, 255, 255);--p-text-on-primary:rgb(255, 255, 255);--p-surface-primary-selected:rgb(241, 248, 245);--p-surface-primary-selected-hovered:rgb(179, 208, 195);--p-surface-primary-selected-pressed:rgb(162, 188, 176);--p-border-critical:rgb(253, 87, 73);--p-border-critical-subdued:rgb(224, 179, 178);--p-border-critical-disabled:rgb(255, 167, 163);--p-icon-critical:rgb(215, 44, 13);--p-surface-critical:rgb(254, 211, 209);--p-surface-critical-subdued:rgb(255, 244, 244);--p-surface-critical-subdued-hovered:rgb(255, 240, 240);--p-surface-critical-subdued-pressed:rgb(255, 233, 232);--p-surface-critical-subdued-depressed:rgb(254, 188, 185);--p-text-critical:rgb(215, 44, 13);--p-action-critical:rgb(216, 44, 13);--p-action-critical-disabled:rgb(241, 241, 241);--p-action-critical-hovered:rgb(188, 34, 0);--p-action-critical-pressed:rgb(162, 27, 0);--p-action-critical-depressed:rgb(108, 15, 0);--p-icon-on-critical:rgb(255, 255, 255);--p-text-on-critical:rgb(255, 255, 255);--p-interactive-critical:rgb(216, 44, 13);--p-interactive-critical-disabled:rgb(253, 147, 141);--p-interactive-critical-hovered:rgb(205, 41, 12);--p-interactive-critical-pressed:rgb(103, 15, 3);--p-border-warning:rgb(185, 137, 0);--p-border-warning-subdued:rgb(225, 184, 120);--p-icon-warning:rgb(185, 137, 0);--p-surface-warning:rgb(255, 215, 157);--p-surface-warning-subdued:rgb(255, 245, 234);--p-surface-warning-subdued-hovered:rgb(255, 242, 226);--p-surface-warning-subdued-pressed:rgb(255, 235, 211);--p-text-warning:rgb(145, 106, 0);--p-border-highlight:rgb(68, 157, 167);--p-border-highlight-subdued:rgb(152, 198, 205);--p-icon-highlight:rgb(0, 160, 172);--p-surface-highlight:rgb(164, 232, 242);--p-surface-highlight-subdued:rgb(235, 249, 252);--p-surface-highlight-subdued-hovered:rgb(228, 247, 250);--p-surface-highlight-subdued-pressed:rgb(213, 243, 248);--p-text-highlight:rgb(52, 124, 132);--p-border-success:rgb(0, 164, 124);--p-border-success-subdued:rgb(149, 201, 180);--p-icon-success:rgb(0, 127, 95);--p-surface-success:rgb(174, 233, 209);--p-surface-success-subdued:rgb(241, 248, 245);--p-surface-success-subdued-hovered:rgb(236, 246, 241);--p-surface-success-subdued-pressed:rgb(226, 241, 234);--p-text-success:rgb(0, 128, 96);--p-decorative-one-icon:rgb(126, 87, 0);--p-decorative-one-surface:rgb(255, 201, 107);--p-decorative-one-text:rgb(61, 40, 0);--p-decorative-two-icon:rgb(175, 41, 78);--p-decorative-two-surface:rgb(255, 196, 176);--p-decorative-two-text:rgb(73, 11, 28);--p-decorative-three-icon:rgb(0, 109, 65);--p-decorative-three-surface:rgb(146, 230, 181);--p-decorative-three-text:rgb(0, 47, 25);--p-decorative-four-icon:rgb(0, 106, 104);--p-decorative-four-surface:rgb(145, 224, 214);--p-decorative-four-text:rgb(0, 45, 45);--p-decorative-five-icon:rgb(174, 43, 76);--p-decorative-five-surface:rgb(253, 201, 208);--p-decorative-five-text:rgb(79, 14, 31);--p-border-radius-base:0.4rem;--p-border-radius-wide:0.8rem;--p-card-shadow:0px 0px 5px var(--p-shadow-from-ambient-light), 0px 1px 2px var(--p-shadow-from-direct-light);--p-popover-shadow:-1px 0px 20px var(--p-shadow-from-ambient-light), 0px 1px 5px var(--p-shadow-from-direct-light);--p-modal-shadow:0px 6px 32px var(--p-shadow-from-ambient-light), 0px 1px 6px var(--p-shadow-from-direct-light);--p-top-bar-shadow:0 2px 2px -1px var(--p-shadow-from-direct-light);--p-override-none:none;--p-override-transparent:transparent;--p-override-one:1;--p-override-visible:visible;--p-override-zero:0;--p-override-loading-z-index:514;--p-button-font-weight:500;--p-choice-size:2rem;--p-icon-size:1rem;--p-choice-margin:0.1rem;--p-control-border-width:0.2rem;--p-text-field-spinner-offset:0.2rem;--p-text-field-focus-ring-offset:-0.4rem;--p-text-field-focus-ring-border-radius:0.7rem;--p-button-group-item-spacing:0.2rem;--p-top-bar-height:68px;--p-contextual-save-bar-height:64px;--p-banner-border-default:inset 0 0.2rem 0 0 var(--p-border), inset 0 0 0 0.2rem var(--p-border);--p-banner-border-success:inset 0 0.2rem 0 0 var(--p-border-success), inset 0 0 0 0.2rem var(--p-border-success);--p-banner-border-highlight:inset 0 0.2rem 0 0 var(--p-border-highlight), inset 0 0 0 0.2rem var(--p-border-highlight);--p-banner-border-warning:inset 0 0.2rem 0 0 var(--p-border-warning), inset 0 0 0 0.2rem var(--p-border-warning);--p-banner-border-critical:inset 0 0.2rem 0 0 var(--p-border-critical), inset 0 0 0 0.2rem var(--p-border-critical);--p-badge-mix-blend-mode:luminosity;--p-badge-font-weight:500;--p-non-null-content:'';--p-thin-border-subdued:0.1rem solid var(--p-border-subdued);--p-duration-1-0-0:100ms;--p-duration-1-5-0:150ms;--p-ease-in:cubic-bezier(0.5, 0.1, 1, 1);--p-ease:cubic-bezier(0.4, 0.22, 0.28, 1); color: var(--p-text);">
    <div class="page-container">
        <div class="row" style="background: #3bafda;">
            <div class="col-md-6">
                <div class="page-main">
                    <div class="page-content">
                        <div class="account-pages mt-5 mb-5 ml-3">
{{--                                <div class="row justify-content-center">--}}
                                    <div class="col-md-12">
                                        <div class="imagesBackground ribbon-box mt-2 mb-2" style="padding: 5px 0px 5px 0px;">
                                            <div class="ribbon-two ribbon-two-warning float-right"><span><a href="#" target="_blank" class="text-white"></a></span></div>
                                            <div class="col-lg-11 mt-5 mb-5">
                                                <div class="text-center w-75 m-auto">
                                                    <a>
                                                        <h2 class="text-primary">Healthy Homes</h2>
                                                    </a>
                                                    <p class="text-muted mb-4 mt-3">Sign in system</p>
                                                </div>

                                                <form method="post">
                                                    {{csrf_field()}}
                                                    <div class="form-group mb-3 @error("email") has-error @enderror">
                                                        <label for="email">Email <b class="startcharater">(*)</b></label>
                                                        <input class="form-control" type="text" style="background: #f5f5f552" name="email" value="{{@old('email')}}" required autocomplete="off">
                                                        @error("email")<span class="form-text text-danger"><small>{{@$message}}</small></span>@enderror
                                                    </div>

                                                    <div class="form-group mb-3 @error("password") has-error @enderror">
                                                        <label for="password">Password <b class="startcharater">(*)</b></label>
                                                        <input id="txtPassword" class="form-control" style="background: #f5f5f552" type="password" name="password" required autocomplete="off">
                                                        <i class="mdi mdi-lock" id="btnPasswordControl"></i>
                                                        @error("password")<span class="form-text text-danger"><small>{{@$message}}</small></span>@enderror
                                                    </div>

{{--                                                    <div class="form-group mb-3">--}}
{{--                                                        <div class="custom-control custom-checkbox">--}}
{{--                                                            <input type="checkbox" class="custom-control-input" name="remember">--}}
{{--                                                            <label class="custom-control-label" for="checkbox-signin">Ghi nhớ</label>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}

                                                    <div class="form-group col-lg-3 mb-0 text-right">
                                                        <button class="btn btn-primary btn-block" type="submit"> log in </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
{{--                                    </div>--}}
                                </div>
                            </div>

                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <svg class="artwork artwork--mobile" viewBox="0 0 1500 1855" fill="none" id="imagesLefft">
                    <g clip-path="url(#clip0)">
                        <rect width="100%" height="100%" fill="#3bafda"></rect>
                        <path d="M-298.062 2049.67C-376.167 1971.57 -376.167 1844.94 -298.062 1766.83L1171.27 297.501L2197.95 1324.18L728.617 2793.51C650.512 2871.62 523.879 2871.62 445.774 2793.51L-298.062 2049.67Z" fill="#3bafda"></path>
                        <path d="M1712.33 340.572C1712.33 655.926 1455.35 911.572 1138.33 911.572C821.323 911.572 564.335 655.926 564.335 340.572C564.335 25.2172 821.323 -230.428 1138.33 -230.428C1455.35 -230.428 1712.33 25.2172 1712.33 340.572Z" fill="#3bafda"></path>
                        <path d="M728.645 740.262L1171.25 296.932L728.28 -146.034C972.923 -390.677 1369.57 -390.677 1614.21 -146.034C1858.85 98.6091 1858.85 495.254 1614.21 739.897C1369.69 984.419 973.317 984.54 728.645 740.262Z" fill="#FFCB67"></path>
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="1500" height="1855" fill="white"></rect>
                        </clipPath>
                    </defs>
                </svg>
            </div>
        </div>
{{--        <svg class="artwork artwork--desktop" viewBox="0 0 536 617" fill="none">--}}
{{--            <g>--}}
{{--                <defs>--}}
{{--                    <rect id="SVGID_1_" width="536" height="617"></rect>--}}
{{--                </defs>--}}
{{--                <clipPath id="SVGID_2_">--}}
{{--                    <use xlink:href="#SVGID_1_" overflow="visible"></use>--}}
{{--                </clipPath>--}}
{{--                <g clip-path="url(#SVGID_2_)">--}}
{{--                    <path fill-rule="evenodd" clip-rule="evenodd" fill="#FFA781" d="M0,616.926V617h915V0H179.949l218.488,218.489L0,616.926z"></path>--}}
{{--                    <g>--}}
{{--                        <path fill="#FFCB67" d="M712.79,218.83c0,173.59-140.72,314.32-314.31,314.32c-86.9,0-165.55-35.26-222.45-92.26l0.35-444.46--}}
{{--        c56.86-56.79,135.38-91.91,222.1-91.91C572.07-95.48,712.79,45.24,712.79,218.83z"></path>--}}
{{--                    </g>--}}
{{--                    <path fill="#008060" d="M398.44,218.49l-222.41,222.4c-56.76-56.86-91.87-135.36-91.87-222.06c0-86.87,35.25-165.51,92.22-222.4--}}
{{--      L398.44,218.49z"></path>--}}
{{--                </g>--}}
{{--            </g>--}}
{{--        </svg>--}}



    </div>

</div>



{{--<div id="global-icon-symbols" class="icon-symbols" data-tg-refresh="global-icon-symbols" data-tg-refresh-always="true"><svg xmlns="http://www.w3.org/2000/svg"><symbol id="next-remove"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M18.263 16l10.07-10.07c.625-.625.625-1.636 0-2.26s-1.638-.627-2.263 0L16 13.737 5.933 3.667c-.626-.624-1.637-.624-2.262 0s-.624 1.64 0 2.264L13.74 16 3.67 26.07c-.626.625-.626 1.636 0 2.26.312.313.722.47 1.13.47s.82-.157 1.132-.47l10.07-10.068 10.068 10.07c.312.31.722.468 1.13.468s.82-.157 1.132-.47c.626-.625.626-1.636 0-2.26L18.262 16z"></path></svg></symbol>--}}
{{--        <symbol id="CircleAlertMajor"><svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0zM9 6a1 1 0 1 1 2 0v4a1 1 0 1 1-2 0V6zm1 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path></svg></symbol></svg></div>--}}
{{--<div id="UIModalContainer"><div style="--p-background:rgb(246, 246, 247);--p-background-hovered:rgb(241, 242, 243);--p-background-pressed:rgb(237, 238, 239);--p-background-selected:rgb(237, 238, 239);--p-surface:rgb(255, 255, 255);--p-surface-neutral:rgb(228, 229, 231);--p-surface-neutral-hovered:rgb(219, 221, 223);--p-surface-neutral-pressed:rgb(201, 204, 208);--p-surface-neutral-disabled:rgb(241, 242, 243);--p-surface-neutral-subdued:rgb(246, 246, 247);--p-surface-subdued:rgb(250, 251, 251);--p-surface-disabled:rgb(250, 251, 251);--p-surface-hovered:rgb(246, 246, 247);--p-surface-pressed:rgb(241, 242, 243);--p-surface-depressed:rgb(237, 238, 239);--p-backdrop:rgba(0, 0, 0, 0.5);--p-overlay:rgba(255, 255, 255, 0.5);--p-shadow-from-dim-light:rgba(0, 0, 0, 0.2);--p-shadow-from-ambient-light:rgba(23, 24, 24, 0.05);--p-shadow-from-direct-light:rgba(0, 0, 0, 0.15);--p-hint-from-direct-light:rgba(0, 0, 0, 0.15);--p-on-surface-background:rgb(241, 242, 243);--p-border:rgb(140, 145, 150);--p-border-neutral-subdued:rgb(186, 191, 195);--p-border-hovered:rgb(153, 158, 164);--p-border-disabled:rgb(210, 213, 216);--p-border-subdued:rgb(201, 204, 207);--p-border-depressed:rgb(87, 89, 89);--p-border-shadow:rgb(174, 180, 185);--p-border-shadow-subdued:rgb(186, 191, 196);--p-divider:rgb(225, 227, 229);--p-icon:rgb(92, 95, 98);--p-icon-hovered:rgb(26, 28, 29);--p-icon-pressed:rgb(68, 71, 74);--p-icon-disabled:rgb(186, 190, 195);--p-icon-subdued:rgb(140, 145, 150);--p-text:rgb(32, 34, 35);--p-text-disabled:rgb(140, 145, 150);--p-text-subdued:rgb(109, 113, 117);--p-interactive:rgb(44, 110, 203);--p-interactive-disabled:rgb(189, 193, 204);--p-interactive-hovered:rgb(31, 81, 153);--p-interactive-pressed:rgb(16, 50, 98);--p-focused:rgb(69, 143, 255);--p-surface-selected:rgb(242, 247, 254);--p-surface-selected-hovered:rgb(237, 244, 254);--p-surface-selected-pressed:rgb(229, 239, 253);--p-icon-on-interactive:rgb(255, 255, 255);--p-text-on-interactive:rgb(255, 255, 255);--p-action-secondary:rgb(255, 255, 255);--p-action-secondary-disabled:rgb(255, 255, 255);--p-action-secondary-hovered:rgb(246, 246, 247);--p-action-secondary-pressed:rgb(241, 242, 243);--p-action-secondary-depressed:rgb(109, 113, 117);--p-action-primary:rgb(0, 128, 96);--p-action-primary-disabled:rgb(241, 241, 241);--p-action-primary-hovered:rgb(0, 110, 82);--p-action-primary-pressed:rgb(0, 94, 70);--p-action-primary-depressed:rgb(0, 61, 44);--p-icon-on-primary:rgb(255, 255, 255);--p-text-on-primary:rgb(255, 255, 255);--p-surface-primary-selected:rgb(241, 248, 245);--p-surface-primary-selected-hovered:rgb(179, 208, 195);--p-surface-primary-selected-pressed:rgb(162, 188, 176);--p-border-critical:rgb(253, 87, 73);--p-border-critical-subdued:rgb(224, 179, 178);--p-border-critical-disabled:rgb(255, 167, 163);--p-icon-critical:rgb(215, 44, 13);--p-surface-critical:rgb(254, 211, 209);--p-surface-critical-subdued:rgb(255, 244, 244);--p-surface-critical-subdued-hovered:rgb(255, 240, 240);--p-surface-critical-subdued-pressed:rgb(255, 233, 232);--p-surface-critical-subdued-depressed:rgb(254, 188, 185);--p-text-critical:rgb(215, 44, 13);--p-action-critical:rgb(216, 44, 13);--p-action-critical-disabled:rgb(241, 241, 241);--p-action-critical-hovered:rgb(188, 34, 0);--p-action-critical-pressed:rgb(162, 27, 0);--p-action-critical-depressed:rgb(108, 15, 0);--p-icon-on-critical:rgb(255, 255, 255);--p-text-on-critical:rgb(255, 255, 255);--p-interactive-critical:rgb(216, 44, 13);--p-interactive-critical-disabled:rgb(253, 147, 141);--p-interactive-critical-hovered:rgb(205, 41, 12);--p-interactive-critical-pressed:rgb(103, 15, 3);--p-border-warning:rgb(185, 137, 0);--p-border-warning-subdued:rgb(225, 184, 120);--p-icon-warning:rgb(185, 137, 0);--p-surface-warning:rgb(255, 215, 157);--p-surface-warning-subdued:rgb(255, 245, 234);--p-surface-warning-subdued-hovered:rgb(255, 242, 226);--p-surface-warning-subdued-pressed:rgb(255, 235, 211);--p-text-warning:rgb(145, 106, 0);--p-border-highlight:rgb(68, 157, 167);--p-border-highlight-subdued:rgb(152, 198, 205);--p-icon-highlight:rgb(0, 160, 172);--p-surface-highlight:rgb(164, 232, 242);--p-surface-highlight-subdued:rgb(235, 249, 252);--p-surface-highlight-subdued-hovered:rgb(228, 247, 250);--p-surface-highlight-subdued-pressed:rgb(213, 243, 248);--p-text-highlight:rgb(52, 124, 132);--p-border-success:rgb(0, 164, 124);--p-border-success-subdued:rgb(149, 201, 180);--p-icon-success:rgb(0, 127, 95);--p-surface-success:rgb(174, 233, 209);--p-surface-success-subdued:rgb(241, 248, 245);--p-surface-success-subdued-hovered:rgb(236, 246, 241);--p-surface-success-subdued-pressed:rgb(226, 241, 234);--p-text-success:rgb(0, 128, 96);--p-decorative-one-icon:rgb(126, 87, 0);--p-decorative-one-surface:rgb(255, 201, 107);--p-decorative-one-text:rgb(61, 40, 0);--p-decorative-two-icon:rgb(175, 41, 78);--p-decorative-two-surface:rgb(255, 196, 176);--p-decorative-two-text:rgb(73, 11, 28);--p-decorative-three-icon:rgb(0, 109, 65);--p-decorative-three-surface:rgb(146, 230, 181);--p-decorative-three-text:rgb(0, 47, 25);--p-decorative-four-icon:rgb(0, 106, 104);--p-decorative-four-surface:rgb(145, 224, 214);--p-decorative-four-text:rgb(0, 45, 45);--p-decorative-five-icon:rgb(174, 43, 76);--p-decorative-five-surface:rgb(253, 201, 208);--p-decorative-five-text:rgb(79, 14, 31);--p-border-radius-base:0.4rem;--p-border-radius-wide:0.8rem;--p-card-shadow:0px 0px 5px var(--p-shadow-from-ambient-light), 0px 1px 2px var(--p-shadow-from-direct-light);--p-popover-shadow:-1px 0px 20px var(--p-shadow-from-ambient-light), 0px 1px 5px var(--p-shadow-from-direct-light);--p-modal-shadow:0px 6px 32px var(--p-shadow-from-ambient-light), 0px 1px 6px var(--p-shadow-from-direct-light);--p-top-bar-shadow:0 2px 2px -1px var(--p-shadow-from-direct-light);--p-override-none:none;--p-override-transparent:transparent;--p-override-one:1;--p-override-visible:visible;--p-override-zero:0;--p-override-loading-z-index:514;--p-button-font-weight:500;--p-choice-size:2rem;--p-icon-size:1rem;--p-choice-margin:0.1rem;--p-control-border-width:0.2rem;--p-text-field-spinner-offset:0.2rem;--p-text-field-focus-ring-offset:-0.4rem;--p-text-field-focus-ring-border-radius:0.7rem;--p-button-group-item-spacing:0.2rem;--p-top-bar-height:68px;--p-contextual-save-bar-height:64px;--p-banner-border-default:inset 0 0.2rem 0 0 var(--p-border), inset 0 0 0 0.2rem var(--p-border);--p-banner-border-success:inset 0 0.2rem 0 0 var(--p-border-success), inset 0 0 0 0.2rem var(--p-border-success);--p-banner-border-highlight:inset 0 0.2rem 0 0 var(--p-border-highlight), inset 0 0 0 0.2rem var(--p-border-highlight);--p-banner-border-warning:inset 0 0.2rem 0 0 var(--p-border-warning), inset 0 0 0 0.2rem var(--p-border-warning);--p-banner-border-critical:inset 0 0.2rem 0 0 var(--p-border-critical), inset 0 0 0 0.2rem var(--p-border-critical);--p-badge-mix-blend-mode:luminosity;--p-badge-font-weight:500;--p-non-null-content:'';--p-thin-border-subdued:0.1rem solid var(--p-border-subdued);--p-duration-1-0-0:100ms;--p-duration-1-5-0:150ms;--p-ease-in:cubic-bezier(0.5, 0.1, 1, 1);--p-ease:cubic-bezier(0.4, 0.22, 0.28, 1); color: var(--p-text);"><div id="UIModalBackdrop" data-tg-refresh="ui-modal-backdrop" class="ui-modal-backdrop"></div><div id="UIModalContents" class="ui-modal-contents" data-tg-refresh="ui-modal-contents"></div></div></div>--}}




</body>


<script src="{{asset('assets/js/vendor.min.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
<script>
    $(document).on("click", "#btnPasswordControl", function (event) {
        if($(this).prop("class") === "mdi mdi-lock"){
            $(this).prop("class", "mdi mdi-lock-open");
            $("#txtPassword").prop("type", "text");
        }else{
            $(this).prop("class", "mdi mdi-lock");
            $("#txtPassword").prop("type", "password");
        }
    })
</script>

</body>
</html>
