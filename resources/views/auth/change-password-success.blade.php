@extends('template')

@section('title', 'Đổi mật khẩu thành công')

@section('content')


<div class="row">
    <div class="col-12">
        <div class="text-center mt-1">
            <i class="h1 mdi mdi mdi-shield-lock-outline text-warning"></i>
            <h3 class="mb-3">Mật khẩu đã được thay đổi</h3>
            <p class="text-muted"> Để bảo vệ tài khoản của bạn, vui lòng đổi mật khẩu hàng tháng hoặc bất cứ khi nào cảm thấy không an toàn </p>

            <button type="button" class="btn btn-success waves-effect waves-light mt-2 mr-1"><i class="mdi mdi mdi-shield-account mr-1"></i> Liên hệ quản trị</button>
            <button type="button" class="btn btn-primary waves-effect waves-light mt-2"><i class="mdi mdi mdi-help-rhombus-outline mr-1"></i> Báo lỗi</button>

        </div>
    </div>
</div>

@endsection('content')
