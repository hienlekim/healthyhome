@extends('template')

@section('title', 'Đổi mật khẩu')

@section('content')

    <div class="row">
        <div class="col-md-4 offset-4">
            <div class="card-box">
                <form role="form" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Mật khẩu hiện tại</label>
                        <input type="password" name="old_password" class="form-control" placeholder="Nhập mật khẩu hiện tại">
                    </div>
                    <div class="form-group">
                        <label>Mật khẩu mới</label>
                        <input type="password" name="new_password" class="form-control" placeholder="Nhập mật khẩu mới">
                        <small class="form-text text-muted">Mật khẩu phải từ 6 ký tự bao gồm khoảng trắng, số và ký tự đặc biệt</small>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Đổi mật khẩu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
