@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
<style>
    .ms-container {
        background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
        width: auto;
        max-width: 370px;
    }
    .selectize-dropdown-header{
        display: none!important;
    }
</style>
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">KPI Sales Sub Manage</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/sub-sales-kpi")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
    <div class="card-box">
        <form class="mb-1" method="get" id="formSearch" action="{{url("api/sub-sales-kpi/customers-period-kpi-sub")}}">
            <div class="row">
                <div class="mb-2 col-md-12">
                    <div class="mb-2 row">
                        <label class="col-md-3 col-form-label" for="simpleinput">Sub Sales<span
                                class="text-danger">*</span></label>
                        <div class="col-md-5">
                            <select id="sales_kpi" class="selectize-drop-header" placeholder="Select sales..." name="sales_kpi">
                                <option value=""></option>
                                @foreach($salesSub as $key)
                                    <option value="{{$key->id}}">{{$key->staff_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light btnSearch">
                                <i class="fe-search mr-1"></i> Search
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mb-2 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" style="min-width: 1200px">
                            <thead style="border-top: none!important; ">
                            <tr>
                                <th style="width: 200px;">Sales Name</th>
                                <th style="width: 200px;">Customers</th>
                                <th style="width: 200px;">Stock Code</th>
                                <th style="width: 200px;">KPI Code</th>
                                <th style="width: 200px;">Brand</th>
                                <th style="width: 200px;">KPI Status</th>
                                <th style="width: 200px;">Action</th>
                            </tr>
                            </thead>
                            <tbody class="contentTable">
                            <?php $t=0 ?>
                            @if($count >0)
                                @foreach($listAll as $key)
                                    @if($key['trowParent']==1){{--nếu có 1 khách hàng--}}
                                        @foreach($key['listCustomer'] as $value){{--vòng lặp ds khách hàng--}}
                                            @if($value['trowOrders']==1){{--nếu chỉ có 1 đơn hàng--}}
                                                @foreach($value['listOrders'] as $val){{--foreach ds đơn hàng--}}
                                                    @if($val['trowKPICode']==1){{--nếu có 1 phiếu kpi được tính--}}
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <tr>
                                                                <td>{{$key['staffs_name']}}</td>
                                                                <td>{{$value['customers_name']}}</td>
                                                                <td>{{$val['stock_code']}}</td>
                                                                <td>{{$v['staff_kpi_code']}}</td>
                                                                <td>{{$v['brand_name']}}</td>
                                                                <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @elseif($val['trowKPICode']>1)
                                                        <?php $t=0; ?>
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <?php $t+=1; ?>
                                                            @if($t==1)
                                                                <tr>
                                                                    <td>{{$key['staffs_name']}}</td>
                                                                    <td>{{$value['customers_name']}}</td>
                                                                    <td>{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @elseif($value['trowOrders']>1){{--nếu chỉ có 1 đơn hàng--}}
                                                <?php $k=0; ?>
                                                @foreach($value['listOrders'] as $val){{--foreach ds đơn hàng--}}
                                                    <?php $k+=1; ?>
                                                    @if($k==1 && $val['trowKPICode']==1){{--nếu ptu đầu tiên có 1 kpi code--}}
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <tr>
                                                                <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                                <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                <td>{{$val['stock_code']}}</td>
                                                                <td>{{$v['staff_kpi_code']}}</td>
                                                                <td>{{$v['brand_name']}}</td>
                                                                <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @elseif($k==1 && $val['trowKPICode']>1)
                                                        <?php $j=0; ?>
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <?php $j+=1; ?>
                                                            @if($j==1)
                                                                <tr>
                                                                    <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                                    <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                    <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        @if($val['trowKPICode']==1)
                                                            @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                <tr>
                                                                    <td>{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($val['trowKPICode']>1)
                                                            <?php $j=0; ?>
                                                            @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                <?php $j+=1; ?>
                                                                @if($j==1)
                                                                    <tr>
                                                                        <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                        <td>{{$v['staff_kpi_code']}}</td>
                                                                        <td>{{$v['brand_name']}}</td>
                                                                        <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                        <td>
                                                                            <div class="form-check font-16 mb-0">
                                                                                <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @else
                                                                    <tr>
                                                                        <td>{{$v['staff_kpi_code']}}</td>
                                                                        <td>{{$v['brand_name']}}</td>
                                                                        <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                        <td>
                                                                            <div class="form-check font-16 mb-0">
                                                                                <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @elseif($key['trowParent']>1)
                                        <?php $kp=0; ?>
                                        @foreach($key['listCustomer'] as $value){{--vòng lặp ds khách hàng--}}
                                            <?php $kp +=1; ?>
                                            @if($kp==1 && $value['trowOrders']==1){{--nếu ptu đầu tiên có 1 đơn hàng--}}
                                                @foreach($value['listOrders'] as $val){{--foreach ds đơn hàng--}}
                                                    @if($val['trowKPICode']==1){{--nếu có 1 phiếu kpi được tính--}}
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <tr>
                                                                <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                                <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                <td>{{$v['staff_kpi_code']}}</td>
                                                                <td>{{$v['brand_name']}}</td>
                                                                <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @elseif($val['trowKPICode']>1)
                                                        <?php $ki=0; ?>
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <?php $ki +=1; ?>
                                                            @if($ki==1)
                                                                <tr>
                                                                    <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                                    <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                    <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @elseif($kp==1 && $value['trowOrders']>1)
                                                @foreach($value['listOrders'] as $val){{--foreach ds đơn hàng--}}
                                                    @if($val['trowKPICode']==1){{--nếu có 1 phiếu kpi được tính--}}
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <tr>
                                                                <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                                <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                <td>{{$v['staff_kpi_code']}}</td>
                                                                <td>{{$v['brand_name']}}</td>
                                                                <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @elseif($val['trowKPICode']>1)
                                                        <?php $kr=0; ?>
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                            <?php $kr +=1; ?>
                                                            @if($kr==1)
                                                                <tr>
                                                                    <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                                    <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                    <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @else
                                                @if($value['trowOrders']==1)
                                                    @foreach($value['listOrders'] as $val){{--foreach ds đơn hàng--}}
                                                        @if($val['trowKPICode']==1){{--nếu có 1 phiếu kpi được tính--}}
                                                            @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                <tr>
                                                                    <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                    <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($val['trowKPICode']>1)
                                                            <?php $ki=0; ?>
                                                            @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                <?php $ki +=1; ?>
                                                                @if($ki==1)
                                                                    <tr>
                                                                        <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                        <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                        <td>{{$v['staff_kpi_code']}}</td>
                                                                        <td>{{$v['brand_name']}}</td>
                                                                        <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                        <td>
                                                                            <div class="form-check font-16 mb-0">
                                                                                <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @else
                                                                    <tr>
                                                                        <td>{{$v['staff_kpi_code']}}</td>
                                                                        <td>{{$v['brand_name']}}</td>
                                                                        <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                        <td>
                                                                            <div class="form-check font-16 mb-0">
                                                                                <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($value['trowOrders']>1)
                                                    <?php $t_oders=0; ?>
                                                    @foreach($value['listOrders'] as $val){{--foreach ds đơn hàng--}}
                                                        <?php $t_oders +=1; ?>
                                                        @if($t_oders==1 && $val['trowKPICode']==1)
                                                            @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                <tr>
                                                                    <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                    <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($t_oders==1 && $val['trowKPICode']>1)
                                                            <?php $kr=0; ?>
                                                            @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                <?php $kr +=1; ?>
                                                                @if($kr==1)
                                                                    <tr>
                                                                        <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                                        <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                        <td>{{$v['staff_kpi_code']}}</td>
                                                                        <td>{{$v['brand_name']}}</td>
                                                                        <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                        <td>
                                                                            <div class="form-check font-16 mb-0">
                                                                                <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @else
                                                                    <tr>
                                                                        <td>{{$v['staff_kpi_code']}}</td>
                                                                        <td>{{$v['brand_name']}}</td>
                                                                        <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                        <td>
                                                                            <div class="form-check font-16 mb-0">
                                                                                <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($val['trowKPICode']==1){{--nếu có 1 phiếu kpi được tính--}}
                                                                @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                <tr>
                                                                    <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                    <td>{{$v['staff_kpi_code']}}</td>
                                                                    <td>{{$v['brand_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @elseif($val['trowKPICode']>1)
                                                                <?php $kr=0; ?>
                                                                @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                                    <?php $kr +=1; ?>
                                                                    @if($kr==1)
                                                                        <tr>
                                                                            <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                            <td>{{$v['staff_kpi_code']}}</td>
                                                                            <td>{{$v['brand_name']}}</td>
                                                                            <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                            <td>
                                                                                <div class="form-check font-16 mb-0">
                                                                                    <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                    <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @else
                                                                        <tr>
                                                                            <td>{{$v['staff_kpi_code']}}</td>
                                                                            <td>{{$v['brand_name']}}</td>
                                                                            <td><span class="badge badge-soft-success">@if($v['staff_kpi_sales_sub_status']=="") Chưa thanh toán @else @endif</span></td>
                                                                            <td>
                                                                                <div class="form-check font-16 mb-0">
                                                                                    <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$v['stt']}}" value="{{$v['id']}}">
                                                                                    <label class="form-check-label" for="listOdersCheck_{{$v['stt']}}">&nbsp;</label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach{{--kết thúc foreach lớn--}}
                                <input type="hidden" value="{{$stt}}" id="array_stt">
                            @else
                                <tr>
                                    <td colspan="7" class="text-center">Content is not available or does not exist</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
        <form class="mb-1" method="post" id="formAdd">
            {{csrf_field()}}
            <div class="row">
                <div class="mb-2 col-md-12">
                    <div class="col-md-5">
                        <input type="hidden" name="staff_kpi_sales_id" id="staff_kpi_sales_id" value="{{$sales}}">
                        <input type="hidden" name="PeriodId" id="PeriodId">
                        <input type="hidden" name="staff_kpi_order_brand_id" id="staff_kpi_order_brand_id" value="@if($Brand['staff_brand_id']==0) 0 @else {{$Brand['brands']['id']}}@endif">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                    <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                        <i class="fe-save mr-1"></i> Save  @if($Brand['decenTLizaTion']['positions']['position_code'] == 'headbranch') @else disabled @endif
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
<script>
$(document).ready(function () {
    let localhost = window.location.hostname;
    $('.publish').click(function(){
        let number_stt = $('#array_stt').val();
        let arrayPeriodId = new Array();
        for(let i = 1; i <= number_stt; i++) {
            if($('#listOdersCheck_' + i).is(':checked')) {
                // arrayId[i] = $('#listOdersCheck_' + i).val();
                let numberCheck = $('#listOdersCheck_' + i).val();
                const myArray = numberCheck.split("-");
                arrayPeriodId[i] = myArray[0];
            }
        }
        $('#PeriodId').val(arrayPeriodId);
    })

    $(document).on("click", ".btnCreate", function (event) {
        let periodId = $('#PeriodId').val();
        let staff_kpi_order_brand_id = $('#staff_kpi_order_brand_id').val();
        let staff_kpi_sales_id = $('#staff_kpi_sales_id').val();
        $('#fadeShow').css('display','block');
        if(staff_kpi_sales_id==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please choose staff sales !')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            $.ajax({
                url:'{{url("api/sub-sales-kpi/create")}}',
                type: "POST",
                dataType: 'json',
                data: {
                    '_token':"{{ csrf_token() }}",
                    'periodId':periodId,
                    'staff_kpi_order_brand_id':staff_kpi_order_brand_id,
                    'staff_kpi_sales_id':staff_kpi_sales_id,
                },
                success: function(data) {
                    console.log(data)
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#origin_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully added new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/sub-sales-kpi")}}"; }, 3000);
                    }
                }
            });
        }
    });
})
</script>
        {{--sucess--}}
        <div class="jq-toast-wrap top-right">
            <style>
                #loader_loaded_success {
                    background-color: #5ba035;
                }
                #loader_loaded_wram {
                    background-color: #da8609;
                }
                #loader_loaded_errors {
                    background-color: #bf441d;
                }
            </style>
            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                <span class="close-jq-toast-single">×</span>
                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
            </div>
        </div>
@endsection

