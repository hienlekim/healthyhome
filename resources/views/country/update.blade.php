@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">Country</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/country")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
    <div class="card-box">
        <form class="mb-1" method="post" id="formAdd">
            {{csrf_field()}}
            <div class="row">
                <div class="mb-2 col-md-6">
                    <input type="hidden" class="form-control" id="country_id"
                           name="country_id" value="{{$data['id']}}">
                    <div class="mb-2 row">
                        <label class="col-md-5 col-form-label" for="simpleinput">Country Code <span
                                class="text-danger">*</span></label>
                        <div class="col-md-7">

                            <input type="txt" class="form-control text-uppercase" id="country_code" value="{{$data['country_code']}}" disabled
                                   name="country_code" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="mb-2 col-md-6">
                    <div class="mb-2 row">
                        <label class="col-md-5 col-form-label" for="simpleinput">Country Name <span
                                class="text-danger">*</span></label>
                        <div class="col-md-7">
                            <input type="txt" class="form-control" id="country_name" name="country_name" value="{{$data['country_name']}}"
                                   autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                    <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                        <i class="fe-save mr-1"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
    <script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
    <script>
        $(document).ready(function () {
            let localhost = window.location.hostname;
            $(document).on("click", ".btnCreate", function (event) {
                let id = $('#country_id').val();
                let country_name = $('#country_name').val();
                if (country_name == "") {
                    $('.jq-toast-loader').attr('id', 'loader_loaded_wram').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-warning fade show')
                    $('#content_success').text('warning!')
                    $('#content_tb').text('please enter the name country!')
                    setTimeout(function () {
                        $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in')
                    }, 3000);
                } else {
                    // setTimeout(() => {
                    $('#fadeShow').css('display', 'block');
                    $.ajax({
                        url: '{{url("api/country/update")}}/' + id,
                        type: "PUT",
                        dataType: 'json',
                        data: {
                            '_token': "{{ csrf_token() }}",
                            'id': id,
                            'country_name': country_name,
                        },
                        success: function (data) {
                            if (data.errors == true) {
                                $('.jq-toast-loader').attr('id', 'loader_loaded_wram').addClass('jq-toast-loaded');
                                $('#successrequest').addClass('jq-icon-warning fade show')
                                $('#content_success').text('warning!')
                                $('#content_tb').text(data.message)
                                setTimeout(function () {
                                    $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');
                                    $('#fadeShow').css('display', 'none');
                                }, 3000);
                                $('#bank_code').focus();
                            } else {
                                $('.jq-toast-loader').attr('id', 'loader_loaded_success').addClass('jq-toast-loaded');
                                $('#successrequest').addClass('jq-icon-success fade show')
                                $('#content_success').text('success!')
                                $('#content_tb').text('Data update successful')
                                setTimeout(function () {
                                    $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');
                                    location.href = "{{url("api/country")}}";
                                }, 3000);
                            }
                        }
                    });
                    // }, 1000);
                }
            });
        })
    </script>
    {{--sucess--}}
    <div class="jq-toast-wrap top-right">
        <style>
            #loader_loaded_success {
                background-color: #5ba035;
            }

            #loader_loaded_wram {
                background-color: #da8609;
            }

            #loader_loaded_errors {
                background-color: #bf441d;
            }
        </style>
        <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
            <span class="jq-toast-loader"
                  style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
            <span class="close-jq-toast-single">×</span>
            <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
        </div>
    </div>
@endsection
