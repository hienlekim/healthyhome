@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">Bank</h2>
                </div>
            </td>
            <td>
                <div class="col-xl-12 text-right">
                    <h2 class="header-title">
                        <a href="{{url("api/bank/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>
                    </h2>
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/bank")}}">
            <div class="row">
                <div class="col-xl-9 text-left">
                    <div class="dataTables_length" id="products-datatable_length"><label>Display
                            <select class="form-select form-select-sm mx-1" id="limit" name="limit">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="all">all</option>
                            </select>Bank</label></div>
                </div>
                <div class="col-xl-3 text-right">
                    <div class="mb-2 row">
                        <div class="input-group">
                            <input type="text" class="form-control" id="searchInput" name="searchInput"
                                   autocomplete="off">
                            <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                    type="submit"><i class="fe-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead style="border-top: none!important; ">
                        <tr>
                            <th style="width: 50px;">#</th>
                            <th style="width: 200px;">Bank Code</th>
                            <th style="width: 200px;">Bank Name</th>
                            <th style="width: 200px;">Bank Number</th>
                            <th style="width: 200px;">Create Date</th>
                            <th style="width: 200px;">Update Date</th>
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        <?php $t=0 ?>
                        @if($count >0)
                            @foreach($results as $key)
                                <?php $t=$t+1 ?>
                                <tr>
                                    <td>{{$t}}</td>
                                    <td class="text-uppercase"><a href="{{url("api/bank/edit/$key->id")}}">{{$key->bank_code}}</a></td>
                                    <td>{{$key->bank_name}}</td>
                                    <td>{{$key->bank_number}}</td>
                                    <td>{{date_format($key->created_at,"d-m-Y H:i:s")}}</td>
                                    <td>{{date_format($key->updated_at,"d-m-Y H:i:s")}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-left">
                <b>Total:</b> <span class="TotalAll">{{$count}}</span>
            </div>
            <div class="col-md-4">
                <div class="dataTables_paginate paging_simple_numbers">{{$results->links()}}</div>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
@endsection


