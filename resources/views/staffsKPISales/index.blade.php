@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
<style>
    .ms-container {
        background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
        width: auto;
        max-width: 370px;
    }
    .selectize-dropdown-header{
        display: none!important;
    }
    .selectize-control.selectize-drop-header.single.plugin-dropdown_header {
        text-align: left;
    }
</style>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">KPI Staff Sales</h2>
                </div>
            </td>
            <td>
                <div class="col-xl-12 text-right">
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/sales-kpi-payment/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/staffs-sales-kpi")}}">
                <div class="row">
                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-0 row">
                            <div class="input-group">
                                <div class="col-md-11 float-right">
                                    <input type="hidden" class="form-control" id="ranks_id" name="ranks_id" value="1">
                                    <select id="searchInput" class="selectize-drop-header" placeholder="Select a staffs sales..." name="searchInput">
                                        <option value="">Customers</option>
                                        @if($chooseCustomers->count() >0)
                                            @foreach($chooseCustomers as $key)
                                                <option value="{{$key->staff_kpi_customers_id}}">{{$key['kpiCustomers']['customers_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-1 float-lg-left pl-lg-0">
                                    <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                            type="submit"><i class="fe-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container-fluid">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="min-width: 1200px">
                                <thead style="border-top: none!important; ">
                                <tr>
                                    <th style="width: 200px;">Customers</th>
                                    <th style="width: 200px;">Stock Code</th>
                                    <th style="width: 200px;">KPI Code</th>
                                    <th style="width: 200px;">KPI Status</th>
                                </tr>
                                </thead>
                                <tbody class="contentTable">
                                <?php $t=0 ?>
                                @if($dbCustomers->count() >0)
                                    @foreach($listCustomer as $value)
                                        @if($value['trowOrders']==1)
                                            @foreach($value['listOrders'] as $val)
                                                @if($val['trowKPICode']==1)
                                                    @foreach($val['listKPICode'] as $v)
                                                        <tr>
                                                            <td>{{$value['customers_name']}}</td>
                                                            <td>{{$val['stock_code']}}</td>
                                                            <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                            <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                        </tr>
                                                    @endforeach
                                                @elseif($val['trowKPICode']>1)
                                                    <?php $t=0; ?>
                                                    @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                    <?php $t+=1; ?>
                                                    @if($t==1)
                                                        <tr>
                                                            <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                            <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                            <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                            <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                            <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                        </tr>
                                                    @endif
                                                    @endforeach{{--kthuc foreach ds  phiếu kpi được tính--}}
                                                @endif
                                            @endforeach
                                        @elseif($value['trowOrders']>1)
                                            <?php $t_orders=0; ?>
                                            @foreach($value['listOrders'] as $val)
                                                <?php $t_orders +=1; ?>
                                                @if($t_orders==1 && $val['trowKPICode']==1)
                                                    @foreach($val['listKPICode'] as $v)
                                                        <tr>
                                                            <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                            <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                            <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                            <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                        </tr>
                                                    @endforeach
                                                @elseif($t_orders==1 && $val['trowKPICode']>1)
                                                    <?php $t=0; ?>
                                                    @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                    <?php $t+=1; ?>
                                                    @if($t==1)
                                                        <tr>
                                                            <td rowspan="{{$value['trowOrders']}}">{{$value['customers_name']}}</td>
                                                            <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                            <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                            <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                            <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                        </tr>
                                                    @endif
                                                    @endforeach{{--kthuc foreach ds  phiếu kpi được tính--}}
                                                @else
                                                    @if($val['trowKPICode']==1)
                                                        @foreach($val['listKPICode'] as $v)
                                                            <tr>
                                                                <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                                <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                            </tr>
                                                        @endforeach
                                                    @elseif($val['trowKPICode']>1)
                                                        <?php $t=0; ?>
                                                        @foreach($val['listKPICode'] as $v){{--foreach ds  phiếu kpi được tính--}}
                                                        <?php $t+=1; ?>
                                                        @if($t==1)
                                                            <tr>
                                                                <td rowspan="{{$val['trowKPICode']}}">{{$val['stock_code']}}</td>
                                                                <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                                <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td> <a href="{{url("api/sales-kpi-payment/edit")}}/{{$v['id']}}">{{$v['staff_kpi_code']}}</a></td>
                                                                <td>@if($v['staff_kpi_sales_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($v['staff_kpi_sales_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                            </tr>
                                                        @endif
                                                        @endforeach{{--kthuc foreach ds  phiếu kpi được tính--}}
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Content is not available or does not exist</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
@endsection


