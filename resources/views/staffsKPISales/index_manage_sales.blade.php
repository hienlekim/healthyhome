@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
<style>
    .ms-container {
        background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
        width: auto;
        max-width: 370px;
    }
    .selectize-dropdown-header{
        display: none!important;
    }
    .selectize-control.selectize-drop-header.single.plugin-dropdown_header {
        text-align: left;
    }
</style>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">Sales Statistics of sales staff</h2>
                </div>
            </td>
            <td>
                <div class="col-xl-12 text-right">
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/sales-kpi-payment/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/staffs-sales-kpi/kpi-ranks-manage-sales")}}">
                <div class="row">
                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-0 row">
                            <div class="input-group">
                                <div class="col-md-11 float-right">
                                    <input type="hidden" class="form-control" id="ranks_id" name="ranks_id" value="3">
                                    <select id="searchInputManageSalesName" class="selectize-drop-header" placeholder="Select a staffs sales..." name="searchInputManageSalesName">
                                        <option value="">Sub Sales name</option>
                                        @if($ranksManageKpi->count() >0)
                                            @foreach($ranksManageKpi as $key)
                                                <option value="{{$key->id}}">{{$key->staff_name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-1 float-lg-left pl-lg-0">
                                    <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                            type="submit"><i class="fe-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" style="min-width: 1200px">
                        <thead style="border-top: none!important; ">
                        <tr>
                            <th style="width: 200px;">Sub Sales Name</th>
                            <th style="width: 200px;">Sales Name</th>
                            <th style="width: 200px;">Customers</th>
                            <th style="width: 200px;">Stock Code</th>
                            <th style="width: 200px;">KPI Code</th>
                            <th style="width: 200px;">Brand</th>
                            <th style="width: 200px;">KPI Status</th>
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        <?php $t=0 ?>
                        @if($results_manage->count() >0)
                            @foreach($listAll_Manage_sales as $values)
                                @if($values['trowSales']==1)
                                    @foreach($values['listSales'] as $value)
                                        @if($value['trowCustomers']==1)
                                            @foreach($value['listCustomers'] as $val)
                                                @if($val['trowOrders']==1)
                                                    @foreach($val['listOrders'] as $v)
                                                        @if($v['trowKPIOders']==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td>{{$values['sales_sub_name']}}</td>
                                                                    <td>{{$value['sales_name']}}</td>
                                                                    <td>{{$val['customer_name']}}</td>
                                                                    <td>{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                            {{--KPIOders >0--}} @elseif($v['trowKPIOders']>1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif {{--stock_code >0--}}
                                                    @endforeach
                                                    {{--stock_code >0--}} @elseif($val['trowOrders']>1)
                                                    <?php $stt_orders =0; ?>
                                                    @foreach($val['listOrders'] as $v)
                                                        <?php $stt_orders +=1; ?>
                                                        @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                            {{--customers >1--}}    @elseif($value['trowCustomers']>1)
                                            <?php $stt_kpi_customers =0; ?>
                                            @foreach($value['listCustomers'] as $val)
                                                <?php $stt_kpi_customers +=1; ?>
                                                @if($stt_kpi_customers==1 && $val['trowOrders']==1)
                                                    @foreach($val['listOrders'] as $v)
                                                        @if($v['trowKPIOders'] ==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($v['trowKPIOders'] > 1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($stt_kpi_customers==1 && $val['trowOrders']>1)
                                                    <?php $stt_orders =0; ?>
                                                    @foreach($val['listOrders'] as $v)
                                                        <?php $stt_orders +=1; ?>
                                                        @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders']==1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @if($val['trowOrders']==1)
                                                        @foreach($val['listOrders'] as $v)
                                                            @if($v['trowKPIOders'] ==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders'] > 1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($val['trowOrders']>1)
                                                        <?php $stt_orders =0; ?>
                                                        @foreach($val['listOrders'] as $v)
                                                            <?php $stt_orders +=1; ?>
                                                            @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($v['trowKPIOders']==1)
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <tr>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($v['trowKPIOders']==1)
                                                                    <?php $stt_kpi_orders =0; ?>
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <?php $stt_kpi_orders +=1; ?>
                                                                        @if($stt_kpi_orders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @elseif($stt_kpi_orders>1)
                                                                            <tr>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                    {{--sales >1--}}    @elseif($values['trowSales']>1)
                                    <?php $stt_sales =0; ?>
                                    @foreach($values['listSales'] as $value)
                                        <?php $stt_sales +=1; ?>
                                        @if($stt_sales==1 && $value['trowCustomers']==1)
                                            @foreach($value['listCustomers'] as $val)
                                                @if($val['trowOrders']==1)
                                                    @foreach($val['listOrders'] as $v)
                                                        @if($val['trowKPIOders']==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($val['trowKPIOders']>1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($val['trowOrders']>1)
                                                    <?php $stt_orders =0; ?>
                                                    @foreach($val['listOrders'] as $v)
                                                        <?php $stt_orders +=1; ?>
                                                        @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @elseif($stt_sales==1 && $value['trowCustomers']>1)
                                            <?php $stt_customers =0; ?>
                                            @foreach($value['listCustomers'] as $val)
                                                <?php $stt_customers +=1; ?>
                                                @if($stt_customers==1 && $val['trowOrders']==1)
                                                    @foreach($val['listOrders'] as $v)
                                                        @if($v['trowKPIOders']==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($v['trowKPIOders']>1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($stt_customers==1 && $val['trowOrders']>1)
                                                    <?php $stt_orders =0; ?>
                                                    @foreach($val['listOrders'] as $v)
                                                        <?php $stt_orders +=1; ?>
                                                        @if($v['trowKPIOders']==1)
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                    <td>{{$vs['brand_name']}}</td>
                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @elseif($v['trowKPIOders']>1)
                                                            <?php $stt_kpi_orders =0; ?>
                                                            @foreach($v['listKPIOders'] as $vs)
                                                                <?php $stt_kpi_orders +=1; ?>
                                                                @if($stt_kpi_orders==1)
                                                                    <tr>
                                                                        <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @elseif($stt_kpi_orders>1)
                                                                    <tr>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @if($val['trowOrders']==1)
                                                        @foreach($val['listOrders'] as $v)
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($val['trowOrders']>1)
                                                        <?php $stt_orders =0; ?>
                                                        @foreach($val['listOrders'] as $v)
                                                            <?php $stt_orders +=1; ?>
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @else
                                            @if($value['trowCustomers']==1)
                                                @foreach($value['listCustomers'] as $val)
                                                    @if($val['trowOrders']==1)
                                                        @foreach($val['listOrders'] as $v)
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($val['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($val['trowOrders']>1)
                                                        <?php $stt_orders =0; ?>
                                                        @foreach($val['listOrders'] as $v)
                                                            <?php $stt_orders +=1; ?>
                                                            @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($v['trowKPIOders']==1)
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <tr>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($v['trowKPIOders']>1)
                                                                    <?php $stt_kpi_orders =0; ?>
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <?php $stt_kpi_orders +=1; ?>
                                                                        @if($stt_kpi_orders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @elseif($stt_kpi_orders>1)
                                                                            <tr>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @elseif($value['trowCustomers']>1)
                                                <?php $stt_customers =0; ?>
                                                @foreach($value['listCustomers'] as $val)
                                                    <?php $stt_customers +=1; ?>
                                                    @if($stt_customers==1 && $val['trowOrders']==1)
                                                        @foreach($val['listOrders'] as $v)
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($stt_customers==1 && $val['trowOrders']>1)
                                                        <?php $stt_orders =0; ?>
                                                        @foreach($val['listOrders'] as $v)
                                                            <?php $stt_orders +=1; ?>
                                                            @if($v['trowKPIOders']==1)
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <tr>
                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                        <td>{{$vs['brand_name']}}</td>
                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                    </tr>
                                                                @endforeach
                                                            @elseif($v['trowKPIOders']>1)
                                                                <?php $stt_kpi_orders =0; ?>
                                                                @foreach($v['listKPIOders'] as $vs)
                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                    @if($stt_kpi_orders==1)
                                                                        <tr>
                                                                            <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                            <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @elseif($stt_kpi_orders>1)
                                                                        <tr>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        @if($val['trowOrders']==1)
                                                            @foreach($val['listOrders'] as $v)
                                                                @if($v['trowKPIOders']==1)
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <tr>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($v['trowKPIOders']>1)
                                                                    <?php $stt_kpi_orders =0; ?>
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <?php $stt_kpi_orders +=1; ?>
                                                                        @if($stt_kpi_orders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @elseif($stt_kpi_orders>1)
                                                                            <tr>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($val['trowOrders']>1)
                                                            <?php $stt_orders =0; ?>
                                                            @foreach($val['listOrders'] as $v)
                                                                <?php $stt_orders +=1; ?>
                                                                @if($v['trowKPIOders']==1)
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <tr>
                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                            <td>{{$vs['brand_name']}}</td>
                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($v['trowKPIOders']>1)
                                                                    <?php $stt_kpi_orders =0; ?>
                                                                    @foreach($v['listKPIOders'] as $vs)
                                                                        <?php $stt_kpi_orders +=1; ?>
                                                                        @if($stt_kpi_orders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @elseif($stt_kpi_orders>1)
                                                                            <tr>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã thanh toán </span>@elseif($vs['staff_kpi_sales_manage_status']=="") <span class="badge badge-soft-warning"> Chưa thanh toán </span> @endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
@endsection


