@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">Brand</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/brand")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
        <form class="mb-1" method="post" id="formBankAdd">
            {{csrf_field()}}
            <div class="card-box" style="margin-bottom: 4px!important;">
                <div class="row">
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Brand Code <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control text-uppercase" id="brand_code" name="brand_code" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Brand Name <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="brand_name" name="brand_name" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Brand Country <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <select class="form-control form-select-lg" id="brand_country_id" name="brand_country_id">
                                    <option value="">------</option>
                                    @foreach($dataCountry as $item)
                                        <option value="{{$item->id}}">{{$item->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Brand Addrees <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="brand_address" name="brand_address" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>{{----end row_1----}}
            </div>{{----end card_1----}}

            <div class="card-box" style="margin-bottom: 4px!important;">
                <div class="row">
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Trading Account 1 </label>
                            <div class="col-md-7">
{{--                                <input type="txt" class="form-control text-uppercase" id="brand_bank_id" name="brand_bank_id" autocomplete="off">--}}
                                <select class="form-control form-select-lg" id="brand_bank_id" name="brand_bank_id">
                                    <option value="">------</option>
                                    @foreach($dataBank as $item)
                                        <option value="{{$item->id}}">{{$item->bank_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Trading Account 2 </label>
                            <div class="col-md-7">
{{--                                <input type="txt" class="form-control" id="brand_bank_id_1" name="brand_bank_id_1" autocomplete="off">--}}
                                <select class="form-control form-select-lg" id="brand_bank_id_1" name="brand_bank_id_1">
                                    <option value="">------</option>
                                    @foreach($dataBank as $item)
                                        <option value="{{$item->id}}">{{$item->bank_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Trading Account 3</label>
                            <div class="col-md-7">
{{--                                <input type="txt" class="form-control" id="brand_bank_id_2" name="brand_bank_id_2" autocomplete="off">--}}
                                <select class="form-control form-select-lg" id="brand_bank_id_2" name="brand_bank_id_2">
                                    <option value="">------</option>
                                    @foreach($dataBank as $item)
                                        <option value="{{$item->id}}">{{$item->bank_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Trading Account 4 </label>
                            <div class="col-md-7">
{{--                                <input type="txt" class="form-control" id="brand_bank_id_3" name="brand_bank_id_3" autocomplete="off">--}}
                                <select class="form-control form-select-lg" id="brand_bank_id_3" name="brand_bank_id_3">
                                    <option value="">------</option>
                                    @foreach($dataBank as $item)
                                        <option value="{{$item->id}}">{{$item->bank_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Trading Account 5 </label>
                            <div class="col-md-7">
{{--                                <input type="txt" class="form-control" id="brand_bank_id_4" name="brand_bank_id_4" autocomplete="off">--}}
                                <select class="form-control form-select-lg" id="brand_bank_id_4" name="brand_bank_id_4">
                                    <option value="">------</option>
                                    @foreach($dataBank as $item)
                                        <option value="{{$item->id}}">{{$item->bank_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>{{----end row_2----}}
            </div>{{----end card_2----}}


            <div class="card-box">
                <div class="row">
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Currency <span class="text-danger">*</span></label>
                            <div class="col-md-7">
{{--                                <input type="txt" class="form-control text-uppercase" id="brand_currency_id" name="brand_currency_id" autocomplete="off">--}}
                                <select class="form-control form-select-lg" id="brand_currency_id" name="brand_currency_id">
                                    <option value="">------</option>
                                    @foreach($dataCurrency as $item)
                                        <option value="{{$item->id}}">{{$item->currency_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Warehouse </label>
                            <div class="col-md-7">
{{--                                <input type="txt" class="form-control" id="brand_ware_house_id" name="brand_ware_house_id" autocomplete="off">--}}
                                <select class="form-control form-select-lg" id="brand_ware_house_id" name="brand_ware_house_id">
                                    <option value="">------</option>
                                    @foreach($dataWareHouse as $item)
                                        <option value="{{$item->id}}">{{$item->ware_house_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>{{----end row_3----}}
                <div class="row">
                    <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                        <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                            <i class="fe-save mr-1"></i> Save
                        </button>
                    </div>
                </div>
            </div>{{----end card_3----}}


        </form>

</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script>
$(document).ready(function () {
    let localhost = window.location.hostname;
    $(document).on("click", ".btnCreate", function (event) {
        let brand_code = $('#brand_code').val();
        let brand_name = $('#brand_name').val();
        let brand_country_id = $('#brand_country_id').val();
        let brand_address = $('#brand_address').val();
        let brand_bank_id = $('#brand_bank_id').val();
        let brand_bank_id_1 = $('#brand_bank_id_1').val();
        let brand_bank_id_2 = $('#brand_bank_id_2').val();
        let brand_bank_id_3 = $('#brand_bank_id_3').val();
        let brand_bank_id_4 = $('#brand_bank_id_4').val();
        let brand_currency_id = $('#brand_currency_id').val();
        let brand_ware_house_id = $('#brand_ware_house_id').val();
        if(brand_code ==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the code brand!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(brand_name==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the name brand!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(brand_country_id==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter country!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(brand_address==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter address!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(brand_currency_id==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter currency!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            // setTimeout(() => {
                $('#fadeShow').css('display','block');
                $.ajax({
                    url:'{{url("api/brand/create")}}',
                    type: "POST",
                    dataType: 'json',
                    data: {
                        '_token':"{{ csrf_token() }}",
                        'brand_code':brand_code,
                        'brand_name':brand_name,
                        'brand_country_id':brand_country_id,
                        'brand_address':brand_address,
                        'brand_bank_id':brand_bank_id,
                        'brand_bank_id_1':brand_bank_id_1,
                        'brand_bank_id_2':brand_bank_id_2,
                        'brand_bank_id_3':brand_bank_id_3,
                        'brand_bank_id_4':brand_bank_id_4,
                        'brand_currency_id':brand_currency_id,
                        'brand_ware_house_id':brand_ware_house_id,
                    },
                    success: function(data) {
                        if(data.errors==true){
                            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-warning fade show')
                            $('#content_success').text('warning!')
                            $('#content_tb').text(data.message)
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                            $('#brand_code').focus();
                        }else{
                            $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-success fade show')
                            $('#content_success').text('success!')
                            $('#content_tb').text('successfully added new data')
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/brand")}}"; }, 3000);
                        }
                    }
                });
            // }, 1000);
        }
    });
})
</script>
        {{--sucess--}}
        <div class="jq-toast-wrap top-right">
            <style>
                #loader_loaded_success {
                    background-color: #5ba035;
                }
                #loader_loaded_wram {
                    background-color: #da8609;
                }
                #loader_loaded_errors {
                    background-color: #bf441d;
                }
            </style>
            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                <span class="close-jq-toast-single">×</span>
                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
            </div>
        </div>
@endsection

