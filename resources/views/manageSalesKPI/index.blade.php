@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
<style>
    .ms-container {
        background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
        width: auto;
        max-width: 370px;
    }
    .selectize-dropdown-header{
        display: none!important;
    }
    .selectize-control.selectize-drop-header.single.plugin-dropdown_header {
        text-align: left;
    }
</style>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">KPI Sales Manage</h2>
                </div>
            </td>
            <td>
                <div class="col-xl-12 text-right">
                    <h2 class="header-title">
                        <a href="{{url("api/manage-sales-kpi/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>
                    </h2>
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/manage-sales-kpi")}}">
            <div class="row">
                @if($Userbranch['Users']['name'] == 'administrators' || $Userbranch['Users']['name'] == 'admin')
                    <div class="col-xl-3 text-right">
                        <select class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                            <option value="">Chi Nhánh</option>
                            @foreach($branch as $item)
                                <option value="{{$item->id}}">{{$item->brand_name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                @else
                    <div class="col-xl-3 text-right">
                        <select style="display: none" class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                            <option value="">Chi Nhánh</option>
                        </select>
                    </div>
                @endif
                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                                <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                        type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    </div>
                <div class="col-xl-3 text-right">
                    <div class="mb-0 row">
                        <div class="input-group">
                            <div class="col-md-11 float-right">
                                <select id="searchInput" class="selectize-drop-header" placeholder="Select a staffs sales..." name="searchInput">
                                    <option value="">Sales</option>
                                    @foreach($Staffs as $key)
                                        <option value="{{$key->id}}">{{$key->staff_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1 float-lg-left pl-lg-0">
                            <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                    type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered" style="min-width: 1200px">
                        <thead style="border-top: none!important; ">
                        <tr>
                            <th style="width: 200px;">Manage Sales Name</th>
                            <th style="width: 200px;">Sub Sales Name</th>
                            <th style="width: 200px;">Sales Name</th>
                            <th style="width: 200px;">Customers</th>
                            <th style="width: 200px;">Stock Code</th>
                            <th style="width: 200px;">KPI Code</th>
                            <th style="width: 200px;">Brand</th>
                            <th style="width: 200px;">KPI Status</th>
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        <?php $t=0 ?>
                        @if($count >0)
                            @foreach($listAll as $values)
                                @if($values['trowSubSales']==1){{--subSales >1--}}
                                    @foreach($values['listSubSales'] as $value)
                                        @if($value['trowSales']==1)
                                            @foreach($value['listSales'] as $val)
                                                @if($val['trowCustomers']==1)
                                                    @foreach($val['listCustomers'] as $v)
                                                        @if($v['trowOrders']==1){{--order ==1--}}
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders']==1){{--kpioders ==1--}}
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td>{{$value['sales_sub_name']}}</td>
                                                                            <td>{{$val['sales_name']}}</td>
                                                                            <td>{{$v['customer_name']}}</td>
                                                                            <td>{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($v['trowOrders']>1){{--order >1--}}
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders +=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($val['trowCustomers']>1)
                                                    <?php $stt_customers=0; ?>
                                                    @foreach($val['listCustomers'] as $v)
                                                        <?php $stt_customers +=1; ?>
                                                        @if($stt_customers==1 && $v['trowOrders']==1)
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders +=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @elseif($value['trowSales']>1)
                                            <?php $stt_sales=0; ?>
                                            @foreach($value['listSales'] as $val)
                                                <?php $stt_sales +=1; ?>
                                                @if($stt_sales==1 && $val['trowCustomers']==1)
                                                    @foreach($val['listCustomers'] as $v)
                                                        @if($v['trowOrders']==1)
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($v['trowOrders']>1)
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders +=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($stt_sales==1 && $val['trowCustomers']>1)
                                                    <?php $stt_customers=0; ?>
                                                    @foreach($val['listCustomers'] as $v)
                                                        <?php $stt_customers +=1; ?>
                                                        @if($stt_customers==1 && $v['trowOrders']==1)
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders +=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @if($val['trowCustomers']==1)
                                                        @foreach($val['listCustomers'] as $v)
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($val['trowCustomers']>1)
                                                        <?php $stt_customers=0; ?>
                                                        @foreach($val['listCustomers'] as $v)
                                                            <?php $stt_customers +=1; ?>
                                                            @if($stt_customers==1 && $v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($v['trowOrders']==1)
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($v['trowOrders']>1)
                                                                    <?php $stt_oders=0; ?>
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        <?php $stt_oders +=1; ?>
                                                                        @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if($vs['trowKPIOders']==1)
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($vs['trowKPIOders']>1)
                                                                                <?php $stt_kpi_oders=0; ?>
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <?php $stt_kpi_oders +=1; ?>
                                                                                    @if($stt_kpi_oders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @else
                                                                                        <tr>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
             {{--subSales >1--}}@elseif($values['trowSubSales']>1)
                                    <?php $stt_sub_sales=0; ?>
                                    @foreach($values['listSubSales'] as $value)
                                        <?php $stt_sub_sales +=1; ?>
                                        @if($stt_sub_sales==1 && $value['trowSales']==1)
                                            @foreach($value['listSales'] as $val)
                                                @if($val['trowCustomers']==1)
                                                    @foreach($val['listCustomers'] as $v)
                                                        @if($v['trowOrders']==1)
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($v['trowOrders']>1)
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders +=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($val['trowCustomers']>1)
                                                    <?php $stt_customers=0; ?>
                                                    @foreach($val['listCustomers'] as $v)
                                                        <?php $stt_customers +=1; ?>
                                                        @if($stt_customers==1 && $v['trowOrders']==1)
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders +=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @elseif($stt_sub_sales==1 && $value['trowSales']>1)
                                            <?php $stt_sales=0; ?>
                                            @foreach($value['listSales'] as $val)
                                                <?php $stt_sales +=1; ?>
                                                @if($stt_sales==1 && $val['trowCustomers']==1)
                                                    @foreach($val['listCustomers'] as $v)
                                                        @if($v['trowOrders']==1)
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($v['trowOrders']>1)
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders+=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @elseif($stt_sales==1 && $val['trowCustomers']>1)
                                                    <?php $stt_customers=0; ?>
                                                    @foreach($val['listCustomers'] as $v)
                                                        <?php $stt_customers +=1; ?>
                                                        @if($stt_customers==1 && $v['trowOrders']==1)
                                                            @foreach($v['listOrders'] as $vs)
                                                                @if($vs['trowKPIOders'] ==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($vs['trowKPIOders'] >1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                            <?php $stt_oders=0; ?>
                                                            @foreach($v['listOrders'] as $vs)
                                                                <?php $stt_oders +=1; ?>
                                                                @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <tr>
                                                                            <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                            <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                            <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                            <td>{{$ves['brand_name']}}</td>
                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                        </tr>
                                                                    @endforeach
                                                                @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                    <?php $stt_kpi_oders=0; ?>
                                                                    @foreach($vs['listKPIOders'] as $ves)
                                                                        <?php $stt_kpi_oders +=1; ?>
                                                                        @if($stt_kpi_oders==1)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders'] ==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders'] >1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @if($val['trowCustomers']==1)
                                                        @foreach($val['listCustomers'] as $v)
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders+=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($val['trowCustomers']>1)
                                                        <?php $stt_customers=0; ?>
                                                        @foreach($val['listCustomers'] as $v)
                                                            <?php $stt_customers +=1; ?>
                                                            @if($stt_customers==1 && $v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders'] ==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders'] >1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($v['trowOrders']==1)
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        @if($vs['trowKPIOders'] ==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders'] >1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($v['trowOrders']>1)
                                                                    <?php $stt_oders=0; ?>
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        <?php $stt_oders +=1; ?>
                                                                        @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @else
                                            @if($value['trowSales']==1)
                                                @foreach($value['listSales'] as $val)
                                                    @if($val['trowCustomers']==1)
                                                        @foreach($val['listCustomers'] as $v)
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($val['trowCustomers']>1)
                                                        <?php $stt_customers=0; ?>
                                                        @foreach($val['listCustomers'] as $v)
                                                            <?php $stt_customers +=1; ?>
                                                            @if($stt_customers==1 && $v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($v['trowOrders']==1)
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($v['trowOrders']>1)
                                                                    <?php $stt_oders=0; ?>
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        <?php $stt_oders +=1; ?>
                                                                        @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @elseif($value['trowSales']>1)
                                                <?php $stt_sales=0; ?>
                                                @foreach($value['listSales'] as $val)
                                                    <?php $stt_sales +=1; ?>
                                                    @if($stt_sales==1 && $val['trowCustomers']==1)
                                                        @foreach($val['listCustomers'] as $v)
                                                            @if($v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders+=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
{{--                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
{{--                                                                                    <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($stt_sales==1 && $val['trowCustomers']>1)
                                                        <?php $stt_customers=0; ?>
                                                        @foreach($val['listCustomers'] as $v)
                                                            <?php $stt_customers +=1; ?>
                                                            @if($stt_customers==1 && $v['trowOrders']==1)
                                                                @foreach($v['listOrders'] as $vs)
                                                                    @if($vs['trowKPIOders'] ==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
{{--                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($vs['trowKPIOders'] >1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
{{--                                                                                    <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                                <?php $stt_oders=0; ?>
                                                                @foreach($v['listOrders'] as $vs)
                                                                    <?php $stt_oders +=1; ?>
                                                                    @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <tr>
{{--                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
                                                                                <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                <td>{{$ves['staff_kpi_code']}}</td>
                                                                                <td>{{$ves['brand_name']}}</td>
                                                                                <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                        <?php $stt_kpi_oders=0; ?>
                                                                        @foreach($vs['listKPIOders'] as $ves)
                                                                            <?php $stt_kpi_oders +=1; ?>
                                                                            @if($stt_kpi_oders==1)
                                                                                <tr>
{{--                                                                                    <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @else
                                                                                <tr>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($v['trowOrders']==1)
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        @if($vs['trowKPIOders'] ==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders'] >1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($v['trowOrders']>1)
                                                                    <?php $stt_oders=0; ?>
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        <?php $stt_oders +=1; ?>
                                                                        @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        @if($val['trowCustomers']==1)
                                                            @foreach($val['listCustomers'] as $v)
                                                                @if($v['trowOrders']==1)
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        @if($vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
{{--                                                                                    <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
{{--                                                                                        <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                        <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                        <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($v['trowOrders']>1)
                                                                    <?php $stt_oders=0; ?>
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        <?php $stt_oders+=1; ?>
                                                                        @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    {{--                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        {{--<td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                        <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                        <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if($vs['trowKPIOders']==1)
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <tr>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($vs['trowKPIOders']>1)
                                                                                <?php $stt_kpi_oders=0; ?>
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <?php $stt_kpi_oders +=1; ?>
                                                                                    @if($stt_kpi_oders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @else
                                                                                        <tr>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($val['trowCustomers']>1)
                                                            <?php $stt_customers=0; ?>
                                                            @foreach($val['listCustomers'] as $v)
                                                                <?php $stt_customers +=1; ?>
                                                                @if($stt_customers==1 && $v['trowOrders']==1)
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        @if($vs['trowKPIOders'] ==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    {{--                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($vs['trowKPIOders'] >1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        {{--                                                                                    <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                        <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                        <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($stt_customers==1 && $v['trowOrders']>1)
                                                                    <?php $stt_oders=0; ?>
                                                                    @foreach($v['listOrders'] as $vs)
                                                                        <?php $stt_oders +=1; ?>
                                                                        @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <tr>
                                                                                    {{--                                                                                <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                    <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                    <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                    <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                    <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                    <td>{{$ves['staff_kpi_code']}}</td>
                                                                                    <td>{{$ves['brand_name']}}</td>
                                                                                    <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                            <?php $stt_kpi_oders=0; ?>
                                                                            @foreach($vs['listKPIOders'] as $ves)
                                                                                <?php $stt_kpi_oders +=1; ?>
                                                                                @if($stt_kpi_oders==1)
                                                                                    <tr>
                                                                                        {{--                                                                                    <td rowspan="{{$values['trowSubSales']}}"><a href="{{url("api/manage-sales-kpi/edit")}}/{{$values['staff_manage_id']}}">{{$values['sales_manage_name']}}</a></td>--}}
{{--                                                                                        <td rowspan="{{$value['trowSales']}}">{{$value['sales_sub_name']}}</td>--}}
                                                                                        <td rowspan="{{$val['trowCustomers']}}">{{$val['sales_name']}}</td>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @else
                                                                                    <tr>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($v['trowOrders']==1)
                                                                        @foreach($v['listOrders'] as $vs)
                                                                            @if($vs['trowKPIOders'] ==1)
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($vs['trowKPIOders'] >1)
                                                                                <?php $stt_kpi_oders=0; ?>
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <?php $stt_kpi_oders +=1; ?>
                                                                                    @if($stt_kpi_oders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @else
                                                                                        <tr>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @elseif($v['trowOrders']>1)
                                                                        <?php $stt_oders=0; ?>
                                                                        @foreach($v['listOrders'] as $vs)
                                                                            <?php $stt_oders +=1; ?>
                                                                            @if($stt_oders==1 && $vs['trowKPIOders']==1)
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                        <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                        <td>{{$ves['staff_kpi_code']}}</td>
                                                                                        <td>{{$ves['brand_name']}}</td>
                                                                                        <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($stt_oders==1 && $vs['trowKPIOders']>1)
                                                                                <?php $stt_kpi_oders=0; ?>
                                                                                @foreach($vs['listKPIOders'] as $ves)
                                                                                    <?php $stt_kpi_oders +=1; ?>
                                                                                    @if($stt_kpi_oders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$v['trowOrders']}}">{{$v['customer_name']}}</td>
                                                                                            <td rowspan="{{$vs['trowKPIOders']}}">{{$vs['stock_code']}}</td>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @else
                                                                                        <tr>
                                                                                            <td>{{$ves['staff_kpi_code']}}</td>
                                                                                            <td>{{$ves['brand_name']}}</td>
                                                                                            <td>@if($ves['staff_kpi_sales_sub_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-left">
                <b>Total:</b> <span class="TotalAll">{{$count}}</span>
            </div>
            <div class="col-md-4">
                <div class="dataTables_paginate paging_simple_numbers">{{$results->links()}}</div>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
@endsection


