@extends('template')
@section('title', "Bank")

@section('css')
    <link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    {{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
    {{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
    <style>
        .ms-container {
            background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
            width: auto;
            max-width: 370px;
        }
        .selectize-dropdown-header{
            display: none!important;
        }
    </style>
@endsection

@section('content')

    {{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
    <div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
        <table style="width: 100%">
            <tr>
                <td>
                    <div class="col-xl-12 text-left">
                        <h2 class="header-title">KPI Sales Manage</h2>
                    </div>
                </td>
                <td>
                    <div class="col-xl-12 text-right">
                        <h2 class="header-title">
                            <a href="{{url("api/manage-sales-kpi")}}" class="btn btn-primary waves-effect waves-light"><i
                                    class="fe-rewind pr-1"></i>Back</a>
                        </h2>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="row content_description">
        <div class="col-12">
            <div class="card-box">
                <form class="mb-1" method="get" id="formSearch" action="{{url("api/manage-sales-kpi/customers-period-kpi-sub")}}">
                    <div class="row">
                        <div class="mb-2 col-md-12">
                            <div class="mb-2 row">
                                <label class="col-md-3 col-form-label" for="simpleinput">Manage Sales<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-5">
                                    <select id="sales_kpi" class="selectize-drop-header" placeholder="Select sales..." name="sales_kpi">
                                        <option value=""></option>
                                        @foreach($salesSub as $key)
                                            @if($id==$key->id)
                                                <option value="{{$key->id}}" selected>{{$key->staff_name}}</option>
                                            @else
                                                <option value="{{$key->id}}">{{$key->staff_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
{{--                                <div class="col-md-2">--}}
{{--                                    <button type="submit" class="btn btn-primary waves-effect waves-light btnSearch">--}}
{{--                                        <i class="fe-search mr-1"></i> Search--}}
{{--                                    </button>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                        <div class="mb-2 col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="min-width: 1200px">
                                    <thead style="border-top: none!important; ">
                                    <tr>
                                        <th style="width: 200px;">Sub Sales Name</th>
                                        <th style="width: 200px;">Sales Name</th>
                                        <th style="width: 200px;">Customers</th>
                                        <th style="width: 200px;">Stock Code</th>
                                        <th style="width: 200px;">KPI Code</th>
                                        <th style="width: 200px;">Brand</th>
                                        <th style="width: 200px;">KPI Status</th>
                                    </tr>
                                    </thead>
                                    <tbody class="contentTable">
                                    <?php $t=0 ?>
                                    @if($count >0)
                                        @foreach($listAll as $values)
                                            @if($values['trowSales']==1)
                                                @foreach($values['listSales'] as $value)
                                                    @if($value['trowCustomers']==1)
                                                        @foreach($value['listCustomers'] as $val)
                                                            @if($val['trowOrders']==1)
                                                                @foreach($val['listOrders'] as $v)
                                                                    @if($v['trowKPIOders']==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td>{{$values['sales_sub_name']}}</td>
                                                                                <td>{{$value['sales_name']}}</td>
                                                                                <td>{{$val['customer_name']}}</td>
                                                                                <td>{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                        {{--KPIOders >0--}} @elseif($v['trowKPIOders']>1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif {{--stock_code >0--}}
                                                                @endforeach
                                                                {{--stock_code >0--}} @elseif($val['trowOrders']>1)
                                                                <?php $stt_orders =0; ?>
                                                                @foreach($val['listOrders'] as $v)
                                                                    <?php $stt_orders +=1; ?>
                                                                    @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                        {{--customers >1--}}    @elseif($value['trowCustomers']>1)
                                                        <?php $stt_kpi_customers =0; ?>
                                                        @foreach($value['listCustomers'] as $val)
                                                            <?php $stt_kpi_customers +=1; ?>
                                                            @if($stt_kpi_customers==1 && $val['trowOrders']==1)
                                                                @foreach($val['listOrders'] as $v)
                                                                    @if($v['trowKPIOders'] ==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($v['trowKPIOders'] > 1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($stt_kpi_customers==1 && $val['trowOrders']>1)
                                                                <?php $stt_orders =0; ?>
                                                                @foreach($val['listOrders'] as $v)
                                                                    <?php $stt_orders +=1; ?>
                                                                    @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders']==1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($val['trowOrders']==1)
                                                                    @foreach($val['listOrders'] as $v)
                                                                        @if($v['trowKPIOders'] ==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders'] > 1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($val['trowOrders']>1)
                                                                    <?php $stt_orders =0; ?>
                                                                    @foreach($val['listOrders'] as $v)
                                                                        <?php $stt_orders +=1; ?>
                                                                        @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if($v['trowKPIOders']==1)
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($v['trowKPIOders']==1)
                                                                                <?php $stt_kpi_orders =0; ?>
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                                    @if($stt_kpi_orders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @elseif($stt_kpi_orders>1)
                                                                                        <tr>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                                {{--sales >1--}}    @elseif($values['trowSales']>1)
                                                <?php $stt_sales =0; ?>
                                                @foreach($values['listSales'] as $value)
                                                    <?php $stt_sales +=1; ?>
                                                    @if($stt_sales==1 && $value['trowCustomers']==1)
                                                        @foreach($value['listCustomers'] as $val)
                                                            @if($val['trowOrders']==1)
                                                                @foreach($val['listOrders'] as $v)
                                                                    @if($val['trowKPIOders']==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($val['trowKPIOders']>1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($val['trowOrders']>1)
                                                                <?php $stt_orders =0; ?>
                                                                @foreach($val['listOrders'] as $v)
                                                                    <?php $stt_orders +=1; ?>
                                                                    @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @else
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @elseif($stt_sales==1 && $value['trowCustomers']>1)
                                                        <?php $stt_customers =0; ?>
                                                        @foreach($value['listCustomers'] as $val)
                                                            <?php $stt_customers +=1; ?>
                                                            @if($stt_customers==1 && $val['trowOrders']==1)
                                                                @foreach($val['listOrders'] as $v)
                                                                    @if($v['trowKPIOders']==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($v['trowKPIOders']>1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @elseif($stt_customers==1 && $val['trowOrders']>1)
                                                                <?php $stt_orders =0; ?>
                                                                @foreach($val['listOrders'] as $v)
                                                                    <?php $stt_orders +=1; ?>
                                                                    @if($v['trowKPIOders']==1)
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <tr>
                                                                                <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                <td>{{$vs['staff_kpi_code']}}</td>
                                                                                <td>{{$vs['brand_name']}}</td>
                                                                                <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @elseif($v['trowKPIOders']>1)
                                                                        <?php $stt_kpi_orders =0; ?>
                                                                        @foreach($v['listKPIOders'] as $vs)
                                                                            <?php $stt_kpi_orders +=1; ?>
                                                                            @if($stt_kpi_orders==1)
                                                                                <tr>
                                                                                    <td rowspan="{{$values['trowSales']}}">{{$values['sales_sub_name']}}</td>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @elseif($stt_kpi_orders>1)
                                                                                <tr>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                @if($val['trowOrders']==1)
                                                                    @foreach($val['listOrders'] as $v)
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($val['trowOrders']>1)
                                                                    <?php $stt_orders =0; ?>
                                                                    @foreach($val['listOrders'] as $v)
                                                                        <?php $stt_orders +=1; ?>
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        @if($value['trowCustomers']==1)
                                                            @foreach($value['listCustomers'] as $val)
                                                                @if($val['trowOrders']==1)
                                                                    @foreach($val['listOrders'] as $v)
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($val['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($val['trowOrders']>1)
                                                                    <?php $stt_orders =0; ?>
                                                                    @foreach($val['listOrders'] as $v)
                                                                        <?php $stt_orders +=1; ?>
                                                                        @if($stt_orders==1 && $v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($stt_orders==1 && $v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @else
                                                                            @if($v['trowKPIOders']==1)
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($v['trowKPIOders']>1)
                                                                                <?php $stt_kpi_orders =0; ?>
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                                    @if($stt_kpi_orders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @elseif($stt_kpi_orders>1)
                                                                                        <tr>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        @elseif($value['trowCustomers']>1)
                                                            <?php $stt_customers =0; ?>
                                                            @foreach($value['listCustomers'] as $val)
                                                                <?php $stt_customers +=1; ?>
                                                                @if($stt_customers==1 && $val['trowOrders']==1)
                                                                    @foreach($val['listOrders'] as $v)
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @elseif($stt_customers==1 && $val['trowOrders']>1)
                                                                    <?php $stt_orders =0; ?>
                                                                    @foreach($val['listOrders'] as $v)
                                                                        <?php $stt_orders +=1; ?>
                                                                        @if($v['trowKPIOders']==1)
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <tr>
                                                                                    <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                    <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                    <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                    <td>{{$vs['staff_kpi_code']}}</td>
                                                                                    <td>{{$vs['brand_name']}}</td>
                                                                                    <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @elseif($v['trowKPIOders']>1)
                                                                            <?php $stt_kpi_orders =0; ?>
                                                                            @foreach($v['listKPIOders'] as $vs)
                                                                                <?php $stt_kpi_orders +=1; ?>
                                                                                @if($stt_kpi_orders==1)
                                                                                    <tr>
                                                                                        <td rowspan="{{$value['trowCustomers']}}">{{$value['sales_name']}}</td>
                                                                                        <td rowspan="{{$val['trowOrders']}}">{{$val['customer_name']}}</td>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @elseif($stt_kpi_orders>1)
                                                                                    <tr>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @if($val['trowOrders']==1)
                                                                        @foreach($val['listOrders'] as $v)
                                                                            @if($v['trowKPIOders']==1)
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($v['trowKPIOders']>1)
                                                                                <?php $stt_kpi_orders =0; ?>
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                                    @if($stt_kpi_orders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @elseif($stt_kpi_orders>1)
                                                                                        <tr>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @elseif($val['trowOrders']>1)
                                                                        <?php $stt_orders =0; ?>
                                                                        @foreach($val['listOrders'] as $v)
                                                                            <?php $stt_orders +=1; ?>
                                                                            @if($v['trowKPIOders']==1)
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <tr>
                                                                                        <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                        <td>{{$vs['staff_kpi_code']}}</td>
                                                                                        <td>{{$vs['brand_name']}}</td>
                                                                                        <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @elseif($v['trowKPIOders']>1)
                                                                                <?php $stt_kpi_orders =0; ?>
                                                                                @foreach($v['listKPIOders'] as $vs)
                                                                                    <?php $stt_kpi_orders +=1; ?>
                                                                                    @if($stt_kpi_orders==1)
                                                                                        <tr>
                                                                                            <td rowspan="{{$v['trowKPIOders']}}">{{$v['stock_code']}}</td>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @elseif($stt_kpi_orders>1)
                                                                                        <tr>
                                                                                            <td>{{$vs['staff_kpi_code']}}</td>
                                                                                            <td>{{$vs['brand_name']}}</td>
                                                                                            <td>@if($vs['staff_kpi_sales_manage_status']==1)<span class="badge badge-soft-success"> Đã Thanh Toán</span>@endif</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7" class="text-center">Content is not available or does not exist</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
                <form class="mb-1" method="post" id="formAdd">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="mb-2 col-md-12">
                            <div class="col-md-5">
                                <input type="hidden" name="staff_kpi_sales_id" id="staff_kpi_sales_id" value="{{$id}}">
                                <input type="hidden" name="PeriodId" id="PeriodId">
{{--                                <input type="hidden" name="staff_kpi_order_brand_id" id="staff_kpi_order_brand_id" value="@if($Brand['staff_brand_id']==0) 0 @else {{$Brand['brands']['id']}}@endif">--}}
                            </div>
                        </div>
                    </div>
                    @if($Brand['decenTLizaTion']['positions']['position_code'] == 'headbranch')
                        <div class="row">
                            <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                                <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                                    <i class="fe-save mr-1"></i> Cancel
                                </button>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
        <div class="modal-backdrop fade show" id="fadeShow" style="display: none">
            <div class="row">
                <div class="col-md-12 text-center" style="margin-top: 10%">
                    <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
                    </br>
                    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                </div>
            </div>
        </div>

        @endsection

        @section('javascript')
            <script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
            <script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
            <script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
            <script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
            <script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
            <script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
            <script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
            {{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
            <script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
            <script>
                $(document).ready(function () {
                    let localhost = window.location.hostname;
                    $(document).on("click", ".btnCreate", function (event) {
                        let periodId = $('#staff_kpi_sales_id').val();
                        $('#fadeShow').css('display','block');
                        $.ajax({
                            url:'{{url("api/manage-sales-kpi/update")}}/'+ periodId,
                            type: "PUT",
                            dataType: 'json',
                            data: {
                                '_token':"{{ csrf_token() }}",
                            },
                            success: function(data) {
                                console.log(data)
                                if(data.errors==true){
                                    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                                    $('#successrequest').addClass('jq-icon-warning fade show')
                                    $('#content_success').text('warning!')
                                    $('#content_tb').text(data.message)
                                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                                    $('#origin_code').focus();
                                }else{
                                    $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                                    $('#successrequest').addClass('jq-icon-success fade show')
                                    $('#content_success').text('success!')
                                    $('#content_tb').text('successfully added update data')
                                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/manage-sales-kpi")}}"; }, 3000);
                                }
                            }
                        });
                    });
                })
            </script>
            {{--sucess--}}
            <div class="jq-toast-wrap top-right">
                <style>
                    #loader_loaded_success {
                        background-color: #5ba035;
                    }
                    #loader_loaded_wram {
                        background-color: #da8609;
                    }
                    #loader_loaded_errors {
                        background-color: #bf441d;
                    }
                </style>
                <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                    <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                    <span class="close-jq-toast-single">×</span>
                    <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
                </div>
            </div>
@endsection

