@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<style>
    /*!*@media only screen and (min-device-width : 1400px) and (max-device-width : 1920px) {*!*/
    /*!*   #asignsalesmanage{*!*/
    /*!*       width: 14%;*!*/
    /*!*   }*!*/
    /*!*}*!*/
    /*@media only screen and (min-device-width : 1200px) and (max-device-width : 1284px) {*/
    /*    #asignsalesmanage{*/
    /*        width: 22%;*/
    /*    }*/
    /*}*/
    /*@media only screen and (min-device-width : 1285px) and (max-device-width : 1300px) {*/
    /*    #asignsalesmanage{*/
    /*        width: 21%;*/
    /*    }*/
    /*}*/
    /*@media only screen and (min-device-width : 1301px) and (max-device-width : 1388px) {*/
    /*    #asignsalesmanage{*/
    /*        width: 19%;*/
    /*    }*/
    /*}*/
    /*@media only screen and (min-device-width : 1389px) and (max-device-width : 1920px) {*/
    /*    #asignsalesmanage{*/
    /*        width: 14%;*/
    /*    }*/
    /*}*/
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">Employee Rank</h2>
                </div>
            </td>
            <td>
                <div class="text-right">
                    <h2 class="header-title">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" id="standard_modal" data-bs-target="#standard-modal">New</button>
                    </h2>
                </div>
            </td>
{{--            <td id="asignsalesmanage">--}}
{{--                <div class="text-right">--}}
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/rank/assign-manage")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>Assign Manage Sales</a>--}}
{{--                    </h2>--}}
{{--                </div>--}}
{{--            </td>--}}
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/rank")}}">
{{--            <div class="row">--}}
{{--                <div class="col-xl-9 text-left">--}}
{{--                    <div class="dataTables_length" id="products-datatable_length"><label>Display--}}
{{--                            <select class="form-select form-select-sm mx-1" id="limit" name="limit">--}}
{{--                                <option value="10">10</option>--}}
{{--                                <option value="20">20</option>--}}
{{--                                <option value="30">30</option>--}}
{{--                                <option value="50">50</option>--}}
{{--                                <option value="100">100</option>--}}
{{--                                <option value="all">all</option>--}}
{{--                            </select>Bank</label></div>--}}
{{--                </div>--}}
{{--                <div class="col-xl-3 text-right">--}}
{{--                    <div class="mb-2 row">--}}
{{--                        <div class="input-group">--}}
{{--                            <input type="text" class="form-control" id="searchInput" name="searchInput"--}}
{{--                                   autocomplete="off">--}}
{{--                            <button class="btn btn-primary waves-effect waves-light" id="searchButton"--}}
{{--                                    type="submit"><i class="fe-search"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead style="border-top: none!important; ">
                        <tr>
                            <th style="width: 50px;">#</th>
                            <th style="width: 200px;">Staffs Code</th>
                            <th style="width: 200px;">Staffs Name</th>
                            <th style="width: 200px;">Rank Name </th>
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        @if($count >0)
                            <?php $t=0;$array_manage=[]; ?>
                            @foreach($resultSalesManages as $k)
                                <?php $t=$t+1; array_push($array_manage,1); ?>
                                {{--ds nv manage--}}
                                <tr class="footable-odd footable-detail-show" id="trcustomerChildrent1_{{$t}}">
                                    <td>
                                        <span id="linedown0_{{$t}}" style="cursor: pointer;display: none;"><i class="fe-plus-circle float-left pr-1 pt-1"></i></span>
                                        <span id="lineup0_{{$t}}" style="display: none;cursor: pointer;"><i class="fe-minus-circle float-left pr-1 pt-1"></i></span>
                                    </td>
                                    <td><a href="{{url("api/rank/edit-manage-sales/$k->id")}}" id="staff_childrent_{{$t}}">{{$k['staff_code']}}</a></td>
                                    <td>{{$k['staff_name']}}</td>
                                    <td>@if($k['staff_rank_id']!="") {{$k['ranks']['rank_name']}} @endif</td>
                                </tr>
                                {{--ds nv sales thuộc dưới quyền manage--}}
                                <?php $ts=0;$arr1=[]; ?>
                                @foreach($resultSales as $vs)
                                    @if($k['id']==$vs['staff_parent_id'])
                                        <?php $ts=$ts+1;array_push($arr1, 1); ?>
                                        <tr class="footable-odd footable-detail-show" id="trcustomerChildrent2_{{$t}}_{{$ts}}" style="display: none">
                                            <td>{{$ts}}</td>
                                            <td>{{$vs['staff_code']}}</td>
                                            <td>{{$vs['staff_name']}}</td>
                                            <td>@if($vs['staff_rank_id']!="") {{$vs['ranks']['rank_name']}} @else Sales @endif</td>
                                        </tr>
                                    @endif
                                @endforeach
                                {{--ds nv sub manage thuộc dưới quyền manage--}}
                                <?php $ts2=$ts;$arr2=[]; ?>
                                @foreach($resultSalesSubManages as $ks)
                                    @if($k['id']==$ks['staff_parent_id'])
                                        <?php $ts2=$ts2+1;array_push($arr2, 1); ?>
                                        <tr class="footable-odd footable-detail-show" id="trcustomerChildrent2_{{$t}}_{{$ts2}}" style="display: none">
                                            <td>
                                                <span id="linedownSubManage_{{$t}}_{{$ts2}}" style="cursor: pointer;display: none;"><i class="fe-plus-circle float-left pr-1 pt-1"></i></span>
                                                <span id="lineupSubManage_{{$t}}_{{$ts2}}" style="display: none;cursor: pointer;"><i class="fe-minus-circle float-left pr-1 pt-1"></i></span>
                                            </td>
                                            <td><a href="{{url("api/rank/edit/$ks->id")}}" id="staff_childrent2_{{$t}}_{{$ts2}}">{{$ks['staff_code']}}</a></td>
                                            <td>{{$ks['staff_name']}}</td>
                                            <td>@if($ks['staff_rank_id']!="") {{$ks['ranks']['rank_name']}} @else Sales @endif</td>
                                        </tr>
                                        {{--ds nv sales cấp thấp nhất--}}
                                        <?php $ts3=0;$arr3=[]; ?>
                                        @foreach($resultSales as $ka)
                                            @if($ks['id']==$ka['staff_parent_id'])
                                                <?php $ts3=$ts3+1;array_push($arr3, 1); ?>
                                                <tr class="footable-odd footable-detail-show" id="trcustomerChildrent3_{{$t}}_{{$ts2}}_{{$ts3}}" style="display: none">
                                                    <td>{{$ts3}}</td>
                                                    <td>{{$ka['staff_code']}}</td>
                                                    <td>{{$ka['staff_name']}}</td>
                                                    <td>@if($ka['staff_rank_id']!="") {{$ka['ranks']['rank_name']}} @else Sales @endif</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        <input type="hidden" id="childrent_sum_sales_{{$t}}_{{$ts2}}" value="{{array_sum($arr3)}}">
                                        {{--ds nv sales cấp thấp nhất--}}
                                    @endif
                                @endforeach
                                <input type="hidden" id="childrent_sub_sales_manage_{{$t}}" value="{{array_sum($arr1)+array_sum($arr2)}}">
                            @endforeach
                            <input type="hidden" id="childrent_manage" value="{{array_sum($array_manage)}}">
                            {{--cap sub manage sales--}}
                            @if($salesSubManagesNoAssign->count() > 0)
                                <?php $k=0;$arr4=[];?>
                                @foreach($salesSubManagesNoAssign as $key)
                                    <?php $k=$k+1;array_push($arr4, 1); ?>
                                    <tr class="footable-odd footable-detail-show" id="trcustomerChildrent4_{{$k}}">
                                        <td class="text-uppercase">
                                            <span id="linedown22_{{$k}}" style="cursor: pointer;display: none;"><i class="fe-plus-circle float-left pr-1 pt-1"></i></span>
                                            <span id="lineup22_{{$k}}" style="display: none;cursor: pointer;"><i class="fe-minus-circle float-left pr-1 pt-1"></i></span>
                                        </td>
                                        <td><a href="{{url("api/rank/edit/$key->id")}}" id="staff_childrent4_{{$k}}">{{$key['staff_code']}}</a></td>
                                        <td>{{$key['staff_name']}}</td>
                                        <td>@if($key['staff_rank_id']!="") {{$key['ranks']['rank_name']}} @endif</td>
                                    </tr>
                                    {{--cap sales--}}
                                    @if($resultSales)
                                        <?php $g=0;$arr5=[];?>
                                        @foreach($resultSales as $val)
                                            @if($val['staff_parent_id'] == $key['id'])
                                                <?php $g=$g+1;array_push($arr5, 1); ?>
                                                <tr class="footable-odd footable-detail-show" id="trcustomerChildrent5_{{$k}}_{{$g}}" style="display: none">
                                                    <td class="text-uppercase">{{$g}}</td>
                                                    <td>{{$val['staff_code']}}</td>
                                                    <td>{{$val['staff_name']}}</td>
                                                    <td>@if($val['staff_rank_id']!="") {{$val['ranks']['rank_name']}} @else Sales @endif</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        <input type="hidden" id="childrentCustom_Sum5_{{$k}}" value="{{array_sum($arr5)}}">
                                    @endif
                                    {{--cap sales--}}
                                @endforeach
                                <input type="hidden" id="childrentCustom_Sum4" value="{{array_sum($arr4)}}">
                            @endif
                        @elseif($resultSalesSubManages->count()>0)
                            <?php $j=0;$arr2=[];?>
                            @foreach($resultSalesSubManages as $key)
                                <?php $j=$j+1;array_push($arr2, 1); ?>
                                <tr class="footable-odd footable-detail-show" id="trcustomerChildrent1_{{$j}}">
                                    <td class="text-uppercase">
                                        <span id="linedown11_{{$j}}" style="cursor: pointer;display: none;"><i class="fe-plus-circle float-left pr-1 pt-1"></i></span>
                                        <span id="lineup11_{{$j}}" style="display: none;cursor: pointer;"><i class="fe-minus-circle float-left pr-1 pt-1"></i></span>
                                    </td>
                                    <td><a href="{{url("api/rank/edit/$key->id")}}" id="staff_childrent_{{$j}}">{{$key['staff_code']}}</a></td>
                                    <td>{{$key['staff_name']}}</td>
                                    <td>@if($key['staff_rank_id']!="") {{$key['ranks']['rank_name']}} @endif</td>
                                </tr>
                                {{--cap sales--}}
                                @if($resultSales)
                                    <?php $e=0;$arr3=[];?>
                                    @foreach($resultSales as $val)
                                        @if($val['staff_parent_id'] == $key['id'])
                                            <?php $e=$e+1;array_push($arr3, 1); ?>
                                            <tr class="footable-odd footable-detail-show" id="trcustomerChildrent2_{{$j}}_{{$e}}" style="display: none">
                                                <td class="text-uppercase">{{$e}}</td>
                                                <td>{{$val['staff_code']}}</td>
                                                <td>{{$val['staff_name']}}</td>
                                                <td>@if($val['staff_rank_id']!="") {{$val['ranks']['rank_name']}} @else Sales @endif</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <input type="hidden" id="childrentCustom_Sum3_{{$j}}" value="{{array_sum($arr3)}}">
                                @endif
                                {{--cap sales--}}
                            @endforeach
                            <input type="hidden" id="childrentCustom_Sum22" value="{{array_sum($arr2)}}">
                        @else
                            <tr>
                                <td colspan="5" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
    <!-- Long Content Scroll Modal -->
    <div class="modal fade" id="scrollable-modal" tabindex="-1" role="dialog"
         aria-labelledby="scrollableModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollableModalTitle">Assign Rank Staff Sales</h5>
                </div>
                <div class="modal-body">
                    <table>
                        <td style="width: auto"></td>
                        <td><a href="{{url("api/rank/assign-sub-manage")}}" class="btn btn-primary waves-effect waves-light"><i
                                    class="fe-user-plus pr-1"></i>Sub Manage Sales</a></td>
                        <td><a href="{{url("api/rank/assign-manage")}}" class="btn btn-primary waves-effect waves-light"><i
                                    class="fe-users pr-1"></i>Manage Sales</a></td>
                        <td><button type="button" class="btn btn-light" data-bs-dismiss="modal" id="close">Close</button></td>
                    </table>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script>
    $('#standard_modal').click(function(){
        $('#scrollable-modal').addClass('show');
        $('#scrollable-modal').css('display','block')
    })
    $('#close').click(function(){
        $('#scrollable-modal').removeClass('show')
    })
    let sumRow1 = $('#childrent_manage').val();
    if(sumRow1==1){
        let Childrent2 = $('#childrent_sub_sales_manage_1').val();
        if(Childrent2 > 0){
            $('#linedown0_1').css('display','block');
            $('#linedown0_1').click(function (){
                if(Childrent2==1){
                        $('#lineup0_1').css('display','block')
                        $('#linedown0_1').css('display','none');
                        $('#trcustomerChildrent1_1').css('background-color','#1abc9c')
                        $('#trcustomerChildrent1_1').css('color','#ffff')
                        $('#staff_childrent_1').css('color','#ffff')
                        $('#trcustomerChildrent2_1_1').css('display','revert');
                }else{
                    for(let j=1;j<=Childrent2;j++){
                        $('#lineup0_1').css('display','block')
                        $('#linedown0_1').css('display','none');
                        $('#trcustomerChildrent1_1').css('background-color','#1abc9c')
                        $('#trcustomerChildrent1_1').css('color','#ffff')
                        $('#staff_childrent_1').css('color','#ffff')
                        $('#trcustomerChildrent2_1'+'_'+j).css('display','revert');
                        //xong show sub manage
                        let childrent3 = $('#childrent_sum_sales_1_'+j).val();
                        if(childrent3 >0){
                            $('#linedownSubManage_1_'+j).css('display','block');
                            $('#linedownSubManage_1_'+j).click(function (){
                                for(let e=1;e<=childrent3;e++) {
                                    $('#linedownSubManage_1_'+j).css('display', 'none');
                                    $('#lineupSubManage_1_'+j).css('display','block');
                                    $('#trcustomerChildrent2_1_'+j).css('background-color','#f7b84b')
                                    $('#trcustomerChildrent2_1_'+j).css('color','#ffff')
                                    $('#staff_childrent2_1_'+j).css('color','#ffff')
                                    $('#trcustomerChildrent3_1_'+j+'_'+e).css('display','revert');
                                }
                            });
                            $('#lineupSubManage_1_'+j).click(function (){
                                for(let f=1;f<=childrent3;f++){
                                    $('#lineupSubManage_1_'+j).css('display','none')
                                    $('#linedownSubManage_1_'+j).css('display','block');
                                    $('#trcustomerChildrent2_1_'+j).css('background-color','#f1f5f7')
                                    $('#trcustomerChildrent2_1_'+j).css('color','#6c757d')
                                    $('#staff_childrent2_1_'+j).css('color','#3bafda')
                                    $('#trcustomerChildrent3_1_'+j+'_'+f).css('display','none');
                                }
                            })
                        }
                    }
                }
            });
            $('#lineup0_1').click(function (){
                for(let j=1;j<=Childrent2;j++){
                    $('#lineup0_1').css('display','none')
                    $('#linedown0_1').css('display','block');
                    $('#trcustomerChildrent1_1').css('background-color','#f1f5f7')
                    $('#trcustomerChildrent1_1').css('color','#6c757d')
                    $('#staff_childrent_1').css('color','#3bafda')
                    $('#trcustomerChildrent2_1_'+j).css('display','none');
                }
            })
        }
    }else{
        for(let i=1;i<=sumRow1;i++){
            let Childrent2 = $('#childrent_sub_sales_manage_'+i).val();
            if(Childrent2 > 0){
                $('#linedown0_'+i).css('display','block');
                $('#linedown0_'+i).click(function (){
                    for(let j=1;j<=Childrent2;j++){
                        $('#lineup0_'+i).css('display','block')
                        $('#linedown0_'+i).css('display','none');
                        $('#trcustomerChildrent1_'+i).css('background-color','#1abc9c')
                        $('#trcustomerChildrent1_'+i).css('color','#ffff')
                        $('#staff_childrent_'+i).css('color','#ffff')
                        $('#trcustomerChildrent2_'+i+'_'+j).css('display','revert');
                        //xong show sub manage
                        let childrent3 = $('#childrent_sum_sales_'+i+'_'+j).val();
                        if(childrent3 >0){
                            $('#linedownSubManage_'+i+'_'+j).css('display','block');
                            $('#linedownSubManage_'+i+'_'+j).click(function (){
                                for(let e=1;e<=Childrent2;e++) {
                                    $('#linedownSubManage_'+i+'_'+j).css('display', 'none');
                                    $('#lineupSubManage_'+i+'_'+j).css('display','block');
                                    $('#trcustomerChildrent2_'+i+'_'+j).css('background-color','#f7b84b')
                                    $('#trcustomerChildrent2_'+i+'_'+j).css('color','#ffff')
                                    $('#staff_childrent2_'+i).css('color','#ffff')
                                    $('#trcustomerChildrent3_'+i+'_'+j+'_'+e).css('display','revert');
                                }
                            });
                            $('#lineupSubManage_'+i+'_'+j).click(function (){
                                for(let f=1;f<=childrent3;f++){
                                    $('#lineupSubManage_'+i+'_'+j).css('display','none')
                                    $('#linedownSubManage_'+i+'_'+j).css('display','block');
                                    $('#trcustomerChildrent2_'+i+'_'+j).css('background-color','#f1f5f7')
                                    $('#trcustomerChildrent2_'+i+'_'+j).css('color','#6c757d')
                                    $('#staff_childrent2_'+i).css('color','#3bafda')
                                    $('#trcustomerChildrent3_'+i+'_'+j+'_'+f).css('display','none');
                                }
                            })
                        }
                    }
                });
                $('#lineup0_'+i).click(function (){
                    for(let j=1;j<=Childrent2;j++){
                        $('#lineup0_'+i).css('display','none')
                        $('#linedown0_'+i).css('display','block');
                        $('#trcustomerChildrent1_'+i).css('background-color','#f1f5f7')
                        $('#trcustomerChildrent1_'+i).css('color','#6c757d')
                        $('#staff_childrent_'+i).css('color','#3bafda')
                        $('#trcustomerChildrent2_'+i+'_'+j).css('display','none');
                    }
                })
            }
            //xong show sub manage

        }
    }

    // bắt đầu là submanage
    let sumRow12 = $('#childrentCustom_Sum22').val();
    for(let i=1;i<=sumRow12;i++){
        let childrentCustom_Sum3 = $('#childrentCustom_Sum3_'+i).val();
        if(childrentCustom_Sum3 > 0){
            $('#linedown11_'+i).css('display','block');
            $('#linedown11_'+i).click(function (){
                let sumchild3 = $('#childrentCustom_Sum3_'+i).val();
                for(let j=1;j<=sumchild3;j++){
                    $('#lineup11_'+i).css('display','block')
                    $('#linedown11_'+i).css('display','none');
                    $('#trcustomerChildrent1_'+i).css('background-color','#1abc9c')
                    $('#trcustomerChildrent1_'+i).css('color','#ffff')
                    $('#staff_childrent_'+i).css('color','#ffff')
                    $('#trcustomerChildrent2_'+i+'_'+j).css('display','revert');
                }
            })
            $('#lineup11_'+i).click(function (){
                let sumchild3 = $('#childrentCustom_Sum3_'+i).val();
                for(let j=1;j<=sumchild3;j++){
                    $('#lineup11_'+i).css('display','none')
                    $('#linedown11_'+i).css('display','block');
                    $('#trcustomerChildrent1_'+i).css('background-color','#f1f5f7')
                    $('#trcustomerChildrent1_'+i).css('color','#6c757d')
                    $('#staff_childrent_'+i).css('color','#3bafda')
                    $('#trcustomerChildrent2_'+i+'_'+j).css('display','none');
                }
            })
        }
    }
    // bắt đầu là submanage chưa được assign
    let sumRows4 = $('#childrentCustom_Sum4').val();
    for(let i=1;i<=sumRows4;i++){
        let childrenSum4 = $('#childrentCustom_Sum5_'+i).val();
        if(childrenSum4 > 0){
            $('#linedown22_'+i).css('display','block');
            $('#linedown22_'+i).click(function (){
                for(let j=1;j<=childrenSum4;j++){
                    $('#lineup22_'+i).css('display','block')
                    $('#linedown22_'+i).css('display','none');
                    $('#trcustomerChildrent4_'+i).css('background-color','#f7b84b')
                    $('#trcustomerChildrent4_'+i).css('color','#ffff')
                    $('#staff_childrent4_'+i).css('color','#ffff')
                    $('#trcustomerChildrent5_'+i+'_'+j).css('display','revert');
                }
            })
            $('#lineup22_'+i).click(function (){
                let sumchild3 = $('#childrentCustom_Sum5_'+i).val();
                for(let j=1;j<=sumchild3;j++){
                    $('#lineup22_'+i).css('display','none')
                    $('#linedown22_'+i).css('display','block');
                    $('#trcustomerChildrent4_'+i).css('background-color','#f1f5f7')
                    $('#trcustomerChildrent4_'+i).css('color','#6c757d')
                    $('#staff_childrent4_'+i).css('color','#3bafda')
                    $('#trcustomerChildrent5_'+i+'_'+j).css('display','none');
                }
            })
        }
    }
</script>
@endsection


