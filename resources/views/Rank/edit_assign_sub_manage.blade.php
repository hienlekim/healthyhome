@extends('template')
@section('title', "Bank")

@section('css')
    <link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    {{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
    <link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
    {{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
    <style>
        .ms-container {
            background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
            width: auto;
            max-width: 370px;
        }
        .selectize-dropdown-header{
            display: none!important;
        }
    </style>
@endsection

@section('content')

    {{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
    <div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
        <table style="width: 100%">
            <tr>
                <td>
                    <div class="col-xl-12 text-left">
                        <h2 class="header-title">Assign Sub Manage Sales</h2>
                    </div>
                </td>
                <td>
                    <div class="col-xl-12 text-right">
                        <h2 class="header-title">
                            <a href="{{url("api/rank")}}" class="btn btn-primary waves-effect waves-light"><i
                                    class="fe-rewind pr-1"></i>Back</a>
                        </h2>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="row content_description">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <form class="mb-1" method="post" id="formAssignStaff">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="mb-2 col-md-12">
                                <div class="mb-2 row">
                                    <label class="col-md-3 col-form-label" for="simpleinput">Sub Manage Sales<span
                                            class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <input type="hidden" name="id" id="idChildrent" value="{{$data->id}}">
                                        <select id="sub_manage_sales" class="selectize-drop-header" placeholder="Select a sub manage sales..." name="sub_manage_sales">
                                            <option value=""></option>
                                            @foreach($salesSub as $key)
                                                @if($key->id == $data->id)
                                                    <option selected="selected" value="{{$key->id}}">{{$key->staff_name}}</option>
                                                @else
                                                    <option value="{{$key->id}}">{{$key->staff_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2 col-md-12">
                                <div class="mb-2 row">
                                    <label class="col-md-3 col-form-label" for="simpleinput">Sales <span
                                            class="text-danger">*</span></label>
                                    <div class="col-md-7">
                                        <select class="multi-select sales" multiple="" id="my_multi_select3" name="sales">
                                                @foreach($array_select as $key)
                                                    <option selected="selected" value="{{$key['id']}}">{{$key['staff_name']}}</option>
                                                @endforeach
                                                @foreach($salesSubChildrent as $key)
                                                        <option value="{{$key->id}}">{{$key->staff_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                                <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                                    <i class="fe-save mr-1"></i> Assign Sub Manage Sales
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-backdrop fade show" id="fadeShow" style="display: none">
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top: 10%">
                <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
                </br>
                <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
    <script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
    <script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
    <script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
    <script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
    {{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
    <script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
    <script>
        $(document).ready(function () {
            let localhost = window.location.hostname;
            $(document).on("click", ".btnCreate", function (event) {
                let id = $('#idChildrent').val();
                let sub_manage_sales = $('#sub_manage_sales').val();
                let sales = $('#my_multi_select3').val();
                if(sub_manage_sales ==""){
                    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-warning fade show')
                    $('#content_success').text('warning!')
                    $('#content_tb').text('please enter the sub manage staffs sales!')
                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
                }else{
                    // setTimeout(() => {
                    $('#fadeShow').css('display','block');
                    $.ajax({
                        url:'{{url("api/rank/update-sub-manage")}}/'+id,
                        type: "PUT",
                        dataType: 'json',
                        data: {
                            '_token':"{{ csrf_token() }}",
                            'sub_manage_sales':sub_manage_sales,
                            'sales':sales,
                        },
                        success: function(data) {
                            if(data.errors==true){
                                $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                                $('#successrequest').addClass('jq-icon-warning fade show')
                                $('#content_success').text('warning!')
                                $('#content_tb').text(data.message)
                                setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                                $('#rank_code').focus();
                            }else{
                                $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                                $('#successrequest').addClass('jq-icon-success fade show')
                                $('#content_success').text('success!')
                                $('#content_tb').text('successfully added new data')
                                setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/rank")}}"; }, 3000);
                            }
                        }
                    });
                    // }, 1000);
                }
            });
        })
    </script>
    {{--sucess--}}
    <div class="jq-toast-wrap top-right">
        <style>
            #loader_loaded_success {
                background-color: #5ba035;
            }
            #loader_loaded_wram {
                background-color: #da8609;
            }
            #loader_loaded_errors {
                background-color: #bf441d;
            }
        </style>
        <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
            <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
            <span class="close-jq-toast-single">×</span>
            <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
        </div>
    </div>
@endsection

