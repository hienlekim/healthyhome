@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
<style>
    .ms-container {
        background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
        width: auto;
        max-width: 370px;
    }
    .selectize-dropdown-header{
        display: none!important;
    }
</style>
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">CUSTOMER HANDOVER</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/customer-handover-staff")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
        <form class="mb-1" method="post" id="formBankAdd">
            {{csrf_field()}}
            <div class="card-box" style="margin-bottom: 4px!important;">
                <div class="row">
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Handover Staff <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="customers_staffs" value="{{$StaffCustomerBrand->staff_name}}" name="customers_staffs" autocomplete="off" disabled>
                                <input type="hidden" class="form-control" id="customers_staff" name="customers_staff" autocomplete="off" value="{{$StaffCustomerBrand->staff_name}}">
                                <input type="hidden" class="form-control" id="customers_staffs_id" name="customers_staffs_id" autocomplete="off" value="{{$StaffCustomerBrand->id}}">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Employee Takes The Job <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <select id="employee_takes_the_job" class="selectize-drop-header" placeholder="Select sales..." name="employee_takes_the_job">
                                    @foreach($Staffs as $item)
                                        <option value="{{$item->id}}">{{$item->staff_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>{{----end row_1----}}
            </div>{{----end card_1----}}

            <div class="card-box">
                <div class="row">
                    <div class="mb-2 col-md-12">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Customer Code</th>
                                    <th>Customer Name</th>
                                    <th>Payment Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $stt=0;?>
                                @if($customersCount > 0)
                                    @foreach($customers as $key)
                                            <?php $stt+=1; ?>
                                        <tr>
                                            <th scope="row">{{$stt}}</th>
                                            <th scope="row">{{$key['customers_code']}}</th>
                                            <th scope="row">{{$key['customers_name']}}</th>
                                            <th scope="row">@if($key->customers_payment_status==2)<p class="text-success">Đã Trả</p> @elseif($key->customers_payment_status==1) <p class="text-warning">Đang Nợ</p>@elseif($key->customers_payment_status==-1 || $key->customers_payment_status=='')<p class="text-info">Chưa giao dịch</p>@endif</th>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Content is not available or does not exist</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>{{----end row_3----}}
                <div class="row">
                    <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                        <button type="button" class="btn btn-primary waves-effect waves-light btnCreate" @if($brandUser['decenTLizaTion']['positions']['position_code'] == 'sales') @else disabled @endif >
                            <i class="fe-save mr-1"></i> Save
                        </button>
                    </div>
                </div>
            </div>{{----end card_3----}}


        </form>

</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
<script>
$(document).ready(function () {
    let localhost = window.location.hostname;
    $(document).on("click", ".btnCreate", function (event) {
        let employee_takes_the_job = $('#employee_takes_the_job').val();
        if(employee_takes_the_job==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the employee takes the job !')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            let myForm = document.getElementById('formBankAdd')
            // setTimeout(() => {
            $('#fadeShow').css('display','block');
            $.ajax({
                url:'{{url("api/customer-handover-staff/create")}}',
                type: "POST",
                dataType: 'json',
                data:new FormData(myForm),'_token':"{{ csrf_token() }}",
                cache : false,
                processData : false,
                contentType: false,
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#brand_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully added new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/customer-handover-staff")}}"; }, 3000);
                    }
                }
            });
        }
    });
})
</script>
        {{--sucess--}}
        <div class="jq-toast-wrap top-right">
            <style>
                #loader_loaded_success {
                    background-color: #5ba035;
                }
                #loader_loaded_wram {
                    background-color: #da8609;
                }
                #loader_loaded_errors {
                    background-color: #bf441d;
                }
            </style>
            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                <span class="close-jq-toast-single">×</span>
                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
            </div>
        </div>
@endsection

