@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">Staff Meet Customers</h2>
                </div>
            </td>
            <td>
{{--                <div class="col-xl-12 text-right">--}}
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/customers/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
{{--                </div>--}}
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table mb-0" style="min-width: 780px">
                        <thead style="border-top: none!important; ">
                        <tr>
                            <th style="width: 50px;">#</th>
                            <th style="width: 200px;">Staffs</th>
                            <th style="width: 200px;">Number Of Guests Meet</th>
                            <th style="width: 200px;">Meet Date</th>
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        @if($results->count()>0)
                            @foreach($list as $key)
                                <tr>
                                    <td>{{$key['stt']}}</td>
                                    <td class="text-uppercase"><a href="{{url("api/staff-meet-customers/edit")}}/{{$key['customers_staffs_id']}}">{{$key['staff_name']}}</a></td>
                                    <td class="text-right">{{$key['counts_customers']}}</td>
                                    <td>{{date_format($key['created_at'],"d-m-Y H:i:s")}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
@endsection


