@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<style>
    #images_avatas,#images_avatases,#images_avatasese,#images_avataseses{
        background-color: #fff;
        border: 1px dashed #ccc;
        border-radius: 3px;
        float: left;
        margin-left: 15px;
        max-height: 150px;
    }
</style>
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
<tr>
    <td>
        <div class="col-xl-12 text-left">
            <h2 class="header-title">Customer</h2>
        </div>
    </td>
    <td>
        <div class="col-xl-12 text-right">
            <h2 class="header-title">
                <a href="{{url("api/customers")}}" class="btn btn-primary waves-effect waves-light"><i
                        class="fe-rewind pr-1"></i>Back</a>
            </h2>
        </div>
    </td>
</tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
<form class="mb-1" method="Post" id="formStaffImages" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="card-box" style="margin-bottom: 4px!important;">
        <div class="row">
                <div class="col-md-6">
                    <div class="mb-2 row">
                        <label class="col-md-5 col-form-label" for="simpleinput">Customer Code <span class="text-danger">*</span></label>
                        <div class="col-md-7">
                            <input type="txt" class="form-control text-uppercase" id="customers_code1" value="{{$code}}" name="customers_code1" autocomplete="off" disabled>
                            <input type="hidden" class="form-control text-uppercase" id="customers_code" value="{{$code}}" name="customers_code" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-2 row">
                        <label class="col-md-5 col-form-label" for="simpleinput">Customer Name <span class="text-danger">*</span></label>
                        <div class="col-md-7">
                            <input type="txt" class="form-control" id="customers_name" name="customers_name" autocomplete="off">
                        </div>
                    </div>
                </div>
            <div class="mb-2 col-md-6">
                <div class="mb-2 row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Responsible Staff <span class="text-danger">*</span></label>
                    <div class="col-md-7">
                        <input type="txt" class="form-control" id="customers_staffs" value="{{$StaffCustomerBrand->staff_name}}" name="customers_staffs" autocomplete="off" disabled>
                        <input type="hidden" class="form-control" id="customers_staffs_id" name="customers_staffs_id" autocomplete="off" value="{{$StaffCustomerBrand->id}}">
                    </div>
                </div>
            </div>
            <div class="mb-2 col-md-6">
                <div class="mb-2 row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Phone</label>
                    <div class="col-md-7">
                            <input type="txt" class="form-control" id="customers_phone" name="customers_phone" autocomplete="off">
                            <input type="hidden" class="form-control" id="customers_brand_id" value="@if($StaffCustomerBrand['staff_brand_id']==0) 0 @else {{$StaffCustomerBrand['brands']['id']}}@endif" name="customers_brand_id" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="mb-2 col-md-6">
                <div class="mb-2 row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Customers Address</label>
                    <div class="col-md-7">
                        <input type="hidden" class="form-control" id="customers_parent_id" name="customers_parent_id" value="">
                        <textarea class="form-control" id="customers_address" name="customers_address" autocomplete="off" rows="1"></textarea>
                    </div>
                </div>
            </div>
{{--            <div class="mb-2 col-md-6">--}}
{{--                <div class="mb-2 row">--}}
{{--                    <label class="col-md-5 col-form-label" for="simpleinput">Customers Parent</label>--}}
{{--                    <div class="col-md-7">--}}
{{--                        <select class="form-control form-select-lg" id="customers_parent_id" name="customers_parent_id">--}}
{{--                            <option value="">------</option>--}}
{{--                            @foreach($customers as $item)--}}
{{--                                <option value="{{$item->id}}">{{$item->customers_name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="mb-2 col-md-6">
                <div class="mb-2 row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Customers Email</label>
                    <div class="col-md-7">
                        <input type="txt" class="form-control" id="customers_email" name="customers_email" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="mb-2 col-md-6">
                <div class="mb-2 row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Car</label>
                    <div class="col-md-7">
                        <input type="txt" class="form-control" id="car" name="car" autocomplete="off" >
                    </div>
                </div>
            </div>
            <div class="mb-2 col-md-6">
                <div class="mb-2 row">
                    <label class="col-md-5 col-form-label" for="simpleinput">House Type</label>
                    <div class="col-md-7">
                        <select class="form-control form-select-lg" id="house_type" name="house_type">
                            <option value="">------</option>
                            <option value="Villa">Villa</option>
                            <option value="Flat">Flat</option>
                            <option value="Apartment">Apartment</option>
                            <option value="Small house">Small house</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="mb-2 col-md-6">
                <div class="mb-2 row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Level</label>
                    <div class="col-md-7">
                        <select class="form-control form-select-lg" id="level" name="level">
                            <option value="">------</option>
                            <option value="Rich">Rich</option>
                            <option value="Medium">Medium</option>
                            <option value="Poor">Poor</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>{{----end row_1----}}
        <div class="row">
            <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                <button type="button" class="btn btn-primary waves-effect waves-light btnCreate" @if($StaffCustomerBrand['decenTLizaTion']['positions']['position_code'] == 'sales') @else disabled @endif>
                    <i class="fe-save mr-1"></i> Save
                </button>
            </div>
        </div>
    </div>{{----end card_1----}}
</form>
</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
<div class="row">
    <div class="col-md-12 text-center" style="margin-top: 10%">
        <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
        </br>
        <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
    </div>
</div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script>
$(document).ready(function () {
let localhost = window.location.hostname;
$(document).on("click", ".btnCreate", function (event) {
    let customers_code = $('#customers_code').val();
    let customers_name = $('#customers_name').val();
    let customers_staffs_id = $('#customers_staffs_id').val();
    let customers_brand_id = $('#customers_brand_id').val();
    let customers_parent_id = $('#customers_parent_id').val();
    let pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    let customers_email = $('#customers_email').val();
    let car = $('#car').val();
    let house_type = $('#house_type').val();
    let level = $('#level').val();
    let customers_phone = $('#customers_phone').val();
    if(!pattern.test(customers_email))
    {
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter not a valid e-mail!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_code ==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the code customer!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_name==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the name customers!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_staffs_id==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the responsible staff!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_brand_id==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the customers branch!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else{
        setTimeout(() => {
            $('#fadeShow').css('display','block');
            $.ajax({
                url:'{{url("api/customers/create")}}',
                type: "POST",
                dataType: 'json',
                data: {
                    '_token':"{{ csrf_token() }}",
                    'customers_code':customers_code,
                    'customers_name':customers_name,
                    'customers_staffs_id':customers_staffs_id,
                    'customers_brand_id':customers_brand_id,
                    'customers_parent_id':customers_parent_id,
                    'car':car,
                    'house_type':house_type,
                    'level':level,
                    'customers_phone':customers_phone,
                    'customers_email':customers_email,
                },
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#rank_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully added new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/customers")}}"; }, 3000);
                    }
                }
            });
        }, 1000);
    }
});
})
</script>
    {{--sucess--}}
    <div class="jq-toast-wrap top-right">
        <style>
            #loader_loaded_success {
                background-color: #5ba035;
            }
            #loader_loaded_wram {
                background-color: #da8609;
            }
            #loader_loaded_errors {
                background-color: #bf441d;
            }
        </style>
        <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
            <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
            <span class="close-jq-toast-single">×</span>
            <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
        </div>
    </div>
@endsection

