@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<style>
#images_avatas,#images_avatases,#images_avatasese,#images_avataseses{
background-color: #fff;
border: 1px dashed #ccc;
border-radius: 3px;
float: left;
margin-left: 15px;
max-height: 150px;
}
</style>
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
{{--    <link href="{{asset('assets/css/default/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />--}}
{{--    <link href="{{asset('assets/css/default/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />--}}
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
<tr>
<td>
<div class="col-xl-12 text-left">
    <h2 class="header-title">Customer</h2>
</div>
</td>
<td>
<div class="col-xl-12 text-right">
    <h2 class="header-title">
        <a href="{{url("api/customers")}}" class="btn btn-primary waves-effect waves-light"><i
                class="fe-rewind pr-1"></i>Back</a>
    </h2>
</div>
</td>
</tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
<form class="mb-1" method="Post" id="formStaffImages" enctype="multipart/form-data">
{{csrf_field()}}
<div class="card-box" style="margin-bottom: 4px!important;">
<div class="row">
    <div class="col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Customer Code <span class="text-danger">*</span></label>
            <div class="col-md-7">
                <input type="hidden" name="idCusomter_edit" id="idCusomter_edit" value="{{$data->id}}">
                <input type="txt" class="form-control text-uppercase" id="customers_code_edit" name="customers_code_edit" value="{{$data->customers_code}}" autocomplete="off" disabled>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Customer Name <span class="text-danger">*</span></label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="customers_name_edit" name="customers_name_edit" value="{{$data->customers_name}}" autocomplete="off" />
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Responsible Staff <span class="text-danger">*</span></label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="customers_staffs" value="{{$data['customerStaffs']['staff_name']}}" name="customers_staffs" autocomplete="off" disabled>
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Phone</label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="customers_phone_edit" name="customers_phone_edit" autocomplete="off" value="{{$data['customers_phone']}}">
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Customers Address</label>
            <div class="col-md-7">
                <textarea class="form-control" id="customers_address_edit" name="customers_address_edit" autocomplete="off" rows="1">
                    {{$data->customers_address}}
                </textarea>
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Customers Parent</label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="customers_phone_edit" name="customers_phone_edit" autocomplete="off" value="{{$customer_name}}" disabled>
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Customers Email</label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="customers_email_edit" name="customers_email_edit" autocomplete="off" value="{{$data['customers_email']}}">
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Car</label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="car_edit" name="car_edit" autocomplete="off"  value="{{$data['car']}}">
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">House Type</label>
            <div class="col-md-7">
                <select class="form-control form-select-lg" id="house_type_edit" name="house_type_edit">
                    <option value="">------</option>
                    <option value="Villa" @if($data['house_type']=="Villa") selected @else @endif>Villa</option>
                    <option value="Flat" @if($data['house_type']=="Flat") selected @else @endif>Flat</option>
                    <option value="Apartment" @if($data['house_type']=="Apartment") selected @else @endif>Apartment</option>
                    <option value="Small house" @if($data['house_type']=="Small house") selected @else @endif>Small house</option>
                </select>
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Level</label>
            <div class="col-md-7">
                <select class="form-control form-select-lg" id="level_edit" name="level_edit">
                    <option value="">------</option>
                    <option value="Rich" @if($data['level']=="Rich") selected @else @endif>Rich</option>
                    <option value="Medium" @if($data['level']=="Medium") selected @else @endif>Medium</option>
                    <option value="Poor" @if($data['level']=="Poor") selected @else @endif>Poor</option>
                </select>
            </div>
        </div>
    </div>
</div>{{----end row_1----}}
<div class="row">
    <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
        @if($StaffCustomerBrand['decenTLizaTion']['positions']['position_code'] == 'headbranch')
            <button type="button" class="btn btn-primary waves-effect waves-light btnUpdate">
                <i class="fe-save mr-1"></i> Save
            </button>
        @else  @endif
    </div>
</div>
</div>{{----end card_1----}}
</form>
<div class="row">
<div class="col-xl-12">
<div class="card">
    <div class="card-body">

        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                            aria-expanded="true" aria-controls="collapseOne">
                        <i class="fe-dollar-sign me-1 text-primary"></i> Sort payment Installment
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <div class="table-responsive mb-4">
                            <table class="table table-bordered mb-0" style="min-width: 1200px">
                                <thead>
                                <tr>
                                    <th>Oders Code</th>
                                    <th>Payment Installment</th>
                                    <th>payments</th>
                                    <th>Staff In Charge</th>
                                    <th>Payment Amount</th>
                                    <th>Installment Payment Amount</th>
                                    <th>Day Contract</th>
                                    <th>Actual Payment Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $t=0; ?>
                                @if($Period->count() > 0)
                                    @foreach($listPaymentPeriod as $item)
                                        @if($item['trow']==1)
                                            @foreach($item['listpayment'] as $val)
                                                <tr>
                                                    <td scope="row">{{$item['stock_code']}}</td>
                                                    <td>{{$val['period_name']}}</td>
                                                    <td>{{$val['payments_code']}}</td>
                                                    <td>{{$val['staff_name']}}</td>
                                                    <td>{{number_format($val['period_amount'], 2, ',', '.')}}</td>
                                                    <td>{{number_format($val['period_amount_batch'], 2, ',', '.')}}</td>
                                                    <td>{{date('d-m-Y', strtotime($val['period_date']))}}</td>
                                                    <td>@if($val['period_date_reality']!="") {{date('d-m-Y', strtotime($val['period_date_reality']))}} @else @endif</td>
                                                    <td>@if($val['period_status']==0) <span class="badge badge-success">Đã thanh toán</span> @elseif($val['period_status']==1) <span class="badge badge-danger">Chưa thanh toán</span> @endif</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <?php $t=0; ?>
                                            @foreach($item['listpayment'] as $val)
                                                <?php $t+=1; ?>
                                                @if($t==1)
                                                    <tr>
                                                        <td scope="row" rowspan="{{$item['trow']}}">{{$item['stock_code']}}</td>
                                                        <td>{{$val['period_name']}}</td>
                                                        <td>{{$val['payments_code']}}</td>
                                                        <td>{{$val['staff_name']}}</td>
                                                        <td>{{number_format($val['period_amount'], 2, ',', '.')}}</td>
                                                        <td>{{number_format($val['period_amount_batch'], 2, ',', '.')}}</td>
                                                        <td>{{date('d-m-Y', strtotime($val['period_date']))}}</td>
                                                        <td>@if($val['period_date_reality']!="") {{date('d-m-Y', strtotime($val['period_date_reality']))}} @else @endif</td>
                                                        <td>@if($val['period_status']==0) <span class="badge badge-success">Đã thanh toán</span> @elseif($val['period_status']==1) <span class="badge badge-danger">Chưa thanh toán</span> @endif</td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{$val['period_name']}}</td>
                                                        <td>{{$val['payments_code']}}</td>
                                                        <td>{{$val['staff_name']}}</td>
                                                        <td>{{number_format($val['period_amount'], 2, ',', '.')}}</td>
                                                        <td>{{number_format($val['period_amount_batch'], 2, ',', '.')}}</td>
                                                        <td>{{date('d-m-Y', strtotime($val['period_date']))}}</td>
                                                        <td>@if($val['period_date_reality']!="") {{date('d-m-Y', strtotime($val['period_date_reality']))}} @else @endif</td>
                                                        <td>@if($val['period_status']==0) <span class="badge badge-success">Đã thanh toán</span> @elseif($val['period_status']==1) <span class="badge badge-danger">Chưa thanh toán</span> @endif</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9" class="text-center">Content is not available or does not exist</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="fe-users me-1 text-primary"></i>Referral customer list
                    </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <table class="table table-bordered" style="min-width: 1200px">
                            <thead style="border-top: none!important; ">
                            <tr>
                                <th style="width: 50px;">#</th>
                                <th style="width: 100px;">Customer Code</th>
                                <th style="width: 200px;">Customer Name</th>
                                <th style="width: 200px;">Customer Branch</th>
                                <th style="width: 200px;">Responsible Staff</th>
                                <th style="width: 200px;">Payment Status</th>
                            </tr>
                            </thead>
                            <tbody class="contentTable">
                            <?php $t=0;$arrP = [];?>
                            @if($count>0)
                                @foreach($customerChildrent as $key)
                                    <?php $t=$t+1;array_push($arrP, 1); ?>
                                    <tr>
                                        <td>{{$t}}</td>
                                        <td class="text-uppercase">
                                            <a href="{{url("api/customers/edit/$key->id")}}">{{$key->customers_code}}</a></td>
                                        <td>{{$key->customers_name}}</td>
                                        <td>{{$key['brandCustomer']['brand_name']}}</td>
                                        <td>{{$key['customerStaffs']['staff_name']}}</td>
                                        <td>@if($key->customers_payment_status==2)<p class="text-success">Đã Trả</p> @elseif($key->customers_payment_status==1) <p class="text-warning">Đang Nợ</p>@elseif($key->customers_payment_status==-1 || $key->customers_payment_status=='')<p class="text-info">Chưa giao dịch</p>@endif</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">Content is not available or does not exist</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="fe-users me-1 text-primary"></i> Add new customer referrals
                    </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                     data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        <form class="mb-1" method="Post" id="formStaffImages" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="card-box" style="margin-bottom: 4px!important;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Customer Code <span class="text-danger">*</span></label>
                                            <div class="col-md-7">
                                                <input type="txt" class="form-control text-uppercase" id="customers_code1" name="customers_code1" autocomplete="off" disabled>
                                                <input type="hidden" class="form-control text-uppercase" id="customers_code" name="customers_code" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Customer Name <span class="text-danger">*</span></label>
                                            <div class="col-md-7">
                                                <input type="txt" class="form-control" id="customers_name" name="customers_name" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2 col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Responsible Staff <span class="text-danger">*</span></label>
                                            <div class="col-md-7">
                                                <input type="txt" class="form-control" id="customers_staffs" value="{{$StaffCustomerBrand->staff_name}}" name="customers_staffs" autocomplete="off" disabled>
                                                <input type="hidden" class="form-control" id="customers_staffs_id" name="customers_staffs_id" autocomplete="off" value="{{$StaffCustomerBrand->id}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2 col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Phone</label>
                                            <div class="col-md-7">
                                                <input type="txt" class="form-control" id="customers_phone" name="customers_phone" autocomplete="off">
                                                <input type="hidden" class="form-control" id="customers_brand_id" value="@if($StaffCustomerBrand['staff_brand_id']==0) 0 @else {{$StaffCustomerBrand['brands']['id']}}@endif" name="customers_brand_id" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2 col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Customers Address</label>
                                            <div class="col-md-7">
                                                <input type="hidden" class="form-control" id="customers_parent_id" name="customers_parent_id" value="{{$customer_id}}">
                                                <textarea class="form-control" id="customers_address" name="customers_address" autocomplete="off" rows="1"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-2 col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Customers Email</label>
                                            <div class="col-md-7">
                                                <input type="txt" class="form-control" id="customers_email" name="customers_email" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2 col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Car</label>
                                            <div class="col-md-7">
                                                <input type="txt" class="form-control" id="car" name="car" autocomplete="off" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2 col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">House Type</label>
                                            <div class="col-md-7">
                                                <select class="form-control form-select-lg" id="house_type" name="house_type">
                                                    <option value="">------</option>
                                                    <option value="Villa">Villa</option>
                                                    <option value="Flat">Flat</option>
                                                    <option value="Apartment">Apartment</option>
                                                    <option value="Small house">Small house</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-2 col-md-6">
                                        <div class="mb-2 row">
                                            <label class="col-md-5 col-form-label" for="simpleinput">Level</label>
                                            <div class="col-md-7">
                                                <select class="form-control form-select-lg" id="level" name="level">
                                                    <option value="">------</option>
                                                    <option value="Rich">Rich</option>
                                                    <option value="Medium">Medium</option>
                                                    <option value="Poor">Poor</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div>{{----end row_1----}}
                                <div class="row">
                                    <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                                        <button type="button" class="btn btn-primary waves-effect waves-light btnCreate" @if($StaffCustomerBrand['decenTLizaTion']['positions']['position_code'] == 'sales') @else disabled @endif>
                                            <i class="fe-save mr-1"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </div>{{----end card_1----}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
<!-- end accordion -->
    </div>
    <!-- end card body -->
</div>
<!-- end card -->
</div> <!-- end col -->

</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
<div class="row">
<div class="col-md-12 text-center" style="margin-top: 10%">
<div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
</br>
<div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
<div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
<div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
</div>
</div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>

<script>
$(document).ready(function () {
$('#collapseOnefadeIn').click(function(){
    $('#collapseOnefadeOut').css('display','flex');
    $('#collapseOnefadeIn').css('display','none');
    $('div#accordioncollapsecollapseOne').addClass('show')
})
$('#collapseOnefadeOut').click(function(){
    $('#collapseOnefadeIn').css('display','flex');
    $('#collapseOnefadeOut').css('display','none');
    $('div#accordioncollapsecollapseOne').removeClass('show')
})
$('#collapseTwofadeIn').click(function(){
    $('#collapseTwofadeOut').css('display','flex');
    $('#collapseTwofadeIn').css('display','none');
    $('div#accordioncollapsecollapseTwo').addClass('show')
})
$('#collapseTwofadeOut').click(function(){
    $('#collapseTwofadeIn').css('display','flex');
    $('#collapseTwofadeOut').css('display','none');
    $('div#accordioncollapsecollapseTwo').removeClass('show')
})
$('#collapseThreefadeIn').click(function(){
    $('#collapseThreefadeOut').css('display','flex');
    $('#collapseThreefadeIn').css('display','none');
    $('div#accordioncollapsecollapseThree').addClass('show')
})
$('#collapseThreefadeOut').click(function(){
    $('#collapseThreefadeIn').css('display','flex');
    $('#collapseThreefadeOut').css('display','none');
    $('div#accordioncollapsecollapseThree').removeClass('show')
})
let localhost = window.location.hostname;
$(document).on("click", ".btnUpdate", function (event) {
    let customers_name = $('#customers_name_edit').val();
    let id = $('#idCusomter_edit').val();
    let customers_address = $('#customers_address_edit').val();
    let pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    let customers_email = $('#customers_email_edit').val();
    let car = $('#car_edit').val();
    let house_type = $('#house_type_edit').val();
    let level = $('#level_edit').val();
    let customers_phone = $('#customers_phone_edit').val();
    if(!pattern.test(customers_email))
    {
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter not a valid e-mail!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_name==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the name customers!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else{
        setTimeout(() => {
            $('#fadeShow').css('display','block');
            $.ajax({
                url:'{{url("api/customers/update")}}/'+id,
                type: "PUT",
                dataType: 'json',
                data: {
                    '_token':"{{ csrf_token() }}",
                    'customers_name':customers_name,
                    'customers_address':customers_address,
                    'car':car,
                    'house_type':house_type,
                    'level':level,
                    'customers_phone':customers_phone,
                    'customers_email':customers_email,
                },
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#rank_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully update new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/customers")}}"; }, 3000);
                    }
                }
            });
        }, 1000);
    }
});
setInterval(function(){
    $.ajax({
        url:'{{url("api/customers/customer-type")}}',
        type: "GET",
        dataType: 'json',
        success: function(data) {
            $('#customers_code,#customers_code1').val(data['data']);
        }
    });
}, 9000);
$(document).on("click", ".btnCreate", function (event) {
    let id = $('#idCusomter_edit').val();
    let customers_code = $('#customers_code').val();
    let customers_name = $('#customers_name').val();
    let customers_staffs_id = $('#customers_staffs_id').val();
    let customers_brand_id = $('#customers_brand_id').val();
    let customers_parent_id = $('#customers_parent_id').val();
    let pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    let customers_email = $('#customers_email').val();
    let car = $('#car').val();
    let house_type = $('#house_type').val();
    let level = $('#level').val();
    let customers_phone = $('#customers_phone').val();
    if(!pattern.test(customers_email))
    {
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter not a valid e-mail!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_code ==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the code customer!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_name==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the name customers!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_staffs_id==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the responsible staff!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else if(customers_brand_id==""){
        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
        $('#successrequest').addClass('jq-icon-warning fade show')
        $('#content_success').text('warning!')
        $('#content_tb').text('please enter the customers branch!')
        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
    }else{
        setTimeout(() => {
            $('#fadeShow').css('display','block');
            $.ajax({
                url:'{{url("api/customers/create")}}',
                type: "POST",
                dataType: 'json',
                data: {
                    '_token':"{{ csrf_token() }}",
                    'customers_code':customers_code,
                    'customers_name':customers_name,
                    'customers_staffs_id':customers_staffs_id,
                    'customers_brand_id':customers_brand_id,
                    'customers_parent_id':customers_parent_id,
                    'car':car,
                    'house_type':house_type,
                    'level':level,
                    'customers_phone':customers_phone,
                    'customers_email':customers_email,
                },
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#rank_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully added new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/customers/edit/$data->id")}}"; }, 3000);
                    }
                }
            });
        }, 1000);
    }
});
})
</script>
{{--sucess--}}
<div class="jq-toast-wrap top-right">
<style>
#loader_loaded_success {
    background-color: #5ba035;
}
#loader_loaded_wram {
    background-color: #da8609;
}
#loader_loaded_errors {
    background-color: #bf441d;
}
</style>
<div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
<span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
<span class="close-jq-toast-single">×</span>
<h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
</div>
</div>
@endsection

