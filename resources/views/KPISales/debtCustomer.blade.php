@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
<style>
    .ms-container {
        background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
        width: auto;
        max-width: 370px;
    }
    .selectize-dropdown-header{
        display: none!important;
    }
    .selectize-control.selectize-drop-header.single.plugin-dropdown_header {
        text-align: left;
    }
</style>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">Debt Customer List</h2>
                </div>
            </td>
            <td>
{{--                <div class="col-xl-12 text-right">--}}
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/sales-kpi-payment/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
{{--                </div>--}}
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/sales-kpi-payment/debt-customer")}}">
            <div class="row">
                @if($Userbranch['Users']['name'] == 'administrators' || $Userbranch['Users']['name'] == 'admin')
                    <div class="col-xl-3 text-right">
                        <select class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                            <option value="">Chi Nhánh</option>
                            @foreach($branch as $item)
                                <option value="{{$item->id}}">{{$item->brand_name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                @else
                    <div class="col-xl-3 text-right">
                        <select style="display: none" class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                            <option value="">Chi Nhánh</option>
                        </select>
                    </div>
                @endif
                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                                <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                        type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    </div>
                <div class="col-xl-3 text-right">
                    <div class="mb-0 row">
                        <div class="input-group">
                            <div class="col-md-11 float-right">
                                <select id="searchInput" class="selectize-drop-header" placeholder="Select a customers..." name="searchInput">
                                    <option value="">Customers</option>
                                    @foreach($customers as $key)
                                        <option value="{{$key->period_customers_id}}">{{$key['periodcustomer']['customers_name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-1 float-lg-left pl-lg-0">
                            <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                    type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table table-bordered" style="min-width: 1200px">
                        <thead style="border-top: none!important; ">
                        <tr>
                            <th style="width: 200px;">Customers</th>
                            <th style="width: 200px;">Brand</th>
                            <th style="width: 200px;">Staffs Name</th>
                            <th style="width: 200px;">Stock Code</th>
                            <th style="width: 200px;">payment installment</th>
                            <th style="width: 200px;">Contract Payment Term</th>
                            <th style="width: 200px;">Status Payment</th>
                            <th style="width: 200px;">Date Customer Payment </th>
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        <?php $t=0 ?>
                        @if($count >0)
                            @foreach($listAllPaymentPeriod as $key)
                                @if($key['trowParent']==1)
                                    @foreach($key['listOdersPayment'] as $values)
                                        @if($values['trowChildrent']==1)
                                            @foreach($values['listOdersPeriod'] as $vs)
                                                <tr>
                                                    <td>{{$key['customer_name']}}</td>
                                                    <td>{{$key['brand']}}</td>
                                                    <td>{{$key['staffs_name']}}</td>
                                                    <td>{{$values['stock_code']}}</td>
                                                    <td>{{$vs['period_name']}}</td>
                                                    <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                    <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                    <td>
                                                        @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @elseif($values['trowChildrent']>1)
                                            <?php $t=0; ?>
                                            @foreach($values['listOdersPeriod'] as $vs)
                                                <?php $t +=1; ?>
                                                @if($t==1)
                                                    <tr>
                                                        <td rowspan="{{$values['trowChildrent']}}">{{$key['customer_name']}}</td>
                                                        <td rowspan="{{$values['trowChildrent']}}">{{$key['brand']}}</td>
                                                        <td rowspan="{{$values['trowChildrent']}}">{{$key['staffs_name']}}</td>
                                                        <td rowspan="{{$values['trowChildrent']}}">{{$values['stock_code']}}</td>
                                                        <td>{{$vs['period_name']}}</td>
                                                        <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                        <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                        <td>
                                                            @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{$vs['period_name']}}</td>
                                                        <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                        <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                        <td>
                                                            @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                    {{--kêt thuc 1--}}
                                @else
                                    <?php $k=0; ?>
                                    @foreach($key['listOdersPayment'] as $values)
                                        <?php $k +=1; ?>
                                        @if($k==1 && $values['trowChildrent']==1)
                                            @foreach($values['listOdersPeriod'] as $vs)
                                                <tr>
                                                    <td rowspan="{{$key['trowParent']}}">{{$key['customer_name']}}</td>
                                                    <td rowspan="{{$key['trowParent']}}">{{$key['brand']}}</td>
                                                    <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                    <td>{{$values['stock_code']}}</td>
                                                    <td>{{$vs['period_name']}}</td>
                                                    <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                    <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                    <td>
                                                        @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @elseif($k==1 && $values['trowChildrent']>1)
                                            <?php $j=0; ?>
                                            @foreach($values['listOdersPeriod'] as $vs)
                                                <?php $j +=1; ?>
                                                @if($j==1)
                                                    <tr>
                                                        <td rowspan="{{$key['trowParent']}}">{{$key['customer_name']}}</td>
                                                        <td rowspan="{{$key['trowParent']}}">{{$key['brand']}}</td>
                                                        <td rowspan="{{$key['trowParent']}}">{{$key['staffs_name']}}</td>
                                                        <td rowspan="{{$values['trowChildrent']}}">{{$values['stock_code']}}</td>
                                                        <td>{{$vs['period_name']}}</td>
                                                        <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                        <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                        <td>
                                                            @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{$vs['period_name']}}</td>
                                                        <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                        <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                        <td>
                                                            @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @else
                                            @if($values['trowChildrent']==1)
                                                @foreach($values['listOdersPeriod'] as $vs)
                                                    <tr>
                                                        <td>{{$values['stock_code']}}</td>
                                                        <td>{{$vs['period_name']}}</td>
                                                        <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                        <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                        <td>
                                                            @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @elseif($values['trowChildrent']>1)
                                                <?php $x=0; ?>
                                                @foreach($values['listOdersPeriod'] as $vs)
                                                    <?php $x +=1; ?>
                                                    @if($x==1)
                                                        <tr>
                                                            <td rowspan="{{$values['trowChildrent']}}">{{$values['stock_code']}}</td>
                                                            <td>{{$vs['period_name']}}</td>
                                                            <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                            <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                            <td>
                                                                @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                            </td>
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td>{{$vs['period_name']}}</td>
                                                            <td>{{date('d-m-Y', strtotime($vs['period_date']))}}</td>
                                                            <td>@if($vs['period_status']==0)<span class="badge badge-soft-success">Đã thanh toán</span>@else<span class="badge badge-soft-warning">Chưa thanh toán</span>@endif</td>
                                                            <td>
                                                                @if($vs['period_date_reality']!='') {{date('d-m-Y', strtotime($vs['period_date_reality']))}} @else @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                                {{--kêt thuc--}}
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-left">
                <b>Total:</b> <span class="TotalAll">{{$count}}</span>
            </div>
            <div class="col-md-4">
                <div class="dataTables_paginate paging_simple_numbers">{{$customersPeriodKpi->links()}}</div>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
@endsection


