@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
{{--<link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
<style>
    .ms-container {
        background: transparent url("http://reports.healthyhomes.com.vn/public/assets/images/plugins/multiple-arrow.png") no-repeat 50% 50%!important;
        width: auto;
        max-width: 370px;
    }
    .selectize-dropdown-header{
        display: none!important;
    }
</style>
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">Payment KPI All</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/sales-kpi-payment")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
    <div class="card-box">
        <form class="mb-1" method="get" id="formSearch" action="{{url("api/sales-kpi-payment/customers-period-kpi")}}">
            <div class="row">
                <div class="mb-2 col-md-12">
                    <div class="mb-2 row">
                        <label class="col-md-3 col-form-label" for="simpleinput">Sales<span
                                class="text-danger">*</span></label>
                        <div class="col-md-5">
                            <select id="sales_kpi" class="selectize-drop-header" placeholder="Select sales..." name="sales_kpi">
                                <option value=""></option>
                                @foreach($salesKpi as $key)
                                    <option value="{{$key->id}}">{{$key->staff_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary waves-effect waves-light btnSearch">
                                <i class="fe-search mr-1"></i> Search
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mb-2 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0" style="min-width: 1200px">
                            <thead style="border-top: none!important; ">
                            <tr>
                                <th style="width: 200px;">Customer Name</th>
                                <th style="width: 200px;">Orders Code</th>
                                <th style="width: 200px;">Period name</th>
                                <th style="width: 200px;">Period Payment Status</th>
                                <th style="width: 200px;">Payment Status KPI</th>
                                <th style="width: 200px;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($listAllPaymentPeriod)
                                    @foreach($listAllPaymentPeriod as $key)
                                        @if($key['trowParent']==1)
                                            @foreach($key['listOdersPayment'] as $values)
                                                @if($values['trowChildrent']==1)
                                                    @foreach($values['listOdersPeriod'] as $vs)
                                                        <tr>
                                                            <td>{{$key['customer_name']}}</td>
                                                            <td>{{$values['stock_code']}}</td>
                                                            <td>{{$vs['period_name']}}</td>
                                                            <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                            <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                            <td>
                                                                <div class="form-check font-16 mb-0">
                                                                    <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                    <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @elseif($values['trowChildrent']>1)
                                                    <?php $t=0; ?>
                                                    @foreach($values['listOdersPeriod'] as $vs)
                                                        <?php $t +=1; ?>
                                                        @if($t==1)
                                                            <tr>
                                                                <td rowspan="{{$values['trowChildrent']}}">{{$key['customer_name']}}</td>
                                                                <td rowspan="{{$values['trowChildrent']}}">{{$values['stock_code']}}</td>
                                                                <td>{{$vs['period_name']}}</td>
                                                                <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                                <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{$vs['period_name']}}</td>
                                                                <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                                <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                            {{--kêt thuc 1--}}
                                        @else
                                            <?php $k=0; ?>
                                            @foreach($key['listOdersPayment'] as $values)
                                                <?php $k +=1; ?>
                                                @if($k==1 && $values['trowChildrent']==1)
                                                    @foreach($values['listOdersPeriod'] as $vs)
                                                        <tr>
                                                            <td rowspan="{{$key['trowParent']}}">{{$key['customer_name']}}</td>
                                                            <td>{{$values['stock_code']}}</td>
                                                            <td>{{$vs['period_name']}}</td>
                                                            <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                            <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                            <td>
                                                                <div class="form-check font-16 mb-0">
                                                                    <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                    <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @elseif($k==1 && $values['trowChildrent']>1)
                                                    <?php $j=0; ?>
                                                    @foreach($values['listOdersPeriod'] as $vs)
                                                        <?php $j +=1; ?>
                                                        @if($j==1)
                                                            <tr>
                                                                <td rowspan="{{$key['trowParent']}}">{{$key['customer_name']}}</td>
                                                                <td rowspan="{{$values['trowChildrent']}}">{{$values['stock_code']}}</td>
                                                                <td>{{$vs['period_name']}}</td>
                                                                <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                                <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{$vs['period_name']}}</td>
                                                                <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                                <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @if($values['trowChildrent']==1)
                                                        @foreach($values['listOdersPeriod'] as $vs)
                                                            <tr>
                                                                <td>{{$values['stock_code']}}</td>
                                                                <td>{{$vs['period_name']}}</td>
                                                                <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                                <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                                <td>
                                                                    <div class="form-check font-16 mb-0">
                                                                        <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                        <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @elseif($values['trowChildrent']>1)
                                                        <?php $x=0; ?>
                                                        @foreach($values['listOdersPeriod'] as $vs)
                                                            <?php $x +=1; ?>
                                                            @if($x==1)
                                                                <tr>
                                                                    <td rowspan="{{$values['trowChildrent']}}">{{$values['stock_code']}}</td>
                                                                    <td>{{$vs['period_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                                    <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td>{{$vs['period_name']}}</td>
                                                                    <td><span class="badge badge-soft-success">{{$vs['period_status']}}</span></td>
                                                                    <td><span class="badge badge-soft-warning">{{$vs['period_kpi_status']}}</span></td>
                                                                    <td>
                                                                        <div class="form-check font-16 mb-0">
                                                                            <input class="form-check-input publish" type="checkbox" id="listOdersCheck_{{$vs['stt']}}" value="{{$vs['id']}}-{{$vs['period_customers_id']}}-{{$vs['period_stock_id']}}">
                                                                            <label class="form-check-label" for="listOdersCheck_{{$vs['stt']}}">&nbsp;</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                {{--kêt thuc--}}
                                    @endforeach
                                    <input type="hidden" value="{{$stt}}" id="array_stt">
                                @else
                                    <td colspan="6" class="text-center">Content is not available or does not exist</td>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
        <form class="mb-1" method="post" id="formAdd">
            {{csrf_field()}}
            <div class="row">
                <div class="mb-2 col-md-12">
                    <div class="col-md-5">
                        <input type="hidden" name="staff_kpi_sales_id" id="staff_kpi_sales_id" value="{{$sales}}">
                        <input type="hidden" name="PeriodId" id="PeriodId">
                        <input type="hidden" name="PeriodCustomersId" id="PeriodCustomersId">
                        <input type="hidden" name="PeriodStockId" id="PeriodStockId">
                        <input type="hidden" name="staff_kpi_order_brand_id" id="staff_kpi_order_brand_id" value="@if($Brand['staff_brand_id']==0) 0 @else {{$Brand['brands']['id']}}@endif">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                    <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                        <i class="fe-save mr-1"></i> Save  @if($Brand['decenTLizaTion']['positions']['position_code'] == 'headbranch') @else disabled @endif
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
<script>
$(document).ready(function () {
    let localhost = window.location.hostname;
    $('.publish').click(function(){
        let number_stt = $('#array_stt').val();
        let arrayPeriodId = new Array();
        let arrayPeriodCustomersId = new Array();
        let arrayPeriodStockId = new Array();
        for(let i = 1; i <= number_stt; i++) {
            if($('#listOdersCheck_' + i).is(':checked')) {
                // arrayId[i] = $('#listOdersCheck_' + i).val();
                let numberCheck = $('#listOdersCheck_' + i).val();
                const myArray = numberCheck.split("-");
                arrayPeriodId[i] = myArray[0];
                arrayPeriodCustomersId[i] = myArray[1];
                arrayPeriodStockId[i] = myArray[2];
            }
        }
        $('#PeriodId').val(arrayPeriodId);
        $('#PeriodCustomersId').val(arrayPeriodCustomersId);
        $('#PeriodStockId').val(arrayPeriodStockId);
    })

    $(document).on("click", ".btnCreate", function (event) {
        let periodId = $('#PeriodId').val();
        let periodCustomersId = $('#PeriodCustomersId').val();
        let periodStockId = $('#PeriodStockId').val();
        let staff_kpi_order_brand_id = $('#staff_kpi_order_brand_id').val();
        let staff_kpi_sales_id = $('#staff_kpi_sales_id').val();
        $('#fadeShow').css('display','block');
        if(staff_kpi_sales_id==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please choose staff sales !')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            $.ajax({
                url:'{{url("api/sales-kpi-payment/create")}}',
                type: "POST",
                dataType: 'json',
                data: {
                    '_token':"{{ csrf_token() }}",
                    'periodId':periodId,
                    'periodCustomersId':periodCustomersId,
                    'periodStockId':periodStockId,
                    'staff_kpi_order_brand_id':staff_kpi_order_brand_id,
                    'staff_kpi_sales_id':staff_kpi_sales_id,
                },
                success: function(data) {
                    console.log(data)
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#origin_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully added new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/sales-kpi-payment")}}"; }, 3000);
                    }
                }
            });
        }
    });
})
</script>
        {{--sucess--}}
        <div class="jq-toast-wrap top-right">
            <style>
                #loader_loaded_success {
                    background-color: #5ba035;
                }
                #loader_loaded_wram {
                    background-color: #da8609;
                }
                #loader_loaded_errors {
                    background-color: #bf441d;
                }
            </style>
            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                <span class="close-jq-toast-single">×</span>
                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
            </div>
        </div>
@endsection

