@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">Received Customers</h2>
                </div>
            </td>
{{--            <td>--}}
{{--                <div class="col-xl-12 text-right">--}}
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/customer-care/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
{{--                </div>--}}
{{--            </td>--}}
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/customer-gift")}}">
                <div class="row">
                    <div class="col-xl-3 text-left">
                        <div class="dataTables_length" id="products-datatable_length"><label>Display
                                <select class="form-select form-select-sm mx-1" id="limit" name="limit">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="all">all</option>
                                </select>column</label>
                        </div>
                    </div>
                    @if($StaffCustomerBrand->staff_brand_id==0)
                        <div class="col-xl-3">
                            <select class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                                <option value="">Chi Nhánh</option>
                                @foreach($branch as $item)
                                    <option value="{{$item->id}}">{{$item->brand_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <div class="col-xl-3">
                            <select style="display: none" class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                                <option value="">Chi Nhánh</option>
                            </select>
                        </div>
                    @endif

                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                                <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                        type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead style="border-top: none!important; ">
                        <tr>
                            <th style="width: 50px;">#</th>
                            <th style="width: 200px;">Customer Code</th>
                            <th style="width: 200px;">Customer Name</th>
                            <th style="width: 200px;">Customer Branch</th>
                            <th style="width: 200px;">Responsible Staff</th>
                            <th style="width: 200px;">Gifts Status</th>
                        </tr>
                        </thead>
                        <tbody class="contentTable">
                        <?php $t=0 ?>
                        @if($count >0)
                            @foreach($results as $key)
                                <?php $t=$t+1 ?>
                                <tr>
                                    <td>{{$t}}</td>
                                    <td class="text-uppercase">
                                        @if($key->customers_status==2)

                                            <a href="{{url("api/orders-gift/edit/$key->customers_stock_id")}}">{{$key->customers_code}}</a>
                                        @else
                                            <a href="{{url("api/orders-gift/store")}}">{{$key->customers_code}}</a>
                                        @endif
                                    </td>
                                    <td>{{$key->customers_name}}</td>
                                    <td>{{$key['brandCustomer']['brand_name']}}</td>
                                    <td>{{$key['customerStaffs']['staff_name']}}</td>
                                    <td>@if($key->customers_status==2)<p class="text-success">Đã Tặng Quà</p> @elseif($key->customers_status==0||$key->customers_status=="") <p class="text-warning">Chưa Tặng Quà</p>@endif</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="text-center">Content is not available or does not exist</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-left">
                <b>Total:</b> <span class="TotalAll">{{$count}}</span>
            </div>
            <div class="col-md-4">
                <div class="dataTables_paginate paging_simple_numbers">{{$results->links()}}</div>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
@endsection


