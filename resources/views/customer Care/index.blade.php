@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled="disabled">
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" disabled="disabled">
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">CUSTOMERS CARE</h2>
                </div>
            </td>
            <td>
{{--                <div class="col-xl-12 text-right">--}}
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/orders/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
{{--                </div>--}}
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/customer-care")}}">
                <div class="row">
                    <div class="col-xl-3 text-left">
                        <div class="dataTables_length" id="products-datatable_length"><label>Display
                                <select class="form-select form-select-sm mx-1" id="limit" name="limit">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="all">all</option>
                                </select>column</label>
                        </div>
                    </div>
                    @if($StaffCustomerBrand->staff_brand_id==0)
                        <div class="col-xl-3">
                            <select class="form-control form-select-lg" id="customers_parent_id" name="customers_parent_id">
                                <option value="">Chi Nhánh</option>
                                @foreach($branch as $item)
                                    <option value="{{$item->id}}">{{$item->brand_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <div class="col-xl-3">
                            <select style="display: none" class="form-control form-select-lg" id="customers_parent_id" name="customers_parent_id">
                                <option value="">Chi Nhánh</option>
                            </select>
                        </div>
                    @endif

                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                                <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                        type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="table mb-0 mt-4" style="min-width: 980px">
                        <thead class="title_table">
                            <th style="width: 50px;">#</th>
                            <th style="width: 200px;">Staff</th>
                            <th style="width: 200px;">Customers</th>
                            <th style="width: 200px;">Branch</th>
                            <th style="width: 200px;">Status</th>
                            <th style="width: 200px;">Number Days Overdue</th>
                            <th style="width: 200px;">Take Care Customers</th>
                            <th style="width: 200px;">Date Now</th>
                        </thead>
                        <tbody>
                            <?php $t=0 ?>
                            @if($count >0)
                                @foreach($results as $key)
                                    <?php
                                    $t=$t+1;
                                    $datediff = floor((strtotime($date) - strtotime($key['customer_care_date_end']))/(60*60*24));
                                    ?>
                                    <tr>
                                        <td>{{$t}}</td>
                                        <td>{{$key['staffCustom']['staff_name']}}</td>
                                        <td>{{$key['custom']['customers_name']}}</td>
                                        <td>{{$key['brandCustom']['brand_name']}}</td>
                                        <td>@if($key['customer_care_status']==1 || $key['customer_care_status']==0)
                                                @if($datediff >=0)
                                                    @if($brandUser['decenTLizaTion']['positions']['position_code'] == 'sales')
                                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light" id="CSKH" data-id="{{$key->id}}">Chưa CSKH</button>
                                                    @else
                                                        <span class="badge badge-danger text-white">Chưa CSKH</span>
                                                    @endif
                                                @else
                                                    <span class="badge badge-warning text-white">Chờ CSKH</span>
                                                @endif
                                            @elseif($key['customer_care_status']==2)
                                                <span class="badge badge-success text-white">Đã CSKH</span>
                                            @endif
                                        </td>
                                        <td>@if($key['customer_care_status']==1 || $key['customer_care_status']==0)
                                                @if($datediff >=0)
                                                    <span class="badge badge-danger">{{abs($datediff)}}</span>
                                                @else
                                                    <span class="badge badge-warning text-white">{{abs($datediff)}}</span>
                                                @endif
                                            @elseif($key['customer_care_status']==2)
                                                <span class="badge badge-success text-white">Đã CSKH</span>
                                            @endif
                                        </td>
                                        <td>{{date('d-m-Y', strtotime($key->customer_care_date_end))}}</td>
                                        <td>{{date('d-m-Y', strtotime($date))}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">Content is not available or does not exist</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-left">
                <b>Total:</b> <span class="TotalAll">{{$count}}</span>
            </div>
            <div class="col-md-4">
                <div class="dataTables_paginate paging_simple_numbers">{{$results->links()}}</div>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/libs/tablesaw/tablesaw.js')}}"></script>
<script src="{{asset('assets/js/pages/tablesaw.init.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        {{--let date_now = {{$date}};--}}
        {{--let customer_care_date_end = $('#customer_care_date_end').val();--}}
        {{--// let period_date =data['data']['dates'];--}}
        {{--let d_date_now = new Date(date_now);--}}
        {{--let d_customer_care_date_end = new Date(customer_care_date_end);--}}
        {{--// sum = Math.ceil(d_period_date - d_receipts_amount)--}}
        {{--// let dtime = Math.ceil(sum/(1000*60*60*24))--}}
        {{--// // console.log(period_date);--}}
        // $('#number_days_overdue,#number_days_overdue1').val(dtime);
        $('#CSKH').on("click",function () {
            let id = $(this).attr('data-id');
            // setTimeout(() => {
            $('#fadeShow').css('display', 'block');
            if(id>0){
                $.ajax({
                    url: '{{url("api/customer-care/care-cskh")}}/' + id,
                    type: "put",
                    dataType: 'json',
                    data: {
                        '_token': "{{ csrf_token() }}",
                        'id': id,
                    },
                    success: function (data) {
                        if (data.errors == true) {
                            $('.jq-toast-loader').attr('id', 'loader_loaded_wram').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-warning fade show')
                            $('#content_success').text('warning!')
                            $('#content_tb').text(data.message)
                            setTimeout(function () {
                                $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');
                                $('#fadeShow').css('display', 'none');
                            }, 3000);
                            $('#bank_code').focus();
                        } else {
                            $('.jq-toast-loader').attr('id', 'loader_loaded_success').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-success fade show')
                            $('#content_success').text('success!')
                            $('#content_tb').text('Customers care successful')
                            setTimeout(function () {
                                $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');
                                location.href = "{{url("api/customer-care")}}";
                            }, 3000);
                        }
                    }
                });
            }
        });
    });
</script>
{{--sucess--}}
{{--<div class="jq-toast-wrap top-right">--}}
{{--    <style>--}}
{{--        #loader_loaded_success {--}}
{{--            background-color: #5ba035;--}}
{{--        }--}}
{{--        #loader_loaded_wram {--}}
{{--            background-color: #da8609;--}}
{{--        }--}}
{{--        #loader_loaded_errors {--}}
{{--            background-color: #bf441d;--}}
{{--        }--}}
{{--    </style>--}}
{{--    <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">--}}
{{--        <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>--}}
{{--        <span class="close-jq-toast-single">×</span>--}}
{{--        <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>--}}
{{--    </div>--}}
{{--</div>--}}
@endsection


