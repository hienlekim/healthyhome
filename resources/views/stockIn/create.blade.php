@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">STOCK</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/purchases")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
        <form class="mb-1" method="post" id="formBankAdd">
            {{csrf_field()}}
            <div class="card-box" style="margin-bottom: 4px!important;">
                <div class="row">
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Stock Code <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control text-uppercase" id="stock_code1" name="stock_code1" autocomplete="off" disabled>
                                <input type="hidden" class="form-control text-uppercase" id="stock_code" name="stock_code" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Supplier <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <select class="form-control form-select-lg" id="stock_supplier_id" name="stock_supplier_id">
                                    <option value="">------</option>
                                    @foreach($superliser as $item)
                                        <option value="{{$item->id}}">{{$item->supplier_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Location <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <select class="form-control form-select-lg" id="stock_location" name="stock_location">
                                    <option value="">------</option>
                                    @foreach($location as $item)
                                        <option value="{{$item->id}}">{{$item->ware_house_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Staff  <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="stock_staff_id" name="stock_staff_id" autocomplete="off" value="{{$name_user}}" disabled>
                            </div>
                        </div>
                    </div>
                </div>{{----end row_1----}}
            </div>{{----end card_1----}}

            <div class="card-box" style="margin-bottom: 4px!important;">
                <div class="row">
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Brand <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="stock_brand_id" name="stock_brand_id" autocomplete="off" value="{{$brandUser['brands']['brand_name']}}" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Description </label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="stock_description" name="stock_description" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Product Type <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <select class="form-control form-select-lg" id="product_type_name" name="product_type_name">
                                    <option value="">------</option>
                                    @foreach($productType as $item)
                                        <option value="{{$item->product_type_code}}">{{$item->product_type_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                </div>{{----end row_2----}}
            </div>{{----end card_2----}}

            <div class="card-box">
                <div class="row">
                    <div class="mb-2 col-md-12">
                        <div class="table-responsive">
                            <table class="table mb-0" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Product</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                </tr>
                                </thead>
                                <tbody>
                                @for($i=1;$i<11;$i++)
                                    <tr>
                                        <th scope="row">{{$i}}</th>
                                        <td style="width: 40%">
                                            <select class="form-control form-select-lg" id="stock_product_id_{{$i}}" name="stock_product_id_{{$i}}">
                                                <option value="0">------</option>
                                                @foreach($products as $item)
                                                    <option value="{{$item->id}}">{{$item->product_name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="width: 22%">
                                            <select class="form-control form-select-lg" id="stock_product_unit_id_{{$i}}" name="stock_product_unit_id_{{$i}}">
                                                <option value="0">------</option>
                                                @foreach($unit as $item)
                                                    <option value="{{$item->id}}">{{$item->unit_name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td style="width: 22%">
                                            <input type="txt" class="form-control text-right" id="stock_quality_reality_{{$i}}" name="stock_quality_reality_{{$i}}" autocomplete="off" value="0">
                                        </td>
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>{{----end row_3----}}
                <div class="row">
                    <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                        <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                            <i class="fe-save mr-1"></i> Save
                        </button>
                    </div>
                </div>
            </div>{{----end card_3----}}


        </form>

</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script>
$(document).ready(function () {
    setInterval(function(){
        $.ajax({
            url:'{{url("api/purchases/stock-type")}}',
            type: "GET",
            dataType: 'json',
            success: function(data) {
               $('#stock_code,#stock_code1').val(data['data']);
            }
        });
    }, 7000);

    var thousand_sep = '.',
    dec_sep = ',',
    dec_length = 3;
    var format = function (num) {
        var str = num.toString().replace("", ""),
            parts = false,
            output = [],
            i = 1,
            formatted = null;
        if (str.indexOf(dec_sep) > 0) {
            parts = str.split(dec_sep);
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != thousand_sep) {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(thousand_sep);
                }
                i++;
            }
        }

        if (output.slice(-1)[0] === '-' && output.slice(-2)[0] === thousand_sep ) {
            output.splice(-2, 1);
        }

        formatted = output.reverse().join("");
        return (formatted + ((parts) ? dec_sep + parts[1].substr(0, dec_length) : ""));
    };
    for(let i=1;i<11;i++){
        $("#stock_quality_reality_"+i).keyup(function (e) {
            $(this).val(format($(this).val()));
        });
        $('#stock_product_id_'+i).on('change',function (){
            let product_id = $(this).val()

        })
    }
    let localhost = window.location.hostname;
    $(document).on("click", ".btnCreate", function (event) {
        let stock_supplier_id = $('#stock_supplier_id').val();
        let stock_location = $('#stock_location').val();
        let product_type_name = $('#product_type_name').val();
        if(stock_supplier_id ==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the supplier!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(stock_location==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the location !')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(product_type_name==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter product type!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            let myForm = document.getElementById('formBankAdd')
            // setTimeout(() => {
                $('#fadeShow').css('display','block');
                $.ajax({
                    url:'{{url("api/purchases/create")}}',
                    type: "POST",
                    dataType: 'json',
                    data:new FormData(myForm),'_token':"{{ csrf_token() }}",
                    cache : false,
                    processData : false,
                    contentType: false,
                    success: function(data) {
                        if(data.errors==true){
                            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-warning fade show')
                            $('#content_success').text('warning!')
                            $('#content_tb').text(data.message)
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                            $('#brand_code').focus();
                        }else{
                            $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-success fade show')
                            $('#content_success').text('success!')
                            $('#content_tb').text('successfully added new data')
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/purchases")}}"; }, 3000);
                        }
                    }
                });
            // }, 1000);
        }
    });
})
</script>
        {{--sucess--}}
        <div class="jq-toast-wrap top-right">
            <style>
                #loader_loaded_success {
                    background-color: #5ba035;
                }
                #loader_loaded_wram {
                    background-color: #da8609;
                }
                #loader_loaded_errors {
                    background-color: #bf441d;
                }
            </style>
            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                <span class="close-jq-toast-single">×</span>
                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
            </div>
        </div>
@endsection

