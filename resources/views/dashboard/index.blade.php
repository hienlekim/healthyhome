@extends('template')

@section('title', "Dashboard")

@section('css')

@endsection

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Instruction File</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <h4 class="mt-0 font-16">Manager</h4>
                <p class="text-muted mb-0"> is the administrator of the system</p>
            </div>
        </div>
    </div>

    <!-- end row -->
@endsection

@section('javascript')

@endsection
