@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
<tr>
<td>
    <div class="col-xl-12 text-left">
        <h2 class="header-title">SALES ORDER</h2>
    </div>
</td>
<td>
    <div class="col-xl-12 text-right">
        <h2 class="header-title">
            <a href="{{url("api/orders")}}" class="btn btn-primary waves-effect waves-light"><i
                    class="fe-rewind pr-1"></i>Back</a>
        </h2>
    </div>
</td>
</tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
<form class="mb-1" method="post" id="formBankAdd">
{{csrf_field()}}
<div class="card-box" style="margin-bottom: 4px!important;">
    <div class="row">
        <div class="mb-2 col-md-6">
            <input type="hidden" value="{{$stockCode}}" id="stockId" name="stockId">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Stock Code <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <input type="txt" class="form-control text-uppercase" id="stock_code" name="stock_code" value="{{$data->stock_code}}" autocomplete="off" disabled>
                </div>
            </div>
        </div>

        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Customer <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <select class="form-control form-select-lg" id="stock_customer_id" name="stock_customer_id">
                        <option value="">------</option>
                        @if($Customer)
                            @foreach($Customer as $item)
                                @if($data->stock_customer_id == $item->id)
                                    <option value="{{$item->id}}" selected>{{$item->customers_name}}</option>
                                @else
                                    <option value="{{$item->id}}">{{$item->customers_name}}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Location <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <select class="form-control form-select-lg" id="stock_location" name="stock_location">
                        <option value="">------</option>
                        @foreach($location as $item)
                            @if($data->stock_location == $item->id)
                                <option value="{{$item->id}}" selected>{{$item->ware_house_name}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item->ware_house_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Staff  <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <input type="txt" class="form-control" id="stock_staff" name="stock_staff" autocomplete="off" value="{{$data['staffStock']['staff_name']}}" disabled>
                    <input type="hidden" class="form-control" id="stock_staff_id" name="stock_staff_id" autocomplete="off" value="{{$brandUser['id']}}">
                </div>
            </div>
        </div>
    </div>{{----end row_1----}}
</div>{{----end card_1----}}

<div class="card-box" style="margin-bottom: 4px!important;">
    <div class="row">
{{--        <div class="mb-2 col-md-6">--}}
{{--            <div class="mb-2 row">--}}
{{--                <label class="col-md-5 col-form-label" for="simpleinput">Branch <span class="text-danger">*</span></label>--}}
{{--                <div class="col-md-7">--}}
{{--                    <input type="txt" class="form-control" id="stock_brand_id" name="stock_brand_id" autocomplete="off" value="{{$data['brands']['brand_name']}}" disabled>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Description </label>
                <div class="col-md-7">
                    <input type="txt" class="form-control" id="stock_description" name="stock_description" autocomplete="off"  value="{{$data->stock_description}}">
                </div>
            </div>
        </div>

        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Type <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <select class="form-control form-select-lg" id="product_type_name" name="product_type_name">
                        <option value="">------</option>
                        @foreach($productType as $item)
                            @if($data->stock_type == $item->product_type_code)
                                <option value="{{$item->product_type_code}}" selected>{{$item->product_type_name}}</option>
                            @else
                                <option value="{{$item->product_type_code}}">{{$item->product_type_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
{{--        <div class="mb-2 col-md-6">--}}
{{--            <div class="mb-2 row">--}}
{{--                <label class="col-md-5 col-form-label" for="simpleinput">Currency <span class="text-danger">*</span></label>--}}
{{--                <div class="col-md-7">--}}
{{--                    <input type="txt" class="form-control" id="stock_currency" name="stock_currency" autocomplete="off" value="{{$data['currencies']['currency_name']}}" disabled>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Serial Number <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <input type="txt" class="form-control" id="stock_seri_number" name="stock_seri_number" autocomplete="off"  value="{{$data['stock_seri_number']}}">
                </div>
            </div>
        </div>
        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Payments <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <select class="form-control form-select-lg" id="period_payments_id" name="period_payments_id">
                        <option value="">------</option>
                        @foreach($payment as $item)
                            @if($PeriodPayment->period_payments_id == $item->id)
                                <option value="{{$item->id}}" selected>{{$item->payments_name}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item->payments_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

        </div>
    </div>{{----end row_2----}}
</div>{{----end card_2----}}

<div class="card-box">
    <div class="row">
        <div class="mb-2 col-md-12">
            <div class="table-responsive">
                <table class="table mb-0" style="min-width: 900px;">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Product</th>
{{--                        <th>Unit</th>--}}
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total Money</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $stt =0; ?>
                    @foreach($dataProduct as $items)
                        <?php $stt +=1; ?>
                        <tr>
                            <th scope="row">{{$stt}}</th>
                            <td style="width: 20%">
                                <select class="form-control form-select-lg" id="stock_product_id_{{$stt}}" name="stock_product_id_{{$stt}}">
                                    <option value="0">------</option>
                                    @foreach($products as $item)
                                        @if($items->stock_product_id == $item->id)
                                            <option value="{{$item->id}}" selected>{{$item->product_name}}</option>
                                        @else
                                            <option value="{{$item->id}}">{{$item->product_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
{{--                            <td style="width: 10%">--}}
{{--                                <input type="txt" class="form-control" id="stock_product_unit_id" value="{{$items['units']['unit_name']}}" name="stock_product_unit_id" autocomplete="off" disabled>--}}
{{--                            </td>--}}
                            <td style="width: 20%">
                                <input type="txt" class="form-control text-right" id="stock_quality_reality" name="stock_quality_reality" autocomplete="off" value="{{number_format(-$items['stock_quality_reality'], 2, ',', '.')}}">
                            </td>
                            <td style="width: 20%">
                                <input type="txt" class="form-control text-right" id="product_price_ex" name="product_price_ex" autocomplete="off" value="{{number_format($items['stock_price_export'], 2, ',', '.')}}">
                            </td>
                            <td style="width: 30%">
                                <input type="txt" class="form-control text-right" id="stock_amount}" name="stock_amount" autocomplete="off" value="{{number_format(-$items['stock_amount'], 2, ',', '.')}}">
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>{{----end row_3----}}
    <div class="row">
        <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
            @if($brandUser['decenTLizaTion']['positions']['position_code'] == 'sales' && $data->stock_status==1)
                <button type="button" class="btn btn-primary waves-effect waves-light btnApprove" >
                    <i class="fe-save mr-1"></i> Approve
                </button>
                <button type="button" class="btn btn-danger waves-effect waves-light btnCancell" >
                    <i class="fe-trash-2 mr-1"></i>Cancel
                </button>
{{--            @elseif($brandUser['decenTLizaTion']['positions']['position_code'] == 'sales' && $data->stock_status==2)--}}
{{--                <button type="button" class="btn btn-primary waves-effect waves-light btn" disabled>--}}
{{--                    <i class="fe-save mr-1"></i> Send Approve--}}
{{--                </button>--}}
{{--            @elseif($brandUser['decenTLizaTion']['positions']['position_code'] == 'thukho' && $data->stock_status==2)--}}
{{--                <button type="button" class="btn btn-primary waves-effect waves-light btnApprove" >--}}
{{--                    <i class="fe-save mr-1"></i> Approve--}}
{{--                </button>--}}
{{--                <button type="button" class="btn btn-danger waves-effect waves-light btnCancellApprove" >--}}
{{--                    <i class="fe-trash-2 mr-1"></i>Cancel Approve--}}
{{--                </button>--}}
            @elseif($data->stock_status==3)
                <a href="{{url("api/receipts/create")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>Receipts</a>
            @endif
        </div>
    </div>
</div>{{----end card_3----}}


</form>

</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
<div class="row">
<div class="col-md-12 text-center" style="margin-top: 10%">
    <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
    </br>
    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
</div>
</div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script>
$(document).ready(function () {
    let localhost = window.location.hostname;
    $(document).on("click", ".btnCancell", function (event) {
        let id = $('#stockId').val();
            // let myForm = document.getElementById('formBankAdd')
            // setTimeout(() => {
            $('#fadeShow').css('display','block');
            $.ajax({
                url:'{{url("api/orders/cancel")}}/'+id,
                type: "PUT",
                dataType: 'json',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'id': id,
                },
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#brand_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('Cancel data successfully')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/orders")}}"; }, 3000);
                    }
                }
            });
    });
    //send approve
    $(document).on("click", ".btnSendApprove", function (event) {
        let id = $('#stockId').val();
        // let myForm = document.getElementById('formBankAdd')
        // setTimeout(() => {
        $('#fadeShow').css('display','block');
        $.ajax({
            url:'{{url("api/orders/send-approve")}}/'+id,
            type: "PUT",
            dataType: 'json',
            data: {
                '_token': "{{ csrf_token() }}",
                'id': id,
            },
            success: function(data) {
                if(data.errors==true){
                    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-warning fade show')
                    $('#content_success').text('warning!')
                    $('#content_tb').text(data.message)
                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                    $('#brand_code').focus();
                }else{
                    $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-success fade show')
                    $('#content_success').text('success!')
                    $('#content_tb').text('Send approve data successfully')
                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/orders")}}"; }, 3000);
                }
            }
        });
    });
    //thu kho duyet
    $(document).on("click", ".btnApprove", function (event) {
        let id = $('#stockId').val();
        // let myForm = document.getElementById('formBankAdd')
        // setTimeout(() => {
        $('#fadeShow').css('display','block');
        $.ajax({
            url:'{{url("api/orders/send-approve")}}/'+id,
            type: "PUT",
            dataType: 'json',
            data: {
                '_token': "{{ csrf_token() }}",
                'id': id,
            },
            success: function(data) {
                if(data.errors==true){
                    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-warning fade show')
                    $('#content_success').text('warning!')
                    $('#content_tb').text(data.message)
                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                    $('#brand_code').focus();
                }else{
                    $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-success fade show')
                    $('#content_success').text('success!')
                    $('#content_tb').text('Stocker approve data successfully')
                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/orders")}}"; }, 3000);
                }
            }
        });
    });
    //thu kho huy duyet  btnCancellApprove
    $(document).on("click", ".btnCancellApprove", function (event) {
        let id = $('#stockId').val();
        // let myForm = document.getElementById('formBankAdd')
        // setTimeout(() => {
        $('#fadeShow').css('display','block');
        $.ajax({
            url:'{{url("api/orders/cancel-approve")}}/'+id,
            type: "PUT",
            dataType: 'json',
            data: {
                '_token': "{{ csrf_token() }}",
                'id': id,
            },
            success: function(data) {
                if(data.errors==true){
                    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-warning fade show')
                    $('#content_success').text('warning!')
                    $('#content_tb').text(data.message)
                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                    $('#brand_code').focus();
                }else{
                    $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                    $('#successrequest').addClass('jq-icon-success fade show')
                    $('#content_success').text('success!')
                    $('#content_tb').text('Stocker cancel approve data successfully')
                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/orders")}}"; }, 3000);
                }
            }
        });
    });
})
</script>
{{--sucess--}}
<div class="jq-toast-wrap top-right">
<style>
    #loader_loaded_success {
        background-color: #5ba035;
    }
    #loader_loaded_wram {
        background-color: #da8609;
    }
    #loader_loaded_errors {
        background-color: #bf441d;
    }
</style>
<div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
    <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
    <span class="close-jq-toast-single">×</span>
    <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
</div>
</div>
@endsection

