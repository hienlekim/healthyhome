@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<style>
/*#content_images{*/
/*    position: absolute;*/
/*    top: 0px;*/
/*    right: 21px;*/
/*}*/
</style>
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">Staff</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/staffs")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
    <form class="mb-1" method="Post" id="formStaffImages" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="card-box" style="margin-bottom: 4px!important;">
            <div class="row">
                <table style="width: 100%">
                    <td rowspan="2">
                        <div class="col-sm-12 text-center">
                            <input type="hidden" id="staff_id" name="staff_id" value="{{$data['id']}}"/>
                            <div class="mt-3">
                                <img src="{{asset("images/staff/$Images")}}" alt="image" id="images_avata" class="img-fluid rounded-circle" style="width: 100px">
                                <div class="fileupload waves-effect text-center" id="content_images">
                                    <span class="text-info text-center"><i class="fe-instagram font-24"></i></span>
                                    <input type="file" class="upload" id="images_avata" name="images_avata" onchange="preview_image(event)">
                                </div>
                            </div>
                        </div>

                    </td>
                    <td>
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Staff Code <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control text-uppercase" value="{{$data['staff_code']}}" id="staff_code" name="staff_code" autocomplete="off" disabled>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="col-md-12">
                            <div class="row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Staff Name <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="staff_name" name="staff_name" autocomplete="off" value="{{$data['staff_name']}}">
                                </div>
                            </div>
                        </div>
                    </td>
    <tr>
        <td>
            <div class="mb-2 col-md-12">
                <div class="row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Gender</label>
                    <div class="col-md-7">
                        <select class="form-control form-select-lg" id="staff_sex" name="staff_sex">
                            <option value="0" @if($data['staff_sex'] ==0) selected @else @endif>Nam</option>
                                <option value="1" @if($data['staff_sex'] ==1) selected @else @endif>Nữ</option>
                        </select>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div class="col-md-12">
                <div class="row">
                    <label class="col-md-5 col-form-label" for="simpleinput">Staff new</label>
                    <div class="col-md-7">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="staff_status_new" name="staff_status_new"
                                   value="{{$data['staff_status_new']}}" @if($data['staff_status_new']==1) checked="True" @else @endif>
                            <label class="custom-control-label" for="staff_status_new"></label>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    </table>
</div>{{----end row_1----}}
</div>{{----end card_1----}}
<div class="card-box" style="margin-bottom: 4px!important;">
<div class="row">
    <div class="mb-2 col-md-4">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Username<span
                    class="text-danger">*</span></label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="name" name="name" autocomplete="off" value="{{$users['name']}}">
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-4">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Email<span class="text-danger">*</span></label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="emails" name="emails" autocomplete="off" value="{{$users['email']}}">
            </div>
        </div>
    </div>

    <div class="mb-2 col-md-4">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Password<span
                    class="text-danger">*</span></label>
            <div class="col-md-7">
                <input type="password" class="form-control" id="password" name="passwords" autocomplete="off"
                       value="{{$users['password']}}">
            </div>
        </div>
    </div>

</div>{{----end row_2----}}
</div>{{----end card_2----}}
<div class="card-box" >
<div class="row">
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Date of Birth <span class="text-danger">*</span></label>
            <div class="col-md-7">
                <input class="form-control" type="date" name="staff_year_old" id="example-date staff_year_old" value="{{$data['staff_year_old']}}">
            </div>
        </div>
    </div>

    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Working Day <span class="text-danger">*</span></label>
            <div class="col-md-7">
                <input class="form-control" type="date" name="staff_date_start" id="example-date staff_date_start" value="{{$data['staff_date_start']}}">
            </div>
        </div>
    </div>

    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Branch<span class="text-danger">*</span></label>
            <div class="col-md-7">
                <select class="form-control form-select-lg" id="staff_brand_id" name="staff_brand_id">
                    <option value="">------</option>
                    @foreach($dataBrand as $item)
                        @if($data->staff_brand_id ==0)
                            <option value="0" selected>Both Brand</option>
                        @elseif($data->staff_brand_id ==$item->id)
                            <option value="{{$item->id}}" selected>{{$item->brand_name}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->brand_name}}</option>
                        @endif
                    @endforeach

                </select>
            </div>
        </div>
    </div>

    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Bank Number</label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="staff_number_bank" name="staff_number_bank" autocomplete="off" value="{{$data['staff_number_bank']}}">
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Bank Name</label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="staff_name_bank" name="staff_name_bank" autocomplete="off" value="{{$data['staff_name_bank']}}">
            </div>
        </div>
    </div>
    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Address</label>
            <div class="col-md-7">
                <input type="txt" class="form-control" id="staff_address" name="staff_address" autocomplete="off" value="{{$data['staff_address']}}">
            </div>
        </div>
    </div>

    <div class="mb-2 col-md-6">
        <div class="mb-2 row">
            <label class="col-md-5 col-form-label" for="simpleinput">Position <span class="text-danger">*</span></label>
            <div class="col-md-7">
                <select class="form-control form-select-lg" id="staff_decentralization_id" name="staff_decentralization_id">
                    <option value="">------</option>
                    @foreach($dataPositionDecentral as $item)
                        @if($item->id==$data->staff_decentralization_id)
                            <option value="{{$item->id}}" selected>{{$item['positions']['position_name']}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item['positions']['position_name']}}</option>
                        @endif

                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>{{----end row_3----}}
<div class="row">
    <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
        <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
            <i class="fe-save mr-1"></i> Save
        </button>
    </div>
</div>
</div>{{----end card_3----}}
</form>
</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
<div class="row">
    <div class="col-md-12 text-center" style="margin-top: 10%">
        <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
        </br>
        <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
    </div>
</div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script>
function preview_image(event)
{
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('images_avata');
        output.src = reader.result;
    }
    let images = reader.readAsDataURL(event.target.files[0]);
}
$(document).ready(function () {
    $(document).on("click", ".btnCreate", function (event) {
        event.preventDefault();
        // Get form
        let staff_id = $('#staff_id').val();
        let staff_name = $('#staff_name').val();
        let staff_year_old = $('#staff_year_old').val();
        let staff_date_start = $('#staff_date_start').val();
        let staff_brand_id = $('#staff_brand_id').val();
        let staff_decentralization_id = $('#staff_decentralization_id').val();
        let name = $('#name').val();
        let emails = $('#emails').val();
        let password = $('#password').val();
        let pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
        if(!pattern.test(emails))
        {
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter not a valid e-mail!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(staff_name==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the name staff!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(staff_year_old==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the staff year old!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(staff_date_start==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the working day!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(staff_brand_id==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the brand!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(staff_decentralization_id==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the position!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(name==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the name!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(emails==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the email!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(password==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the password!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            let myForm = document.getElementById('formStaffImages')
            $.ajax({
                url:'{{url("api/staffs/update")}}/'+ staff_id,
                type: "POST",
                dataType: 'json',
                data:new FormData(myForm),'_token': "{{ csrf_token() }}",
                cache : false,
                processData : false,
                contentType: false,
                // body: JSON.stringify(data) || null,
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#currency_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully added new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/staffs")}}"; }, 3000);
                    }
                }
            });
        }
    });
})
</script>
{{--sucess--}}
<div class="jq-toast-wrap top-right">
<style>
    #loader_loaded_success {
        background-color: #5ba035;
    }
    #loader_loaded_wram {
        background-color: #da8609;
    }
    #loader_loaded_errors {
        background-color: #bf441d;
    }
</style>
<div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
    <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
    <span class="close-jq-toast-single">×</span>
    <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
</div>
</div>
@endsection

