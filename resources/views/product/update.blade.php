@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<style>
#images_avatas,#images_avatases,#images_avatasese,#images_avataseses{
background-color: #fff;
border: 1px dashed #ccc;
border-radius: 3px;
float: left;
margin-left: 15px;
max-height: 150px;
}
</style>
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
<tr>
<td>
    <div class="col-xl-12 text-left">
        <h2 class="header-title">Product</h2>
    </div>
</td>
<td>
    <div class="col-xl-12 text-right">
        <h2 class="header-title">
            <a href="{{url("api/products")}}" class="btn btn-primary waves-effect waves-light"><i
                    class="fe-rewind pr-1"></i>Back</a>
        </h2>
    </div>
</td>
</tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
<form class="mb-1" method="Post" id="formStaffImages" enctype="multipart/form-data">
{{csrf_field()}}
<div class="card-box" style="margin-bottom: 4px!important;">
    <div class="row">
        <div class="col-md-6">
            <div class="mb-2 row">
                <input type="hidden" id="product_id" name="product_id" value="{{$data['id']}}">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Code <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <input type="txt" class="form-control text-uppercase" id="product_code" name="product_code" value="{{$data['product_code']}}" autocomplete="off" disabled>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Name <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <input type="txt" class="form-control" id="product_name" name="product_name" autocomplete="off" value="{{$data['product_name']}}">
                </div>
            </div>
        </div>
        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Origin</label>
                <div class="col-md-7">
                    <select class="form-control form-select-lg" id="product_origin_id" name="product_origin_id">
                        <option value="">------</option>
                        @foreach($origin as $item)
                            @if($data->product_origin_id ==$item->id)
                                <option value="{{$item->id}}"selected>{{$item->origin_name}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item->origin_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Size</label>
                <div class="col-md-7">
                    <input type="txt" class="form-control" id="product_size" name="product_size" autocomplete="off"
                           value="{{$data['product_size']}}">
                </div>
            </div>
        </div>
    </div>{{----end row_1----}}
</div>{{----end card_1----}}
<div class="card-box" style="margin-bottom: 4px!important;">
    <div class="row">
        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product content </label>
                <div class="col-md-7">
                    <textarea class="form-control" id="example-textarea product_content" name="product_content" autocomplete="off" rows="1">{{$data['product_content']}}</textarea>
                </div>
            </div>
        </div>
        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Description</label>
                <div class="col-md-7">
                    <textarea class="form-control" id="example-textarea product_description" name="product_description" autocomplete="off" rows="1">
                        {{$data['product_description']}}
                    </textarea>
                </div>
            </div>
        </div>

        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Price Import </label>
                <div class="col-md-7">
                    <input type="txt" class="form-control" id="product_price_import" value="{{number_format($data['product_price_import'], 2, ',', '.')}}" name="product_price_import" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="mb-2 col-md-6">
            <div class="mb-2 row">
                <label class="col-md-5 col-form-label" for="simpleinput">Product Price Export <span class="text-danger">*</span></label>
                <div class="col-md-7">
                    <input type="txt" class="form-control" id="product_price_ex" name="product_price_ex" autocomplete="off" value="{{number_format($data['product_price_ex'], 2, ',', '.')}}">
                </div>
            </div>
        </div>

    </div>{{----end row_2----}}
</div>{{----end card_2----}}
<div class="card-box" >
    <div class="row">
        <div class="mb-2 ">
            <img src="{{asset("images/product/$img_1")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 150px">
            <div class="fileupload waves-effect text-center" id="images_avata">
                <span class="text-info text-center"><i class="fe-instagram font-24"></i></span>
                <input type="file" class="upload" id="images_avatas" name="images_avata_1" onchange="preview_image_1(event)">
            </div>
        </div>

        <div class="mb-2">
            <img src="{{asset("images/product/$img_2")}}" alt="image" id="images_avatases" class="img-fluid" style="width: 150px">
            <div class="fileupload waves-effect text-center" id="content_images">
                <span class="text-info text-center"><i class="fe-instagram font-24"></i></span>
                <input type="file" class="upload" id="images_avatases" name="images_avata_2" onchange="preview_image_2(event)">
            </div>
        </div>
        <div class="mb-2">
            <img src="{{asset("images/product/$img_3")}}" alt="image" id="images_avatasese" class="img-fluid" style="width: 150px">
            <div class="fileupload waves-effect text-center" id="content_images">
                <span class="text-info text-center"><i class="fe-instagram font-24"></i></span>
                <input type="file" class="upload" id="images_avatasese" name="images_avata_3" onchange="preview_image_3(event)">
            </div>
        </div>
        <div class="mb-2">
            <img src="{{asset("images/product/$img_4")}}" alt="image" id="images_avataseses" class="img-fluid"
                 style="width: 150px">
            <div class="fileupload waves-effect text-center" id="content_images">
                <span class="text-info text-center"><i class="fe-instagram font-24"></i></span>
                <input type="file" class="upload" id="images_avataseses" name="images_avata_4"
                       onchange="preview_image_4(event)">
            </div>
        </div>
    </div>{{----end row_3----}}
    <div class="row">
        <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
            <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                <i class="fe-save mr-1"></i> Save
            </button>
        </div>
    </div>
</div>{{----end card_3----}}
</form>
</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
<div class="row">
<div class="col-md-12 text-center" style="margin-top: 10%">
    <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
    </br>
    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
</div>
</div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script>
function preview_image_1(event)
{
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('images_avatas');
        output.src = reader.result;
    }
    let images = reader.readAsDataURL(event.target.files[0]);
}

function preview_image_2(event)
{
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('images_avatases');
        output.src = reader.result;
    }
    let images = reader.readAsDataURL(event.target.files[0]);
}
function preview_image_3(event)
{
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('images_avatasese');
        output.src = reader.result;
    }
    let images = reader.readAsDataURL(event.target.files[0]);
}
function preview_image_4(event)
{
    var reader = new FileReader();
    reader.onload = function()
    {
        var output = document.getElementById('images_avataseses');
        output.src = reader.result;
    }
    let images = reader.readAsDataURL(event.target.files[0]);
}
$(document).ready(function () {
    var thousand_sep = '.',
        dec_sep = ',',
        dec_length = 3;
    var format = function (num) {
        var str = num.toString().replace("", ""),
            parts = false,
            output = [],
            i = 1,
            formatted = null;
        if (str.indexOf(dec_sep) > 0) {
            parts = str.split(dec_sep);
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != thousand_sep) {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(thousand_sep);
                }
                i++;
            }
        }

        if (output.slice(-1)[0] === '-' && output.slice(-2)[0] === thousand_sep ) {
            output.splice(-2, 1);
        }

        formatted = output.reverse().join("");
        return (formatted + ((parts) ? dec_sep + parts[1].substr(0, dec_length) : ""));
    };
    $("#product_price_import").keyup(function (e) {
        $(this).val(format($(this).val()));

    });
    $('#product_price_ex').keyup(function (e){
        $(this).val(format($(this).val()));
    })
    $(document).on("click", ".btnCreate", function (event) {
        event.preventDefault();
        // Get form
        let product_name = $('#product_name').val();
        let product_price_ex = $('#product_price_ex').val();
        let id = $('#product_id').val();
        // let product_size = $('#product_size').val();
        // let product_content = $('#product_content').val();
        // let product_description = $('#product_description').val();
        // let product_price_import = $('#product_price_import').val();
        // let product_price_ex = $('#product_price_ex').val();
        if(product_name ==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the product name!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(product_price_ex =="" || product_price_ex==0){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the product price export!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            let myForm = document.getElementById('formStaffImages')
            $.ajax({
                url:'{{url("api/products/update")}}/'+ id,
                type: "POST",
                dataType: 'json',
                data:new FormData(myForm),'_token':"{{ csrf_token() }}",
                cache : false,
                processData : false,
                contentType: false,
                // body: JSON.stringify(data) || null,
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#currency_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('successfully added new data')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/products")}}"; }, 3000);
                    }
                }
            });
        }
    });
})
</script>
{{--sucess--}}
<div class="jq-toast-wrap top-right">
<style>
    #loader_loaded_success {
        background-color: #5ba035;
    }
    #loader_loaded_wram {
        background-color: #da8609;
    }
    #loader_loaded_errors {
        background-color: #bf441d;
    }
</style>
<div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
    <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
    <span class="close-jq-toast-single">×</span>
    <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
</div>
</div>
@endsection

