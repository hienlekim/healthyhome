<!DOCTYPE html>
<html lang="en" foxified>
<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | Healthyhome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css"/>
    {{--    <link href="https://coderthemes.com/minton/layouts/assets/css/default/app.min.css" rel="stylesheet" type="text/css" />--}}
    @yield('css')
</head>

<body>

<div id="wrapper">
<?php  $staff = DB::table('staff')->select('b.brand_name','staff.staff_brand_id','staff.staff_decentralization_id','staff.staff_rank_id')
    ->leftjoin('brand as b','staff.staff_brand_id','=','b.id')
    ->where('staff.staff_user_id',\Illuminate\Support\Facades\Auth::user()->id)->first();
?>

<!-- Topbar Start -->
    <div class="navbar-custom">
        <div class="container-fluid">

            <ul class="list-unstyled topnav-menu float-right mb-0">
                @if($staff->staff_decentralization_id=='5')
                    <li class="dropdown notification-list topbar-dropdown">
                        <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="fe-bell noti-icon"></i>
                            <span class="badge bg-danger rounded-circle noti-icon-badge" id="countnotification"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end dropdown-lg" id="content">
                        </div>
                    </li>
                @endif
                @if(\Illuminate\Support\Facades\Auth::guard()->check())
                    {{--                            <li class="dropdown notification-list topbar-dropdown">--}}
                    {{--                                <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown"--}}
                    {{--                                   href="#" role="button" aria-haspopup="false" aria-expanded="false">--}}
                    {{--                                    <img src="{{asset('assets/images/users/avatar-1.jpg')}}" alt="user-image" class="rounded-circle">--}}
                    {{--                                    <span class="pro-user-name ml-1">--}}
                    {{--                         {{\Illuminate\Support\Facades\Auth::user()->name}} <i class="mdi mdi-chevron-down"></i>--}}
                    {{--                    </span>--}}
                    {{--                                </a>--}}
                    {{--                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">--}}
                    {{--                                    <a href="{{url('account/profile')}}" class="dropdown-item notify-item">--}}
                    {{--                                        <i class="remixicon-account-circle-line"></i>--}}
                    {{--                                        <span>Account</span>--}}
                    {{--                                    </a>--}}

                    {{--                                    <a href="{{url('account/change-password')}}" class="dropdown-item notify-item">--}}
                    {{--                                        <i class="remixicon-lock-2-fill"></i>--}}
                    {{--                                        <span>Change Password</span>--}}
                    {{--                                    </a>--}}

                    {{--                                    <a href="javascript:void(0);" class="dropdown-item notify-item">--}}
                    {{--                                        <i class="remixicon-wallet-line"></i>--}}
                    {{--                                        <span>My Wallet <span class="badge badge-success float-right">3</span> </span>--}}
                    {{--                                    </a>--}}

                    {{--                                    <div class="dropdown-divider"></div>--}}



                    {{--                                </div>--}}
                    {{--                            </li>--}}
{{--                    <li class="dropdown notification-list topbar-dropdown">--}}
{{--                        <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light show" data-toggle="dropdown"--}}
{{--                           href="#" role="button" aria-haspopup="false" aria-expanded="true">--}}
{{--                            <img src="{{asset('images/staff/avatar-6.jpg')}}" alt="user-image" class="rounded-circle" style="width: 40px">--}}
{{--                            <span class="pro-user-name ml-1">--}}
{{--                                     {{\Illuminate\Support\Facades\Auth::user()->name}} <i class="mdi mdi-chevron-down"></i>--}}
{{--                                </span>--}}
{{--                            <div class="dropdown-menu dropdown-menu-end profile-dropdown" style="">--}}
{{--                                <a href="{{url('logout')}}" class="dropdown-item notify-item">--}}
{{--                                    <i class="remixicon-logout-box-line"></i>--}}
{{--                                    <span>Logout</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                    </li>--}}

                        <li class="dropdown notification-list topbar-dropdown">
                            <a class="nav-link dropdown-toggle nav-user me-0 waves-effect waves-light" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="{{asset('images/staff/avatar-6.jpg')}}" alt="user-image" class="rounded-circle">
                                <span class="pro-user-name ms-1">
                                    {{\Illuminate\Support\Facades\Auth::user()->name}} <i class="mdi mdi-chevron-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end profile-dropdown ">

                                <!-- item-->
                                <a href="{{url('logout')}}" class="dropdown-item notify-item">
                                    <i class="ri-logout-box-line"></i>
                                    <span>Logout</span>
                                </a>
                            </div>
                        </li>
                @endif
                <li class="d-none d-lg-block">
                    <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#"
                       role="button" aria-haspopup="false" aria-expanded="false">
                        <i class="mdi mdi-bank footable pr-1"></i> @if($staff->staff_brand_id==0) Both Brand @else {{$staff->brand_name}} @endif
                    </a>
                </li>

                <li class="dropdown d-inline-block d-lg-none">
                    <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#"
                       role="button" aria-haspopup="false" aria-expanded="false">
                        <i class="mdi mdi-bank footable pr-1"></i> @if($staff->staff_brand_id==0) Both Brand @else {{$staff->brand_name}} @endif
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
                <li>
                    <button class="button-menu-mobile waves-effect waves-light">
                        <i class="fe-menu"></i>
                    </button>
                </li>
                <li>
                    <div class="logo-box">
                        <a href="{{url('/')}}" class="logo text-left">
                <span class="logo-lg">
                    {{--<img src="assets/images/logo-light.png" alt="" height="20">--}}
                    <span class="logo-lg-text-light text-white">Healthy Homes</span>
                </span>
                            <span class="logo-sm">
                            <span class="logo-sm-text-light text-white font-weight-bold font-13">Healthy Homes</span>
                            <img src="assets/images/logo-sm.png" alt="" height="24">
                        </span>
                        </a>
                    </div>
                </li>

            </ul>
        </div>
    </div>

    <div class="left-side-menu">
        <div class="slimscroll-menu">
            <div id="sidebar-menu">
                <ul class="metismenu" id="side-menu">
                    @if($staff->staff_decentralization_id=='3' || $staff->staff_decentralization_id=='4' || $staff->staff_decentralization_id=='5')
                        <li>
                            <!--  <a href="javascript: void(0);" class="waves-effect">
                                 <i class="mdi mdi-cards"></i>
                                 <span> Business System </span>
                                 <span class="menu-arrow"></span>
                             </a> -->
                            <a href="#BusinessSystem" data-bs-toggle="collapse" aria-expanded="false" aria-controls="BusinessSystem">
                                <i class="fe-briefcase"></i>
                                <span>Business System</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <div class="collapse pl-2" id="BusinessSystem">
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{url("api/orders")}}"><i class="fas fa-angle-double-right pr-1"></i>Order</a>
                                    </li>
                                    {{--                            <li>--}}
                                    {{--                                <a href="{{url("api/orders-gift")}}"><i class="fas fa-angle-double-right pr-1"></i>Installment Orders</a>--}}
                                    {{--                            </li>--}}
                                    <li>
                                        <a href="{{url("api/orders-gift")}}"><i class="fas fa-angle-double-right pr-1"></i>Gift Order</a>
                                    </li>
                                    @if($staff->staff_decentralization_id=='5')
                                        <li>
                                            <a href="{{url("api/payment")}}"><i class="fas fa-angle-double-right pr-1"></i>Payment</a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </li>
                    @endif
                    @if($staff->staff_decentralization_id=='2' || $staff->staff_decentralization_id=='5')
                        <li>
                            <a href="#WarehousesSystem" data-bs-toggle="collapse" aria-expanded="false" aria-controls="WarehousesSystem">
                                <i class="fas fa-building"></i>
                                <span> Warehouses System</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <div class="collapse pl-2" id="WarehousesSystem">
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{url("api/products")}}"><i class="fas fa-angle-double-right pr-1"></i>Product</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/productType")}}"><i class="fas fa-angle-double-right pr-1"></i>Product Type</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/units")}}"><i class="fas fa-angle-double-right pr-1"></i>Unit</a>
                                    </li>
                                    {{--                            <li>--}}
                                    {{--                                <a href="{{url("api/origin")}}"><i class="fas fa-angle-double-right pr-1"></i>Origin</a>--}}
                                    {{--                            </li>--}}
                                    <li>
                                        <a href="{{url("api/purchases")}}"><i class="fas fa-angle-double-right pr-1"></i>Stock</a>
                                    </li>

                                    <li>
                                        <a href="{{url("api/inventories")}}"><i class="fas fa-angle-double-right pr-1"></i>List Inventory</a>
                                    </li>
                                    {{--                            <li>--}}
                                    {{--                                <a href="{{url('metric')}}"><i class="fas fa-angle-double-right pr-1"></i>Synthesis Report</a>--}}
                                    {{--                            </li>--}}
                                </ul>
                            </div>
                        </li>
                    @endif
                    @if($staff->staff_decentralization_id=='3' || $staff->staff_decentralization_id=='4' || $staff->staff_decentralization_id=='5')
                        <li>
                            <a href="#Customers" data-bs-toggle="collapse" aria-expanded="false" aria-controls="Customers">
                                <i class="fas fa-diagnoses"></i>
                                <span>Customers </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <div class="collapse pl-2" id="Customers">
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{url("api/customers")}}"><i class="fas fa-angle-double-right pr-1"></i>Customers </a>
                                    </li>
                                    @if($staff->staff_decentralization_id=='3' || $staff->staff_decentralization_id=='4')
                                        <li>
                                            <a href="{{url('api/company-assets')}}"><i class="fas fa-angle-double-right pr-1"></i>Smell</a>
                                        </li>
                                        <li>
                                            <a href="{{url("api/staff-meet-customers/list")}}"><i class="fas fa-angle-double-right pr-1"></i>Staff Meet Customers</a>
                                        </li>
                                        <li>
                                            <a href="{{url('api/customer-handover-staff')}}"><i class="fas fa-angle-double-right pr-1"></i>Customer Handover</a>
                                        </li>
                                        <li>
                                            <a href="{{url("api/sales-kpi-payment/debt-customer")}}"><i class="fas fa-angle-double-right pr-1"></i>Debt Customer</a>
                                        </li>
                                    @endif
                                    <li>
                                        <a href="{{url('api/customer-care')}}"><i class="fas fa-angle-double-right pr-1"></i>Customers Care</a>
                                    </li>
                                    <li>
                                        <a href="{{url('api/customer-gift')}}"><i class="fas fa-angle-double-right pr-1"></i>Received Customers</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endif

                    <li>
                    {{--                        <a href="#" class="waves-effect">--}}
                    {{--                            <i class="fas fa-clinic-medical"></i>--}}
                    {{--                            <span>Supplier</span>--}}
                    {{--                            <span class="menu-arrow"></span>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav-second-level" aria-expanded="false">--}}
                    {{--                            <li>--}}
                    {{--                                <a href="{{url("api/suppliers")}}"><i class="fas fa-angle-double-right pr-1"></i>Suppliers</a>--}}
                    {{--                            </li>--}}
                    {{--                            <li>--}}
                    {{--                                <a href="{{url('carrier')}}"><i class="fas fa-angle-double-right pr-1"></i>Supplier Order List</a>--}}
                    {{--                            </li>--}}
                    {{--                            <li>--}}
                    {{--                                <a href="{{url('carrier')}}"><i class="fas fa-angle-double-right pr-1"></i>Supplier Debt List</a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    <li>
                        <a href="#Personnel" data-bs-toggle="collapse" aria-expanded="false" aria-controls="Personnel">
                            <i class="fas fa-chalkboard-teacher"></i>
                            <span> Personnel </span>
                            <span class="menu-arrow"></span>
                        </a>
                        <div class="collapse pl-2" id="Personnel">
                            <ul class="nav-second-level" aria-expanded="false">
                                <li>
                                    <a href="{{url("api/staffs")}}"><i class="fas fa-angle-double-right pr-1"></i>Staff</a>
                                </li>
                                {{--                            <li>--}}
                                {{--                                <a href="#"><i class="fas fa-angle-double-right pr-1"></i>KPI Sales</a>--}}
                                {{--                            </li>--}}
                                @if($staff->staff_decentralization_id=='3')
                                    @if($staff->staff_rank_id=='2')
                                        <li>
                                            <a href="{{url("api/staffs-sales-kpi")}}"><i class="fas fa-angle-double-right pr-1"></i>KPI Staff Sales</a>
                                        </li>
                                        <li>
                                            <a href="{{url("api/staffs-sales-kpi/kpi-ranks-sub-sales")}}"><i class="fas fa-angle-double-right pr-1"></i>KPI Sub of Manage</a>
                                        </li>
                                        <li>
                                            <a href="{{url("api/staffs-sales-kpi/kpi-ranks-manage-sales")}}"><i class="fas fa-angle-double-right pr-1"></i>KPI Manage</a>
                                        </li>
                                    @elseif($staff->staff_rank_id=='3')
                                        <li>
                                            <a href="{{url("api/staffs-sales-kpi")}}"><i class="fas fa-angle-double-right pr-1"></i>KPI Staff Sales</a>
                                        </li>
                                        <li>
                                            <a href="{{url("api/staffs-sales-kpi/kpi-ranks-sub-sales")}}"><i class="fas fa-angle-double-right pr-1"></i>KPI Sub Manage</a>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{url("api/staffs-sales-kpi")}}"><i class="fas fa-angle-double-right pr-1"></i>KPI Staff Sales</a>
                                        </li>
                                    @endif
                                @endif
                                @if($staff->staff_decentralization_id=='4')
                                    <li>
                                        <a href="{{url("api/rank")}}"><i class="fas fa-angle-double-right pr-1"></i>Employee Rank</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/sales-kpi-payment")}}"><i class="fas fa-angle-double-right pr-1"></i>KPI All</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/sub-sales-kpi")}}"><i class="fas fa-angle-double-right pr-1"></i>Sub Sales KPI</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/manage-sales-kpi")}}"><i class="fas fa-angle-double-right pr-1"></i>Manage Sales KPI</a>
                                    </li>
                                @endif
                                @if( $staff->staff_decentralization_id=='5')
                                    <li>
                                        <a href="{{url("api/position")}}"><i class="fas fa-angle-double-right pr-1"></i>Position</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/decentralization")}}"><i
                                                class="fas fa-angle-double-right pr-1"></i>Decentralization</a>
                                    </li>
                                @endif
                                {{--                            <li>--}}
                                {{--                                <a href="{{url("api/rank")}}"><i class="fas fa-angle-double-right pr-1"></i>Rank</a>--}}
                                {{--                            </li>--}}
                                {{--                            <li>--}}
                                {{--                                <a href="#"><i class="fas fa-angle-double-right pr-1"></i>Commission Rank</a>--}}
                                {{--                            </li>--}}
                                {{--                            <li>--}}
                                {{--                                <a href="#"><i class="fas fa-angle-double-right pr-1"></i>Commission</a>--}}
                                {{--                            </li>--}}
                                {{--                            <li>--}}
                                {{--                                <a href="#"><i class="fas fa-angle-double-right pr-1"></i>Handing Over Work</a>--}}
                                {{--                            </li>--}}

                            </ul>
                        </div>
                    </li>
                    @if($staff->staff_decentralization_id=='3' || $staff->staff_decentralization_id=='4' || $staff->staff_decentralization_id=='5')
                        <li>
                            <a href="#finance" data-bs-toggle="collapse" aria-expanded="false" aria-controls="finance">
                                <i class="fas fa-comments-dollar"></i>
                                <span> Finance </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <div class="collapse pl-2" id="finance">
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{url("api/receipts")}}"><i class="fas fa-angle-double-right pr-1"></i>Receipts</a>
                                    </li>
                                    @if($staff->staff_decentralization_id=='4')
                                        <li>
                                            <a href="{{url("api/monthlycashflow")}}"><i class="fas fa-angle-double-right pr-1"></i>Monthly CashFlow</a>
                                        </li>
                                    @endif
                                    @if($staff->staff_decentralization_id=='5')
                                        <li>
                                            <a href="{{url("api/receipts/approve-index")}}"><i class="fas fa-angle-double-right pr-1"></i>Approve Receipts</a>
                                        </li>
                                    @endif
                                    {{--                            <li>--}}
                                    {{--                                <a href="apps-calendar.html"><i class="fas fa-angle-double-right pr-1"></i>Approve Payment</a>--}}
                                    {{--                            </li>--}}
                                </ul>
                            </div>
                        </li>
                    @endif
                    {{--                    <li>--}}
                    {{--                        <a href="#" class="waves-effect">--}}
                    {{--                            <i class="fas fa-dna"></i>--}}
                    {{--                            <span>History System </span>--}}
                    {{--                            <span class="menu-arrow"></span>--}}
                    {{--                        </a>--}}
                    {{--                        <ul class="nav-second-level" aria-expanded="false">--}}
                    {{--                            <li>--}}
                    {{--                                <a href="{{ url('config/shop') }}"><i class="fas fa-angle-double-right pr-1"></i>Stock In</a>--}}
                    {{--                            </li>--}}
                    {{--                            <li>--}}
                    {{--                                <a href="{{url('carrier')}}"><i class="fas fa-angle-double-right pr-1"></i>Stock Out</a>--}}
                    {{--                            </li>--}}
                    {{--                            <li>--}}
                    {{--                                <a href="{{url('carrier')}}"><i class="fas fa-angle-double-right pr-1"></i>Approve Receipts</a>--}}
                    {{--                            </li>--}}
                    {{--                            <li>--}}
                    {{--                                <a href="{{url('carrier')}}"><i class="fas fa-angle-double-right pr-1"></i>Approve Payment</a>--}}
                    {{--                            </li>--}}
                    {{--                        </ul>--}}
                    {{--                    </li>--}}
                    @if($staff->staff_decentralization_id=='5')
                        <li>
                            <a href="#GeneralDirectory" data-bs-toggle="collapse" aria-expanded="false" aria-controls="GeneralDirectory">
                                <i class="fe-settings"></i>
                                <span> General Directory </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <div class="collapse pl-2" id="GeneralDirectory">
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{url("api/brand")}}"><i class="fas fa-angle-double-right pr-1"></i>Branch</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/warehouses")}}"><i class="fas fa-angle-double-right pr-1"></i>WareHouse</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/currency")}}"><i class="fas fa-angle-double-right pr-1"></i>Currency</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/bank")}}"><i class="fas fa-angle-double-right pr-1"></i>Bank</a>
                                    </li>
                                    <li>
                                        <a href="{{url("api/country")}}"><i class="fas fa-angle-double-right pr-1"></i>country</a>
                                    </li>
                                    {{--                            <li>--}}
                                    {{--                                <a href="{{url("api/tax")}}"><i class="fas fa-angle-double-right pr-1"></i>Tax</a>--}}
                                    {{--                            </li>--}}
                                </ul>
                            </div>
                        </li>
                    @endif
                </ul>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">

                @yield('content')

            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        {{date("Y")}} &copy; <a href="#" class="text-warning">created by IT Team </a>
                    </div>
                    <div class="col-md-6">
                        <div class="text-md-right footer-links d-none d-sm-block">
                            <a href="javascript:void(0);">Về chúng tôi</a>
                            <a href="javascript:void(0);">Trợ giúp</a>
                            <a href="javascript:void(0);">Liên hệ</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

</div>

<div class="rightbar-overlay"></div>
<!-- Vendor js -->
<script src="https://coderthemes.com/minton/layouts/assets/js/vendor.min.js"></script>
<!-- App js -->
<script src="https://coderthemes.com/minton/layouts/assets/js/app.min.js"></script>
<script>
    {{--$(document).ready(function () {--}}
    {{--    console.log($.ajax);--}}
    {{--    setInterval(function () {--}}
    {{--        $.ajax({--}}
    {{--            url: '{{url("api/notification")}}',--}}
    {{--            type: "GET",--}}
    {{--            dataType: 'json',--}}
    {{--            success: function (data) {--}}
    {{--                console.log(data['data']['notification']);--}}
    {{--                $('#countnotification').text(data['data']['count'])--}}
    {{--                if (data['data']['notification'].length > 0) {--}}
    {{--                    for (let i = 0; i < data['data']['notification'].length; i++) {--}}
    {{--                        console.log(i);--}}
    {{--                        $('#content').html('<a href="{{url('api/receipts/edit-approve')}}/'+data['data']['notification'][i]['id']+'" class="dropdown-item notify-item active">' +--}}
    {{--                            '<div class="notify-icon bg-soft-primary text-primary">' +--}}
    {{--                            '<i class="mdi mdi-comment-account-outline"></i>' +--}}
    {{--                            '</div>' +--}}
    {{--                            '<p class="notify-details">Customer :' + data['data']['notification'][i]['customers_name'] + '</br> Staff : ' +  data['data']['notification'][i]['staff_name'] + '</p>' +--}}
    {{--                            '</a>' +--}}
    {{--                            '<div style="border-bottom: 1px solid #eeee"></div>'+--}}
    {{--                        ' <a href="{{url("api/receipts/approve-index")}}" class="dropdown-item notify-item active">'+--}}
    {{--                        'View All </a>')--}}
    {{--                    }--}}
    {{--                } else {--}}
    {{--                    $('#content').html('<a href="#" class="dropdown-item notify-item active">' +--}}
    {{--                        '<div class="notify-icon bg-soft-primary text-primary">' +--}}
    {{--                        ' <i class="mdi mdi-comment-account-outline"></i>' +--}}
    {{--                        '</div>' +--}}
    {{--                        '<p class="notify-details">data is empty </p>' +--}}
    {{--                        '</a>' +--}}
    {{--                        '<div style="border-bottom: 1px solid #eeee"></div>'+--}}
    {{--                        ' <a href="{{url("api/receipts/approve-index")}}" class="dropdown-item notify-item active">'+--}}
    {{--                        'View All </a>')--}}
    {{--                }--}}
    {{--            }--}}
    {{--        });--}}
    {{--    }, 5000);--}}
    {{--});--}}
</script>
@yield('javascript')
</body>
</html>
