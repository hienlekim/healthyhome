@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled="disabled">
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" disabled="disabled">
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">List Inventory</h2>
                </div>
            </td>
            <td>
{{--                <div class="col-xl-12 text-right">--}}
{{--                    <h2 class="header-title">--}}
{{--                        <a href="{{url("api/purchases/store")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                    </h2>--}}
{{--                </div>--}}
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/inventories")}}">
                <div class="row">
                    <div class="col-xl-3 text-left">
                        <div class="dataTables_length" id="products-datatable_length"><label>Display
                                <select class="form-select form-select-sm mx-1" id="limit" name="limit">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="all">all</option>
                                </select>column</label>
                        </div>
                    </div>
                    @if($StaffCustomerBrand->staff_brand_id==0)
                        <div class="col-xl-3">
                            <select class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                                <option value="">Chi Nhánh</option>
                                @foreach($branch as $item)
                                    <option value="{{$item->id}}">{{$item->brand_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xl-3 pr-3">
                            <div class="mb-2 row">
                                <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                        type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    @else
                        <div class="col-xl-3">
                            <select style="display: none" class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                                <option value="">Chi Nhánh</option>
                            </select>
                        </div>
                    @endif

                </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="tablesaw table mb-0">
                        <thead class="title_table">
                        <tr>
                            <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">
                                #</th>
                            <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">Product Name</th>
                            <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">Product Unit</th>
                            <th scope="col" data-tablesaw-sortable-col="">Branch</th>
                            <th scope="col" data-tablesaw-sortable-col="">Quantity Inventory</th>
                            <th scope="col" data-tablesaw-sortable-col="">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @if($count >0)
                                @foreach($list as $key)
                                    <tr>
                                        <td>{{$key['stt']}}</td>
                                        <td class="text-uppercase">{{$key['product_name']}}</td>
                                        <td>{{$key['product_unit']}}</td>
                                        <td>{{$key['brand_name']}}</td>
                                        <td>{{number_format($key['product_quality'], 2, ',', '.')}}</td>
                                        <td>@if($key['product_quality'] <=4)<span class="badge badge-warning">Sắp hết hàng</span>@elseif($key['product_quality'] ==0)<span class="badge badge-danger">Hết hàng</span>@elseif($key['product_quality'] >=5)<span class="badge badge-success">Còn hàng</span> @endif</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" class="text-center">Content is not available or does not exist</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-left">
                <b>Total:</b> <span class="TotalAll">{{$count}}</span>
            </div>
            <div class="col-md-4">
                <div class="dataTables_paginate paging_simple_numbers">{{$results->links()}}</div>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/libs/tablesaw/tablesaw.js')}}"></script>
<script src="{{asset('assets/js/pages/tablesaw.init.js')}}"></script>

@endsection


