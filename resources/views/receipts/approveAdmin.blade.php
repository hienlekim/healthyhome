@extends('template')
@section('title', "Bank")

@section('css')
    <link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

    {{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
    <div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
        <table style="width: 100%">
            <tr>
                <td>
                    <div class="col-xl-12 text-left">
                        <h2 class="header-title">RECEIPTS APPROVE</h2>
                    </div>
                </td>
                <td>
                    <div class="col-xl-12 text-right">
                        <h2 class="header-title">
                            <a href="{{url("api/receipts/approve-index")}}" class="btn btn-primary waves-effect waves-light"><i
                                    class="fe-rewind pr-1"></i>Back</a>
                        </h2>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="row content_description">
        <div class="col-12">
            <form class="mb-1" method="post" id="formBankAdd" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="card-box" style="margin-bottom: 4px!important;">
                    <div class="row">
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Receipts Code <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <input type="hidden" id="idReceipt" name="idReceipt" value="{{$data['id']}}" >
                                    <input type="txt" class="form-control text-uppercase" id="receipts_code1" name="receipts_code1" value="{{$data['receipts_code']}}" autocomplete="off" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Receipts Customer <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <select class="form-control form-select-lg" id="receipts_customer_id" name="receipts_customer_id">
                                        <option value="">------</option>
                                        @foreach($Customers as $item)
                                            @if($data->receipts_customer_id==$item->id)
                                                <option value="{{$item->id}}" selected>{{$item->customers_name}}</option>
                                            @else
                                                <option value="{{$item->id}}">{{$item->customers_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Receipts Bank Receive<span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <select class="form-control form-select-lg" id="receipts_bank_id" name="receipts_bank_id">
                                        <option value="">------</option>
                                        @foreach($bank as $item)
                                            @if($data['receipts_bank_id']==$item->id)
                                                <option value="{{$item->id}}" selected>{{$item->bank_name}}</option>
                                            @else
                                                <option value="{{$item->id}}">{{$item->bank_name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Receipts Period <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control text-uppercase" id="receipts_period_id1" value="{{$data['receiptsperiod']['period_name']}}" name="receipts_period_id1" autocomplete="off" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Staff  <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="receipts_staff_id1" name="receipts_staff_id1" value="{{$data['receiptstaff']['staff_name']}}" autocomplete="off"  disabled>
                                    <input type="hidden" class="form-control" id="receipts_staff_id" name="receipts_staff_id" autocomplete="off" value="{{$brandUser['id']}}">
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Currency <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="receipts_currency_id" name="receipts_currency_id" autocomplete="off" value="{{$data['receiptscurrency']['currency_name']}}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>{{----end row_1----}}
                </div>{{----end card_1----}}

                <div class="card-box" style="margin-bottom: 4px!important;">
                    <div class="row">
{{--                        <div class="mb-2 col-md-6">--}}
{{--                            <div class="mb-2 row">--}}
{{--                                <label class="col-md-5 col-form-label" for="simpleinput">Branch <span class="text-danger">*</span></label>--}}
{{--                                <div class="col-md-7">--}}
{{--                                    <input type="txt" class="form-control" id="stock_brand_id1" name="stock_brand_id1" autocomplete="off" value="{{$data['receiptsbranch']['brand_name']}}" disabled>--}}
{{--                                    <input type="hidden" class="form-control" id="receipts_brand_id" name="receipts_brand_id" autocomplete="off" value="{{$brandUser['brands']['id']}}" >--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Date Receipts</label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="receipts_amount_date" name="receipts_amount_date" autocomplete="off" value="{{date('d-m-Y', strtotime($data['receipts_amount_date']))}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Period Date <span class="text-danger">*</span></label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="period_date1" name="period_date1" value="{{date('d-m-Y', strtotime($data['receipts_period_date']))}}" autocomplete="off" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Period Amount</label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="period_amount" name="period_amount" autocomplete="off"  value="{{number_format($data['receipts_period_amount'], 2, ',', '.')}}">
                                </div>
                            </div>
                        </div>

                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Period Amount Batch</label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="period_amount_batch" name="period_amount_batch" autocomplete="off" value="{{number_format($data['receipts_amount'], 2, ',', '.')}}">
                                </div>
                            </div>
                        </div>
{{--                        <div class="mb-2 col-md-6">--}}
{{--                            <div class="mb-2 row">--}}
{{--                                <label class="col-md-5 col-form-label" for="simpleinput">Payment Period <span class="text-danger">*</span></label>--}}
{{--                                <div class="col-md-7">--}}
{{--                                    <input type="txt" class="form-control" id="period_payments_id1" name="period_payments_id1" value="{{$data['receiptspayment']['payments_code']}}" autocomplete="off" disabled>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="mb-2 col-md-6">--}}
{{--                            <div class="mb-2 row">--}}
{{--                                <label class="col-md-5 col-form-label" for="simpleinput">Interest Rate<span class="text-danger">(*)(%)</span></label>--}}
{{--                                <div class="col-md-7">--}}
{{--                                    <input type="txt" class="form-control" id="interest_rate" name="interest_rate" autocomplete="off" value="{{$data['interest_rate']}}">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="mb-2 col-md-6">--}}
{{--                            <div class="mb-2 row">--}}
{{--                                <label class="col-md-5 col-form-label" for="simpleinput">Number Days Overdue</label>--}}
{{--                                <div class="col-md-7">--}}
{{--                                    <input type="txt" class="form-control" id="number_days_overdue1" name="number_days_overdue1" autocomplete="off" value="{{$data['number_days_overdue']}}" disabled>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="mb-2 col-md-6">--}}
{{--                            <div class="mb-2 row">--}}
{{--                                <label class="col-md-5 col-form-label" for="simpleinput">Fines <span class="text-danger">(%)</span></label>--}}
{{--                                <div class="col-md-7">--}}
{{--                                    <input type="txt" class="form-control" id="fines" name="fines" autocomplete="off" value="{{$data['fines']}}">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="mb-2 col-md-6">--}}
{{--                            <div class="mb-2 row">--}}
{{--                                <label class="col-md-5 col-form-label" for="simpleinput">Overdue Fines</label>--}}
{{--                                <div class="col-md-7">--}}
{{--                                    <input type="txt" class="form-control" id="overdue_fines" name="overdue_fines" autocomplete="off" value="{{number_format($data['overdue_fines'], 2, ',', '.')}}">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="mb-2 col-md-6">
                            <div class="mb-2 row">
                                <label class="col-md-5 col-form-label" for="simpleinput">Actual Amount</label>
                                <div class="col-md-7">
                                    <input type="txt" class="form-control" id="actual_amount" name="actual_amount" autocomplete="off"  value="{{number_format($data['actual_amount'], 2, ',', '.')}}">
                                </div>
                            </div>
                        </div>
{{--                        <div class="mb-2 col-md-6">--}}
{{--                            <div class="mb-2 row">--}}
{{--                                <label class="col-md-5 col-form-label" for="simpleinput">Description </label>--}}
{{--                                <div class="col-md-7">--}}
{{--                                    <input type="txt" class="form-control" id="receipts_description" name="receipts_description" autocomplete="off" value="{{$data['receipts_description']}}">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>{{----end row_2----}}
{{--                    <div class="row">--}}
{{--                        <div class="mb-2 ">--}}
{{--                            <img src="{{asset("images/product/$Images")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 150px">--}}
{{--                            <div class="fileupload waves-effect text-center" id="images_avata">--}}
{{--                                <span class="text-info text-center"><i class="fe-instagram font-24"></i></span>--}}
{{--                                <input type="file" class="upload" id="images_avatas" name="images_avata_1" onchange="preview_image_1(event)">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="mb-2 ">
                        <a data-toggle="modal" data-target="#full-width-modal">
                            @if($images_approve)
                                <img src="{{asset("images/receipts/$images_approve")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 150px">
                            @else
                                <img src="{{asset("images/product/$Images")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 150px">
                            @endif
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                            @if($brandUser['decenTLizaTion']['positions']['position_code'] == 'sales' && $data->receipts_status==1)
                                <button type="button" class="btn btn-primary waves-effect waves-light btnSendApprove">
                                    <i class="fe-save mr-1"></i> Send Approve
                                </button>
                                <button type="button" class="btn btn-danger waves-effect waves-light btnCancell" >
                                    <i class="fe-trash-2 mr-1"></i>Cancel
                                </button>
                            @elseif($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators' && $data->receipts_status==2)
                                <button type="button" class="btn btn-primary waves-effect waves-light btnApprove" >
                                    <i class="fe-save mr-1"></i> Approve
                                </button>
                                <button type="button" class="btn btn-danger waves-effect waves-light btnCancellApprove" >
                                    <i class="fe-trash-2 mr-1"></i>Cancel Approve
                                </button>
                            @elseif($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators' && $data->receipts_status==1)
                                <button type="button" class="btn btn-primary waves-effect waves-light" disabled>
                                    <i class="fe-save mr-1"></i> Approve
                                </button>
                                <button type="button" class="btn btn-danger waves-effect waves-light" disabled>
                                    <i class="fe-trash-2 mr-1"></i>Cancel Approve
                                </button>
                            @endif
                        </div>
                    </div>

                </div>{{----end card_2----}}


            </form>
            <div id="full-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            @if($images_approve)
                                <img src="{{asset("images/receipts/$images_approve")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 150px">
                            @else
                                <img src="{{asset("images/product/$Images")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 150px">
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
        </div>
        <div class="modal-backdrop fade show" id="fadeShow" style="display: none">
            <div class="row">
                <div class="col-md-12 text-center" style="margin-top: 10%">
                    <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
                    </br>
                    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                    <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
                </div>
            </div>
        </div>

        @endsection

        @section('javascript')
            <script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
            <script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
            <script src="{{asset('assets/libs/moment/moment.min.js')}}"></script>
            <script>
                // function preview_image_1(event)
                // {
                //     var reader = new FileReader();
                //     reader.onload = function()
                //     {
                //         var output = document.getElementById('images_avatas');
                //         output.src = reader.result;
                //     }
                //     let images = reader.readAsDataURL(event.target.files[0]);
                // }
                $(document).ready(function () {
                    $(document).on("click", ".btnCancellApprove", function (event) {
                        let id = $('#idReceipt').val();
                        // let myForm = document.getElementById('formBankAdd')
                        // setTimeout(() => {
                        $('#fadeShow').css('display','block');
                        $.ajax({
                            url:'{{url("api/receipts/cancel-approve")}}/'+id,
                            type: "PUT",
                            dataType: 'json',
                            data: {
                                '_token': "{{ csrf_token() }}",
                                'id': id,
                            },
                            success: function(data) {
                                if(data.errors==true){
                                    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                                    $('#successrequest').addClass('jq-icon-warning fade show')
                                    $('#content_success').text('warning!')
                                    $('#content_tb').text(data.message)
                                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                                    $('#brand_code').focus();
                                }else{
                                    $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                                    $('#successrequest').addClass('jq-icon-success fade show')
                                    $('#content_success').text('success!')
                                    $('#content_tb').text('Cancel data successfully')
                                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/receipts/approve-index")}}"; }, 3000);
                                }
                            }
                        });
                    });
                    //send approve
                    $(document).on("click", ".btnApprove", function (event) {
                        let id = $('#idReceipt').val();
                        let myForm = document.getElementById('formBankAdd')
                        // setTimeout(() => {
                        $('#fadeShow').css('display','block');
                        $.ajax({
                            url:'{{url("api/receipts/approve")}}/'+id,
                            type: "PUT",
                            dataType: 'json',
                            data: {
                                '_token': "{{ csrf_token() }}",
                                'id': id,
                            },
                            success: function(data) {
                                if(data.errors==true){
                                    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                                    $('#successrequest').addClass('jq-icon-warning fade show')
                                    $('#content_success').text('warning!')
                                    $('#content_tb').text(data.message)
                                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                                    $('#brand_code').focus();
                                }else{
                                    $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                                    $('#successrequest').addClass('jq-icon-success fade show')
                                    $('#content_success').text('success!')
                                    $('#content_tb').text('Cancel data successfully')
                                    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/receipts/approve-index")}}"; }, 3000);
                                }
                            }
                        });
                        {{--// setTimeout(() => {--}}
                        {{--let images_avatas = $('#images_avatas').val();--}}
                        {{--if(images_avatas =="" || images_avatas ==0){--}}
                        {{--    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');--}}
                        {{--    $('#successrequest').addClass('jq-icon-warning fade show')--}}
                        {{--    $('#content_success').text('warning!')--}}
                        {{--    $('#content_tb').text('please enter the images receipts !')--}}
                        {{--    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);--}}
                        {{--}else{--}}
                        {{--    let id =  $('#idReceipt').val();--}}
                        {{--    let myForm = document.getElementById('formBankAdd')--}}
                        {{--    $('#fadeShow').css('display','block');--}}
                        {{--    $.ajax({--}}
                        {{--        url:'{{url("api/receipts/approve")}}/'+id,--}}
                        {{--        type: "POST",--}}
                        {{--        dataType: 'json',--}}
                        {{--        data:new FormData(myForm),'_token':"{{ csrf_token() }}",--}}
                        {{--        cache : false,--}}
                        {{--        processData : false,--}}
                        {{--        contentType: false,--}}
                        {{--        success: function(data) {--}}
                        {{--            if(data.errors==true){--}}
                        {{--                $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');--}}
                        {{--                $('#successrequest').addClass('jq-icon-warning fade show')--}}
                        {{--                $('#content_success').text('warning!')--}}
                        {{--                $('#content_tb').text(data.message)--}}
                        {{--                setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);--}}
                        {{--                $('#brand_code').focus();--}}
                        {{--            }else{--}}
                        {{--                $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');--}}
                        {{--                $('#successrequest').addClass('jq-icon-success fade show')--}}
                        {{--                $('#content_success').text('success!')--}}
                        {{--                $('#content_tb').text('Send approve data successfully')--}}
                        {{--                setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/receipts/approve-index")}}"; }, 3000);--}}
                        {{--            }--}}
                        {{--        }--}}
                        {{--    });--}}
                        {{--}--}}

                    });
                    $('#images_avatas').click(function (){
                        $('#images_receipt').modal('show');
                    })
                })
            </script>
            {{--sucess--}}
            <div class="jq-toast-wrap top-right">
                <style>
                    #loader_loaded_success {
                        background-color: #5ba035;
                    }
                    #loader_loaded_wram {
                        background-color: #da8609;
                    }
                    #loader_loaded_errors {
                        background-color: #bf441d;
                    }
                </style>
                <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                    <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                    <span class="close-jq-toast-single">×</span>
                    <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
                </div>
            </div>
@endsection

