@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
<link href="https://coderthemes.com/minton/layouts/assets/css/default/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('assets/css/default/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />--}}
<link href="{{asset('assets/libs/selectize/css/selectize.bootstrap3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
<style>
    .selectize-dropdown-header{
        display: none;
    }
</style>
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">RECEIPTS</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/receipts")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
        <form class="mb-1" method="post" id="formBankAdd">
            {{csrf_field()}}
            <div class="card-box" style="margin-bottom: 4px!important;">
                <div class="row">
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <input type="hidden" class="form-control" id="rId" name="rId" autocomplete="off">
                            <label class="col-md-5 col-form-label" for="simpleinput">Receipts Code <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control text-uppercase" id="receipts_code1" value="{{$receiptCode}}" name="receipts_code1" autocomplete="off" disabled>
                                <input type="hidden" class="form-control text-uppercase" id="receipts_code" value="{{$receiptCode}}" name="receipts_code" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Receipts Customer <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <select id="select-code-language" class="selectize-drop-header" placeholder="Select a customer..." name="receipts_customer_id">
                                    <option value="">------</option>
                                    @foreach($Customer as $item)
                                        <option value="{{$item->id}}">{{$item->customers_name}}</option>
                                    @endforeach
                                </select>
{{--                                <select class="form-control form-select-lg" id="receipts_customer_id" name="receipts_customer_id">--}}
{{--                                    <option value="">------</option>--}}
{{--                                    --}}
{{--                                </select>--}}
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Receipts Stock <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control text-uppercase" id="receipts_stock_id1" name="receipts_stock_id1" autocomplete="off" disabled>
                                <input type="hidden" class="form-control text-uppercase" id="receipts_stock_id" name="receipts_stock_id" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Receipts Period <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control text-uppercase" id="receipts_period_id1" name="receipts_period_id1" autocomplete="off" disabled>
                                <input type="hidden" class="form-control text-uppercase" id="receipts_period_id" name="receipts_period_id" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Staff  <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="hidden" class="form-control" id="receipts_brand_id" name="receipts_brand_id" autocomplete="off" value="@if($brandUser['staff_brand_id']==0) 0 @else {{$brandUser['brands']['id']}}@endif">
                                <input type="txt" class="form-control" id="receipts_staff_id1" name="receipts_staff_id1" autocomplete="off" value="{{$name_user}}" disabled>
                                <input type="hidden" class="form-control" id="receipts_staff_id" name="receipts_staff_id" autocomplete="off" value="{{$brandUser['id']}}">
                                <input type="hidden" class="form-control" id="receipts_currency_id" name="receipts_currency_id" autocomplete="off" value="@if($brandUser['staff_brand_id']==0)  @else {{$brandUser['brands']['currencies']['id']}}@endif">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Description </label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="receipts_description" name="receipts_description" autocomplete="off">
                            </div>
                        </div>
                    </div>
{{--                    <div class="mb-2 col-md-6">--}}
{{--                        <div class="mb-2 row">--}}
{{--                            <label class="col-md-5 col-form-label" for="simpleinput">Currency <span class="text-danger">*</span></label>--}}
{{--                            <div class="col-md-7">--}}
{{--                                --}}
{{--                                <input type="txt" class="form-control" id="receipts_currency_id1" name="receipts_currency_id1" autocomplete="off" value="@if($brandUser['staff_brand_id']==0)  @else {{$brandUser['brands']['currencies']['currency_name']}}@endif" disabled>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>{{----end row_1----}}
            </div>{{----end card_1----}}

            <div class="card-box" style="margin-bottom: 4px!important;">
                <div class="row">
{{--                    <div class="mb-2 col-md-6">--}}
{{--                        <div class="mb-2 row">--}}
{{--                            <label class="col-md-5 col-form-label" for="simpleinput">Branch <span class="text-danger">*</span></label>--}}
{{--                            <div class="col-md-7">--}}
{{--                                <input type="txt" class="form-control" id="stock_brand_id1" name="stock_brand_id1" autocomplete="off" value="@if($brandUser['staff_brand_id']==0) Both Brand @else {{$brandUser['brands']['brand_name']}}@endif" disabled>--}}
{{--                                --}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Period Date <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="period_date1" name="period_date1" autocomplete="off" disabled>
                                <input type="hidden" class="form-control" id="period_date" name="period_date" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Period Amount</label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="period_amount" name="period_amount" autocomplete="off" value="0">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Period Amount Batch</label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="period_amount_batch" name="period_amount_batch" autocomplete="off" value="0">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Payment Period <span class="text-danger">*</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="period_payments_id1" name="period_payments_id1" autocomplete="off" disabled>
                                <input type="hidden" class="form-control" id="period_payments_id" name="period_payments_id" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Interest Rate<span class="text-danger">(%)</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="interest_rate" name="interest_rate" autocomplete="off" value="0">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Date Receipts</label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="receipts_amount_date" name="receipts_amount_date" autocomplete="off" value="{{$date}}" disabled>
                                <input type="hidden" class="form-control" id="receipts_amount_date1" name="receipts_amount_date1" autocomplete="off" value="{{$date_2}}">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Number Days Overdue</label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="number_days_overdue1" name="number_days_overdue1" autocomplete="off" disabled>
                                <input type="hidden" class="form-control" id="number_days_overdue" name="number_days_overdue" autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Fines <span class="text-danger">(%)</span></label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="fines" name="fines" autocomplete="off" value="0">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Overdue Fines</label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="overdue_fines" name="overdue_fines" autocomplete="off" value="0">
                            </div>
                        </div>
                    </div>
                    <div class="mb-2 col-md-6">
                        <div class="mb-2 row">
                            <label class="col-md-5 col-form-label" for="simpleinput">Actual Amount</label>
                            <div class="col-md-7">
                                <input type="txt" class="form-control" id="actual_amount" name="actual_amount" autocomplete="off" value="0">
                            </div>
                        </div>
                    </div>

                </div>{{----end row_2----}}
                <div class="row">
                    <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                        <button type="button" class="btn btn-primary waves-effect waves-light btnCreate" @if($brandUser['decenTLizaTion']['positions']['position_code'] == 'sales') @else disabled @endif >
                            <i class="fe-save mr-1"></i> Save
                        </button>
                    </div>
                </div>

            </div>{{----end card_2----}}


        </form>

</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/moment/moment.min.js')}}"></script>
<script src="{{asset('assets/libs/selectize/js/standalone/selectize.min.js')}}"></script>
<script src="{{asset('assets/libs/jquery-quicksearch/jquery.quicksearch.min.js')}}"></script>
<script src="{{asset('assets/libs/multiselect/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
{{--<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>--}}
<script src="https://coderthemes.com/minton/layouts/assets/js/pages/form-advanced.init.js"></script>
<script>
$(document).ready(function () {
    var thousand_sep = '.',
    dec_sep = ',',
    dec_length = 3;
    var format = function (num) {
        var str = num.toString().replace("", ""),
            parts = false,
            output = [],
            i = 1,
            formatted = null;
        if (str.indexOf(dec_sep) > 0) {
            parts = str.split(dec_sep);
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != thousand_sep) {
                output.push(str[j]);
                if (i % 2 == 0 && j < (len - 1)) {
                    output.push(thousand_sep);
                }
                i++;
            }
        }
        if (output.slice(-1)[0] === '-' && output.slice(-2)[0] === thousand_sep ) {
            output.splice(-2, 1);
        }

        formatted = output.reverse().join("");
        return (formatted + ((parts) ? dec_sep + parts[1].substr(0, dec_length) : ""));
    };

    var formats = function (num) {
        var str = num.toString().replace("", ""),
            parts = false,
            output = [],
            i = 1,
            formatted = null;
        if (str.indexOf(dec_sep) > 0) {
            parts = str.split(dec_sep);
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != thousand_sep) {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(thousand_sep);
                }
                i++;
            }
        }
        if (output.slice(-1)[0] === '-' && output.slice(-2)[0] === thousand_sep ) {
            output.splice(-2, 1);
        }

        formatted = output.reverse().join("");
        return (formatted + ((parts) ? dec_sep + parts[1].substr(0, dec_length) : ""));
    };
    $('#select-code-language').change(function(){
        let id = $(this).val();
        $.ajax({
            url:'{{url("api/receipts/load-customer")}}/'+id,
            type: "GET",
            dataType: 'json',
            success: function(data) {
                // console.log(formats(data['data']['product']['period_amount']));
                    $('#rId').val(data['data']['product']['id']);
                    $('#receipts_stock_id1').val(data['data']['product']['periodstock']['stock_code']);
                    $('#receipts_stock_id').val(data['data']['product']['period_stock_id']);
                    $('#receipts_period_id1').val(data['data']['product']['period_name']);
                    $('#receipts_period_id').val(data['data']['product']['id']);
                    $('#period_date1').val(data['data']['date']);
                    $('#period_date').val(data['data']['product']['period_date']);
                    $('#period_payments_id1').val(data['data']['product']['periodpayments']['payments_code']);
                    $('#period_payments_id').val(data['data']['product']['periodpayments']['id']);
                    $('#period_amount').val(formats(data['data']['product']['period_amount']));
                    $('#period_amount_batch').val(formats(data['data']['product']['period_amount_batch']));
                    $('#actual_amount').val(formats(data['data']['product']['period_amount_batch']));
                    $('#interest_rate').val(0);
                    $('#fines').val(0);
                    $('#overdue_fines').val(0);
                    // $('#receipts_code,#receipts_code1').val(data['data']); period_amount_batch
                    let period_date =data['data']['dates'];
                    let d_period_date = new Date(period_date);
                    let receipts_amount_date = $('#receipts_amount_date1').val();
                    let d_receipts_amount = new Date(receipts_amount_date);
                    sum = Math.ceil(d_period_date - d_receipts_amount)
                    let dtime = Math.ceil(sum/(1000*60*60*24))
                    // console.log(period_date);
                    $('#number_days_overdue,#number_days_overdue1').val(dtime);
                    if(data['data']['product']['periodpayments']['payments_code']=="full" || data['data']['product']['periodpayments']['payments_code']=="Smell" || data['data']['product']['periodpayments']['payments_code']=="Deposit" ){
                        $('#actual_amount').val(formats(data['data']['product']['period_amount_batch']));
                    }
            }
        });
    })


    $("#interest_rate").keyup(function (e) {
        $(this).val(format($(this).val()));
        let interest_rate = $(this).val();
        let receipts_brand_id = $('#receipts_brand_id').val();
        let period_payments_id1 = $('#period_payments_id1').val();
        // tổng số tháng thanh toán cthuc tính  = 0.75% x 11 tháng = 8.25% trong 11 tháng.
        // $2,000 x 8.25% = $165
        //$165 + $2000 = $2,165
        let period_payments = $('#period_payments_id1').val();
        let period_amount = $('#period_amount').val().replace('.','');
        let period_amount_1 = period_amount.replace('.','');
        let period_amount_batch = $('#period_amount_batch').val().replaceAll('.','');
        let sum_1 = 0;
        let sum_2 = 0;
        let sum_3 = 0;
        let sum_4 = 0;
        let sum_mlsuat_amount = 0;
        // tính lãi xuất campuchia trươc
        if(receipts_brand_id == 4){
            if(period_payments_id1==4 || period_payments_id1==6 || period_payments_id1==9 || period_payments_id1==12){
                //tính ra mức lãi xuất = lấy tổng số tháng thanh toán -1
                sum_1 = parseFloat(interest_rate) * (parseFloat(period_payments)-1)
                // //lấy mức lãi xuât tính phía trên  * tổng số giá trị
                sum_2 = parseFloat(sum_1) * parseFloat(period_amount_1)
                // //lấy sum_mlsuat_amount = $165 + $2000 = $2,165
                sum_3 = parseFloat(sum_2) + parseFloat(period_amount_1)
                // //lấy $2,165 / 11 tháng = $196.8
                sum_4 = parseFloat(period_amount_batch)+(parseFloat(sum_3)/(parseFloat(period_payments)-1))
                $('#actual_amount').val(formats(sum_4));
            }
            // tính lãi xuất Việt nam
        }else if(receipts_brand_id == 3){
            let id = $('#rId').val();
            let receipts_stock_id = $('#receipts_stock_id').val();
            $.ajax({
                url:'{{url("api/receipts/load-amount-batch")}}/'+id + '/'+ receipts_stock_id,
                type: "GET",
                dataType: 'json',
                success: function(data) {
                    //lấy 1.68 % * số tiền trả theo kì còn lại
                    let dta = Math.abs(data['data']);
                    sum_1 = ((parseFloat(interest_rate)/100) * parseFloat(dta)) + parseFloat(period_amount_batch)
                    console.log(dta);
                    $('#actual_amount').val(formats(sum_1));
                }
            });
        }

    });
    $("#fines").keyup(function (e) {
        $(this).val(formats($(this).val()));
        let fines =$(this).val();
        let interest_rate = $('#interest_rate').val();
        let days_overdue = $('#number_days_overdue').val();
        let receipts_brand_id = $('#receipts_brand_id').val();
        let period_amount_batch = $('#period_amount_batch').val().replaceAll('.','');
        if(receipts_brand_id == 3){
            if(days_overdue <0){
                let id = $('#rId').val();
                let receipts_stock_id = $('#receipts_stock_id').val();
                $.ajax({
                    url:'{{url("api/receipts/load-amount-batch")}}/'+id + '/'+ receipts_stock_id,
                    type: "GET",
                    dataType: 'json',
                    success: function(data) {
                        //lấy 1.68 % * số tiền trả theo kì còn lại
                        if(fines==''){
                            let Amount = (parseFloat(interest_rate)/100) * parseFloat(data['data'])
                            let sum_1 = parseFloat(0) * parseFloat(data['data']) * parseFloat(Math.abs(days_overdue))
                            let sum_amount = 0
                            sum_amount = parseFloat(period_amount_batch) + parseFloat(Amount) + parseFloat(0)
                            $('#overdue_fines').val(formats(sum_1));
                            $('#actual_amount').val(formats(sum_amount));
                        }else{
                            let dta = Math.abs(data['data']);
                            let days_overdues = Math.abs(days_overdue);
                            let Amount = (parseFloat(interest_rate)/100) * parseFloat(dta)
                            let sum_1 = parseFloat(fines) * parseFloat(days_overdues) * parseFloat(dta)
                            let sum_amount = 0
                            sum_amount = parseFloat(period_amount_batch) + parseFloat(Amount) + parseFloat(sum_1)
                            $('#overdue_fines').val(formats(sum_1));
                            $('#actual_amount').val(formats(sum_amount));
                            console.log(sum_1);
                        }
                    }
                });
            }else{
                $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                $('#successrequest').addClass('jq-icon-warning fade show')
                $('#content_success').text('warning!')
                $('#content_tb').text('you have no overdue date to calculate!')
                setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
            }

        }
    });
    let localhost = window.location.hostname;
    $(document).on("click", ".btnCreate", function (event) {
        let stock_customer_id = $('#stock_customer_id').val();
        let interest_rate = $('#interest_rate').val();
        let receipts_period_id = $('#receipts_period_id1').val();
        if(stock_customer_id ==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the customer!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else if(receipts_period_id == "Payment 2" || receipts_period_id == "Payment 3" || receipts_period_id == "Payment 4" || receipts_period_id == "Payment 5" || receipts_period_id == "Payment 6" || receipts_period_id == "Payment 7" || receipts_period_id == "Payment 8" || receipts_period_id == "Payment 9" || receipts_period_id == "Payment 10" || receipts_period_id == "Payment 11" || receipts_period_id == "Payment 12" && interest_rate=="") {
            $('.jq-toast-loader').attr('id', 'loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the interest rate !')
            setTimeout(function () {
                $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in')
            }, 3000);
        }else{
                let myForm = document.getElementById('formBankAdd')
                // setTimeout(() => {
                $('#fadeShow').css('display','block');
                $.ajax({
                    url:'{{url("api/receipts/create")}}',
                    type: "POST",
                    dataType: 'json',
                    data:new FormData(myForm),'_token':"{{ csrf_token() }}",
                    cache : false,
                    processData : false,
                    contentType: false,
                    success: function(data) {
                        if(data.errors==true){
                            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-warning fade show')
                            $('#content_success').text('warning!')
                            $('#content_tb').text(data.message)
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                            $('#brand_code').focus();
                        }else{
                            $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-success fade show')
                            $('#content_success').text('success!')
                            $('#content_tb').text('successfully added new data')
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/receipts")}}"; }, 3000);
                        }
                    }
                });
            }
        });
})
</script>
        {{--sucess--}}
        <div class="jq-toast-wrap top-right">
            <style>
                #loader_loaded_success {
                    background-color: #5ba035;
                }
                #loader_loaded_wram {
                    background-color: #da8609;
                }
                #loader_loaded_errors {
                    background-color: #bf441d;
                }
            </style>
            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                <span class="close-jq-toast-single">×</span>
                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
            </div>
        </div>
@endsection

