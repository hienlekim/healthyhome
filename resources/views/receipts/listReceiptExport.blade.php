@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled="disabled">
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" disabled="disabled">
@endsection

@section('content')
<style>
    @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
        /* Styles */
    }
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
    <table style="width: 100%">
        <tr>
            <td>
                <div class="col-xl-12 text-left">
                    <h2 class="header-title">RECEIPTS</h2>
                </div>
            </td>
            <td>
                <div class="col-xl-12 text-right">
                    <form method="post" id="formExport" action="{{url("api/receipts/export-receipts")}}">
                        {{csrf_field()}}
                        <input type="hidden" id="idReceipt" name="idReceipt" value="">
                        <button type="submit" class="btn btn-primary waves-effect waves-light btnReceiptsPrint">
                            <i class="fe-save mr-1"></i> Receipts Print
                        </button>
                    </form>
                </div>
            </td>
        </tr>
    </table>
</div>
<div class="row content_description">
    <div class="col-12">
        <div class="card-box">
            <form class="mb-1" method="get" id="formBankList" action="{{url("api/receipts/list-export")}}">
                <div class="row">
                    <div class="col-xl-3 pr-3">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-2" for="simpleinput">Date Start </label>
                                <input type="date" class="form-control" id="fromDate" name="fromDate"
                                       autocomplete="off" value="{{$date_start}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 text-right">
                        <div class="mb-2 row">
                            <div class="input-group">
                                <label class="col-form-label pr-1" for="simpleinput">Date End </label>
                                <input type="date" class="form-control" id="toDate" name="toDate"
                                       autocomplete="off" value="{{$date_end}}">
                                <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                                        type="submit"><i class="fe-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row">
            <div class="container-fluid">
                <div class="table-responsive">
                    <table class="tablesaw table mb-0">
                        <thead class="title_table">
                        <tr>
                            <th><div class="form-check font-16 mb-0">
                                    <input class="form-check-input" type="checkbox" id="productlistCheck">
                                    <label class="form-check-label" for="productlistCheck">&nbsp;</label>
                                </div></th>
                            <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">Receipts Code</th>
                            <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">Customer</th>
                            <th scope="col" data-tablesaw-sortable-col="">Status</th>
                            <th scope="col" data-tablesaw-sortable-col="">Staff</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $t=0 ?>
                            @if($count >0)
                                @foreach($results as $key)
                                    <?php $t=$t+1 ?>
                                    <tr>
                                        <td> <div class="form-check font-16 mb-0">
                                                <input class="form-check-input" type="checkbox" id="productlistCheck1" value="{{$key->id}}">
                                                <label class="form-check-label" for="productlistCheck1">&nbsp;</label>
                                            </div></td>
                                        <td class="text-uppercase"><a href="{{url("api/receipts/edit/$key->id")}}">{{$key->receipts_code}}</a></td>
                                        <td>{{$key['receiptcustomer']['customers_name']}}</td>
                                        <td>@if($key['receipts_status']==1)<p class="text-warning">Tạo Mới</p>@elseif($key['receipts_status']==2)<p class="text-warning">Chờ duyệt</p>@elseif($key['receipts_status']==3)<p class="text-success">Hoàn Tất</p> @endif</td>
                                        <td class=" tablesaw-priority-3 tablesaw-toggle-cellhidden" style="text-align: right">{{$key['receiptstaff']['staff_name']}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="10" class="text-center">Content is not available or does not exist</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
        </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/libs/tablesaw/tablesaw.js')}}"></script>
<script src="{{asset('assets/js/pages/tablesaw.init.js')}}"></script>
<script src="{{asset('assets/libs/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js')}}"></script>
@endsection


