@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/bootstrap-dark.min.css')}}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled="disabled">
<link href="{{asset('assets/libs/tablesaw/tablesaw.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/css/default/app-dark.min.css')}}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" disabled="disabled">
@endsection

@section('content')
<style>
@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
/* Styles */
}
</style>
{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
<tr>
<td>
<div class="col-xl-12 text-left">
    <h2 class="header-title">WAIT RECEIPTS APPROVE</h2>
</div>
</td>
<td>
{{--                    <div class="col-xl-12 text-right">--}}
{{--                        <h2 class="header-title">--}}
{{--                            <a href="{{url("api/receipts/approve-index")}}" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i>New</a>--}}
{{--                        </h2>--}}
{{--                    </div>--}}
</td>
</tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
<div class="card-box">
<form class="mb-1" method="get" id="formBankList" action="{{url("api/receipts/approve-index")}}">
<div class="row">
    <div class="col-xl-4 pr-3">
        <div class="mb-2 row">
            <div class="input-group">
                <input type="txt" class="form-control" id="searchInput" name="searchInput" autocomplete="off" placeholder="Amount of Money">
            </div>
        </div>
    </div>
    <div class="col-xl-4 pr-3">
        <div class="mb-2 row">
            <div class="input-group">
                <select class="form-control form-select-lg" id="searchBranch" name="searchBranch">
                    <option value="">Branch </option>
                    @foreach($branch as $item)
                        <option value="{{$item->id}}">{{$item->brand_name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-xl-4 pr-3">
        <div class="mb-2 row">
            <div class="input-group">
                <input type="txt" class="form-control" id="searchReceiptCode" name="searchReceiptCode" autocomplete="off" placeholder="Receipts Code">
            </div>
        </div>
    </div>
    <div class="col-xl-4 pr-3">
        <div class="mb-2 row">
            <div class="input-group">
                <select class="form-control form-select-lg" id="searchCustomer" name="searchCustomer">
                    <option value="">Customer</option>
                    @foreach($customer as $item)
                        <option value="{{$item->id}}">{{$item->customers_name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-xl-4 pr-3">
        <div class="mb-2 row">
            <div class="input-group">
                <select class="form-control form-select-lg" id="searchBank" name="searchBank">
                    <option value="">Bank</option>
                    @foreach($bank as $item)
                        <option value="{{$item->id}}">{{$item->bank_name}}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="col-xl-4 pr-3">
        <div class="mb-2 row">
            <div class="input-group">
                <select class="form-control form-select-lg" id="searchStaff" name="searchStaff">
                    <option value="">Staff</option>
                    @foreach($staff as $item)
                        <option value="{{$item->id}}">{{$item->staff_name}}
                        </option>
                    @endforeach
                </select>
                <button class="btn btn-primary waves-effect waves-light" id="searchButton"
                        type="submit"><i class="fe-search"></i></button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="table-responsive">
            <table style="min-width: 1200px;" class="tablesaw table mb-0 tablesaw-columntoggle" data-tablesaw-mode="columntoggle"
                   id="tablesaw-6390">
                <thead class="title_table">
                <tr>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">
                        #</th>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist"></th>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">Receipts Code</th>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">Customer</th>
                    <th scope="col" data-tablesaw-sortable-col="" data-tablesaw-priority="persist">Staffs</th>
                    <th scope="col" data-tablesaw-sortable-col="">Actual Amount</th>
                    <th scope="col" data-tablesaw-sortable-col="">Branch</th>
                    <th scope="col" data-tablesaw-sortable-col="">Bank</th>
                    <th scope="col" data-tablesaw-sortable-col="">Status</th>
                    <th scope="col" data-tablesaw-sortable-col="" width="200">Action</th>

                </tr>
                </thead>
                <tbody>
                <?php $t=0 ?>
                @if($count >0)
                    @foreach($results as $key)
                        <?php $t=$t+1 ?>
                        <tr>
                            <td>{{$t}}</td>
                            <td>
                                @if($key->receipts_images)
                                    <img  src="{{asset("images/receipts/$key->receipts_images")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 30px;border: 1px solid #eaeaea">
                                @else
                                    <img src="{{asset("images/product/$Images")}}" alt="image" id="images_avatas" class="img-fluid" style="width: 30px;border: 1px solid #eaeaea">
                                @endif
                            </td>
                            <td class="text-uppercase"><a href="{{url("api/receipts/edit-approve/$key->id")}}">{{$key->receipts_code}}</a></td>
                            <td>{{$key['receiptcustomer']['customers_name']}}</td>
                            <td>{{$key['receiptstaff']['staff_name']}}</td>
                            <td>{{number_format($key['actual_amount'],2,",",".")}} {{$key['receiptsCurrency']['currency_code']}}</td>
                            <td>{{$key['receiptsbranch']['brand_name']}}</td>
                            <td>@if($key['receipts_bank_id']){{$key['receiptsbank']['bank_name']}} @else @endif</td>
                            <td>@if($key['receipts_status']==1)<p class="text-warning">Tạo Mới</p>@elseif($key['receipts_status']==2)<p class="text-warning">Chờ duyệt</p>@elseif($key['receipts_status']==3)<p class="text-success">Hoàn Tất</p> @endif</td>
{{--                                                <td class=" tablesaw-priority-1 tablesaw-toggle-cellhidden" style="text-align: right">{{$key['receiptsbank']['bank_name']}}</td>--}}
                            <td><input type="hidden" id="idReceipt" name="idReceipt" value="{{$key['id']}}" >
                                @if($brandUser['decenTLizaTion']['positions']['position_code'] == 'sales' && $key->receipts_status==1)
                                    <button type="button" class="btn btn-primary waves-effect waves-light btnSendApprove">
                                        <i class="fe-save mr-1"></i> Send Approve
                                    </button>
{{--                                                        <button type="button" class="btn btn-danger waves-effect waves-light btnCancell" >--}}
{{--                                                            <i class="fe-trash-2 mr-1"></i>Cancel--}}
{{--                                                        </button>--}}
                                @elseif($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators' && $key->receipts_status==2)
                                    <button type="button" class="btn btn-primary waves-effect waves-light btnApprove" >
                                        Approve
                                    </button>
                                    <button type="button" class="btn btn-danger waves-effect waves-light btnCancellApprove">
                                        Cancel
                                    </button>
                                @elseif($brandUser['decenTLizaTion']['positions']['position_code'] == 'administrators' && $key->receipts_status==1)
                                    <button type="button" class="btn btn-primary waves-effect waves-light" disabled>
                                         Approve
                                    </button>
                                    <button type="button" class="btn btn-danger waves-effect waves-light" disabled>
                                        Cancel
                                    </button>
                                @endif</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="10" class="text-center">Content is not available or does not exist</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 text-left">
        <b>Total:</b> <span class="TotalAll">{{$count}}</span>
    </div>
    <div class="col-md-4">
        <div class="dataTables_paginate paging_simple_numbers">{{$results->links()}}</div>
    </div>
</div>
</form>
</div>
</div>
<div class="modal-backdrop fade show"  id="fadeShow" style="display: none">
<div class="row">
<div class="col-md-12 text-center" style="margin-top: 10%">
<div class="spinner-border avatar-lg text-primary m-2 font-24"></div></br>
<div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
<div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
<div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
</div>
</div>
</div>
@endsection
@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script src="{{asset('assets/libs/moment/moment.min.js')}}"></script>
<script>
    // function preview_image_1(event)
    // {
    //     var reader = new FileReader();
    //     reader.onload = function()
    //     {
    //         var output = document.getElementById('images_avatas');
    //         output.src = reader.result;
    //     }
    //     let images = reader.readAsDataURL(event.target.files[0]);
    // }
    $(document).ready(function () {
        var thousand_sep = '.',
            dec_sep = ',',
            dec_length = 3;
        var format = function (num) {
            var str = num.toString().replace("", ""),
                parts = false,
                output = [],
                i = 1,
                formatted = null;
            if (str.indexOf(dec_sep) > 0) {
                parts = str.split(dec_sep);
                str = parts[0];
            }
            str = str.split("").reverse();
            for (var j = 0, len = str.length; j < len; j++) {
                if (str[j] != thousand_sep) {
                    output.push(str[j]);
                    if (i % 3 == 0 && j < (len - 1)) {
                        output.push(thousand_sep);
                    }
                    i++;
                }
            }
            if (output.slice(-1)[0] === '-' && output.slice(-2)[0] === thousand_sep ) {
                output.splice(-2, 1);
            }

            formatted = output.reverse().join("");
            return (formatted + ((parts) ? dec_sep + parts[1].substr(0, dec_length) : ""));
        };
        $("#searchInput").keyup(function (e) {
            $(this).val(format($(this).val()));
        });
        $(document).on("click", ".btnCancellApprove", function (event) {
            let id = $('#idReceipt').val();
            // let myForm = document.getElementById('formBankAdd')
            // setTimeout(() => {
            $('#fadeShow').css('display','block');
            $.ajax({
                url:'{{url("api/receipts/cancel-approve")}}/'+id,
                type: "PUT",
                dataType: 'json',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'id': id,
                },
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#brand_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('Cancel data successfully')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/receipts/approve-index")}}"; }, 3000);
                    }
                }
            });
        });
        //send approve
        $(document).on("click", ".btnApprove", function (event) {
            let id = $('#idReceipt').val();
            // let myForm = document.getElementById('formBankAdd')
            // setTimeout(() => {
            $('#fadeShow').css('display','block');
            $.ajax({
                url:'{{url("api/receipts/approve")}}/'+id,
                type: "PUT",
                dataType: 'json',
                data: {
                    '_token': "{{ csrf_token() }}",
                    'id': id,
                },
                success: function(data) {
                    if(data.errors==true){
                        $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-warning fade show')
                        $('#content_success').text('warning!')
                        $('#content_tb').text(data.message)
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                        $('#brand_code').focus();
                    }else{
                        $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                        $('#successrequest').addClass('jq-icon-success fade show')
                        $('#content_success').text('success!')
                        $('#content_tb').text('Cancel data successfully')
                        setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/receipts")}}"; }, 3000);
                    }
                }
            });
            {{--// setTimeout(() => {--}}
            {{--let images_avatas = $('#images_avatas').val();--}}
            {{--if(images_avatas =="" || images_avatas ==0){--}}
            {{--    $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');--}}
            {{--    $('#successrequest').addClass('jq-icon-warning fade show')--}}
            {{--    $('#content_success').text('warning!')--}}
            {{--    $('#content_tb').text('please enter the images receipts !')--}}
            {{--    setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);--}}
            {{--}else{--}}
            {{--    let id =  $('#idReceipt').val();--}}
            {{--    let myForm = document.getElementById('formBankAdd')--}}
            {{--    $('#fadeShow').css('display','block');--}}
            {{--    $.ajax({--}}
            {{--        url:'{{url("api/receipts/approve")}}/'+id,--}}
            {{--        type: "POST",--}}
            {{--        dataType: 'json',--}}
            {{--        data:new FormData(myForm),'_token':"{{ csrf_token() }}",--}}
            {{--        cache : false,--}}
            {{--        processData : false,--}}
            {{--        contentType: false,--}}
            {{--        success: function(data) {--}}
            {{--            if(data.errors==true){--}}
            {{--                $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');--}}
            {{--                $('#successrequest').addClass('jq-icon-warning fade show')--}}
            {{--                $('#content_success').text('warning!')--}}
            {{--                $('#content_tb').text(data.message)--}}
            {{--                setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);--}}
            {{--                $('#brand_code').focus();--}}
            {{--            }else{--}}
            {{--                $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');--}}
            {{--                $('#successrequest').addClass('jq-icon-success fade show')--}}
            {{--                $('#content_success').text('success!')--}}
            {{--                $('#content_tb').text('Send approve data successfully')--}}
            {{--                setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/receipts/approve-index")}}"; }, 3000);--}}
            {{--            }--}}
            {{--        }--}}
            {{--    });--}}
            {{--}--}}

        });
        $('#images_avatas').click(function (){
            $('#images_receipt').modal('show');
        })
    })
</script>
{{--        sucess--}}
{{--        <div class="jq-toast-wrap top-right">--}}
{{--            <style>--}}
{{--                #loader_loaded_success {--}}
{{--                    background-color: #5ba035;--}}
{{--                }--}}
{{--                #loader_loaded_wram {--}}
{{--                    background-color: #da8609;--}}
{{--                }--}}
{{--                #loader_loaded_errors {--}}
{{--                    background-color: #bf441d;--}}
{{--                }--}}
{{--            </style>--}}
{{--            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">--}}
{{--                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>--}}
{{--                <span class="close-jq-toast-single">×</span>--}}
{{--                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>--}}
{{--            </div>--}}
{{--        </div>--}}
@endsection


