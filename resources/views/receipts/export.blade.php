<table>
    <tr>
        <td  colspan="5">
          <img src="public/assets/image/logo.png" alt="image" id="images_avatas" class="img-fluid" style="width: 200px">
        </td>
        <td colspan="5">
            Số : ................................................
        </td>
    </tr>
    <tr>
        <td colspan="10">
             <span>
                16/72/4 Nguyễn Thiện Thuật, P.2, Q.3, Tp. HCM / 0795 345 799 / MST: 0315 719 824
            </span>
        </td>
    </tr>
    <tr>
        <td colspan="10">
            <span>http://healthyhomes.com.vn</span>
        </td>
    </tr>
    <tr>
        <td colspan="10" style="text-align: center;font-weight: bold;font-size: 15px">
            <span>BIÊN NHẬN THỰC HIỆN THU TIỀN TRẢ GÓP</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left;font-size: 13px">
            Khách Hàng :
        </td>
        <td colspan="4" style="text-align: left;font-size: 13px">
            {{$customer}}
        </td>
        <td style="text-align: left;font-size: 13px">
            <span> Tổng Tiền :</span>
        </td>
        <td colspan="2" style="text-align: left;font-size: 13px">
            <span> {{$receipts_period_amount}}</span>
        </td>
        <td style="text-align: left;font-size: 13px">
            <span>{{$receipts_currency}}</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left;font-size: 13px">
            Số Điện Thoại:
        </td>
        <td colspan="4" style="text-align: left;font-size: 13px">
            {{$phone}}
        </td>
        <td style="text-align: left;font-size: 13px">
            <span>Trả Trước:</span>
        </td>
        <td colspan="2" style="text-align: left;font-size: 13px">
            <span>{{$receipts_amount}}</span>
        </td>
        <td style="text-align: left;font-size: 13px">
            <span>{{$receipts_currency}}</span>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left;font-size: 13px">
            Địa Chỉ :
        </td>
        <td colspan="4" style="text-align: left;font-size: 13px">
            {{$customers_address}}
        </td>
        <td style="text-align: left;font-size: 13px">
            <span> Kì Hạn:</span>
        </td>
        <td colspan="2" style="text-align: left;font-size: 13px">
            <span> {{$period_payments}}</span>
        </td>
        <td>Tháng</td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left;font-size: 13px">
            Ngày Hợp Đồng:
        </td>
        <td colspan="4" style="text-align: left;font-size: 13px">
            {{$receipts_period_date}}
        </td>
        <td style="text-align: left;font-size: 13px">
            <span> Lãi Suất:</span>
        </td>
        <td style="text-align: left;font-size: 13px">
            <span>{{$interest_rate}} %</span>
        </td>
        <td style="text-align: left;font-size: 13px">
            <span>Tháng</span>
        </td>
        <td></td>
    </tr>
    <tr>
        <th style="border: 1px solid #6f7071;width: 5px;" >STT</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >Lần Thu</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >Ngày Đến Hạn</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >Ngày Thanh Toán</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >Số Tiền Thanh Toán</th>
        <th style="border: 1px solid #6f7071;width: 25px;">Số tiền còn lại</th>
        <th style="border: 1px solid #6f7071;width: 15px;">Số Ngày Quá hạn</th>
        <th style="border: 1px solid #6f7071;width: 15px;">Tiền Phạt Quá hạn</th>
        <th style="border: 1px solid #6f7071;width: 15px;">Tổng Tiền</th>
        <th style="border: 1px solid #6f7071;width: 15px;">Ghi Chú</th>
    </tr>
    <tr>
        <th style="border: 1px solid #6f7071;width: 5px;" >1</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >{{$period_name}}</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >{{$period_date}}</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >{{$receipts_amount_date}}</th>
        <th style="border: 1px solid #6f7071;width: 20px;" >{{$receipts_amount}}</th>
        <th style="border: 1px solid #6f7071;width: 25px;">{{$price_lasts}}</th>
        <th style="border: 1px solid #6f7071;width: 20px;">{{-$number_days_overdue}}</th>
        <th style="border: 1px solid #6f7071;width: 20px;">{{$overdue_fines}}</th>
        <th style="border: 1px solid #6f7071;width: 20px;">{{$actual_amount}}</th>
        <th style="border: 1px solid #6f7071;width: 10px;"></th>
    </tr>
    <tr>
        <td colspan="3" style="text-align: left;font-size: 13px;border: 1px solid #6f7071;">
            Tổng (Đơn vị tính: {{$receipts_currency}})
        </td>
        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"> </td>
        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"></td>
        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"></td>
        <td style="text-align: left;font-size: 13px;border: 1px solid #6f7071;"></td>
        <td colspan="3" style="text-align: left;font-size: 13px;border: 1px solid #6f7071;">
            <span>Nhân Viên : {{$staff}}</span>
        </td>
    </tr>
    @if($brand==3)
        <tr>
            <td colspan="10" style="text-align: left;font-size: 13px">
                (1) Tới Ngày Đến Hạn của mỗi đợt thanh toán, nếu Bên B không thực hiện việc thanh toán, Bên B phải chi trả cho Bên A các khoản sau đây:
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                (a) Tiền Gốc Chưa Trả;
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                (b) Tiền Lãi Chưa Trả = 0.056%/Ngày x Dư Nợ Gốc Chưa Trả x M
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                (c) Lãi Quá Hạn = 0.0084%/Ngày x Tiền Gốc Chưa Trả x M
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                (d) Lãi Chậm Trả = 0.028%/Ngày x Tiền Lãi Chưa Trả x M
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                Trong đó: M là số ngày mà Bên B chưa thực hiện việc thanh toán đúng hạn, được tính từ Ngày Đến Hạn cho đến ngày Bên B thực tế trả cho Bên A.
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                (2) Trường hợp Bên B không thanh toán từ 3 tháng trở lên, Công Ty TNHH Healthy Homes Viet Nam có quyền thu hồi lại
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                Hệ thống làm sạch tổng hợp Rainbow để trừ vào số tiền chưa thanh toán.
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                (3) Khi Bên B xác nhận việc thanh toán cho Bên A (bằng tiền mặt), vui lòng liên hệ qua số điện thoại sau: 0344 11 7323 (Mr. Hoang), 0925 632 817 (Ms. Thuy).
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                (4) Thanh toán bằng séc / chuyển khoản qua ngân hàng:
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                a. Séc / chuyển khoản: VCB  - Công ty TNHH Healthy Homes Viet Nam - 1014 552 635
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left;font-size: 12px">
                b. ACB - Hồ Anh Tuấn - 1111 6 9988
            </td>
        </tr>
    @elseif($brand==4)
        <tr>
            <td colspan="20" style="text-align: left;font-size: 13px">
                1.	ការទូទាត់ប្រាក់យឺតនឹងត្រូវធ្វើការពិន័យការប្រាក់បន្ថែម២%ក្នុងមួយខែទៅលើទឹកប្រាក់ដែលមិនបានទូរទាត់នោះ(២៤%ក្នុងមួយឆ្នាំ)ឬអត្រាការប្រាក់ដែលខ្ពស់បំផុតដែលកំណត់ដោយច្បាប់ដោយបូកបញ្ចូលទាំងថ្លៃដើមនឹងថ្លៃមេធាវី។
            </td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left;font-size: 13px">
                2.	ក្នុងករណីមិនបានទូរទាត់រយះពេល៣ខែឬលើសពីនេះ,នោះក្រុមហ៊ុនមានសិទ្ធិក្នុងការរឹបអូសម៉ាស៊ីន Rainbowដោយចាត់ទុកជាការកាត់កងជាមួយប្រាក់ដោយមិនបានបង់នោះ។
            </td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left;font-size: 13px">
                3.	ទាក់ទងនឹងការបញ្ជាក់ជាមួយនឹងការទូរទាត់(សាច់ប្រាក់)របស់លោកអ្នកសូមធ្វើការបញ្ជាក់ជាមួយ 099 777 947 (Mr. Dara), 012 737698 (Mr. Chetha)
            </td>
        </tr>
        <tr>
            <td colspan="20" style="text-align: left;font-size: 13px">
                4.	ការទូទាត់តាមរយះ សែក នឹង ផ្ទេរតាមកុងធនាគារ:ABA Cambodia – Ho Anh Tuan – 000623489.
            </td>
        </tr>
    @else
        <tr>
            <td colspan="20" style="text-align: left;font-size: 13px">
            </td>
        </tr>
    @endif

    <tr>
        <td colspan="5" style="text-align: center;font-size: 12px;font-weight: bold">
            Khách Hàng - Bên B
        </td>
        <td colspan="5" style="text-align: center;font-size: 12px;font-weight: bold">
            Đại Diện Công Ty - Bên A
        </td>
    </tr>
    <tr>
        <td colspan="5" style="text-align: center;font-size: 12px;">
            (Ký và ghi rõ họ tên)
        </td>
        <td colspan="5" style="text-align: center;font-size: 12px;">
            (Ký và ghi rõ họ tên)
        </td>
    </tr>
    <tr>
        <td colspan="5" style="text-align: center;font-size: 12px;"></td>
        <td colspan="5" style="text-align: center;font-size: 12px;"></td>
    </tr>
    <tr>
        <td colspan="5" style="text-align: center;font-size: 12px;"></td>
        <td colspan="5" style="text-align: center;font-size: 12px;"></td>
    </tr>
    <tr>
        <td colspan="5" style="text-align: center;font-size: 12px;"></td>
        <td colspan="5" style="text-align: center;font-size: 12px;"></td>
    </tr>
    <tr>
        <td colspan="10" style="text-align: left;font-size: 12px; font-weight: bold">
            Ngày Lập :
        </td>
    </tr>

</table>
