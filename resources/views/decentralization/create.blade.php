@extends('template')
@section('title', "Bank")

@section('css')
<link href="{{asset('assets/libs/jquery-toast/jquery.toast.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')

{{--<a href="{{url("settings")}}" style="line-height: 52px;padding-left:8px;"><i class="fe-chevron-left" style="font-weight: 800;"></i>Cấu hình chung</a>--}}
<div class="row" style="margin: 3px 0px 25px 0px;border-bottom: 1px solid #dfe4e8;">
<table style="width: 100%">
    <tr>
        <td>
            <div class="col-xl-12 text-left">
                <h2 class="header-title">Decentralization</h2>
            </div>
        </td>
        <td>
            <div class="col-xl-12 text-right">
                <h2 class="header-title">
                    <a href="{{url("api/decentralization")}}" class="btn btn-primary waves-effect waves-light"><i
                            class="fe-rewind pr-1"></i>Back</a>
                </h2>
            </div>
        </td>
    </tr>
</table>
</div>
<div class="row content_description">
<div class="col-12">
    <div class="card-box">
        <form class="mb-1" method="post" id="formAdd">
            {{csrf_field()}}
            <div class="row">
                <div class="mb-2 col-md-6">
                    <div class="mb-2 row">
                        <!-- Default switch -->

                        <label class="col-md-5 col-form-label" for="simpleinput">Position Name <span class="text-danger">*</span></label>
                        <div class="col-md-7">
{{--                            <input type="txt" class="form-control text-uppercase" id="position_code" name="position_code" autocomplete="off">--}}
                            <select class="form-control form-select-lg" id="decen_position_id" name="decen_position_id">
                                <option value="">------</option>
                                @foreach($dataPosition as $item)
                                    <option value="{{$item->id}}">{{$item->position_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="mb-2 col-md-12">
                    <div class="mb-2 row">
                        <table class="table mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Authorization feature</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
{{--                            <tr class="table-active">--}}
{{--                                <th scope="row">1</th>--}}
{{--                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>General Directory</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_general_directory" name="decen_general_directory" value="1">--}}
{{--                                        <label class="custom-control-label" for="decen_general_directory"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <th scope="row">2</th>--}}
{{--                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>History System</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_history_system" name="decen_history_system" value="1">--}}
{{--                                        <label class="custom-control-label" for="decen_history_system"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr class="table-success">
                                <th scope="row">1</th>
                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>Finance</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="finance">
                                        <label class="custom-control-label" for="finance"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr>--}}
{{--                                <th scope="row">4</th>--}}
{{--                                <td> &nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Formality Payment</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_formality_payment" value="1" name="decen_formality_payment">--}}
{{--                                        <label class="custom-control-label" for="decen_formality_payment"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr >
                                <th scope="row">2</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Receipts</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_receipts" value="1" name="decen_receipts">
                                        <label class="custom-control-label" for="decen_receipts"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr>--}}
{{--                                <th scope="row">6</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Payment</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_payment" value="1" name="decen_payment">--}}
{{--                                        <label class="custom-control-label" for="decen_payment"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr class="table-warning">--}}
{{--                                <th scope="row">7</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Approve Receipts</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_approve_receipts" value="1" name="decen_approve_receipts">--}}
{{--                                        <label class="custom-control-label" for="decen_approve_receipts"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <th scope="row">8</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Approve Payment</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_approve_payment" value="1" name="decen_approve_payment">--}}
{{--                                        <label class="custom-control-label" for="decen_approve_payment"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr class="table-success">
                                <th scope="row">3</th>
                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>Personnel</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="personnel">
                                        <label class="custom-control-label" for="personnel"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">4</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Staff</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_Staff" value="1" name="decen_Staff">
                                        <label class="custom-control-label" for="decen_Staff"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr class="table-active">--}}
{{--                                <th scope="row">11</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>KPI Sales</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_kpi_sales" value="1" name="decen_kpi_sales">--}}
{{--                                        <label class="custom-control-label" for="decen_kpi_sales"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr >--}}
{{--                                <th scope="row">13</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Position</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_position" value="1" name="decen_position">--}}
{{--                                        <label class="custom-control-label" for="decen_position"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr class="table-success">--}}
{{--                                <th scope="row">14</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Rank</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_rank" value="1" name="decen_rank">--}}
{{--                                        <label class="custom-control-label" for="decen_rank"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <th scope="row">15</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Commission Rank</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_commission_rank" value="1" name="decen_commission_rank">--}}
{{--                                        <label class="custom-control-label" for="decen_commission_rank"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr  class="table-success">--}}
{{--                                <th scope="row">16</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Commission</td>--}}
{{--                                <td> <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_commission" value="1" name="decen_commission">--}}
{{--                                        <label class="custom-control-label" for="decen_commission"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr >--}}
{{--                                <th scope="row">17</th>--}}
{{--                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>Supplier</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="supplier" >--}}
{{--                                        <label class="custom-control-label" for="supplier"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr class="table-warning">--}}
{{--                                <th scope="row">18</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Supplier</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_supplier" value="1" name="decen_supplier">--}}
{{--                                        <label class="custom-control-label" for="decen_supplier"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <th scope="row">19</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Supplier Order List</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_supplier_order_list" value="1" name="decen_supplier_order_list">--}}
{{--                                        <label class="custom-control-label" for="decen_supplier_order_list"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr class="table-active">--}}
{{--                                <th scope="row">20</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Supplier Debt List</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_supplier_debt_list" value="1" name="decen_supplier_debt_list">--}}
{{--                                        <label class="custom-control-label" for="decen_supplier_debt_list"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr class="table-success">
                                <th scope="row">5</th>
                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>Customers</td>
                                <td><div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="customers">
                                        <label class="custom-control-label" for="customers"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">6</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Customers</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_customers" value="1" name="decen_customers">
                                        <label class="custom-control-label" for="decen_customers"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr>--}}
{{--                                <th scope="row">23</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Customer Debt List</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_customer_debt_list" value="1" name="decen_customer_debt_list">--}}
{{--                                        <label class="custom-control-label" for="decen_customer_debt_list"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr class="table-success">
                                <th scope="row">7</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Customers Care</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_customers_care" value="1" name="decen_customers_care">
                                        <label class="custom-control-label" for="decen_customers_care"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">8</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Received Customers</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_customers_receive_gifts" value="1" name="decen_customers_receive_gifts">
                                        <label class="custom-control-label" for="decen_customers_receive_gifts"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr class="table-success">
                                <th scope="row">9</th>
                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>Warehouses System</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="Warehouses_System" value="1" name="Warehouses_System">
                                        <label class="custom-control-label" for="Warehouses_System"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">10</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Product</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_product" value="1" name="decen_product">
                                        <label class="custom-control-label" for="decen_product"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr class="table-success">
                                <th scope="row">11</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Product Type</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_product_cate" value="1" name="decen_product_cate">
                                        <label class="custom-control-label" for="decen_product_cate"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">12</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Stock</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_purchases" value="1" name="decen_purchases">
                                        <label class="custom-control-label" for="decen_purchases"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr class="table-active">--}}
{{--                                <th scope="row">30</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Successful Order</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_successful_order" value="1" name="decen_successful_order">--}}
{{--                                        <label class="custom-control-label" for="decen_successful_order"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr class="table-success">
                                <th scope="row">13</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>List Inventory</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_inventory" value="1" name="decen_inventory">
                                        <label class="custom-control-label" for="decen_inventory"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr class="table-success">--}}
{{--                                <th scope="row">32</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Synthesis Report</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_synthesis_report" value="1" name="decen_synthesis_report">--}}
{{--                                        <label class="custom-control-label" for="decen_synthesis_report"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr>
                                <th scope="row">14</th>
                                <td><i class="fe-check-square pr-2 font-weight-bold"></i>Business System </td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="Business_System">
                                        <label class="custom-control-label" for="Business_System"></label>
                                    </div>
                                </td>
                            </tr>
                            <tr class="table-success">
                                <th scope="row">15</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Order</td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_Order" value="1" name="decen_Order">
                                        <label class="custom-control-label" for="decen_Order"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr>--}}
{{--                                <th scope="row">35</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Installment Orders</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_installment_orders" value="1" name="decen_installment_orders">--}}
{{--                                        <label class="custom-control-label" for="decen_installment_orders"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr class="table-warning">--}}
{{--                                <th scope="row">36</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Full Payment Order</td>--}}
{{--                                <td>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_full_payment_order" value="1" name="decen_full_payment_order">--}}
{{--                                        <label class="custom-control-label" for="decen_full_payment_order"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            <tr>
                                <th scope="row">16</th>
                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Gift Order</td>
                                <td><div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="decen_gift_order" value="1" name="decen_gift_order">
                                        <label class="custom-control-label" for="decen_gift_order"></label>
                                    </div>
                                </td>
                            </tr>
{{--                            <tr  class="table-warning">--}}
{{--                                <th scope="row">38</th>--}}
{{--                                <td>&nbsp; &nbsp;&nbsp;<i class="fe-chevron-right pr-2 font-weight-bold"></i>Payment</td>--}}
{{--                                <td><div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="decen_payment_methods" value="1" name="decen_payment_methods">--}}
{{--                                        <label class="custom-control-label" for="decen_payment_methods"></label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xl-12 text-right" style="padding-bottom: 4px;">
                    <button type="button" class="btn btn-primary waves-effect waves-light btnCreate">
                        <i class="fe-save mr-1"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-backdrop fade show" id="fadeShow" style="display: none">
    <div class="row">
        <div class="col-md-12 text-center" style="margin-top: 10%">
            <div class="spinner-border avatar-lg text-primary m-2 font-24"></div>
            </br>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
            <div class="spinner-grow avatar-sm text-primary m-2" role="status"></div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="{{asset('assets/libs/jquery-toast/jquery.toast.min.js')}}"></script>
<script src="{{asset('assets/js/pages/toastr.init.js')}}"></script>
<script>
$(document).ready(function () {
    let localhost = window.location.hostname;
    $('#finance').on('click',function(){
        if (this.checked) {
            $('#decen_formality_payment,#decen_receipts,#decen_payment,#decen_approve_receipts,#decen_approve_payment,#decen_formality_payment').attr("checked", true)
        } else {
            $('#decen_formality_payment,#decen_receipts,#decen_payment,#decen_approve_receipts,#decen_approve_payment,#decen_formality_payment').attr("checked", false)
        }
    })
    $('#personnel').on('click',function(){
        if (this.checked) {
            $('#decen_Staff,#decen_kpi_sales,#decen_position,#decen_rank,#decen_commission_rank,#decen_commission').attr("checked", true)
        } else {
            $('#decen_Staff,#decen_kpi_sales,#decen_position,#decen_rank,#decen_commission_rank,#decen_commission').attr("checked", false)
        }
    })
    $('#supplier').on('click',function(){
        if (this.checked) {
            $('#decen_supplier,#decen_supplier_order_list,#decen_supplier_debt_list').attr("checked", true)
        } else {
            $('#decen_supplier,#decen_supplier_order_list,#decen_supplier_debt_list').attr("checked", false)
        }
    })
    $('#customers').on('click',function(){
        if (this.checked) {
            $('#decen_customers,#decen_customer_debt_list,#decen_customers_care,#decen_customers_receive_gifts').attr("checked", true)
        } else {
            $('#decen_customers,#decen_customer_debt_list,#decen_customers_care,#decen_customers_receive_gifts').attr("checked", false)
        }
    })
    $('#Warehouses_System').on('click',function(){
        if (this.checked) {
            $('#decen_product,#decen_product_cate,#decen_purchases,#decen_successful_order,#decen_inventory,#decen_synthesis_report').attr("checked", true)
        } else {
            $('#decen_product,#decen_product_cate,#decen_purchases,#decen_successful_order,#decen_inventory,#decen_synthesis_report').attr("checked", false)
        }
    })
    $('#Business_System').on('click',function(){
        if (this.checked) {
            $('#decen_Order,#decen_installment_orders,#decen_full_payment_order,#decen_gift_order,#decen_payment_methods').attr("checked", true)
        } else {
            $('#decen_Order,#decen_installment_orders,#decen_full_payment_order,#decen_gift_order,#decen_payment_methods').attr("checked", false)
        }
    })
    $(document).on("click", ".btnCreate", function (event) {
        let decen_general_directory = $('#decen_general_directory').is(':checked')?$('#decen_general_directory').val():0
        let decen_history_system = $('#decen_history_system').is(':checked')?$('#decen_history_system').val():0
        let decen_formality_payment = $('#decen_formality_payment').is(':checked')?$('#decen_formality_payment').val():0
        let decen_receipts = $('#decen_receipts').is(':checked')?$('#decen_receipts').val():0
        let decen_payment = $('#decen_payment').is(':checked')?$('#decen_payment').val():0
        let decen_approve_receipts = $('#decen_approve_receipts').is(':checked')?$('#decen_approve_receipts').val():0
        let decen_approve_payment = $('#decen_approve_payment').is(':checked')?$('#decen_approve_payment').val():0
        let decen_Staff = $('#decen_Staff').is(':checked')?$('#decen_Staff').val():0
        let decen_kpi_sales = $('#decen_kpi_sales').is(':checked')?$('#decen_kpi_sales').val():0
        let decen_position = $('#decen_position').is(':checked')?$('#decen_position').val():0
        let decen_rank = $('#decen_rank').is(':checked')?$('#decen_rank').val():0
        let decen_commission_rank = $('#decen_commission_rank').is(':checked')?$('#decen_commission_rank').val():0
        let decen_commission = $('#decen_commission').is(':checked')?$('#decen_commission').val():0
        let decen_supplier = $('#decen_supplier').is(':checked')?$('#decen_supplier').val():0
        let decen_supplier_order_list = $('#decen_supplier_order_list').is(':checked')?$('#decen_supplier_order_list').val():0
        let decen_supplier_debt_list = $('#decen_supplier_debt_list').is(':checked')?$('#decen_supplier_debt_list').val():0
        let decen_customers = $('#decen_customers').is(':checked')?$('#decen_customers').val():0
        let decen_customer_debt_list = $('#decen_customer_debt_list').is(':checked')?$('#decen_customer_debt_list').val():0
        let decen_customers_care = $('#decen_customers_care').is(':checked')?$('#decen_customers_care').val():0
        let decen_customers_receive_gifts = $('#decen_customers_receive_gifts').is(':checked')?$('#decen_customers_receive_gifts').val():0
        let decen_product = $('#decen_product').is(':checked')?$('#decen_product').val():0
        let decen_product_cate = $('#decen_product_cate').is(':checked')?$('#decen_product_cate').val():0
        let decen_purchases = $('#decen_purchases').is(':checked')?$('#decen_purchases').val():0
        let decen_successful_order = $('#decen_successful_order').is(':checked')?$('#decen_successful_order').val():0
        let decen_inventory = $('#decen_inventory').is(':checked')?$('#decen_inventory').val():0
        let decen_synthesis_report = $('#decen_synthesis_report').is(':checked')?$('#decen_synthesis_report').val():0
        let decen_Order = $('#decen_Order').is(':checked')?$('#decen_Order').val():0
        let decen_installment_orders = $('#decen_installment_orders').is(':checked')?$('#decen_installment_orders').val():0
        let decen_full_payment_order = $('#decen_full_payment_order').is(':checked')?$('#decen_full_payment_order').val():0
        let decen_gift_order = $('#decen_gift_order').is(':checked')?$('#decen_gift_order').val():0
        let decen_payment_methods = $('#decen_payment_methods').is(':checked')?$('#decen_payment_methods').val():0
        let decen_position_id = $('#decen_position_id').val();
        if(decen_position_id ==""){
            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
            $('#successrequest').addClass('jq-icon-warning fade show')
            $('#content_success').text('warning!')
            $('#content_tb').text('please enter the name position!')
            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in') }, 3000);
        }else{
            // setTimeout(() => {
                $('#fadeShow').css('display','block');
                $.ajax({
                    url:'{{url("api/decentralization/create")}}',
                    type: "POST",
                    dataType: 'json',
                    data: {
                        '_token':"{{ csrf_token() }}",
                        'decen_position_id':decen_position_id,
                        'decen_general_directory':decen_general_directory,
                        'decen_history_system':decen_history_system,
                        'decen_formality_payment':decen_formality_payment,
                        'decen_receipts':decen_receipts,
                        'decen_payment':decen_payment,
                        'decen_approve_receipts':decen_approve_receipts,
                        'decen_approve_payment':decen_approve_payment,
                        'decen_Staff':decen_Staff,
                        'decen_kpi_sales':decen_kpi_sales,
                        'decen_position':decen_position,
                        'decen_rank':decen_rank,
                        'decen_commission_rank':decen_commission_rank,
                        'decen_commission':decen_commission,
                        'decen_supplier':decen_supplier,
                        'decen_supplier_order_list':decen_supplier_order_list,
                        'decen_supplier_debt_list':decen_supplier_debt_list,
                        'decen_customers':decen_customers,
                        'decen_customer_debt_list':decen_customer_debt_list,
                        'decen_customers_care':decen_customers_care,
                        'decen_customers_receive_gifts':decen_customers_receive_gifts,
                        'decen_product':decen_product,
                        'decen_product_cate':decen_product_cate,
                        'decen_purchases':decen_purchases,
                        'decen_successful_order':decen_successful_order,
                        'decen_inventory':decen_inventory,
                        'decen_synthesis_report':decen_synthesis_report,
                        'decen_Order':decen_Order,
                        'decen_installment_orders':decen_installment_orders,
                        'decen_full_payment_order':decen_full_payment_order,
                        'decen_gift_order':decen_gift_order,
                        'decen_payment_methods':decen_payment_methods,
                    },
                    success: function(data) {
                        if(data.errors==true){
                            $('.jq-toast-loader').attr('id','loader_loaded_wram').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-warning fade show')
                            $('#content_success').text('warning!')
                            $('#content_tb').text(data.message)
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');$('#fadeShow').css('display','none'); }, 3000);
                            $('#rank_code').focus();
                        }else{
                            $('.jq-toast-loader').attr('id','loader_loaded_success').addClass('jq-toast-loaded');
                            $('#successrequest').addClass('jq-icon-success fade show')
                            $('#content_success').text('success!')
                            $('#content_tb').text('successfully added new data')
                            setTimeout(function(){ $('#successrequest').removeClass('jq-icon-warning fade show').addClass('fade in');location.href = "{{url("api/decentralization")}}"; }, 3000);
                        }
                    }
                });
            // }, 1000);
        }
    });
})
</script>
        {{--sucess--}}
        <div class="jq-toast-wrap top-right">
            <style>
                #loader_loaded_success {
                    background-color: #5ba035;
                }
                #loader_loaded_wram {
                    background-color: #da8609;
                }
                #loader_loaded_errors {
                    background-color: #bf441d;
                }
            </style>
            <div class="jq-toast-single jq-has-icon fade hide" id="successrequest" style="text-align: left;">
                <span class="jq-toast-loader" style="-webkit-transition: width 2.6s ease-in;-o-transition: width 2.6s ease-in;transition: width 2.6s ease-in;"></span>
                <span class="close-jq-toast-single">×</span>
                <h2 class="jq-toast-heading" id="content_success"></h2><span id="content_tb"></span>
            </div>
        </div>
@endsection

